% -*-matlab-*-
% runMLI.m  
%
function runMLI(varargin)

global LOG SLV OPT EST SCEN ST MLI;

LOG = []; SLV = []; OPT = []; EST = []; SCEN = []; ST = []; MLI = [];

fprintf('\n*** Multi-Level Iteration Scheme ***\n');

% check input
if (nargin < 1) || isempty(varargin)
    error('No problem name specified.')
end

pname = varargin{1};
init_problem = 1;
args = varargin;

for k = numel(args):-1:2
    if ( strcmpi(args{k}, 'init_from_mat') )
        init_problem = 0;
        matfilename = args{k+1};
%     elseif ( strcmpi(args{k}, 'some option') )
%         do stuff;   
    end
end

MLI.dir=[pwd,'/'];

%add paths for mli
addpath([MLI.dir,'algorithm']);
addpath([MLI.dir,'utils']);
addpath([MLI.dir,'matlab']);


SCEN.problempath = [MLI.dir,'examples/problems/', pname];
if ~exist(SCEN.problempath, 'dir')
    error(['Problem directory ' SCEN.problempath ' does not exist.'])
else
    addpath(SCEN.problempath);
end

if init_problem == 1
  problem_initialize;
  mli_initialize;
else
  mli_initialize_from_mat(matfilename);
end

EST.currentphase = 4;

if SCEN.plotting == 2
    user_plot_init;
    drawnow;
end

%start the actual nmpc loop
mli_nmpc_loop;

if SCEN.plotting == 1
    user_plot_init;
    user_plot;
    drawnow;
end

if OPT.logging == 3
  save([SCEN.name,'.mat']);
end

fprintf('\n\nRun complete, save results...\n');
solvind('reset');

