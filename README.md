Installation instructions for MLI
====================

About MLI
-------------

The Matlab software package MLI (Multi-Level Iterations) provides methods for fast model-based optimizing control of complex dynamical systems.

Dependencies
-------------

* A running Matlab installation at `<Matlab_dir>` with mex compiler (this manual is tested with matlab version ...)
* SolvIND (add Link) installed in `<SolvIND_dir>` with Matlab interface
* [qpOASES](https://projects.coin-or.org/qpOASES)  installed in `<qpOASES_dir>`with Matlab interface
* [Ipopt](https://projects.coin-or.org/Ipopt) installed in `<Ipopt_dir>`(optional)


Cloning MLI
-------------

The MLI software files are stored in a main repository and an example subrepository. 
At first we clone the main repo and the example subrepo into the folder `<install_dir>`. 

```
git clone --recursive git@bitbucket.org:GlimmerMan/mli.git <install_dir>
```

>*Note:* A ssh key is required, otherwise use https.
>
>```
>git clone https://**username**@bitbucket.org:GlimmerMan/mli.git <install_dir>
>```

Setup MLI
-------------

Go to the MLI folder.

```
cd <install_dir>
```

Set the symbolic links to the external packages.

```
cd /utils
```

```
ln -s <qpOASES_dir>/interfaces/matlab/qpOASES.m .
```

```
ln -s <qpOASES_dir>/bin/qpOASES.mexa64 .
```

```
ln -s <qpOASES_dir>/bin/qpOASES_options.m .
```

```
ln -s <qpOASES_dir>/bin/qpOASES_sequence.m .
```

```
ln -s <qpOASES_dir>/bin/qpOASES_sequence.mexa64 .
```

```
ln -s <SolvIND_dir>/Packages/SOLVIND/Debug/matlab/solvind.mexa64 .
```

```
ln -s <SolvIND_dir>/SOLVIND_SUITE/Packages/SOLVIND/Debug/matlab/solvind.m .
```

```
ln -s <Ipopt_dir>/Ipopt/build/lib/ipopt.m .
```

```
ln -s <Ipopt_dir>/Ipopt/build/lib/ipopt.mexa64 .
```

Compile the models of the examples.

```
cd <install_dir>/examples/models/Release
```

```
make
```

If the compilation fails, configure CMake, especially set the SolvIND paths.

```
ccmake ../Src
```

Configure, quit and then compile again.


Running an example
-------------
Start Matlab in the `<install_dir>`.

```
cd <install_dir>
```

```
matlab
```

Run an example in Matlab.

```
runMLI('lotka')
```

Enjoy!

For other problems, replace `lotka` by the specific problem name.
MLI ships with the reference examples
 
* `lotka`,
* `pendulum`,
* `cstr2`,
* `four_tank`,

that are explained in the documentation (Link).


Remarks
-------------

* System library problems, conflict matlab and system:
alias.txt (`LD_PRELOAD`), set alias `matlab_solvind`, see below at SolvIND instructions



Instructions for external software
-------------
The installation of the external software is documented on the respective homepages.
Alternatively, you can use the following instructions. 

### SolvIND (subject to change)
> The instruction below describes installation of the SolvIND Suite. IWR internal only. There will be a new stand-alone SolvIND in the future.

Basis:

SOLVIND_SUITE:  `mercurial ID:hash =  31:f081442abd49`  (or the ID 32)

SOLVIND:        `mercurial ID:hash = 160:a67d69facda4`  (branch: asommer)


* Install necessary packages:

```
sudo apt-get install build-essential cmake cmake-curses-gui autoconf doxygen 
libtool pkg-config gfortran libboost-all-dev libblas-dev 
libatlas-base-dev libatlas-dev liblapack-dev mercurial libgtkmm-2.4-dev
```

It might be necessary to manually generate a symlink libhdf5.so that points to an hdf5-library, e.g.

```
cd /usr/lib/x86_64-linux-gnu
```

```
ln -s libhdf5_serial.so libhdf5.so
```

* Checkout the SOLVIND_SUITE from the mercurial repository and clone the necessary packages using
```
./bootstrap -C
```

* Checkout the SOLVIND branch asommer

```
cd Packages/SOLVIND/Src
```

```
hg checkout asommer
```

```
cd ../../..
```

* First compilation (generates Debug directories)

```
./bootstrap -D -jN
```

where N denotes the number of CPU cores to use. This should work without any errors.
Set N to 1 for easy debugging if compilation fails.

* Setup tests, matlab interface, dynamic models, etc.

Change folder to `Packages/SOLVIND/Debug` and configure cmake:


`ccmake .`  starts configuration of cmake


`t`         toggles advanced mode


Set the `HDF5_DIR` (e.g. `/usr/lib/x86_64-linux-gnu/hdf5/serial`)


Set the vMATLAB_DIR` (matlab root folder, e.g. `/usr/local/MATLAB/R2017a`)


`c`          trigger automatic reconfiguration (sets all the other paths)

`c`          trigger automatic reconfiguration (second time)


Set `WITH_BASIC_EXAMPLES` to `ON`
                    
Set `WITH_DAESOL_II_TEST` to `ON`

Set `WITH_DYN_MODELS`     to `ON`

Set `WITH_EVAL_TESTS`     to `ON`

Set `WITH_EXAMPLES`       to `ON`

Set `WITH_MATLAB`         to `ON`

Set `WITH_MATLAB_MEX`     to `ON`

Set `WITH_TEST`           to `ON`

`c`          trigger automatic reconfiguration

`e`          to confirm message about added compilation flag

`c`          trigger automatic reconfiguration (second time)

`e`          to confirm message about added compilation flag (second time)

`g`          generate makefile and exit


* Make and make install

```
make -j4
```(compile, set -jN to speedup using N CPU cores)

```
make install
```(copies the compilation to Debug dir)

* Ensure matlab uses the system's libs:
   Matlab must be forced to use the same c++-libraries that were used to compile the mex-file. This can be done by including the following alias into one's `.bashrc`:


```
alias matlab_solvind='LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libstdc++.so.6:/usr/lib/x86_64-linux-gnu/libgfortran.so.3 matlab'
```

* Testing the matlab interface
   First start the `use_SolvIND.m` in `Packages/SOLVIND/Debug/matlab` (it sets the matlab path to that directory)
   Run the `mextest.m` in `Packages/SOLVIND/Src/COMMON/INTERFACES/MEX`.
   Quick check: The deviation of forward and adjoint first order derivatives should be around machine precision (approx 2.3e-15)

> NOTE: If ADOL-C is not found during compilation of package SOLVIND, try deleting the folder `PACKAGES/ADOL-C/adolc_base` and recompile using bootstrap.


> NOTE: Check the latest installation instructions at (Link)


### qpOASES
* Download the latest version as archive.

* Unpack the archive at <qpOASES_dir> and go there.

```
cd <qpOASES_dir>
```

* Compile qpOASES.

```
make
```

* Compile the Matlab interface.

```
cd <qpOASES_dir>/interfaces/matlab
```

```
make
```


### Ipopt

* Get a licence and download the archive for your operating system.

* Unpack the archive at `<Ipopt_dir>` and go there.

```
cd <Ipopt_dir>
```

* Download external libraries.

```
cd ThirdParty/
```

```
cd HSL/
```

```
./get.HSL
```

```
cd ../Metis/
```

```
./get.Metis
```

```
cd ../Mumps/
```

```
./get.Mumps 
```

```
cd ../ASL/
```

```
./get.ASL 
```

```
cd ../Blas/
```

```
./get.Blas 
```

```
cd ../Lapack/
```

```
./get.Lapack
```

```
cd ..
```

* Make a build folder.

```
mkdir build
```

```
cd /build
```

* Configure.

```
../configure --disable-shared --with-blas=BUILD --with-lapack=BUILD ADD_CFLAGS="-fPIC" ADD_CXXFLAGS="-fPIC" ADD_FFLAGS="-fPIC" F77=gfortran CC=gcc CXX=g++
```

* Compile Ipopt.

```
make
```

```
make test
```

```
make install
```

* Compile the Matlab interface.

```
cd `<Ipopt_dir>`/contrib/MatlabInterface/src
```
```
make
```

```
make install
```

> NOTE: Check the latest installation instructions at (Link)




