function dif = compare_iterates(path1,path2,iterates)

feedbackq = [];
dif = [];
for ii = iterates
 str_end = sprintf('it%05d.mat', ii);
 str1 = [path1,str_end];
 str2 = [path2,str_end];
 load(str1, 'OPT');
 q1 = OPT.phase{4}.var.primal.q(1,:);
 sd1 = OPT.phase{4}.var.primal.sd(:,:);
 load(str2, 'OPT');
 q2 = OPT.phase{4}.var.primal.q(1,:);
 sd2 = OPT.phase{4}.var.primal.sd(:,:);
 n_part = [norm(q1-q2); norm(sd1(1,:)-sd2(1,:)); norm(sd1(2,:)-sd2(2,:)); norm(sd1(3,:)-sd2(3,:)); norm(sd1(4,:)-sd2(4,:))];
%  n = sum(n_part);
 dif= [dif,n_part];
 feedbackq = [feedbackq, q1(1)-q2(1)];
end

figure
plot(dif','o--')
legend('show')
figure
semilogy(abs(feedbackq),'o--')