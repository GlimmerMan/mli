
function [KKTMat, KKTrhs, v, U, S, V, r, Kern, Hred, eigHred] = kktanalysis(qp, x, y)

global OPT

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;


% KKT Matrix analysis
nvar = size(qp.A,2);

% find active cstrs through multiplier 
% bndidx = find(y(nsd+(1:ns*nq)) ~= 0);
% if ~isempty(bndidx)
%   bndidx = bndidx + nsd;
% end
% cstridx = find(y((nsd+ns*nq+1):end) ~= 0);
% % if ~isempty(cstridx)
% %   cstridx = cstridx + nsd+ns*nq;
% % end

% find active constraints directly by evaluating
bndidx = [];
bndidx = [bndidx; find(x-qp.lb < 1e-10)];
bndidx = [bndidx; find(qp.ub-x < 1e-10)];
if ~isempty(bndidx)
  bndidx = bndidx + nsd;
end
% find(x-lb < 0) ... violation

cstridx = [];
cstridx = [cstridx; find(qp.A*x-qp.ineqlb < 1e-10)];
cstridx = [cstridx; find(qp.inequb-qp.A*x < 1e-10)];
if ~isempty(cstridx)
  cstridx = cstridx + nsd+ns*nq;
end

% assemble KKT matrix
II = speye(nvar);
Aact = [full(II(1:nsd,:));
	full(II(bndidx,:));
	qp.A(cstridx-nsd-ns*nq,:)];
KKTMat = [qp.H, Aact';Aact,zeros(size(Aact,1))];

% assemble KKT rhs %! do together with evaluating above
KKTrhs = [-qp.g;
	  qp.lb(1:nsd)];

for ii = 1:length(bndidx)
  if y(bndidx(ii)) > 0
    KKTrhs(nvar+nsd+ii) = qp.lb(bndidx(ii)-nsd);
  else
    KKTrhs(nvar+nsd+ii) = qp.ub(bndidx(ii)-nsd);
  end
end

for ii = 1:length(cstridx)
  if y(cstridx(ii)) > 0
    KKTrhs(nvar+nsd+length(bndidx)+ii) = qp.ineqlb(cstridx(ii)-nsd-ns*nq);
  else
    KKTrhs(nvar+nsd+length(bndidx)+ii) = qp.inequb(cstridx(ii)-nsd-ns*nq);
  end
end
v = KKTMat\KKTrhs;

% calculate kernel of Aact via SVD
% perform svd
[U,S,V] = svd(Aact);
% determine rank
r = sum(diag(S) > 1e-15);
% calculate basis for kernel
Kern = V(:,r+1:nvar);
% size(Kern,2)

% return reduced Hessian
Hred = Kern' * qp.H * Kern;

% return eigs of reduced Hessian
[~,E] = eig(Hred);
eigHred  = diag(E);
