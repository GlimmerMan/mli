function drawresults

% global PLOT
addpath('algorithm');
addpath('utils');

%%%%%%%%%%%%%%%%%%%
% Newton iterations
%%%%%%%%%%%%%%%%%%%

% nominal
load('1_nicerun_2u.mat')
% x = SCEN.thistory;
% for jj = 1:size(x,2)
%     if isfield(ST.history{jj},'alpha')
%         it1(jj) = size(ST.history{jj}.alpha,2);
%     else
%         it1(jj) = 0;
%     end
% end

% 5 scenarios
load('5_completerun_2u.mat')
x = SCEN.thistory(1:end-1);
for jj = 1:size(x,2)
    if isfield(ST.history{jj},'alpha')
        it5(jj) = size(ST.history{jj}.alpha,2);
    else
        it5(jj) = 0;
    end
end

% 25 scenarios
load('25_completerun_2u.mat')
x = SCEN.thistory(1:end-1);
for jj = 1:size(x,2)
    if isfield(ST.history{jj},'alpha')
        it25(jj) = size(ST.history{jj}.alpha,2);
    else
        it25(jj) = 0;
    end
end

% 125 scenarios
load('125_completerun_2u.mat')
x = SCEN.thistory(1:end-1);
for jj = 1:size(x,2)
    if isfield(ST.history{jj},'alpha')
        it125(jj) = size(ST.history{jj}.alpha,2);
    else
        it125(jj) = 0;
    end
end
id = find(it25 > 60);
it25(id) = 0;

figure(1), hold on

xlabel('Time'), ylabel('Outer level iterations'), hold on, box on;

% plot(x,it5,'--gs',...
%     'LineWidth',2,...
%     'MarkerSize',3,...
%     'MarkerEdgeColor','c',...
%     'MarkerFaceColor',[0.5,0.5,0.5])
%   
% plot(x,it25,'--gs',...
%     'LineWidth',2,...
%     'MarkerSize',3,...
%     'MarkerEdgeColor','b',...
%     'MarkerFaceColor',[0.5,0.5,0.5])
%   
% plot(x,it125,'--gs',...
%     'LineWidth',2,...
%     'MarkerSize',3,...
%     'MarkerEdgeColor','r',...
%     'MarkerFaceColor',[0.5,0.5,0.5])
  
  bar(x,it125,'c','linestyle','none')
  bar(x,it25,'b','linestyle','none')
  bar(x,it5,'r','linestyle','none')
  axis([20 155 0 25])

%   its = [it125',it25',it5'];
%   b = bar(x,its,'linestyle','none');
%   set(b(1),'FaceColor','c');
%   set(b(2),'FaceColor','b');
%   set(b(3),'FaceColor','r');
  

legend 125 25 5
hold off

%%%%%%%%%%%%%%%%%%%
% loading
%%%%%%%%%%%%%%%%%%%

load('1_nicerun_2u.mat')
x1 = SCEN.xhistory;

load('5_completerun_2u.mat')
x5 = SCEN.xhistory;

load('25_completerun_2u.mat')
x25 = SCEN.xhistory;

load('125_completerun_2u.mat')
x125 = SCEN.xhistory;


%%%%%%%%%%%%%%%%%%%%%%%
% this and that

% figure(2);

% subplot(3,1,1)
% hold on
% plot(SCEN.thistory(1:ii),SCEN.xhistory(6,1:ii), 'r-','LineWidth',2);
% xlabel('Time [h]'), ylabel('Control [l/h]')
% 
% subplot(3,1,2)
% hold on
% plot(SCEN.thistory(1:ii),SCEN.xhistory(3,1:ii), 'r-','LineWidth',2);
% xlabel('Time [h]'), ylabel('Penicillin [g/l]')
% 
% subplot(3,1,3)
% hold on
% plot(SCEN.thistory(1:ii),SCEN.xhistory(1,1:ii), 'r-','LineWidth',2);
% xlabel('Time [h]'), ylabel('Biomass [g/l]')
% 
% % subplot(3,1,2)
% % hold on
% % plot(SCEN.thistory(1:ii),SCEN.xhistory(2,1:ii), 'c-','LineWidth',2);
% % % for jj = 1:S
% % %   str = sprintf('PLOT.y2{%i}',jj);
% % %   plot(PLOT.x,PLOT.y2{jj}, 'c-', 'xdatasource', 'PLOT.x', 'ydatasource', str);
% % %   if jj == 1
% % %     hold on
% % %   end
% % % end
% % xlabel('Time [h]'), ylabel('Substrate [g/l]')
% 
% 
% 
% % figure(3);
% % hold on
% % plot(SCEN.thistory(1:ii),SCEN.xhistory(6,1:ii), 'c-','LineWidth',2);
% % % for jj = 1:S
% % %   str = sprintf('PLOT.yu{%i}',jj);
% % %   plot(PLOT.x,PLOT.yu{jj}, 'c-', 'xdatasource', 'PLOT.x', 'ydatasource', str);
% % %   if jj == 1
% % %     hold on
% % %   end
% % % end
% % xlabel('Time [h]'), ylabel('Control [l/h]')
% clear SCEN;
% 
% % 25 scenarios
% load('25_completerun_2u.mat')
% x25 = SCEN.xhistory;
% % S = ST.S;
% % user_plot;
% 
% figure(2);
% subplot(3,1,1)
% plot(SCEN.thistory(1:ii),SCEN.xhistory(6,1:ii), 'b-','LineWidth',2);
% xlabel('Time [h]'), ylabel('Control [l/h]')
% 
% subplot(3,1,2)
% plot(SCEN.thistory(1:ii),SCEN.xhistory(3,1:ii), 'b-','LineWidth',2);
% xlabel('Time [h]'), ylabel('Penicillin [g/l]')
% 
% subplot(3,1,3)
% plot(SCEN.thistory(1:ii),SCEN.xhistory(1,1:ii), 'b-','LineWidth',2);
% xlabel('Time [h]'), ylabel('Biomass [g/l]')
% 
% % subplot(3,1,1)
% % plot(SCEN.thistory(1:ii),SCEN.xhistory(1,1:ii), 'b--','LineWidth',2);
% % % for jj = 1:S
% % %   str = sprintf('PLOT.y1{%i}',jj);
% % %   plot(PLOT.x,PLOT.y1{jj}, 'b--', 'xdatasource', 'PLOT.x', 'ydatasource', str);
% % %   if jj == 1
% % %     hold on
% % %   end
% % % end
% % 
% % subplot(3,1,2)
% % plot(SCEN.thistory(1:ii),SCEN.xhistory(2,1:ii), 'b--','LineWidth',2);
% % % for jj = 1:S
% % %   str = sprintf('PLOT.y2{%i}',jj);
% % %   plot(PLOT.x,PLOT.y2{jj}, 'b--', 'xdatasource', 'PLOT.x', 'ydatasource', str);
% % %   if jj == 1
% % %     hold on
% % %   end
% % % end
% % 
% % subplot(3,1,3)
% % plot(SCEN.thistory(1:ii),SCEN.xhistory(3,1:ii), 'b--','LineWidth',2);
% % % for jj = 1:S
% % %   str = sprintf('PLOT.y3{%i}',jj);
% % %   plot(PLOT.x,PLOT.y3{jj}, 'b--', 'xdatasource', 'PLOT.x', 'ydatasource', str);
% % %   if jj == 1
% % %     hold on
% % %   end
% % % end
% % 
% % figure(3);
% % hold on
% % plot(SCEN.thistory(1:ii),SCEN.xhistory(6,1:ii), 'b--','LineWidth',2);
% % % for jj = 1:S
% % %   str = sprintf('PLOT.yu{%i}',jj);
% % %   plot(PLOT.x,PLOT.yu{jj}, 'b--', 'xdatasource', 'PLOT.x', 'ydatasource', str);
% % %   if jj == 1
% % %     hold on
% % %   end
% % % end
% clear SCEN;
% 
% % 125 scenarios
% load('125_completerun_2u.mat')
% x125 = SCEN.xhistory;
% % S = ST.S;
% % user_plot;
% 
% figure(2);
% plot(SCEN.thistory(1:ii),SCEN.xhistory(6,1:ii), 'c-','LineWidth',2);
% xlabel('Time [h]'), ylabel('Control [l/h]')
% hold off
% 
% subplot(3,1,2)
% plot(SCEN.thistory(1:ii),SCEN.xhistory(3,1:ii), 'c-','LineWidth',2);
% xlabel('Time [h]'), ylabel('Penicillin [g/l]')
% hold off
% 
% subplot(3,1,3)
% plot(SCEN.thistory(1:ii),SCEN.xhistory(1,1:ii), 'c-','LineWidth',2);
% xlabel('Time [h]'), ylabel('Biomass [g/l]')
% hold off
% 
% 
% % subplot(3,1,1)
% % plot(SCEN.thistory(1:ii),SCEN.xhistory(1,1:ii), 'r-.','LineWidth',2);
% % % for jj = 1:S
% % %   str = sprintf('PLOT.y1{%i}',jj);
% % %   plot(PLOT.x,PLOT.y1{jj}, 'r:', 'xdatasource', 'PLOT.x', 'ydatasource', str);
% % %   if jj == 1
% % %     hold on
% % %   end
% % % end
% % 
% % subplot(3,1,2)
% % plot(SCEN.thistory(1:ii),SCEN.xhistory(2,1:ii), 'r-.','LineWidth',2);
% % % for jj = 1:S
% % %   str = sprintf('PLOT.y2{%i}',jj);
% % %   plot(PLOT.x,PLOT.y2{jj}, 'r:', 'xdatasource', 'PLOT.x', 'ydatasource', str);
% % %   if jj == 1
% % %     hold on
% % %   end
% % % end
% % 
% % subplot(3,1,3)
% % plot(SCEN.thistory(1:ii),SCEN.xhistory(3,1:ii), 'r-.','LineWidth',2);
% % % for jj = 1:S
% % %   str = sprintf('PLOT.y3{%i}',jj);
% % %   plot(PLOT.x,PLOT.y3{jj}, 'r:', 'xdatasource', 'PLOT.x', 'ydatasource', str);
% % %   if jj == 1
% % %     hold on
% % %   end
% % % end
% hold off
% 
% % figure(3);
% % hold on
% % plot(SCEN.thistory(1:ii),SCEN.xhistory(6,1:ii), 'r-.','LineWidth',2);
% % % for jj = 1:S
% % %   str = sprintf('PLOT.yu{%i}',jj);
% % %   plot(PLOT.x,PLOT.yu{jj}, 'r:', 'xdatasource', 'PLOT.x', 'ydatasource', str);
% % %   if jj == 1
% % %     hold on
% % %   end
% % % end
% % clear SCEN;
% % hold off

%%%%%%%%%%%%%%%%%%%
% trajectories and deviation from nominal case
%%%%%%%%%%%%%%%%%%%

figure(2);
subplot(3,1,1)
hold on, box on
plot(SCEN.thistory(1:ii),x5(6,1:ii), 'c-','LineWidth',2);
plot(SCEN.thistory(1:ii),x25(6,1:ii), 'b--.','LineWidth',2);
plot(SCEN.thistory(1:ii),x125(6,1:ii), 'r-.','LineWidth',2);
legend 125 25 5
xlabel('Time [h]'), ylabel('Control [l/h]')
hold off
subplot(3,1,2)
hold on, box on
plot(SCEN.thistory(1:ii),x5(3,1:ii), 'c-','LineWidth',2);
plot(SCEN.thistory(1:ii),x25(3,1:ii), 'b--.','LineWidth',2);
plot(SCEN.thistory(1:ii),x125(3,1:ii), 'r-.','LineWidth',2);
legend 125 25 5
xlabel('Time [h]'), ylabel('Penicillin [g/l]')
hold off
subplot(3,1,3)
hold on, box on
plot(SCEN.thistory(1:ii),x5(1,1:ii), 'c-','LineWidth',2);
plot(SCEN.thistory(1:ii),x25(1,1:ii), 'b--.','LineWidth',2);
plot(SCEN.thistory(1:ii),x125(1,1:ii), 'r-.','LineWidth',2);
legend 125 25 5
xlabel('Time [h]'), ylabel('Biomass [g/l]')
plot([0,150],[3.7,3.7], 'k--');
hold off


% figure(3);
% subplot(3,1,1)
% hold on
% plot(SCEN.thistory(1:ii),100*(x5(1,1:ii)-x1(1,1:ii))./(x1(1,1:ii)), 'c-','LineWidth',2);
% plot(SCEN.thistory(1:ii),100*(x25(1,1:ii)-x1(1,1:ii))./(x1(1,1:ii)), 'b--.','LineWidth',2);
% plot(SCEN.thistory(1:ii),100*(x125(1,1:ii)-x1(1,1:ii))./(x1(1,1:ii)), 'r-.','LineWidth',2);
% legend 125 25 5
% xlabel('Time [h]'), ylabel('Biomass Deviation from Nominal [%]')
% hold off
% subplot(3,1,2)
% hold on
% plot(SCEN.thistory(1:ii),100*(x5(2,1:ii)-x1(2,1:ii))./(x1(2,1:ii)), 'c-','LineWidth',2);
% plot(SCEN.thistory(1:ii),100*(x25(2,1:ii)-x1(2,1:ii))./(x1(2,1:ii)), 'b--.','LineWidth',2);
% plot(SCEN.thistory(1:ii),100*(x125(2,1:ii)-x1(2,1:ii))./(x1(2,1:ii)), 'r-.','LineWidth',2);
% legend 125 25 5
% xlabel('Time [h]'), ylabel('Substrate Deviation from Nominal [%]')
% hold off
% subplot(3,1,3)
% hold on
% plot(SCEN.thistory(1:ii),100*(x5(3,1:ii)-x1(3,1:ii))./(x1(3,1:ii)), 'c-','LineWidth',2);
% plot(SCEN.thistory(1:ii),100*(x25(3,1:ii)-x1(3,1:ii))./(x1(3,1:ii)), 'b--.','LineWidth',2);
% plot(SCEN.thistory(1:ii),100*(x125(3,1:ii)-x1(3,1:ii))./(x1(3,1:ii)), 'r-.','LineWidth',2);
% legend 125 25 5
% xlabel('Time [h]'), ylabel('Penicillin Deviation from Nominal [%]')
% hold off

figure(3);
subplot(3,1,1)
hold on, box on
plot(SCEN.thistory(1:ii),100*(x5(6,1:ii)-x1(6,1:ii))./(x1(6,1:ii)), 'c-','LineWidth',2);
plot(SCEN.thistory(1:ii),100*(x25(6,1:ii)-x1(6,1:ii))./(x1(6,1:ii)), 'b--.','LineWidth',2);
plot(SCEN.thistory(1:ii),100*(x125(6,1:ii)-x1(6,1:ii))./(x1(6,1:ii)), 'r-.','LineWidth',2);
legend 125 25 5
xlabel('Time [h]'), ylabel('Control Dev. from Nom. [%]')
hold off
subplot(3,1,2)
hold on, box on
plot(SCEN.thistory(1:ii),100*(x5(3,1:ii)-x1(3,1:ii))./(x1(3,1:ii)), 'c-','LineWidth',2);
plot(SCEN.thistory(1:ii),100*(x25(3,1:ii)-x1(3,1:ii))./(x1(3,1:ii)), 'b--.','LineWidth',2);
plot(SCEN.thistory(1:ii),100*(x125(3,1:ii)-x1(3,1:ii))./(x1(3,1:ii)), 'r-.','LineWidth',2);
legend 125 25 5
xlabel('Time [h]'), ylabel('Penicillin Dev. from Nom. [%]')
hold off
subplot(3,1,3)
hold on, box on
plot(SCEN.thistory(1:ii),100*(x5(1,1:ii)-x1(1,1:ii))./(x1(1,1:ii)), 'c-','LineWidth',2);
plot(SCEN.thistory(1:ii),100*(x25(1,1:ii)-x1(1,1:ii))./(x1(1,1:ii)), 'b--.','LineWidth',2);
plot(SCEN.thistory(1:ii),100*(x125(1,1:ii)-x1(1,1:ii))./(x1(1,1:ii)), 'r-.','LineWidth',2);
legend 125 25 5
xlabel('Time [h]'), ylabel('Biomass Dev. from Nom. [%]')
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computation times
%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(4)

hold on

xlabel('Number of scenarios'), ylabel('Computation time [s]'), hold on, box on;
 
sc = [5,25,125];
times = [0.17,  0.22, 11.55;
0.89,  3.35, 75.53;
4.45,  11.70, 100.54];

  plot(sc,times(:,2),'cs--','LineWidth',2,'MarkerSize',6,'MarkerEdgeColor','k','MarkerFaceColor','c')
  plot(sc,times(:,3),'bs--','LineWidth',2,'MarkerSize',6,'MarkerEdgeColor','k','MarkerFaceColor','b')
  plot(sc,times(:,1),'rs--','LineWidth',2,'MarkerSize',6,'MarkerEdgeColor','k','MarkerFaceColor','r')
  axis([0 130 0 120])
  

legend('Dual Decomposition','Gurobi', 'Ipopt (NLP)')
hold off




