function [x,fval,exitflag,iter,lambda,auxOutput] = check_infeasible_qp(QP)

[m,n] = size(QP.A);

H = zeros(n+2*(n+m));
A = [QP.A, zeros(m,n), zeros(m,n), eye(m), -eye(m);
     eye(n), eye(n), -eye(n), zeros(n,m), zeros(n,m)];
lbA = [QP.ineqlb; QP.lb];
ubA = [QP.inequb; QP.ub];
lb = [-inf*ones(n,1); zeros(2*(n+m),1)];
ub = inf*ones(size(lb));

g = [zeros(n,1); ones(2*(n+m),1)];

[x,fval,exitflag,iter,lambda,auxOutput] = ...
    qpOASES(H,g,A,lb,ub,lbA,ubA);

fprintf('Constraint violation in l1 norm: %g\n', fval)

