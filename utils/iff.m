% -*-matlab-*-
function [retval] = iff(test, trueval, falseval)

if test
  retval = trueval;
else
  retval = falseval;
end