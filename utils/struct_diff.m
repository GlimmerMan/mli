function struct_diff(A, B, prefix)
% STRUCT_DIFF(A, B, prefix)  Recursively parse all fields of A and compare them
% with the respective fields in B, printing out the fields that differ. The
% output is prefixed with the string in prefix.
%

% Author: Andreas Potschka
% Date: Aug 20, 2010

if nargin < 2
	error('struct_diff must be called with at least two arguments')
end

if nargin < 3
	prefix = '';
end

if nargin == 3 && ~ischar(prefix)
	error('prefix must be a string')
end

switch class(A)
case 'struct'
	fnames = fieldnames(A);
	for i = 1:size(fnames,1)
		fstr = sprintf('%s.%s', prefix, fnames{i});
		struct_diff(A.(fnames{i}), B.(fnames{i}), fstr);
	end
case 'cell'
	for i = 1:length(A)
		fstr = sprintf('%s{%d}', prefix, i);
		struct_diff(A{i}, B{i}, fstr);
	end
case 'char'
	if strcmp(A, B) ~= 1
		fprintf('%-40s %15s\n', prefix, 'differ');
	end
case {'int64', 'int32', 'logical'}
	if any(A ~= B)
		fprintf('%-40s %15s\n', prefix, 'differ')
	end
case 'double'
	sca = abs(A(:));
	sca(sca < 1) = 1;
	d = norm((A(:) - B(:)) ./ sca, inf);
	if d > eps
		fprintf('%-40s %15.8e\n', prefix, d)
	end
otherwise
	fprintf('%-40s %15s\n', prefix, 'not compared')
end

