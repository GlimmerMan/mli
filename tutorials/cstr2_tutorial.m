%% A continuous stirred tank reactor
%
% We consider a nonlinear, continuous-time stirred tank reactor (CSTR) problem.
% The model representing a first-order, irreversible, exothermic reaction A
% -> B is described by the following ODE model for the concentration of 
% chemical A $c_A$ and the reactor temperature $\theta$. Control variable
% is the cooling temperature $\theta_C$.
%
% $$ \frac{dc_A}{dt}    = \frac q V (c_{Af} - c_A) - k_0 \exp
% \left(-\frac{E}{RT}\right) c_A $$, 
% $$ \frac{d\theta}{dt} = \frac q V  (\theta_f - \theta) - \frac{\Delta H
% k_0}{\rho C_p} \exp \left(-\frac{E}{R\theta}\right) c_A + \frac{UA}{VC_p}
% (\theta-\theta_C) $$
%
% The objective is of tracking type.

%% Running the example in MLI
%
% In MLI we the example has the problem name cstr2. We run it with the
% default configuration by the runMLI command in the mli main folder.

% runMLI('cstr2'); % must be called in main folder

% After initialization by Ipopt, we solve in every NMPC loop one QP. This
% approach is called Real-Time Iteration. In mli we call it phase 4. If you
% have run the default configuration, you might have noticed several
% trajectories on the prediction horizon. That results from the 
% scenario tree robustification against an uncertain parameter. We 
% will develop the configuration step by step as in the pendulum tutorial. 

%% Remark on Multi-Level Iterations, Sampling Times
%
% For the influence of the choice of phases and the effect of smaller
% sampling times we refer to the pendulum_tutorial. In this cstr2 tutorial we
% rather focus on the Hessian updates, the control move regularization and
% the robustification by the scenario tree approach. Let's start!

%% Initialize the Controller, Scenario and Plots

global LOG SLV OPT EST SCEN ST MLI

LOG = []; SLV = []; OPT = []; EST = []; SCEN = []; ST = []; MLI = [];


% Add paths for mli
MLI.dir=[pwd,'/../'];
addpath([MLI.dir,'algorithm']);
addpath([MLI.dir,'utils']);
addpath([MLI.dir,'matlab']);

% Set the problempath for cstr2 example
SCEN.problempath =[MLI.dir,'examples/problems/cstr2'];
addpath(SCEN.problempath);

%% load the default controller and scenario data
% We initialize the simulation (from a problem-specific file).
scenario_initialize;

% First we do not use the scenario-tree, but nominal NMPC.
SCEN.use_st = 0;

% We start at sampling time 1.
SCEN.start=1;

% We initialize the controller (from a problem-specific file).
controller_initialize;

% We initialize mli.
mli_initialize;

% The highest plotting level is 2 and means that we plot after every NMPC
% iteration.
if SCEN.plotting == 2
    user_plot_init;
    drawnow;
end

%% Start the NMPC Loop with the specified data and measure the time

t0 = tic;
mli_nmpc_loop;
toc(t0)

%% Limited memory BFGS updates
% Assembling the exact Hessian required the expensive computation of second
% order derivatives. Alternatively we start from a scaled identity matrix 
% and use exact L-BFGS updates in every NMPC iteration when we solve a QP
% (phase 4).

OPT.lbfgshess = 0;
OPT.lbfgs_gamma = 1e-3;

close all
mli_initialize;

user_plot_init;
drawnow;

t1 = tic;
mli_nmpc_loop;
t_nom_lbfgs = toc(t1)

%% Control move regularization
%
% TODO section CMR, commands how to use CMR.

% reset stuff before tree

%% Robustification
%
% We add an uncertain parameter to the system. Nominal NMPC can then cause
% severe constraint violation. In mli we have implemented one robustification
% approach against the uncertainty that is known as Scenario Tree NMPC or
% Multi-Stage NMPC. In the cstr2 example we regard the temperature change
% as uncertain by introducing $\tilde\theta$ to the second equation. Thus,
%
% $$ \frac{d\theta}{dt} = \frac q V  (\theta_f - \theta) - \frac{\Delta H
% k_0}{\rho C_p} \exp \left(-\frac{E}{R\theta}\right) c_A + \frac{UA}{VC_p}
% (\theta-\theta_C) + \tilde\theta$$
%
% We enable the scenario tree feature in mli by

SCEN.use_st = 1;

% Then the file tree_initialize from the examples/problems/cstr2 folder is
% called during the initialization of mli. Have a look at it.

edit tree_initialize.m

% A vector of uncertain parameter realizations, the robust horizon and
% scenario weights are required.
% From the methodological point of view we perform a dual decomposition in
% the scenario coupling constraints and employ a non-smooth Newton methods.
% [citation]
% Therefore some algorithmic parameters as the non-smooth Newton tolerance and 
% the maximum iteration number need to be specified. Lets run scenario tree
% NMPC.

OPT = [];
close all

scenario_initialize;
tree_initialize;
controller_initialize;
mli_initialize;

user_plot_init;
drawnow;

t3 = tic;
mli_nmpc_loop;
t_5scen = toc(t3)

% Robustification comes at the cost of higher computation time and a
% slightly less objective value, but it is feasible with high probability.




