%% NMPC for the pendulum example with MLI
% We consider a pendulum that can be controlled via a motor at the pivot
% point.
% The movement of the pendulum can be modeled with the following ODE system
%
% $$ \underbrace{\pmatrix{\dot{x}_{1} \cr \dot{x}_{2} \cr}}_{\dot{x}}= \underbrace{\pmatrix{x_{2}\cr \frac{u}{ml^{2}}-\frac{g}{l}\sin x_{1}\cr}}_{f(x,u)} $$
%
% where $$ x_{1} $$ is the angle describing the current position of the
% pendulum, $$ x_{2} $$ is the angular velocity, $$ u $$ is the torque
% applied by the motor at the pivot and $$ l,g $$ are the length of the pendulum and the gravitational constant. 
% The objective is to steer the pendulum to the neutral position, which
% corresponds to the angle $$ x_{1,ref}=0 $$.
global LOG SLV OPT EST SCEN ST MLI;
%% Run MLI with the default scenario and controller data
% We can start an NMPC simulation for the pendulum example with
% the pre-specified problem data in controller_initialize.m and scenario_initialize.m
cd ..
runMLI('pendulum');


% %% Initialize MLI (Controller, Scenario and Plots)
% % First we initialize the controller and the scenario to the default values
% % specified in controller_initialize.m and scenario_initialize.m.
% 
% global LOG SLV OPT EST SCEN ST MLI;
% 
% LOG = []; SLV = []; OPT = []; EST = []; SCEN = []; ST = []; MLI=[];
% 
% MLI.dir=[pwd,'/../'];
% %add paths for mli
% addpath([MLI.dir,'algorithm']);
% addpath([MLI.dir,'utils']);
% addpath([MLI.dir,'matlab']);
% 
% % set problempath for pendulum example
% SCEN.problempath =[MLI.dir,'examples/problems/pendulum'];
% addpath(SCEN.problempath);
% 
% % load the default controller and scenario data
% % Scenario initialize
% scenario_initialize;
% 
% %don't use scenario-tree 
% SCEN.use_st = 0;
% % start at sampling time 1
% SCEN.start=1;
% 
% 
% % Controller initialize
% controller_initialize;
% 
% % initialize mli
% mli_initialize;
% 
% % initialize plots
% if SCEN.plotting == 2
%     user_plot_init;
%     drawnow;
% end
% 
% %% Start the NMPC Loop with the specified data and measure the time
% % Now that the controller and scenario are initialized, we can start the
% % NMPC simulation.
% t0=tic;
% mli_nmpc_loop;
% toc(t0)


%% Increase the sampling rate
% The vector SCEN.sampling contains all sampling times.
% We want to reduce the sampling time from 0.1 s to 0.02 s
SCEN.sampling = 0:0.02:5;
% initialize mli
mli_initialize;
% initialize plots
if SCEN.plotting == 2
    user_plot_init;
    drawnow;
end
% only print every 20th NMPC iteration
OPT.print_rate =25;
% start the actual nmpc loop
% t0=tic;
% mli_nmpc_loop;
% toc(t0)


%% Use differnt phases for the feedback
% We want to use phase 4 only in every second NMPC iteration and use phase
% 3 in the other itertions.
% This can be specified in the variable OPT as follows
OPT.ctrl.usephases = [3,4];
OPT.ctrl.phase_iter = [1,1];
OPT.ctrl.phasefreq = [1,2];

OPT.solve_initial_nlp=1;
% initialize mli
mli_initialize;
% initialize plots
if SCEN.plotting == 2
    user_plot_init;
    drawnow;
end

% start the actual nmpc loop
t0=tic;
mli_nmpc_loop;
toc(t0)
