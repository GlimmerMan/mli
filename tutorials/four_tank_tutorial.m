%% NMPC for the four tank example with MLI
% We consider a model of four tank  water plant as described in section 4 of [D. Limon et al 2014, Single-layer economic model predictive control for periodic operation]
% point.
% For a detailed description of the system we refer to the mentioned
% paper.
% The system consist of four water tanks that are connected with 2 pumps.
%
% The development of the water heights of in the four tanks ( $$ x_{1},\cdots,x_{4} $$ ) can be described by the following system of nonlinear ODES.
%
% $$\underbrace{\pmatrix{\dot{x}_{1} \cr \dot{x}_{2} \cr \dot{x}_{3} \cr
% \dot{x}_{4}\cr}}_{\dot{x}}=\underbrace{\pmatrix{-\frac{a_{1}}{A} \sqrt{2gx_{1}}+\frac{a_{3}}{A} \sqrt{2gx_{3}}+\frac{\gamma_{1}}{3600 A}u_{a} \cr -\frac{a_{2}}{A} \sqrt{2gx_{2}}+\frac{a_{4}}{A} \sqrt{2gx_{4}}+\frac{\gamma_{2}}{3600 A}u_{b} \cr-\frac{a_{3}}{A} \sqrt{2gx_{3}}+\frac{1-\gamma_{2}}{3600 A}u_{b} \cr -\frac{a_{4}}{A} \sqrt{2gx_{4}}+\frac{1-\gamma_{1}}{3600 A}u_{a} \cr}}_{f(x,u)} $$
%
%
% Here $$ u_{a},  u_{b} $$ are the water flows through pump a and b.
% which are the controls of the system.
%
% The values of the remaining parameters can be found in the following
% table.
%
% <latex>	\begin{tabular}{l |l l l}
% 		\hline
% 		Parameter &  Value  & Description \\
% 		\hline
% 		$H^{\max}$ &  ${1.2} {m}$&  Maximum water level in each tank \\
% 		$H^{\min}$ &  ${0.2}{m}$&  Minimum water level in each tank \\
% 		$Q^{\max}$ &  ${2.5}{m^3 h^{-1}}$  &   Maximal flow through each pump \\
% 		$Q^{\min}$ &  ${0}{m^3 h^{-1}}$  &   Minimal flow through each pump \\
% 		$a_{1}$&   ${1.341e-4}{m^2}$ &   Discharge constant of tank 1  \\
% 		$a_{2}$&   ${1.533e-4}{m^2}$ &   Discharge constant of tank 2  \\
% 		$a_{3}$&   ${9.322e-5}{m^2}$ &   Discharge constant of tank 3  \\
% 		$a_{4}$&   ${9.061e-5}{m^2}$ &   Discharge constant of tank 4  \\
% 		$A$&   ${0.03}{m^2} $ &  Cross section of all tanks \\
% 		$\gamma_{a}$ &   0.3 &   Parameter of three way valve a  \\
% 		$\gamma_{b}$ &   0.4 &   Parameter of three way valve b\\
% 		\hline
% 	\end{tabular}
% </latex>
%
% An illustration of the system can be seen in the following picture.
%
% <<four_tank_picture.png>>
%
% The economic cost function of the sytem is time dependant:
%
% $$ \ell(t,x,u)=(u_{a}^{2}+p(t)u_{b}^{2})+15\frac{2 H^{\mathrm{min}}}{A(x_{1}+x_{2})} $$
%
% The function $$ p(t)=0.15 \sin(\frac{2\pi t}{150})+1 $$ is time periodic
% with a period of 150 s.

%% Run MLI with the default scenario and controller data
% We can start an NMPC simulation for the four tank example example with
% the pre-specified problem data in controller_initialize.m and
% scenario_initialize.m by calling
%
% cd ..
%
% runMLI('four_tank');
%
% In order to explain the main extra requirements for the periodic NMPC
% feature of MLI we explain the main settings in the following:

%% Initialize MLI (Controller, Scenario and Plots)
% First we initialize the controller and the scenario to the default values
% specified in controller_initialize.m and scenario_initialize.m.

global LOG SLV OPT EST SCEN ST MLI;

LOG = []; SLV = []; OPT = []; EST = []; SCEN = []; ST = []; MLI=[];

MLI.dir=[pwd,'/../'];
%add paths for mli
addpath([MLI.dir,'algorithm']);
addpath([MLI.dir,'utils']);
addpath([MLI.dir,'matlab']);

% set problempath for the four tank example
SCEN.problempath =[MLI.dir,'examples/problems/four_tank'];
addpath(SCEN.problempath);

% load the default controller and scenario data
% Scenario initialize
scenario_initialize;

%don't use scenario-tree 
SCEN.use_st = 0;
% start at sampling time 1
SCEN.start=1;

% Controller initialize
controller_initialize;
%% Specify important parameters for periodic NMPC
% Now the setup for the controller and the scenario is ready.
% We repeat the important settings that are required for periodic NMPC:

% Use the perioidc NMPC feature
OPT.periodicity.use=1;

% The discount factor for the objective self tracking in the transient part
OPT.periodicity.discount=1.01;

% The weight of the transient objective part
OPT.periodicity.alpha=1e-2;

% The period length
OPT.periodicity.periodlength=150;
% Number of shooting intervals in the transient time horizon
OPT.periodicity.transient=30;
% Number of shooting intervals in the perioidc time horizon (30 intervals, period length 150 s, i.e. each interval has length 5 s)
OPT.periodicity.periodic=30;
% In total the prediction horizon has 60 shooting intervals of length 5 s,
% so the prediction horizon has the length 300 s.



DeltaT=OPT.periodicity.periodlength/OPT.periodicity.periodic;

% We choose a sampling time that corresponds to the length of one shooting
% interval and we choose a simulation duration of 400 s.
SCEN.sampling = 0:DeltaT:400;

% Currently, only Phase 9 is implemented for periodic NMPC, so we set:
OPT.ctrl.usephases = [9];

% Set the initial value
x0=[0.4594;0.9534;0.4587;0.9521;0];
SCEN.initial_x = x0;

% initialize mli
mli_initialize;

% initialize plots
if SCEN.plotting == 2
    user_plot_init;
    drawnow;
end

%% Start the NMPC Loop with the specified data.
% Now that the controller and scenario are initialized, and we can start the
% NMPC simulation.
mli_nmpc_loop;

%% Result
% We can see the resulting behaviour of the system.
% It can be observed that the controller was able to steer the initially
% perturbed system to a periodic trajectory.
