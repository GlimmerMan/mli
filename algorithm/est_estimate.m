% -*-matlab-*-
function est_estimate

global EST;

cursamp = EST.currentsample;
ns = EST.dims.nshoot;

% You can choose between QPOASES and MATLAB quadprog
if cursamp <= ns
%    est_estimate_quadprog_growinghorizon;
    est_estimate_qpoases_growinghorizon;
else
%    est_estimate_quadprog;
    est_estimate_qpoases;
end


