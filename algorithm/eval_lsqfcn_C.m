function [outp] = eval_lsqfcn_C(inp, nodetype)
    
    global SLV
    
    args.t = inp.t;    
    args.xd = inp.sd;
    args.qph = [inp.q;inp.p]; 

    evl = SLV.opt.evl;
    
    switch nodetype
      case 's'
	args.rhs = solvind('evaluateFcn', evl, 'lsqfcn_s', args);
	args.fwdTCOrder = 0;
	args.nFwdTC = 0;
	args.fwdTC = [];
	args.nAdjTC = 1;
	args.adjTC = args.rhs;
	result = solvind('evaluateDenseDer', evl, 'lsqfcn_s', args);
	
      case 'i'
	args.rhs = solvind('evaluateFcn', evl, 'lsqfcn_i', args);
	args.fwdTCOrder = 0;
	args.nFwdTC = 0;
	args.fwdTC = [];
	args.nAdjTC = 1;
	args.adjTC = args.rhs;
	result = solvind('evaluateDenseDer', evl, 'lsqfcn_i', args);
	
      case 'e'
	args.rhs = solvind('evaluateFcn', evl, 'lsqfcn_e', args);
	args.fwdTCOrder = 0;
	args.nFwdTC = 0;
	args.fwdTC = [];
	args.nAdjTC = 1;
	args.adjTC = args.rhs;
	result = solvind('evaluateDenseDer', evl, 'lsqfcn_e', args);
	
      otherwise
	error('eval_lsqfcn_C: Invalid nodetype');
	
    end

    outp.lsqfcn = result.rhs;
    outp.lsqgrad = result.propAdjTC(2:end);
    outp.lsqgrad = outp.lsqgrad(:);


    
    
