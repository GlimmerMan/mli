function [Hess] = lbfgshess_update(Hess, s, y)
%  [Hess] = LBFGSHESS_UPDATE(Hess, s, y)  add update with variable step
%  s and gradient step y possibly replacing oldest update of Hess. The update is
%  skipped if s'*y is too small.
%

% Authors: Andreas Potschka, Leonard Wirsching
% Date: Dec 3, 2009

if s' * y <= Hess.posdef_thres
	Hess.nskip = Hess.nskip + 1;
	return
end

Hess.s(:,Hess.actcol) = s;
Hess.y(:,Hess.actcol) = y;

Hess.l = max(Hess.l, Hess.actcol);

Hess.actcol = 1 + mod(Hess.actcol, size(Hess.s, 2));

