%the NMPC Loop within mli
function mli_nmpc_loop

global OPT EST SCEN ST;
print_counter = 1;
last_plot = uint64(0);
for ii=SCEN.start:SCEN.steps
  
    tloop = tic;
    SCEN.currentsample = ii;
    
    if SCEN.use_estimator
        EST.phase(ii) = EST.currentphase;
        EST.currentsample = ii;
        EST.eta = SCEN.meashistory(:,ii);
        if EST.fixed_p
            user_est_p_from_scen;
        end
        est_prepare;
        est_estimate;
        % store estimation results in SCEN
        SCEN.xhathistory(:,ii) = EST.sdest;
        if ~EST.fixed_p
            SCEN.phathistory(:,ii) = EST.pest;
        end
        SCEN.covhistory{ii} = EST.covest;
    end
    
    if SCEN.use_controller
        OPT.currentsample = ii;
        
        if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
          mli_prepare_controlmovereg;
        end
        
        if ~OPT.adaptive_level_choice
            OPT.currentitercount = OPT.iterhistory(ii);
            OPT.currentphase = OPT.phasehistory(ii);
        else
            OPT.currentitercount = 2;
            if ii == 1
                OPT.currentphase = 3;
                OPT.phasehistory(1) = 3;
            end
        end

        if SCEN.use_estimator
            % here, information about detected disturbances in the
            % estimator should be transferred 
            user_opt_xpcov_from_est;
            if ii > EST.dims.nshoot
                if EST.with_chi2test
                    if (~EST.chi2test(ii)) 
                        %keyboard;
                    end
                end
            end
        end
        
        
        % Preparation Phase
        if isfield(OPT, 'lbfgshess') && OPT.lbfgshess && SCEN.currentsample > 1
          
        else
        mli_evaluate;
        end
        mli_calchess;
        if SCEN.use_st == 0
          if OPT.currentphase == 9 % ipopt phase
            user_opt_x_from_scen;
            mli_solveNLP;
            OPT.condtime = 0;
            OPT.qptime = 0;
            OPT.blowuptime = 0;
          else
            if OPT.use_condensing
                mli_condense_withblocks;
            end
            
            if ~SCEN.use_estimator
                user_opt_x_from_scen;
            end
            
            % Feedback Phase
            if OPT.use_condensing
                %mli_solveQP_quadprog;
                mli_solveQP;
            else
                %mli_solveuncondensedQP_quadprog;
                mli_solveuncondensedQP;
            end
          end
        else % ST case
          S = ST.S;
          if OPT.currentphase == 9 % ipopt phase
            user_opt_x_from_scen;
            mli_solveSTNLP;
            for jj = 1:S
              OPT.condtime{jj} = 0;
            end
            OPT.qptime = 0;
            OPT.blowuptime = 0;
          else
            if OPT.use_condensing
              mli_condenseST_withblocks;
            end
            
            if ~SCEN.use_estimator
              user_opt_x_from_scen;
            end
            
            % Feedback Phase
            if OPT.use_condensing
              mli_solveSTcondQP;
            else
              mli_solveSTQP;
            end
          end
        end
    end

    scen_feedback;
   
    if SCEN.use_estimator
        EST.newctrl = SCEN.qhistory(:,ii);
        est_transition;
    end
    
    user_scen_update;
   
    if SCEN.use_controller
      if OPT.currentphase ~= 9 % not in ipopt
        % Transition Phase
        if OPT.use_condensing
            mli_blowup;
        else
            mli_blowup_nocond;
        end
        
        if OPT.adaptive_level_choice
            OPT.adapt = [];
            mli_collect_adaptinfo(1);
        end
        
        mli_stepcalc;
        
      end
      
      if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
        mli_preeval;
        if SCEN.currentsample > 1
          mli_prepare_lbfgsupdate;
        end
      end
      %print
      if SCEN.use_st == 0
          mli_printinfo(mod(print_counter,20) == 1)
      else
          mli_printinfoST(mod(print_counter,20) == 1)
      end
      print_counter = print_counter + 1;
      
        
        switch OPT.adaptive_level_choice
          case 0
            mli_postiterate;
           
          case 1
            mli_adaptivechoice_postit;
            
          case 2
            mli_adaptivechoice_sigma;
       
        end
        
        if OPT.logging
            mli_logiteration;
        end
        
        % This is kind of a trivial "parameter estimation" step
        if ~SCEN.use_estimator
            user_opt_p_from_scen;
        end
    end
    
    if isfield(OPT, 'levmar') && isfield(OPT, 'levmarred')
        OPT.levmar = OPT.levmarred * OPT.levmar;
    end
    
    SCEN.looptime{ii} = toc(tloop);
    plot_passed = toc(last_plot);
    if SCEN.plotting == 2 ...
            && (plot_passed >= SCEN.print_interval || ii == SCEN.steps)
        last_plot = tic;
        user_plot;
        drawnow;
    end
    if isfield(OPT,'periodicity')     
        %initialize the NLP variable for the next OCP by a periodic shift.
        mli_timehorizon_shift_periodic;
    end
    mli_scensave(SCEN.problempath, OPT.logging);
end
