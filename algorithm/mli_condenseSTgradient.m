function mli_condenseSTgradient

global OPT ST

S = ST.S;

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;

vidx = reshape(1:ns*nsd,nsd,ns);
vidx2 = reshape(nsd+(1:ns*nq),nq,ns);
g1 = cell(S,1);
g2 = cell(S,1);
g = cell(S,1);

for jj = 1:S
    % adding ST part (=g2) to gradient
    
    g2{jj} = (ST.C{jj}-ST.E{jj})'*ST.lambda;
    
    stride = nsd+nq;
    
    for ii=1:ns
        idx = (ii-1)*stride;
        g1{jj}(idx+(1:stride),1) = ...
            [OPT.eval{jj}.res.modgradx(:,ii)
            OPT.eval{jj}.res.modgradq(:,ii)];
    end
    g1{jj}(ns*(nsd+nq)+(1:nsd),1) = OPT.eval{jj}.res.modgradx(:,ns+1);
    
    g{jj}= g1{jj} + g2{jj};
    
    for ii=1:ns
        idx = (ii-1)*stride;
        tmp = g{jj}(idx+(1:stride),1);
        OPT.eval{jj}.res.modgradx(:,ii) = tmp(1:nsd);
        OPT.eval{jj}.res.modgradq(:,ii) = tmp(nsd+1:end);
    end
    OPT.eval{jj}.res.modgradx(:,ns+1) = g{jj}(ns*(nsd+nq)+(1:nsd),1);
    
    % b2 part
    OPT.cond{jj}.b(1:nsd,1) = OPT.eval{jj}.res.modgradx(:,1);
    for ii=1:ns
        OPT.cond{jj}.b(vidx2(:,ii),1) = OPT.eval{jj}.res.modgradq(:,ii);
    end
    
    % C12' * b1 part +  C12' * B11 * c part
    for ii=1:ns
        OPT.cond{jj}.b(1:nsd,1) = OPT.cond{jj}.b(1:nsd,1) + ...
            OPT.cond{jj}.cblocks.first{ii}' * OPT.eval{jj}.res.modgradx(:,ii+1) + ...
            OPT.cond{jj}.cblocksb.first{ii} * OPT.cond{jj}.c(vidx(:,ii));
    end
    
    for ll=1:ns
        for ii=ll:ns
            OPT.cond{jj}.b(vidx2(:,ll),1) = OPT.cond{jj}.b(vidx2(:,ll),1) + ...
                OPT.cond{jj}.cblocks.mixed{ii,ll}' * OPT.eval{jj}.res.modgradx(:,ii+1) + ...
                OPT.cond{jj}.cblocksb.mixed{ii,ll} * OPT.cond{jj}.c(vidx(:,ii));
        end
    end
    
    % B12' * c part
    for ii=1:ns-1
        OPT.cond{jj}.b(vidx2(:,ii+1),1) = OPT.cond{jj}.b(vidx2(:,ii+1),1) + ...
            OPT.hess{jj}.sqblock{ii+1}' * OPT.cond{jj}.c(vidx(:,ii));
    end
end

OPT.qp{jj}.g = OPT.cond{jj}.b;