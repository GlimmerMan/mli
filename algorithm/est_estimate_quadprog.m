% -*-matlab-*-
function est_estimate_quadprog

global EST;

ns = EST.dims.nshoot;
nsd = EST.dims.nsd;
np = EST.dims.np;
nq = EST.dims.nq;
nlsqs = EST.dims.lsqfcn_s;
nlsqi = EST.dims.lsqfcn_i; 
nlsqe = EST.dims.lsqfcn_e;

pvars = EST.var.primal;
cursamp = EST.currentsample;

% finish evaluation of gradient
switch EST.currentphase
    
  case {2,4}
    msn = ns+1;
    Vmsn =  diag(EST.V(:,msn));
    EST.eval.res.lsq(:,msn) = Vmsn*(EST.eval.res.meas(:,msn) - EST.eta);
    EST.eval.res.modgradx(:,msn) = EST.eval.mat.Jmeasx{msn}'*Vmsn'* ...
        EST.eval.res.lsq(:,msn);
    if ~EST.fixed_p
        EST.eval.res.modgradp = EST.eval.res.modgradp + ...
            EST.eval.mat.Jmeasp{msn}'*Vmsn'*EST.eval.res.lsq(:,msn);
    end

  case 3
    msn = ns+1;
    inp = [];
    inp.t = EST.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.p = pvars.p;
    inp.q = pvars.qconst(:,msn-1);
    outp = eval_lsqfcn_C_est(inp,'e', EST.eta, EST.V(:,msn));
    EST.eval.res.lsq(:,msn) = outp.residual;
    EST.eval.res.modgradx(:,msn) = outp.lsqgrad(1:nsd);
    if ~EST.fixed_p
        EST.eval.res.modgradp = EST.eval.res.modgradp + ...
            outp.lsqgrad(nsd+(1:np));
    end
end

% finish condensing of gradient
EST.cond.grad(1:nsd) = EST.cond.grad(1:nsd) + EST.cond.cblocksx{ns}'* ...
    EST.eval.res.modgradx(:,ns+1);
if ~EST.fixed_p
    EST.cond.grad(nsd+(1:np)) = EST.cond.grad(nsd+(1:np)) + EST.eval.res.modgradp;
    EST.cond.grad(nsd+(1:np)) = EST.cond.grad(nsd+(1:np)) + EST.cond.cblocksp{ns}'* ...
        EST.eval.res.modgradx(:,ns+1);
end

% add condensing modification to gradient
EST.cond.grad = EST.cond.grad + EST.cond.gradmod;

% Build QP gradiend (arrivalCost treated separately)
if ~EST.disable_arrival
    EST.qp.g = [EST.P'*EST.eval.res.arrivalCost + ...
                EST.cond.grad];
else
    EST.qp.g = EST.cond.grad;
end

% now the QP is complete
A = [EST.qp.A
     -EST.qp.A];
b = [EST.qp.ubA
     -EST.qp.lbA];

opts = optimset('Algorithm', 'active-set');
opts = optimset(opts, 'Diagnostics', 'off');
opts = optimset(opts, 'Display', 'off');
opts = optimset(opts, 'MaxIter', 1e4);

[x, obj, status, output, y] = quadprog(EST.qp.H, EST.qp.g, A, b, [], ...
                                       [], EST.qp.lb, EST.qp.ub, [], ...
                                       opts); 
    
if (status <= 0)
  status
  output
  error('QP solution failed!');
end

EST.qp.solstep = x;

% step blowup
EST.step.primal.sd(:,1) = x(1:nsd);
if ~EST.fixed_p
    EST.step.primal.p = x(nsd+(1:np));
end
sdstep = EST.qp.A * EST.qp.solstep + EST.cond.c(:);
EST.step.primal.sd(:,2:ns+1) = reshape(sdstep, nsd, ns);

% return estimates
EST.sdest = EST.var.primal.sd(:,ns+1) + EST.step.primal.sd(:, ns+1);
if ~EST.fixed_p
    EST.pest = EST.var.primal.p + EST.step.primal.p;
    GN = [EST.cond.cblocksx{ns} EST.cond.cblocksp{ns}
          zeros(np,nsd) eye(np)];
else
    GN = EST.cond.cblocksx{ns};
end
EST.covest = GN * inv(EST.qp.H) * GN';

% chi-squared test for parameter jump detection
% only on full horizon, only considering measurements
if EST.with_chi2test
    var = EST.var.primal;
    var.sd = var.sd + EST.step.primal.sd;
    if ~EST.fixed_p
        var.p = var.p + EST.step.primal.p;
    end
    var.t = EST.discr.msgrid;
    eta = [EST.measurements EST.eta];
    
    [chi2test, testval, chi2val] = est_chi2test(var, EST.V, eta, 0.05);
    
    EST.chi2test(cursamp) = chi2test;
    
    if ~chi2test
        fprintf(['\n chi2val=%.3g  testval=%.3g ****** Chi-squared test failed ' ...
                 '******\n'], chi2val, testval);
        EST.currentphase = 4;
    else
        fprintf(['\n chi2val=%.3g  testval=%.3g ------ Chi-squared test passed ' ...
                 '------\n'], chi2val, testval);
        EST.currentphase = 2;
    end
end
