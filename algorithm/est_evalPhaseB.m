% -*-matlab-*-
function est_evalPhaseB

global EST;

ns = EST.dims.nshoot;
nsd = EST.dims.nsd; 
nq = EST.dims.nq; 
np = EST.dims.np;
nlsqs = EST.dims.lsqfcn_s;
nlsqi = EST.dims.lsqfcn_i; 
nlsqe = EST.dims.lsqfcn_e;

pvars = EST.var.primal;

% dynamic equations
for msn = 1:ns
    inp = [];
    inp.thoriz = [EST.discr.msgrid(msn) EST.discr.msgrid(msn+1)];
    inp.sd = pvars.sd(:,msn);
    inp.p = pvars.p;
    inp.q = pvars.qconst(:,msn);
    
    outp = eval_dyn_B_est(inp);
    EST.eval.res.xd(:,msn) = outp.x;
    EST.eval.res.c(:,msn) = outp.x - pvars.sd(:,msn+1);
end

% measurement function residuals
msn = 1;
inp = [];
inp.t = EST.discr.msgrid(msn);
inp.sd = pvars.sd(:,msn);
inp.p = pvars.p;
inp.q = pvars.qconst(:,msn);
outp = eval_lsqfcn_B_est(inp,'s');

EST.eval.res.meas(:,msn) = outp.lsqfcn;
Vmsn =  diag(EST.V(:,msn));
EST.eval.res.lsq(:,msn) = Vmsn * ...
    (EST.eval.res.meas(:,msn) - EST.measurements(:,msn));
EST.eval.res.modgradx(:,msn) = EST.eval.mat.Jmeasx{msn}'*Vmsn'* ...
    EST.eval.res.lsq(:,msn); 
if ~EST.fixed_p
   EST.eval.res.modgradp(:) = 0.0;
   EST.eval.res.modgradp = EST.eval.res.modgradp + ...
       EST.eval.mat.Jmeasp{msn}'*Vmsn'*EST.eval.res.lsq(:,msn); 
end


for msn = 2:ns
    inp = [];
    inp.t = EST.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.p = pvars.p;
    inp.q = pvars.qconst(:,msn);
    outp = eval_lsqfcn_B_est(inp,'i');
    
    EST.eval.res.meas(:,msn) = outp.lsqfcn;
    Vmsn =  diag(EST.V(:,msn));
    EST.eval.res.lsq(:,msn) = Vmsn * ...
        (EST.eval.res.meas(:,msn) - EST.measurements(:,msn));
    EST.eval.res.modgradx(:,msn) = EST.eval.mat.Jmeasx{msn}'*Vmsn'* ...
        EST.eval.res.lsq(:,msn); 
    if ~EST.fixed_p
        EST.eval.res.modgradp = EST.eval.res.modgradp + ...
            EST.eval.mat.Jmeasp{msn}'*Vmsn'*EST.eval.res.lsq(:,msn); 
    end
end

% measurement Function
msn = ns+1;
inp = [];
inp.t = EST.discr.msgrid(msn);
inp.sd = pvars.sd(:,msn);
inp.p = pvars.p;
inp.q = pvars.qconst(:,msn-1);
outp = eval_lsqfcn_B_est(inp,'e');

EST.eval.res.meas(:,msn) = outp.lsqfcn;

%disp('End of evalPhaseB...');
%keyboard;
