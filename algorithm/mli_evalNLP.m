% -*-matlab-*-
function [data] = mli_evalNLP(vars)

global SCEN OPT ST;

ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd; 
nq = OPT.dims.nq; 
np = OPT.dims.np;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nlsqs = OPT.dims.lsqfcn_s;
nlsqi = OPT.dims.lsqfcn_i;
nlsqe = OPT.dims.lsqfcn_e;
nmfcn = OPT.dims.mfcn;
nlfcn = OPT.dims.lfcn;

CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;

if SCEN.use_st == 0

tic
  
pvars = vars.primal;
dvars = vars.dual;
ovars = vars.objective;

if isequal(pvars, OPT.nlp.cache.pvars) && ...
    isequal(dvars, OPT.nlp.cache.dvars) && ...
    isequal(ovars, OPT.nlp.cache.ovars)
  data = OPT.nlp.cache.data;
  return;
end

sensdirs = [zeros(1,nsd+nq);eye(nsd+nq);zeros(np,nsd+nq)];

%%% Start Node
msn = 1;

% dynamical equations
inp = [];
inp.thoriz = [OPT.discr.msgrid(msn) OPT.discr.msgrid(msn+1)];
inp.sd = pvars.sd(:,msn);
inp.q = pvars.q(:,msn);
inp.p = pvars.pconst;
inp.sensdirs = sensdirs;
inp.lambda = dvars.lambda(:,msn);
inp.sigma = ovars.sigma;

outp = eval_dyn_D(inp);

data.res.xd(:,msn) = outp.x;
data.res.c(:,msn) = outp.x - pvars.sd(:,msn+1);
data.res.modgradx(:,msn) = zeros(nsd,1);    
data.res.modgradq(:,msn) = zeros(nq,1);
if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
  data.res.modgraddelq(:,msn) = zeros(nq,1);
  if isfield(OPT, 'controlmove_c')
    c = OPT.controlmove_c;
  else
    c = zeros(nq,1);
  end
  if isfield(OPT, 'controlmove_R')
    R = OPT.controlmove_R;
  else
    R = zeros(nq,nq);
  end
end
if nlfcn > 0
  data.res.lfcn(:,msn) = outp.lfcn;
  data.res.dlfcn(:,msn) = outp.dlfcn;
end
data.mat.Gx{msn} = outp.G(1:nsd,1:nsd);
data.mat.Gq{msn} = outp.G(1:nsd,nsd+(1:nq));
data.mat.lambdaGxx{msn} = outp.H(1:nsd,1:nsd);
data.mat.lambdaGxq{msn} = outp.H(1:nsd,nsd+(1:nq));
data.mat.lambdaGqq{msn} = outp.H(nsd+(1:nq),nsd+(1:nq));

if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
    data.res.modgraddelq(:,msn) = data.res.modgraddelq(:,msn) ...
      + c + 2 * R * pvars.delq(:,msn);
end

% decoupled node constraint
if nrds > 0
  error('Not yet implemented')
end

% least squares objective
if nlsqs > 0
  error('Not yet implemented')
end

%%% Interior Nodes
for msn=2:ns

% dynamical equations
  inp = [];
  inp.thoriz = [OPT.discr.msgrid(msn) OPT.discr.msgrid(msn+1)];
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn);
  inp.p = pvars.pconst;
  inp.lambda = dvars.lambda(:,msn);
  inp.sensdirs = sensdirs;
  inp.sigma = ovars.sigma;
  
  outp = eval_dyn_D(inp);
    
  data.res.xd(:,msn) = outp.x;
  data.res.c(:,msn) = outp.x - pvars.sd(:,msn+1);
  data.res.modgradx(:,msn) = zeros(nsd,1);
  data.res.modgradq(:,msn) = zeros(nq,1);
  if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
    data.res.modgraddelq(:,msn) = zeros(nq,1);
  end
  
  if nlfcn > 0
    data.res.lfcn(:,msn) = outp.lfcn;
    data.res.dlfcn(:,msn) = outp.dlfcn;
  end
  
  data.mat.Gx{msn} = outp.G(1:nsd,1:nsd);
  data.mat.Gq{msn} = outp.G(1:nsd,nsd+(1:nq));
  data.mat.lambdaGxx{msn} = outp.H(1:nsd,1:nsd);
  data.mat.lambdaGxq{msn} = outp.H(1:nsd,nsd+(1:nq));
  data.mat.lambdaGqq{msn} = outp.H(nsd+(1:nq),nsd+(1:nq));

  if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
    if msn < ns
      data.res.modgraddelq(:,msn) = data.res.modgraddelq(:,msn) ...
        + c + 2 * R * pvars.delq(:,msn);
    else
      data.res.modgraddelq(:,msn) = data.res.modgraddelq(:,msn) ...
        + OPT.controlmove_c0 + 2 * OPT.controlmove_R0 * pvars.delq(:,msn);
    end
  end
  
% decoupled node constraint
  if nrdi > 0
    error('Not yet implemented')
  end

  % least squares objective
  if nlsqi > 0
    error('Not yet implemented')
  end

end

%%% End node (depends only on s, not on q)
msn = ns + 1;

data.res.modgradx(:,msn) = zeros(nsd,1);

% decoupled node constraint
if nrde > 0
  error('Not yet implemented')
end

  % least squares objective
if nlsqe > 0
  error('Not yet implemented')
end

% Mayer objective
if nmfcn > 0 && nlfcn <= 0
  if isfield(OPT,'periodicity')
    for ii=1:OPT.dims.nshoot
      start_node=(ii-1)+1;
      end_node=ii+1;
      %start part
      inp = [];
      inp.t = OPT.discr.msgrid(start_node);
      inp.sd = pvars.sd(:,start_node);
      %which control to take
      inp.q = pvars.q(:,start_node);
      inp.p = pvars.pconst;
      inp.sensdirs = sensdirs;
      
      outp = eval_mfcn_D(inp);
      %alpha=OPT.alpha(ii);
      data.res.mfun{ii}.start = outp.mfcn;
      data.res.mgrad{ii}.start = outp.grad(1:nsd);
      data.mat.mhess{ii}.start = outp.H(1:nsd,1:nsd);
      
      %end part
      inp = [];
      inp.t = OPT.discr.msgrid(end_node);
      inp.sd = pvars.sd(:,end_node);
      %which control to take
      if ii==OPT.dims.nshoot
        inp.q = pvars.q(:,end_node-1);
      else
        inp.q = pvars.q(:,end_node);
      end
      inp.p = pvars.pconst;
      inp.sensdirs = sensdirs;
      
      outp = eval_mfcn_D(inp);
      
      data.res.mfun{ii}.end = outp.mfcn;
      data.res.mgrad{ii}.end = outp.grad(1:nsd);
      data.mat.mhess{ii}.end = outp.H(1:nsd,1:nsd);
      
      
    end
  else
    inp = [];
    inp.t = OPT.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.q = pvars.q(:,msn-1);
    inp.p = pvars.pconst;
    inp.sensdirs = sensdirs;
    
    outp = eval_mfcn_D(inp);
    
    data.res.mfun = outp.mfcn;
    data.res.mgrad = outp.grad(1:nsd);
    data.mat.mhess = outp.H(1:nsd,1:nsd);
    data.res.modgradx(:,msn) = data.res.modgradx(:,msn) ...
      + data.res.mgrad;
  end
end

% Periodicity constraint
if isfield(OPT,'periodicity')
    sensdirs = [zeros(1,nsd+nq);eye(nsd+nq)];
    data.res.periodicity=zeros(OPT.dims.periodicity,1);
    %eval at node at beginnign of periodic phase
    inp = [];
    inp.t = OPT.discr.msgrid(OPT.periodicity.transient+1);
    inp.sd = pvars.sd(:,OPT.periodicity.transient+1);
    inp.q = pvars.q(:,OPT.periodicity.transient+1);
    inp.p = pvars.pconst;
    inp.sensdirs = sensdirs;
    outp_start = eval_rcfcn_i_D(inp);
    inp = [];
    inp.t = OPT.discr.msgrid(OPT.periodicity.transient+OPT.periodicity.periodic+1);
    inp.sd = pvars.sd(:,OPT.periodicity.transient+OPT.periodicity.periodic+1);
    %fake control, the last shooting node has no coresponding control
    inp.q = pvars.q(:,OPT.periodicity.transient+OPT.periodicity.periodic);
    inp.p = pvars.pconst;
    inp.sensdirs = sensdirs;
    outp_end= eval_rcfcn_i_D(inp);
    %residual
    data.res.periodicity=outp_end.eval-outp_start.eval;
    %Jacobi at start
    data.periodicity.G_start=outp_start.G;
    %Jacobi at end
    data.periodicity.G_end=outp_end.G;
    %assume no second derivative!
    
end

OPT.nlp.cache.pvars = pvars;
OPT.nlp.cache.dvars = dvars;
OPT.nlp.cache.ovars = ovars;
OPT.nlp.cache.data = data;

OPT.evaltime = toc;

else % ST case
    
    S = ST.S;
    data = cell(S,1);
%     for jj=1:S
%         data{jj} = mli_evalSTPhaseD(vars{jj},jj);
%     end
        pvars = cell(S,1);
        dvars = cell(S,1);
        ovars = cell(S,1);
    for jj=1:S
        pvars{jj} = vars{jj}.primal;
        dvars{jj} = vars{jj}.dual;
        ovars{jj} = vars{jj}.objective;
    end
    

    if isequal(pvars, OPT.nlp.cache.pvars) && ...
            isequal(dvars, OPT.nlp.cache.dvars) && ...
            isequal(ovars, OPT.nlp.cache.ovars)
        data = OPT.nlp.cache.data;
        return;
    end

    for jj = 1:S
      
      tic
      
      sensdirs = [zeros(1,nsd+nq);eye(nsd+nq);zeros(np,nsd+nq)];
      
      %%% Start Node
      msn = 1;
      
      % dynamical equations
      inp = [];
      inp.thoriz = [OPT.discr.msgrid(msn) OPT.discr.msgrid(msn+1)];
      inp.sd = pvars{jj}.sd(:,msn);
      inp.q = pvars{jj}.q(:,msn);
      %       inp.p = pvars.pconst;
      inp.p = ST.branches{jj,msn}(:); % p of first tree node
      inp.sensdirs = sensdirs;
      inp.lambda = dvars{jj}.lambda(:,msn);
      inp.sigma = ovars{jj}.sigma;
      
      outp = eval_dyn_D(inp);
      
      data_loc.res.xd(:,msn) = outp.x;
      data_loc.res.c(:,msn) = outp.x - pvars{jj}.sd(:,msn+1);
      data_loc.res.modgradx = zeros(nsd,ns+1);
      data_loc.res.modgradq = zeros(nq,ns);
%       data_loc.res.modgradx(:,msn) = zeros(nsd,1);    
%       data_loc.res.modgradq(:,msn) = zeros(nq,1);
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
        data_loc.res.modgraddelq = zeros(nq,ns);
%         data_loc.res.modgraddelq(:,msn) = zeros(nq,1);
        if isfield(OPT, 'controlmove_c')
          c = OPT.controlmove_c;
        else
          c = zeros(nq,1);
        end
        if isfield(OPT, 'controlmove_R')
          R = OPT.controlmove_R;
        else
          R = zeros(nq,nq);
        end
      end
      if nlfcn > 0
        data_loc.res.lfcn(:,msn) = outp.lfcn;
        data_loc.res.dlfcn(:,msn) = outp.dlfcn;
      end
      data_loc.mat.Gx{msn} = outp.G(1:nsd,1:nsd);
      data_loc.mat.Gq{msn} = outp.G(1:nsd,nsd+(1:nq));
      data_loc.mat.lambdaGxx{msn} = outp.H(1:nsd,1:nsd);
      data_loc.mat.lambdaGxq{msn} = outp.H(1:nsd,nsd+(1:nq));
      data_loc.mat.lambdaGqq{msn} = outp.H(nsd+(1:nq),nsd+(1:nq));
      
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
        data_loc.res.modgraddelq(:,msn) = data_loc.res.modgraddelq(:,msn) ...
          + c + 2 * R * pvars{jj}.delq(:,msn);
      end
      
      % decoupled node constraint
      if nrds > 0
        error('Not yet implemented')
      end
      
      % least squares objective
      if nlsqs > 0
        error('Not yet implemented')
      end
    
      %%% Interior Nodes
      for msn=2:ns
        
        % dynamical equations
        inp = [];
        inp.thoriz = [OPT.discr.msgrid(msn) OPT.discr.msgrid(msn+1)];
        inp.sd = pvars{jj}.sd(:,msn);
        inp.q = pvars{jj}.q(:,msn);
        if msn > ST.robust_horizon
          inp.p = pvars{jj}.pconst;
        else
          inp.p = ST.branches{jj,msn}(:); % uncertain p of node msn
        end
        inp.lambda = dvars{jj}.lambda(:,msn);
        inp.sensdirs = sensdirs;
        inp.sigma = ovars{jj}.sigma;
        
        outp = eval_dyn_D(inp);
        
        data_loc.res.xd(:,msn) = outp.x;
        data_loc.res.c(:,msn) = outp.x - pvars{jj}.sd(:,msn+1);
        data_loc.res.modgradx(:,msn) = zeros(nsd,1);
        data_loc.res.modgradq(:,msn) = zeros(nq,1);
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
          data_loc.res.modgraddelq(:,msn) = zeros(nq,1);
        end
  
        if nlfcn > 0
          data_loc.res.lfcn(:,msn) = outp.lfcn;
          data_loc.res.dlfcn(:,msn) = outp.dlfcn;
        end
        
        data_loc.mat.Gx{msn} = outp.G(1:nsd,1:nsd);
        data_loc.mat.Gq{msn} = outp.G(1:nsd,nsd+(1:nq));
        data_loc.mat.lambdaGxx{msn} = outp.H(1:nsd,1:nsd);
        data_loc.mat.lambdaGxq{msn} = outp.H(1:nsd,nsd+(1:nq));
        data_loc.mat.lambdaGqq{msn} = outp.H(nsd+(1:nq),nsd+(1:nq));
        
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
          if msn < ns
            data_loc.res.modgraddelq(:,msn) = data_loc.res.modgraddelq(:,msn) ...
              + c + 2 * R * pvars{jj}.delq(:,msn);
          else
            data_loc.res.modgraddelq(:,msn) = data_loc.res.modgraddelq(:,msn) ...
              + OPT.controlmove_c0 + 2 * OPT.controlmove_R0 * pvars{jj}.delq(:,msn);
          end
        end
        
        % decoupled node constraint
        if nrdi > 0
          error('Not yet implemented')
        end
        
        % least squares objective
        if nlsqi > 0
          error('Not yet implemented')
        end
        
      end
      
      %%% End node (depends only on s, not on q)
      msn = ns + 1;
      
      % decoupled node constraint
      if nrde > 0
        error('Not yet implemented')
      end
      
      % non-anticipativity constraint
      if jj < S
        nozrows = sum(ST.C_ipopt{jj},2) > 0;
        data_loc.res.noac = ST.C_ipopt{jj}(nozrows,:)*pvars{jj}.z_ipopt - ST.E_ipopt{jj+1}(nozrows,:)*pvars{jj+1}.z_ipopt;
      end
      
      % least squares objective
      if nlsqe > 0
        error('Not yet implemented')
      end
      
      % Mayer objective
      if nmfcn > 0 && nlfcn <= 0
        inp = [];
        inp.t = OPT.discr.msgrid(msn);
        inp.sd = pvars{jj}.sd(:,msn);
        inp.q = pvars{jj}.q(:,msn-1);
        if msn > ST.robust_horizon
          inp.p = pvars{jj}.pconst;
        else
          inp.p = ST.branches{jj,msn}(:); % uncertain p of node msn
        end
        inp.sensdirs = sensdirs;
        
        outp = eval_mfcn_D(inp);
        
        data_loc.res.mfun = outp.mfcn;
        data_loc.res.mgrad = outp.grad(1:nsd);
        data_loc.mat.mhess = outp.H(1:nsd,1:nsd);
        data_loc.res.modgradx(:,msn) = data_loc.res.modgradx(:,msn) ...
          + data_loc.res.mgrad;
      end
      
      data{jj} = data_loc;
      data_loc = [];
      OPT.evaltime{jj} = toc;
    end
    
    OPT.nlp.cache.pvars = pvars;
    OPT.nlp.cache.dvars = dvars;
    OPT.nlp.cache.ovars = ovars;
    OPT.nlp.cache.data = data;
end

   
