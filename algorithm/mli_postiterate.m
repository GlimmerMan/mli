% -*-matlab-*-
function mli_postiterate

global OPT;

postit = OPT.currentitercount - 1;

if postit > 0
    fprintf('\nMaking %d postiterations...\n\n', postit);
    oldnorm = OPT.step.stepnorm;
    for ii = 1:postit
        mli_evaluate;
        mli_calchess;
        if OPT.use_condensing
            mli_condense_withblocks;
            mli_solveQP;
            mli_blowup;
        else
            mli_solveuncondensedQP;
            mli_blowup_nocond;
        end            
        mli_stepcalc;
        newnorm = OPT.step.stepnorm;
        delta = newnorm/oldnorm;        
        if (ii == 1)
            mli_printinfo(1, delta);
        else
            mli_printinfo(0, delta);
        end
        oldnorm = newnorm;
    end
    fprintf('\n\n');
end

% Data transfer

phasecomm_maximal(OPT.currentphase);

        
   
