function [outp] = eval_rdfcn_C(inp, nodetype)
    
    global SLV
    
    args.t = inp.t;    
    args.xd = inp.sd;
    args.qph = [inp.q;inp.p]; 

    args.fwdTCOrder = 0;
    args.nFwdTC = 0;
    args.fwdTC = [];
    args.nAdjTC = 1;
    args.adjTC = inp.mu;

    evl = SLV.opt.evl;
    
    switch nodetype
      case 's'
	args.rhs = solvind('evaluateFcn', evl, 'rdfcn_s', args);
	result = solvind('evaluateDenseDer', evl, 'rdfcn_s', args);
	
      case 'i'
	args.rhs = solvind('evaluateFcn', evl, 'rdfcn_i', args);
	result = solvind('evaluateDenseDer', evl, 'rdfcn_i', args);
	
      case 'e'
	args.rhs = solvind('evaluateFcn', evl, 'rdfcn_e', args);
	result = solvind('evaluateDenseDer', evl, 'rdfcn_e', args);
	
      otherwise
	error('eval_rdfcn_C: Invalid nodetype');
	
    end

    outp.rdfcn = result.rhs;
    outp.mu_rdjac = result.propAdjTC(2:end);
    outp.mu_rdjac =  outp.mu_rdjac(:);

    
    
