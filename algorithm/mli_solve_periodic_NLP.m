function mli_solve_periodic_NLP
%%Script that determines the best periodic steady state given a periodi
%%length specified in a controller_initialize file

global OPT SCEN SLV;

tic

ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd;
nq = OPT.dims.nq;

nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nlsqs = OPT.dims.lsqfcn_s;
nlsqi = OPT.dims.lsqfcn_i;
nlsqe = OPT.dims.lsqfcn_e;

if nrds > 0 || nrdi > 0 || nrde > 0
  error('Decoupled node constraints currently not implemented for Ipopt.')
end

if nlsqs > 0 || nlsqi > 0 || nlsqe > 0
  error('Least squares objective currently not implemented for Ipopt.')
end

OPT.nlp.data = init_nlp_data;

OPT.nlp.cache.pvars = [];
OPT.nlp.cache.dvars = [];
OPT.nlp.cache.ovars = [];
OPT.nlp.cache.data = init_nlp_data;

OPT.phase{4}.var = init_vars;
vars = OPT.phase{4}.var;
pvars = vars.primal;


% % SCEN.currentsample = 1; % only at init
% % in the case we initialize with ipopt
% if isfield(SCEN,'currentsample')==0
%   SCEN.currentsample = 1;
% end
% user_opt_x_from_scen;

OPT.x0=OPT.phase{4}.var.primal.sd(:,1);

% Set initial values of NLP variables
x0 = zeros(1,(nsd+nq)*ns+nsd);
x0(1:nsd*(ns+1)) = reshape(pvars.sd,[],1);
x0(nsd*(ns+1)+(1:nq*ns)) = reshape(pvars.q,[],1);

x0(1:nsd) = OPT.x0(:);

% Set lower bound values of NLP variables
options.lb = zeros(1,(nsd+nq)*ns+nsd);
options.lb(1:nsd*(ns+1)) = reshape(OPT.bounds.sdLoB,[],1);
options.lb(nsd*(ns+1)+(1:nq*ns)) = reshape(OPT.bounds.qLoB,[],1);

%set mayer term of initial node to zero
options.lb(nsd) = x0(nsd);

% Set upper bound values of NLP variables
options.ub = zeros(1,(nsd+nq)*ns+nsd);
options.ub(1:nsd*(ns+1)) = reshape(OPT.bounds.sdUpB,[],1);
options.ub(nsd*(ns+1)+(1:nq*ns)) = reshape(OPT.bounds.qUpB,[],1);

%set mayer term of initial node to zero
options.ub(nsd) = x0(nsd);
% 
% %for the powerkite_time: set winding number to zero
% options.ub(9)=0;
% options.lb(9)=0;

% Set lower and upper bound values of constraints (shooting and
% periodicity)
if isfield(OPT,'periodicity')
  options.cl = [zeros(1, nsd*ns),OPT.periodicity.lb'];
  options.cu = [zeros(1, nsd*ns),OPT.periodicity.ub'];
else
  options.cl = zeros(1, nsd*ns);
  options.cu = zeros(1, nsd*ns);
end


% Set the IPOPT options.
% options.ipopt.hessian_approximation = 'limited-memory';
options.ipopt.mu_strategy           = 'monotone';
%options.ipopt.mu_strategy           = 'adaptive';
options.ipopt.tol                   = 5*SCEN.discr.int_tol;
% options.ipopt.derivative_test       = 'second-order';
options.ipopt.fixed_variable_treatment = 'make_constraint';
options.ipopt.mu_init = 1e-5;
options.ipopt.bound_push=1e-8;  

% The callback functions.
funcs.objective         = eval_nlp_obj(vars);
funcs.gradient          = eval_nlp_obj_grad(vars);
funcs.constraints       = eval_nlp_cst(vars);
funcs.jacobian          = eval_nlp_cst_jac(vars);
funcs.jacobianstructure = eval_nlp_cst_jac_structure(vars);
funcs.hessian           = eval_nlp_hess(vars);
funcs.hessianstructure  = eval_nlp_hess_structure(vars);

% Run IPOPT.
try
  [ipopt_output, x, info] = evalc('ipopt(x0, funcs, options)');
catch
  error('Exception in Ipopt')
end
fid = fopen('ipopt.out', 'w');
fprintf(fid, ipopt_output);
fclose(fid);

OPT.feedbackdelta = x(nsd*(ns+1)+(1:nq))'-OPT.phase{4}.var.primal.q(:,1); % if not init


OPT.phase{4}.var.primal.sd = reshape(x(1:nsd*(ns+1)), nsd, ns+1);
OPT.phase{4}.var.primal.q = reshape(x(nsd*(ns+1)+(1:nq*ns)), nq, ns);

OPT.phase{4}.var.dual.lambda = reshape(-info.lambda(1:nsd*ns), nsd, ns);

if isfield(OPT,'periodicity')
  OPT.phase{4}.var.dual.periodicity=-info.lambda(nsd*ns+1:nsd*ns+OPT.dims.periodicity);
end

ybnd = info.zl - info.zu;
OPT.phase{4}.var.dual.mu_sd = reshape(ybnd(1:nsd*(ns+1)), nsd, ns+1);
OPT.phase{4}.var.dual.mu_q = reshape(ybnd(nsd*(ns+1)+(1:nq*ns)), nq, ns);

OPT.initime = toc;

end
