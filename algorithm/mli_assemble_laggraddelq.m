% -*-matlab-*-
function laggraddelq = mli_assemble_laggraddelq(dvars,data)

global OPT

ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd; 
nq = OPT.dims.nq; 

laggraddelq = zeros(nq,ns);


for msn=2:ns

data.res.laggraddelq(:,msn) = - dvars.lambda1(:,msn) ...
  - dvars.lambda2(:,msn) - dvars.mu_delq(:,msn);

end
