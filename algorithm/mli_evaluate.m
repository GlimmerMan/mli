% -*-matlab-*-
%
function mli_evaluate

global SCEN OPT ST

if isfield(OPT, 'jump_detected')
    if OPT.jump_detected
        OPT.currentphase = 4;
        OPT.phasehistory(OPT.currentsample) = 4;
    end
end

if SCEN.use_st == 0
    switch OPT.currentphase
        case 0
            switch OPT.algo.hessian
                case 'exact'
                    OPT.phase{4}.data = mli_evalPhaseD(OPT.phase{4}.var);
                case 'gn'
                    OPT.phase{4}.data = mli_evalPhaseD_gn(OPT.phase{4}.var);
            end
            mli_dataTransferD;
            
        case 1
            OPT.evaltime = 0;
            return;
            
        case 2
            switch OPT.algo.hessian
                case 'exact'
                    OPT.phase{2}.data = mli_evalPhaseB(OPT.phase{2}.var,...
                        OPT.phase{2}.ref);
                case 'gn'
                    OPT.phase{2}.data = mli_evalPhaseB_gn(OPT.phase{2}.var,...
                        OPT.phase{2}.ref);
            end
            mli_dataTransferB;
            
        case 3
            switch OPT.algo.hessian
                case 'exact'
                    OPT.phase{3}.data = mli_evalPhaseC(OPT.phase{3}.var);
                case 'gn'
                    OPT.phase{3}.data = mli_evalPhaseC_gn(OPT.phase{3}.var);
            end
            mli_dataTransferC;
            
        case {4, 9}
            switch OPT.algo.hessian
                case 'exact'
                    OPT.phase{4}.data = mli_evalPhaseD(OPT.phase{4}.var);
                case 'gn'
                    OPT.phase{4}.data = mli_evalPhaseD_gn(OPT.phase{4}.var);
            end
            mli_dataTransferD;
    end
else
    S = ST.S;
    switch OPT.currentphase
        case 0
            switch OPT.algo.hessian
                case 'exact'
                    for jj = 1:S
                        tic
                        OPT.phase{4}{jj}.data = mli_evalSTPhaseD(OPT.phase{4}{jj}.var,jj);
                        OPT.evaltime = toc;
                    end
                case 'gn' %!
                    for jj = 1:S
                        OPT.phase{4}{jj}.data = mli_evalSTPhaseD_gn(OPT.phase{4}{jj}.var,jj);
                    end
            end
            mli_dataTransferD;
            
        case 1
            OPT.evaltime = 0;
            return;
            
        case 2 % !
            switch OPT.algo.hessian
                case 'exact'
                    for jj = 1:S
                        OPT.phase{2}{jj}.data = mli_evalPhaseB(OPT.phase{2}{jj}.var,...
                            OPT.phase{2}{jj}.ref);
                    end
                case 'gn'
                    for jj = 1:S
                        OPT.phase{2}{jj}.data = mli_evalPhaseB_gn(OPT.phase{2}{jj}.var,...
                            OPT.phase{2}{jj}.ref);
                    end
            end
            mli_dataTransferB; % !
            
        case 3 % !
            switch OPT.algo.hessian
                case 'exact'
                    for jj = 1:S
                        OPT.phase{3}{jj}.data = mli_evalPhaseC(OPT.phase{3}{jj}.var);
                    end
                case 'gn'
                    for jj = 1:S
                        OPT.phase{3}{jj}.data = mli_evalPhaseC_gn(OPT.phase{3}{jj}.var);
                    end
            end
            mli_dataTransferC; % !
            
        case {4, 9}
            switch OPT.algo.hessian
                case 'exact'
                    for jj = 1:S
                        OPT.phase{4}{jj}.data = mli_evalSTPhaseD(OPT.phase{4}{jj}.var,jj);
                    end
                case 'gn'
                    for jj=1:S
                        OPT.phase{4}{jj}.data = mli_evalSTPhaseD_gn(OPT.phase{4}{jj}.var,jj);
                    end
            end
            mli_dataTransferD;
    end
end

