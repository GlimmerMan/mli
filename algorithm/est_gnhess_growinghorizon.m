% -*-matlab-*-
function est_gnhess_growinghorizon

global EST;

nsd = EST.dims.nsd;
np = EST.dims.np;
nq = EST.dims.nq;
nlsqs = EST.dims.lsqfcn_s;
nlsqi = EST.dims.lsqfcn_i; 
nlsqe = EST.dims.lsqfcn_e;

cursamp = EST.currentsample;


% We start by building the condensing blocks and the 
% condensed matching conditions. This allows to substitute
% (Delta s_1, ..., Delta s_N) by C*(Delta s_0, Delta p) + c_cond.

% In the growing horizon phase, we only make D iterations,
% thus we have to perform full condensing.

% CBlocks
if cursamp > 1 
    EST.cond.cblocksx{1} = EST.eval.mat.Gx{1};
    if ~EST.fixed_p
        EST.cond.cblocksp{1} = EST.eval.mat.Gp{1};
    end
    for ii = 2:cursamp-1
        EST.cond.cblocksx{ii} = EST.eval.mat.Gx{ii} * ...
            EST.cond.cblocksx{ii-1};
        if ~EST.fixed_p
            EST.cond.cblocksp{ii} = EST.eval.mat.Gx{ii} * ...
                EST.cond.cblocksp{ii-1} + EST.eval.mat.Gp{ii};
        end
    end
end

% condensed rhs
if cursamp > 1 
    EST.cond.c(:,1) = EST.eval.res.c(:,1);
    for ii = 2:cursamp-1
        EST.cond.c(:,ii) = EST.eval.mat.Gx{ii} * ...
            EST.cond.c(:,ii-1) + EST.eval.res.c(:,ii);
    end
end

% The condensed objective Jacobian is J*[I 0;C;0 I]

% condensed J
if ~EST.fixed_p
    EST.cond.Jcond(1:nlsqs,1:(nsd+np)) = ...
        diag(EST.V(:,1)) * [EST.eval.mat.Jmeasx{1}, EST.eval.mat.Jmeasp{1}];
    idx = nlsqs;
    for ii = 2:cursamp
        EST.cond.Jcond(idx+(1:nlsqi),1:(nsd+np)) = diag(EST.V(:,ii)) * ...
            [EST.eval.mat.Jmeasx{ii}*EST.cond.cblocksx{ii-1}, ...
             EST.eval.mat.Jmeasx{ii}*EST.cond.cblocksp{ii-1} ...
             + EST.eval.mat.Jmeasp{ii}];
        idx = idx + nlsqi;
    end
else
    EST.cond.Jcond(1:nlsqs,1:nsd) = diag(EST.V(:,1)) * [EST.eval.mat.Jmeasx{1}];
    idx = nlsqs;
    for ii = 2:cursamp
        EST.cond.Jcond(idx+(1:nlsqi),1:nsd) = diag(EST.V(:,ii)) * ...
            [EST.eval.mat.Jmeasx{ii}*EST.cond.cblocksx{ii-1}];
        idx = idx + nlsqi;
    end
end

% The condensing modification of F is J*[0;c_cond;0] and will be
% added to F as soon as the last measurement becomes known.

% condensing modification of F
if cursamp > 1
    EST.cond.Fmod(1:nlsqs,1) = zeros(nlsqs,1);
    idx = nlsqs;
    for ii=2:cursamp
        EST.cond.Fmod(idx+(1:nlsqi),1) = diag(EST.V(:,ii)) * ...
            EST.eval.mat.Jmeasx{ii} * EST.cond.c(:,ii-1);
        idx = idx + nlsqi;
    end
end

% Now everything is available for the condensed constrained linear
% least-squares problem. However, we want to formulate it as a
% QP. Therefore, we build the Hessian and the constraint Jacobian
% and residual. The gradient will be completed as soon as the last
% measurement becomes known. 

% condensed Hessian
if ~EST.disable_arrival
    EST.qp.H = EST.P'*EST.P + EST.cond.Jcond'*EST.cond.Jcond;
else
    EST.qp.H = EST.cond.Jcond'*EST.cond.Jcond;
end

% constraint Jacobian (transformed bounds)
if cursamp > 1
    idx = 0;
    for ii = 1:cursamp-1
        if ~EST.fixed_p
            EST.qp.A(idx+(1:nsd), 1:(nsd+np)) = [EST.cond.cblocksx{ii} ...
                                EST.cond.cblocksp{ii}];
        else
            EST.qp.A(idx+(1:nsd), 1:nsd) = EST.cond.cblocksx{ii};
        end
        idx = idx+nsd;
    end
end

% constraint rhs
if cursamp > 1
    idx = 0;
    for ii = 1:cursamp-1
        EST.qp.lbA(idx+(1:nsd)) = [EST.bounds.sdLoB(:,ii+1) - ...
                            EST.var.primal.sd(:,ii+1) - EST.cond.c(:,ii)];
        EST.qp.ubA(idx+(1:nsd)) = [EST.bounds.sdUpB(:,ii+1) - ...
                            EST.var.primal.sd(:,ii+1) - EST.cond.c(:,ii)]; 
        idx = idx+nsd;
    end
end

% bounds
if ~EST.fixed_p
    EST.qp.lb = [EST.bounds.sdLoB(:,1) - EST.var.primal.sd(:,1)
                 EST.bounds.pLoB - EST.var.primal.p];
    EST.qp.ub = [EST.bounds.sdUpB(:,1) - EST.var.primal.sd(:,1)
                 EST.bounds.pUpB - EST.var.primal.p];
else
    EST.qp.lb = EST.bounds.sdLoB(:,1) - EST.var.primal.sd(:,1);
    EST.qp.ub = EST.bounds.sdUpB(:,1) - EST.var.primal.sd(:,1);
end

%disp('End of gnhess_growinghorizon...');
%keyboard;



