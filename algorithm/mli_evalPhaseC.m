% -*-matlab-*-
function [data] = mli_evalPhaseC(vars)

global OPT;

tic


ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd; 
nq = OPT.dims.nq; 
np = OPT.dims.np;
nlsqs = OPT.dims.lsqfcn_s;
nlsqi = OPT.dims.lsqfcn_i; 
nlsqe = OPT.dims.lsqfcn_e;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nmfcn = OPT.dims.mfcn;
nlfcn = OPT.dims.lfcn;

pvars = vars.primal;
dvars = vars.dual;

%%% Start Node
msn = 1;

% dynamical equations
inp = [];
inp.thoriz = [OPT.discr.msgrid(msn) OPT.discr.msgrid(msn+1)];
inp.sd = pvars.sd(:,msn);
inp.q = pvars.q(:,msn);
inp.p = pvars.pconst;
inp.lambda = dvars.lambda(:,msn);

outp = eval_dyn_C(inp);

data.res.xd(:,msn) = outp.x;
data.res.c(:,msn) = outp.x - pvars.sd(:,msn+1);
data.res.lambdaGx(:,msn) = outp.lag(1:nsd);
data.res.lambdaGq(:,msn) = outp.lag(nsd+(1:nq));
if nlfcn > 0
    data.res.dlfcn(:,msn) = outp.dlfcn;
    data.res.lfcn(msn) = outp.lfcn;
end

% decoupled node constraint
if nrds > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn);
  inp.p = pvars.pconst;
  inp.mu = dvars.mu_rds;

  outp = eval_rdfcn_C(inp,'s');
  
  data.res.rds = outp.rdfcn;
  data.res.muJrdsx = outp.mu_rdjac(1:nsd);    
  data.res.muJrdsq = outp.mu_rdjac(nsd+(1:nq));
end

% least-squares objective
if nlsqs > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn);
  inp.p = pvars.pconst;
  outp = eval_lsqfcn_C(inp,'s');

  data.res.lsqfcn_s = outp.lsqfcn;
  data.res.lsqsgradx = outp.lsqgrad(1:nsd);    
  data.res.lsqsgradq = outp.lsqgrad(nsd+(1:nq));
end

% Gradient modification for Phase C
data.res.modgradx(:,msn) = - data.res.lambdaGx(:,msn) + ...
    OPT.eval.mat.Gx{msn}' * dvars.lambda(:,msn);
data.res.modgradq(:,msn) = - data.res.lambdaGq(:,msn) + ...
    OPT.eval.mat.Gq{msn}' * dvars.lambda(:,msn);

if nlsqs > 0
  data.res.modgradx(:,msn) = data.res.modgradx(:,msn) + data.res.lsqsgradx;
  data.res.modgradq(:,msn) = data.res.modgradq(:,msn) + data.res.lsqsgradq;
end

if nrds > 0
data.res.modgradx(:,msn) = data.res.modgradx(:,msn) - data.res.muJrdsx + ...
    OPT.eval.mat.Jrdsx' * dvars.mu_rds;
data.res.modgradq(:,msn) = data.res.modgradq(:,msn) - data.res.muJrdsq + ...
    OPT.eval.mat.Jrdsq' * dvars.mu_rds;
end

if nlfcn > 0
    data.res.modgradx(:,msn) = data.res.modgradx(:,msn) ...
        + data.res.dlfcn(1:nsd,msn);
    data.res.modgradq(:,msn) = data.res.modgradq(:,msn) ...
        + data.res.dlfcn(nsd+(1:nq),msn);
end

data.res.refgradx(:,msn) = data.res.modgradx(:,msn);    
data.res.refgradq(:,msn) = data.res.modgradq(:,msn);

% Lagrange gradient
data.res.laggradx(:,msn) = - data.res.lambdaGx(:,msn) - dvars.mu_sd(:,msn);
data.res.laggradq(:,msn) = - data.res.lambdaGq(:,msn) - dvars.mu_q(:,msn);

if nlsqs > 0
  data.res.laggradx(:,msn) = data.res.laggradx(:,msn) + data.res.lsqsgradx;
  data.res.laggradq(:,msn) = data.res.laggradq(:,msn) + data.res.lsqsgradq;
end

if nrds > 0
  data.res.laggradx(:,msn) = data.res.laggradx(:,msn) - data.res.muJrdsx;
  data.res.laggradq(:,msn) = data.res.laggradq(:,msn) - data.res.muJrdsq;
end

if nlfcn > 0
    data.res.laggradx(:,msn) = data.res.laggradx(:,msn) ...
        + data.res.dlfcn(1:nsd,msn);
    data.res.laggradq(:,msn) = data.res.laggradq(:,msn) ...
        + data.res.dlfcn(nsd+(1:nq),msn);
end

%%% Interior Nodes
for msn=2:ns
  
% dynamical equations
  inp = [];
  inp.thoriz = [OPT.discr.msgrid(msn) OPT.discr.msgrid(msn+1)];
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn);
  inp.p = pvars.pconst;
  inp.lambda = dvars.lambda(:,msn);
  
  outp = eval_dyn_C(inp);
  
  data.res.xd(:,msn) = outp.x;
  data.res.c(:,msn) = outp.x - pvars.sd(:,msn+1);
  data.res.lambdaGx(:,msn) = outp.lag(1:nsd);
  data.res.lambdaGq(:,msn) = outp.lag(nsd+(1:nq));
  
  if nlfcn > 0
    data.res.dlfcn(:,msn) = outp.dlfcn;
    data.res.lfcn(msn) = outp.lfcn;
  end

% least-squares objective
  if nlsqi > 0
    inp = [];
    inp.t = OPT.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.q = pvars.q(:,msn);
    inp.p = pvars.pconst;
    outp = eval_lsqfcn_C(inp,'i');
    
    data.res.lsqfcn_i(:,msn-1) = outp.lsqfcn;
    data.res.lsqigradx(:,msn-1) = outp.lsqgrad(1:nsd);    
    data.res.lsqigradq(:,msn-1) = outp.lsqgrad(nsd+(1:nq));
  end

% decoupled node constraint
  if nrdi > 0
    inp = [];
    inp.t = OPT.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.q = pvars.q(:,msn);
    inp.p = pvars.pconst;
    inp.mu = dvars.mu_rdi(:,msn-1);
    
    outp = eval_rdfcn_C(inp,'i');
    
    data.res.rdi(:,msn-1) = outp.rdfcn;
    data.res.muJrdix(:,msn-1) = outp.mu_rdjac(1:nsd);    
    data.res.muJrdiq(:,msn-1) = outp.mu_rdjac(nsd+(1:nq));
  end

% Gradient modification for Phase C
  data.res.modgradx(:,msn) = - data.res.lambdaGx(:,msn) + ...
      OPT.eval.mat.Gx{msn}' * dvars.lambda(:,msn);
  data.res.modgradq(:,msn) = - data.res.lambdaGq(:,msn) + ...
      OPT.eval.mat.Gq{msn}' * dvars.lambda(:,msn);
  
  if nlsqi > 0
    data.res.modgradx(:,msn) = data.res.modgradx(:,msn) + data.res.lsqigradx(:,msn-1);
    data.res.modgradq(:,msn) = data.res.modgradq(:,msn) + data.res.lsqigradq(:,msn-1);
  end

  if nrdi > 0
    data.res.modgradx(:,msn) = data.res.modgradx(:,msn) - data.res.muJrdix(:,msn-1) + ...
	OPT.eval.mat.Jrdix{msn-1}' * dvars.mu_rdi(:,msn-1);
    data.res.modgradq(:,msn) = data.res.modgradq(:,msn) - data.res.muJrdiq(:,msn-1) + ...
	OPT.eval.mat.Jrdiq{msn-1}' * dvars.mu_rdi(:,msn-1);
  end
  
  if nlfcn > 0
      data.res.modgradx(:,msn) = data.res.modgradx(:,msn) ...
          + data.res.dlfcn(1:nsd,msn);
      data.res.modgradq(:,msn) = data.res.modgradq(:,msn) ...
          + data.res.dlfcn(nsd+(1:nq),msn);
  end

  data.res.refgradx(:,msn) = data.res.modgradx(:,msn);    
  data.res.refgradq(:,msn) = data.res.modgradq(:,msn);

% Lagrange gradient
  data.res.laggradx(:,msn) = dvars.lambda(:,msn-1) - data.res.lambdaGx(:,msn) ...
      - dvars.mu_sd(:,msn);
  data.res.laggradq(:,msn) = - data.res.lambdaGq(:,msn) - dvars.mu_q(:,msn);
  
  if nlsqi > 0
    data.res.laggradx(:,msn) = data.res.laggradx(:,msn) + data.res.lsqigradx(:,msn-1);
    data.res.laggradq(:,msn) = data.res.laggradq(:,msn) + data.res.lsqigradq(:,msn-1);
  end

  if nrdi > 0
    data.res.laggradx(:,msn) = data.res.laggradx(:,msn) - data.res.muJrdix(:,msn-1);
    data.res.laggradq(:,msn) = data.res.laggradq(:,msn) - data.res.muJrdiq(:,msn-1);
  end
  
  if nlfcn > 0
    data.res.laggradx(:,msn) = data.res.laggradx(:,msn) ...
      + data.res.dlfcn(1:nsd,msn);
    data.res.laggradq(:,msn) = data.res.laggradq(:,msn) ...
      + data.res.dlfcn(nsd+(1:nq),msn);
  end

end

% End node (depends only on s, not on q)
msn = ns + 1;

% least-squares objective
if nlsqe > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn-1);
  inp.p = pvars.pconst;
  outp = eval_lsqfcn_C(inp,'e');
  
  data.res.lsqfcn_e = outp.lsqfcn;
  data.res.lsqegradx = outp.lsqgrad(1:nsd);    
end

% decoupled node constraint
if nrde > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn-1);
  inp.p = pvars.pconst;
  inp.mu = dvars.mu_rde;
  
  outp = eval_rdfcn_C(inp,'e');
  
  data.res.rde = outp.rdfcn;
  data.res.muJrdex = outp.mu_rdjac(1:nsd);    
end

% Mayer objective
if nmfcn > 0 && nlfcn <= 0
    inp = [];
    inp.t = OPT.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.q = pvars.q(:,msn-1);
    inp.p = pvars.pconst;

    outp = eval_mfcn_C(inp);
    
    data.res.mfun = outp.mfcn;
    data.res.mgrad = outp.grad(1:nsd);
end

% Gradient modification for Phase C
data.res.modgradx(:,msn) = zeros(nsd,1);

if nlsqe > 0
    data.res.modgradx(:,msn) = data.res.modgradx(:,msn) + data.res.lsqegradx;
end

if nmfcn > 0 && nlfcn <= 0
    data.res.modgradx(:,msn) = data.res.modgradx(:,msn) + data.res.mgrad;
end

if nrde > 0
    data.res.modgradx(:,msn) = data.res.modgradx(:,msn) - data.res.muJrdex + ...
        OPT.eval.mat.Jrdex' * dvars.mu_rde;
end

data.res.refgradx(:,msn) = data.res.modgradx(:,msn);

% Lagrange gradient
data.res.laggradx(:,msn) = dvars.lambda(:,msn-1) - dvars.mu_sd(:,msn);

if nlsqe > 0
    data.res.laggradx(:,msn) = data.res.laggradx(:,msn) + data.res.lsqegradx;
end

if nmfcn > 0 && nlfcn <= 0
    data.res.laggradx(:,msn) = data.res.laggradx(:,msn) + data.res.mgrad;
end

if nrde > 0
    data.res.laggradx(:,msn) = data.res.laggradx(:,msn) - data.res.muJrdex;
end

OPT.evaltime = toc;

    
