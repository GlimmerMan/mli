% -*-matlab-*-
function mli_solveSTQP

global OPT SLV ST

OPT.condtime = 0;

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;
nz = nsd * (ns+1) + nq * ns;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nrd = nrds + (ns-1) * nrdi + nrde;

CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;

tqp = tic;

% m = ST.nup(2);
S = ST.S;
% n_optimizationparameters_notuncertain =  ST.nup(2) - nq;

phase = OPT.currentphase;
sample  = OPT.currentsample;
pvars = cell(S,1);
switch phase

    case {0,4}
        for jj = 1:S
            pvars{jj} = OPT.phase{4}{jj}.var.primal;
        end
        
    case 1
        for jj = 1:S
            pvars{jj} = OPT.phase{1}{jj}.ref.primal;
        end
        
    case 2
        for jj = 1:S
            pvars{jj} = OPT.phase{2}{jj}.var.primal;
        end
        
    case 3
        for jj = 1:S
            pvars{jj} = OPT.phase{3}{jj}.var.primal;
        end
        
end

if (phase==0) || (phase==4)
    for jj = 1:S
      %  Hessematrix
      OPT.qp{jj}.nreg = 0;
      OPT.qp{jj}.maxreg = 0;
      if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
        for ii=1:ns
          stride = nsd+nq;
          idx = (ii-1)*stride;
          OPT.qp{jj}.H(idx+(1:stride),idx+(1:stride)) = lbfgshess_matrix(OPT.hess{jj}.lbfgshess{ii});
        end
        OPT.qp{jj}.H(ns*(nsd+nq)+(1:nsd),ns*(nsd+nq)+(1:nsd)) = lbfgshess_matrix(OPT.hess{jj}.lbfgshess{ns+1});
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
          xoff = ns*(nsd+nq)+nsd;
          for ii=1:ns
            idx = xoff + (ii-1)*nq;
            OPT.qp{jj}.H(idx+(1:nq),idx+(1:nq)) = lbfgshess_matrix(OPT.hess{jj}.lbfgsCMRhess{ii});
          end
        end
      else
        % regularize Hessian block 
        for ii=1:ns       
            stride = nsd+nq;
            idx = (ii-1)*stride;
            
            H_temp = [OPT.hess{jj}.ssblock{ii} OPT.hess{jj}.sqblock{ii}
                     OPT.hess{jj}.sqblock{ii}' OPT.hess{jj}.qqblock{ii}];
            [V,E] = eig(0.5*(H_temp+H_temp'));
            D = diag(E);
            ep = 1e-6*max(abs(D));
            DD = max(D,ep);     
            OPT.qp{jj}.H(idx+(1:stride),idx+(1:stride)) = V*diag(DD)/V;
            OPT.qp{jj}.nreg = OPT.qp{jj}.nreg + sum(diag(E) < ep);
            maxreg = max(DD-D);
            OPT.qp{jj}.maxreg = max(maxreg,OPT.qp{jj}.maxreg);
%           OPT.qp{jj}.H(idx+(1:stride),idx+(1:stride)) = ...
%                 [OPT.hess{jj}.ssblock{ii} OPT.hess{jj}.sqblock{ii}
%                 OPT.hess{jj}.sqblock{ii}' OPT.hess{jj}.qqblock{ii}];
        end
            H_temp = OPT.hess{jj}.ssblock{ns+1};
            [V,E] = eig(0.5*(H_temp+H_temp'));
            D = diag(E);
            ep = 0e-6*max(abs(D));
            DD = max(D,ep);    
            OPT.qp{jj}.H(ns*(nsd+nq)+(1:nsd),ns*(nsd+nq)+(1:nsd)) = V*diag(DD)/V;
            OPT.qp{jj}.nreg = OPT.qp{jj}.nreg + sum(diag(E) < ep);
            maxreg = max(DD-D);
            OPT.qp{jj}.maxreg = max(maxreg,OPT.qp{jj}.maxreg);
%         OPT.qp{jj}.H(ns*(nsd+nq)+(1:nsd),ns*(nsd+nq)+(1:nsd)) = OPT.hess{jj}.ssblock{ns+1};
%         OPT.qp{jj}.H = eye(size(OPT.qp{jj}.H));
      end
      
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
        if isfield(OPT, 'controlmove_R')
          R = OPT.controlmove_R;
        else
          R = zeros(nq,nq);
        end
        xoff = ns*(nsd+nq)+nsd;
        for ii=1:ns-1
          idx = xoff + (ii-1)*nq;
          OPT.qp{jj}.H(idx+(1:nq),idx+(1:nq)) = 4 * R;
        end
        idx = xoff + (ns-1)*nq;
        OPT.qp{jj}.H(idx+(1:nq),idx+(1:nq)) = 4 * OPT.controlmove_R0;
      end
      
        % Constraint matrix (matching and ipc)
        for ii=1:ns
            xidx = (ii-1)*nsd;
            yidx = (ii-1)*(nsd+nq);
            OPT.qp{jj}.A(xidx+(1:nsd),yidx+(1:nsd+nq+nsd)) = ...
                [OPT.eval{jj}.mat.Gx{ii}, OPT.eval{jj}.mat.Gq{ii}, -eye(nsd)];
        end
        if nrd > 0
            xidx = ns*nsd;
            yidx = 0;
            if nrds > 0
                OPT.qp{jj}.A(xidx+(1:nrds),yidx+(1:nsd+nq)) = ...
                    [OPT.eval{jj}.mat.Jrdsx, OPT.eval{jj}.mat.Jrdsq];
                xidx = xidx+nrds;
            end
            if nrdi > 0
                for ii = 1:ns-1
                    yidx = yidx + (nsd+nq);
                    OPT.qp{jj}.A(xidx+(1:nrdi),yidx+(1:nsd+nq)) = ...
                        [OPT.eval{jj}.mat.Jrdix{ii}, OPT.eval{jj}.mat.Jrdiq{ii}];
                    xidx = xidx + nrdi;
                end
            end
            if nrde > 0
                yidx = ns*(nsd+nq);
                OPT.qp{jj}.A(xidx+(1:nrde),yidx+(1:nsd)) = OPT.eval{jj}.mat.Jrdex;
            end
        end
        
        if CMR
          xoffs = ns*nsd+nrds+nrdi+nrde;
          if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
            xoffs2 = xoffs + (ns-1)*nq+nq;
            yoff = ns*(nsd+nq)+nsd;
            for ii = 1:ns-1
              yidx = (ii-1)*stride+((nsd+1):(nsd+nq));
              OPT.qp{jj}.A(xoffs+(ii-1)*nq+(1:nq),yidx) = eye(nq);
              OPT.qp{jj}.A(xoffs+(ii-1)*nq+(1:nq),yidx+stride) = -eye(nq);
              OPT.qp{jj}.A(xoffs+(ii-1)*nq+(1:nq),yoff+(ii-1)*nq+(1:nq)) = eye(nq);
              OPT.qp{jj}.A(xoffs2+(ii-1)*nq+(1:nq),yidx) = -eye(nq);
              OPT.qp{jj}.A(xoffs2+(ii-1)*nq+(1:nq),yidx+stride) = eye(nq);
              OPT.qp{jj}.A(xoffs2+(ii-1)*nq+(1:nq),yoff+(ii-1)*nq+(1:nq)) = eye(nq);
            end
            OPT.qp{jj}.A(xoffs+(ns-1)*nq+(1:nq),(nsd+1):(nsd+nq)) = -eye(nq);
            OPT.qp{jj}.A(xoffs+(ns-1)*nq+(1:nq),yoff+(ns-1)*nq+(1:nq)) = eye(nq);
            OPT.qp{jj}.A(xoffs2+(ns-1)*nq+(1:nq),(nsd+1):(nsd+nq)) = eye(nq);
            OPT.qp{jj}.A(xoffs2+(ns-1)*nq+(1:nq),yoff+(ns-1)*nq+(1:nq)) = eye(nq);
          else
            for ii = 1:ns-1
              yidx = (ii-1)*stride+((nsd+1):(nsd+nq));
              OPT.qp{jj}.A(xoffs+(ii-1)*nq+(1:nq),yidx) = eye(nq);
              OPT.qp{jj}.A(xoffs+(ii-1)*nq+(1:nq),yidx+stride) = -eye(nq);
            end
            OPT.qp{jj}.A(xoffs+(ns-1)*nq+(1:nq),(nsd+1):(nsd+nq)) = -eye(nq);
          end
        end
    end
end 

if (phase ~= 1)
    bigbnd = 1e12;
    for jj = 1:S
        % Gradient
        stride = nsd+nq;
        
        for ii=1:ns
            idx = (ii-1)*stride;
            OPT.qp{jj}.g(idx+(1:stride),1) = ...
                [OPT.eval{jj}.res.modgradx(:,ii)
                OPT.eval{jj}.res.modgradq(:,ii)];
        end
        OPT.qp{jj}.g(ns*(nsd+nq)+(1:nsd),1) = OPT.eval{jj}.res.modgradx(:,ns+1);
        
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
          xoff = ns*(nsd+nq)+nsd;
          for ii=1:ns-1
            idx = xoff + (ii-1)*nq;
            OPT.qp{jj}.g(idx+(1:nq),1) = OPT.eval{jj}.res.modgraddelq(:,ii);
          end
          idx = xoff + (ns-1)*nq;
          OPT.qp{jj}.g(idx+(1:nq),1) = OPT.eval{jj}.res.modgraddelq(:,ns);
        end
        
        % RHS (matching and ipc)
        for ii=1:ns
            OPT.qp{jj}.ineqlb((ii-1)*nsd+(1:nsd),1) = -OPT.eval{jj}.res.c(:,ii);
            OPT.qp{jj}.inequb((ii-1)*nsd+(1:nsd),1) = -OPT.eval{jj}.res.c(:,ii);
        end
        if nrd > 0
            xoffs = ns*nsd;
            if nrds > 0
                OPT.qp{jj}.ineqlb(xoffs+(1:nrds)) = -OPT.eval{jj}.res.rds;
                OPT.qp{jj}.inequb(xoffs+(1:nrds)) = bigbnd*ones(size(OPT.eval{jj}.res.rds));
                xoffs = xoffs + nrds;
            end
            if nrdi > 0
                for kk=1:ns-1
                    OPT.qp{jj}.ineqlb(xoffs+(1:nrdi)) = -OPT.eval{jj}.res.rdi(:,kk);
                    OPT.qp{jj}.inequb(xoffs+(1:nrdi)) = bigbnd*ones(size(OPT.eval{jj}.res.rdi(:,kk)));
                    xoffs = xoffs + nrdi;
                end
            end
            if nrde > 0
                OPT.qp{jj}.ineqlb(xoffs+(1:nrde)) = -OPT.eval{jj}.res.rde;
                OPT.qp{jj}.inequb(xoffs+(1:nrde)) = bigbnd*ones(size(OPT.eval{jj}.res.rde));
            end
        end
    if CMR
      xoffs = ns*nsd+nrds+nrdi+nrde;
      alpha = OPT.controlmove_alpha;
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        xoffs2 = xoffs + (ns-1)*nq+nq;
        for ii = 1:ns-1
          OPT.qp{jj}.ineqlb(xoffs+(ii-1)*nq+(1:nq)) = pvars{jj}.q(:,ii+1) - pvars{jj}.q(:,ii);
          OPT.qp{jj}.inequb(xoffs+(ii-1)*nq+(1:nq)) = bigbnd*ones(nq,1);
          OPT.qp{jj}.ineqlb(xoffs2+(ii-1)*nq+(1:nq)) = pvars{jj}.q(:,ii) - pvars{jj}.q(:,ii+1);
          OPT.qp{jj}.inequb(xoffs2+(ii-1)*nq+(1:nq)) = bigbnd*ones(nq,1);
        end
        OPT.qp{jj}.ineqlb(xoffs+(ns-1)*nq+(1:nq)) = pvars{jj}.q(:,1) - OPT.controlmove_q0;
        OPT.qp{jj}.inequb(xoffs+(ns-1)*nq+(1:nq)) = bigbnd*ones(nq,1);
        OPT.qp{jj}.ineqlb(xoffs2+(ns-1)*nq+(1:nq)) = OPT.controlmove_q0 - pvars{jj}.q(:,1);
        OPT.qp{jj}.inequb(xoffs2+(ns-1)*nq+(1:nq)) = bigbnd*ones(nq,1);
      else
        for ii = 1:ns-1
          OPT.qp{jj}.ineqlb(xoffs+(ii-1)*nq+(1:nq)) = -alpha;
          OPT.qp{jj}.inequb(xoffs+(ii-1)*nq+(1:nq)) = alpha;
        end
        OPT.qp{jj}.ineqlb(xoffs+(ns-1)*nq+(1:nq)) = -OPT.controlmove_alpha0;
        OPT.qp{jj}.inequb(xoffs+(ns-1)*nq+(1:nq)) = OPT.controlmove_alpha0;
      end
    end
        
        % If OPT.dims.mfcn > 0 we need this matrix to cut off the artificial
        % state
        % SLV.opt.lagObjModSd
        
        
        % Bounds 
        for ii=1:ns
            idx = (ii-1)*stride+nsd;
            OPT.qp{jj}.lb(idx+(1:stride),1) = ...
                [OPT.bounds.qLoB(:,ii) - pvars{jj}.q(:,ii)
                SLV.opt.lagObjModSd*OPT.bounds.sdLoB(:,ii+1) - pvars{jj}.sd(:,ii+1)];
            
            OPT.qp{jj}.ub(idx+(1:stride),1) = ...
                [OPT.bounds.qUpB(:,ii) - pvars{jj}.q(:,ii)
                SLV.opt.lagObjModSd*OPT.bounds.sdUpB(:,ii+1) - pvars{jj}.sd(:,ii+1)];
        end
        
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
          xoff = ns*(nsd+nq)+nsd;
          for ii=1:ns-1
            idx = xoff + (ii-1)*nq;
            OPT.qp{jj}.lb(idx+(1:nq)) = -bigbnd*ones(nq,1);
            OPT.qp{jj}.ub(idx+(1:nq)) = OPT.controlmove_alpha;
          end
          idx = xoff + (ns-1)*nq;
          OPT.qp{jj}.lb(idx+(1:nq)) = -bigbnd*ones(nq,1);
          OPT.qp{jj}.ub(idx+(1:nq)) = OPT.controlmove_alpha0;
        end
        
        OPT.deltas0{jj} = SLV.opt.lagObjModSd*OPT.x0(:) - pvars{jj}.sd(:,1);
        OPT.qp{jj}.lb(1:nsd) = OPT.deltas0{jj};
        OPT.qp{jj}.ub(1:nsd) = OPT.deltas0{jj};
    end
    
    % relax bounds for controls coupled by tree structure
    nd = ST.robust_horizon;
    epsbnd = 1e-6;
    for jj = 2:S
        for kk = 1:nd
            if  ST.Couple(jj-1,kk) == jj
                rows = (kk*nsd + (kk-1)*nq)+1:(kk*nsd + kk*nq);
                OPT.qp{jj}.lb(rows,1) = OPT.qp{jj}.lb(rows,1)-epsbnd;
                OPT.qp{jj}.ub(rows,1) = OPT.qp{jj}.ub(rows,1)+epsbnd;
            end
        end
    end
end

ST.Newton.G = ones(size(ST.lambda))';
it_N = 0;
sample = OPT.currentsample;
% switch sample
%     case 1
%         itmax_N = 10; %!
%     case 2
%         itmax_N = 1e6; %!
%     otherwise
%         itmax_N = ST.Newton.itmax;
% end


itmax_N = ST.Newton.itmax;

OPT.qpoptions = qpOASES_options('reliable', 'maxIter', 10000, 'maxCpuTime', 60);
OPT.scenqpcumtime = cell(1,S);
for jj = 1:S
  OPT.scenqpcumtime{jj} = 0;
end

%%% STQP solution

% tic

while norm(ST.Newton.G) > ST.Newton.eps && it_N < itmax_N
    
    ST.Newton.G = zeros(size(ST.lambda));
    z = cell(S,1);
    
    for jj = 1:S  
        % solve scenario QP 
        mli_solvescenQP_nocond(jj,ST.lambda,'m');
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
          z{jj} = [OPT.qp{jj}.solstep; OPT.qp{jj}.soldelq];
        else
          z{jj} = OPT.qp{jj}.solstep;
        end
        
        % update Newton gradient
        ST.Newton.G = ST.Newton.G + (ST.C{jj}-ST.E{jj})*z{jj};
%         ST.Newton.G = ST.Newton.G + (ST.C{jj}-ST.E{jj})*(OPT.qp{jj}.solstep + OPT.qp{jj}.z0);
        
        % compute jjth component of Newton matrix
        DS = sparse(ST.C{jj}-ST.E{jj});
        DD = full(DS');
        nonzerocols = sum(DD.^2,1) > 0;
        nnz = sum(nonzerocols);
        Ob = ones(size(OPT.qp{jj}.lb(:))) * zeros(1,nnz);
        OA = ones(size(OPT.qp{jj}.ineqlb(:))) * zeros(1,nnz);
        
        [KtildeNNZ,~,~] = qpOASES_sequence( 'e', ...
            OPT.qp{jj}.qphandle,DD(:,nonzerocols),Ob,Ob,OA,OA);
        
        Ktilde = sparse(size(DD,1),size(DD,2));
        Ktilde(:,nonzerocols) = KtildeNNZ;
        ST.Newton.Mj{jj} = DS*Ktilde;
    end
    
    ST.obj = ST.objj*ST.weights';
    
    if norm(ST.Newton.G) > ST.Newton.eps
        
        % Newton matrix 
        M = zeros(ST.na,ST.na);
        qp_nreg = 0;
        qp_maxreg = 0;
        for jj = 1:ST.S
            M = M + ST.Newton.Mj{jj};
            qp_nreg = qp_nreg + OPT.qp{jj}.nreg;
            qp_maxreg = max(qp_maxreg,OPT.qp{jj}.maxreg);
        end

        % Compute Newton step direction
        [ST.deltalam, Newton_reg] = mli_solveSTNewton( M, ST.Newton.G, 1);
        reg = [Newton_reg,qp_nreg,qp_maxreg];
        
        if ~all(isfinite(ST.deltalam))
            keyboard
        end
        
        % Newton line search
        alpha = mli_stepSTNewton;  %linesearch
        
        ST.lambda = ST.lambda + alpha*ST.deltalam;
        it_N = it_N+1;
        ST.history{sample}.alpha{it_N} = alpha;
        ST.history{sample}.lambda{it_N} = ST.lambda;
        ST.history{sample}.deltalam{it_N} = ST.deltalam;
        
    else
        alpha = 0;
        reg = [0,0,0];
        ST.deltalam = 0;
    end
    
    if 1 % print every nonsmooth Newton iteration
      fprintf('It%3d, Ph%1d, NIt%3d, |lam|: %9.2e, |dlam|: %9.2e, |G|: %9.2e, al: %9.2e, %d, %d\n',sample, phase, it_N, norm(ST.lambda), norm(ST.deltalam),norm(ST.Newton.G),alpha,reg(1),reg(2))
    end
    
    ST.it_N = it_N;
    ST.reg = reg;
end

% ST.Newton.clock = toc;
    
% Feedback update
J = ceil(ST.S/2);
OPT.feedbackdelta = OPT.qp{J}.solstep(nsd+(1:nq));
% OPT.feedbackdelta{jj} = x{jj}(nsd+(1:nq));

% Synchronization step 
for jj = J+1:S
    P1 = eye(size(OPT.qp{jj}.solstep,1));
    P2 = P1;
    nonzero = sum(ST.C{jj-1}(:,1:nz).^2,1) > 0;
    P1(~nonzero,:) = 0;
    P2(nonzero,:) = 0;
    OPT.qp{jj}.solstep = P1*OPT.qp{jj-1}.solstep + P2*OPT.qp{jj}.solstep;
end
for jj = J-1:-1:1
    P1 = eye(size(OPT.qp{jj}.solstep,1));
    P2 = P1;
    nonzero = sum(ST.C{jj}(:,1:nz).^2,1) > 0;
    P1(~nonzero,:) = 0;
    P2(nonzero,:) = 0;
    OPT.qp{jj}.solstep = P1*OPT.qp{jj+1}.solstep + P2*OPT.qp{jj}.solstep;
end

OPT.qptime = toc(tqp);

