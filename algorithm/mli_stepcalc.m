% -*-matlab-*-
% trivial implementation: do full step

function mli_stepcalc

global SCEN OPT ST

phase = OPT.currentphase;

nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
ns = OPT.dims.nshoot;

tic

if SCEN.use_st == 0
switch (phase)
    
  case 1 
    OPT.steptime = 0;
    return;
    
  case {0, 4}
    if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
      OPT.phase{4}.varold = OPT.phase{4}.var;
    end
    OPT.phase{4}.var.primal.sd = OPT.phase{4}.var.primal.sd + OPT.step.primal.sd;
    OPT.phase{4}.var.primal.q = OPT.phase{4}.var.primal.q + OPT.step.primal.q;
    OPT.phase{4}.var.dual.lambda = OPT.step.dual.lambda;
    OPT.phase{4}.var.dual.mu_sd = OPT.step.dual.mu_sd;
    OPT.phase{4}.var.dual.mu_q = OPT.step.dual.mu_q;
    if nrds > 0
        OPT.phase{4}.var.dual.mu_rds = OPT.step.dual.mu_rds;
    end
    if nrdi > 0
        OPT.phase{4}.var.dual.mu_rdi = OPT.step.dual.mu_rdi;
    end
    if nrde > 0
        OPT.phase{4}.var.dual.mu_rde = OPT.step.dual.mu_rde;
    end

    if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        for ii = 1:ns-1+1
          OPT.phase{4}.var.primal.delq(:,ii) = OPT.step.primal.delq(:,ii);
        end
        OPT.phase{4}.var.dual.lambda1 = OPT.step.dual.lambda1;
        OPT.phase{4}.var.dual.lambda2 = OPT.step.dual.lambda2;
        OPT.phase{4}.var.dual.mu_delq = OPT.step.dual.mu_delq;
      else
        for ii = 1:ns-1
          OPT.phase{4}.var.primal.delq(:,ii) = OPT.phase{4}.var.primal.q(:,ii+1) - OPT.phase{4}.var.primal.q(:,ii);
        end
        OPT.phase{4}.var.primal.delq(:,ns) = OPT.phase{4}.var.primal.q(:,1) - OPT.controlmove_q0;
      end
    end


    
  case 2
    if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
      OPT.phase{2}.varold = OPT.phase{2}.var;
    end
    OPT.phase{2}.var.primal.sd = OPT.phase{2}.var.primal.sd + OPT.step.primal.sd;
    OPT.phase{2}.var.primal.q = OPT.phase{2}.var.primal.q + OPT.step.primal.q;
    OPT.phase{2}.var.dual.lambda = OPT.step.dual.lambda;
    OPT.phase{2}.var.dual.mu_sd = OPT.step.dual.mu_sd;
    OPT.phase{2}.var.dual.mu_q = OPT.step.dual.mu_q;
    if nrds > 0
        OPT.phase{2}.var.dual.mu_rds = OPT.step.dual.mu_rds;
    end
    if nrdi > 0
        OPT.phase{2}.var.dual.mu_rdi = OPT.step.dual.mu_rdi;
    end
    if nrde > 0
        OPT.phase{2}.var.dual.mu_rde = OPT.step.dual.mu_rde;
    end
    if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        for ii = 1:ns-1+1
          OPT.phase{2}.var.primal.delq(:,ii) = OPT.step.primal.delq(:,ii);
        end
        OPT.phase{2}.var.dual.lambda1 = OPT.step.dual.lambda1;
        OPT.phase{2}.var.dual.lambda2 = OPT.step.dual.lambda2;
        OPT.phase{2}.var.dual.mu_delq = OPT.step.dual.mu_delq;
      else
        for ii = 1:ns-1
          OPT.phase{2}.var.primal.delq(:,ii) = OPT.phase{2}.var.primal.q(:,ii+1) - OPT.phase{2}.var.primal.q(:,ii);
        end
        OPT.phase{2}.var.primal.delq(:,ns) = OPT.phase{2}.var.primal.q(:,1) - OPT.controlmove_q0;
      end
    end
    
  case 3
    if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
      OPT.phase{3}.varold = OPT.phase{3}.var;
    end
    OPT.phase{3}.var.primal.sd = OPT.phase{3}.var.primal.sd + OPT.step.primal.sd;
    OPT.phase{3}.var.primal.q = OPT.phase{3}.var.primal.q + OPT.step.primal.q;
    OPT.phase{3}.var.dual.lambda = OPT.step.dual.lambda;
    OPT.phase{3}.var.dual.mu_sd = OPT.step.dual.mu_sd;
    OPT.phase{3}.var.dual.mu_q = OPT.step.dual.mu_q;
    if nrds > 0
        OPT.phase{3}.var.dual.mu_rds = OPT.step.dual.mu_rds;
    end
    if nrdi > 0
        OPT.phase{3}.var.dual.mu_rdi = OPT.step.dual.mu_rdi;
    end
    if nrde > 0
        OPT.phase{3}.var.dual.mu_rde = OPT.step.dual.mu_rde;
    end
    if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        for ii = 1:ns-1+1
          OPT.phase{3}.var.primal.delq(:,ii) = OPT.step.primal.delq(:,ii);
        end
        OPT.phase{3}.var.dual.lambda1 = OPT.step.dual.lambda1;
        OPT.phase{3}.var.dual.lambda2 = OPT.step.dual.lambda2;
        OPT.phase{3}.var.dual.mu_delq = OPT.step.dual.mu_delq;
      else
        for ii = 1:ns-1
          OPT.phase{3}.var.primal.delq(:,ii) = OPT.phase{3}.var.primal.q(:,ii+1) - OPT.phase{3}.var.primal.q(:,ii);
        end
        OPT.phase{3}.var.primal.delq(:,ns) = OPT.phase{3}.var.primal.q(:,1) - OPT.controlmove_q0;
      end
    end
    
end

else % ST
    
    S = ST.S;
    switch (phase)
        
        case 1
            OPT.steptime = 0;
            return;
            
        case {0, 4}
            for jj = 1:S
              if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
                OPT.phase{4}{jj}.varold = OPT.phase{4}{jj}.var;
              end
                OPT.phase{4}{jj}.var.primal.sd = OPT.phase{4}{jj}.var.primal.sd + OPT.step{jj}.primal.sd;
                OPT.phase{4}{jj}.var.primal.q = OPT.phase{4}{jj}.var.primal.q + OPT.step{jj}.primal.q;
                OPT.phase{4}{jj}.var.dual.lambda = OPT.step{jj}.dual.lambda;
                OPT.phase{4}{jj}.var.dual.mu_sd = OPT.step{jj}.dual.mu_sd;
                OPT.phase{4}{jj}.var.dual.mu_q = OPT.step{jj}.dual.mu_q;
                if nrds > 0
                    OPT.phase{4}{jj}.var.dual.mu_rds = OPT.step{jj}.dual.mu_rds;
                end
                if nrdi > 0
                    OPT.phase{4}{jj}.var.dual.mu_rdi = OPT.step{jj}.dual.mu_rdi;
                end
                if nrde > 0
                    OPT.phase{4}{jj}.var.dual.mu_rde = OPT.step{jj}.dual.mu_rde;
                end
            end
            if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
              if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
                for jj = 1:S
                  for ii = 1:ns-1+1
                    OPT.phase{4}{jj}.var.primal.delq(:,ii) = OPT.step{jj}.primal.delq(:,ii);
                  end
                  OPT.phase{4}{jj}.var.dual.lambda1 = OPT.step{jj}.dual.lambda1;
                  OPT.phase{4}{jj}.var.dual.lambda2 = OPT.step{jj}.dual.lambda2;
                  OPT.phase{4}{jj}.var.dual.mu_delq = OPT.step{jj}.dual.mu_delq;
                end
              else
                for jj = 1:S
                  for ii = 1:ns-1
                    OPT.phase{4}{jj}.var.primal.delq(:,ii) = OPT.phase{4}{jj}.var.primal.q(:,ii+1) - OPT.phase{4}{jj}.var.primal.q(:,ii);
                  end
                  OPT.phase{4}{jj}.var.primal.delq(:,ns) = OPT.phase{4}{jj}.var.primal.q(:,1) - OPT.controlmove_q0;
                end
              end
            end
            
        case 2
            for jj = 1:S
              if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
                OPT.phase{2}{jj}.varold = OPT.phase{2}{jj}.var;
              end
                OPT.phase{2}{jj}.var.primal.sd = OPT.phase{2}{jj}.var.primal.sd + OPT.step{jj}.primal.sd;
                OPT.phase{2}{jj}.var.primal.q = OPT.phase{2}{jj}.var.primal.q + OPT.step{jj}.primal.q;
                OPT.phase{2}{jj}.var.dual.lambda = OPT.step{jj}.dual.lambda;
                OPT.phase{2}{jj}.var.dual.mu_sd = OPT.step{jj}.dual.mu_sd;
                OPT.phase{2}{jj}.var.dual.mu_q = OPT.step{jj}.dual.mu_q;
                if nrds > 0
                    OPT.phase{2}{jj}.var.dual.mu_rds = OPT.step{jj}.dual.mu_rds;
                end
                if nrdi > 0
                    OPT.phase{2}{jj}.var.dual.mu_rdi = OPT.step{jj}.dual.mu_rdi;
                end
                if nrde > 0
                    OPT.phase{2}{jj}.var.dual.mu_rde = OPT.step{jj}.dual.mu_rde;
                end
            end
            if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
              if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
                for jj = 1:S
                  for ii = 1:ns-1+1
                    OPT.phase{2}{jj}.var.primal.delq(:,ii) = OPT.step{jj}.primal.delq(:,ii);
                  end
                  OPT.phase{2}{jj}.var.dual.lambda1 = OPT.step{jj}.dual.lambda1;
                  OPT.phase{2}{jj}.var.dual.lambda2 = OPT.step{jj}.dual.lambda2;
                  OPT.phase{2}{jj}.var.dual.mu_delq = OPT.step{jj}.dual.mu_delq;
                end
              else
                for jj = 1:S
                  for ii = 1:ns-1
                    OPT.phase{2}{jj}.var.primal.delq(:,ii) = OPT.phase{2}{jj}.var.primal.q(:,ii+1) - OPT.phase{2}{jj}.var.primal.q(:,ii);
                  end
                  OPT.phase{2}{jj}.var.primal.delq(:,ns) = OPT.phase{2}{jj}.var.primal.q(:,1) - OPT.controlmove_q0;
                end
              end
            end
            
        case 3
            for jj = 1:S
              if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
                OPT.phase{3}{jj}.varold = OPT.phase{3}{jj}.var;
              end
                OPT.phase{3}{jj}.var.primal.sd = OPT.phase{3}{jj}.var.primal.sd + OPT.step{jj}.primal.sd;
                OPT.phase{3}{jj}.var.primal.q = OPT.phase{3}{jj}.var.primal.q + OPT.step{jj}.primal.q;
                OPT.phase{3}{jj}.var.dual.lambda = OPT.step{jj}.dual.lambda;
                OPT.phase{3}{jj}.var.dual.mu_sd = OPT.step{jj}.dual.mu_sd;
                OPT.phase{3}{jj}.var.dual.mu_q = OPT.step{jj}.dual.mu_q;
                if nrds > 0
                    OPT.phase{3}{jj}.var.dual.mu_rds = OPT.step{jj}.dual.mu_rds;
                end
                if nrdi > 0
                    OPT.phase{3}{jj}.var.dual.mu_rdi = OPT.step{jj}.dual.mu_rdi;
                end
                if nrde > 0
                    OPT.phase{3}{jj}.var.dual.mu_rde = OPT.step{jj}.dual.mu_rde;
                end
            end
            if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
              if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
                for jj = 1:S
                  for ii = 1:ns-1+1
                    OPT.phase{3}{jj}.var.primal.delq(:,ii) = OPT.step{jj}.primal.delq(:,ii);
                  end
                  OPT.phase{3}{jj}.var.dual.lambda1 = OPT.step{jj}.dual.lambda1;
                  OPT.phase{3}{jj}.var.dual.lambda2 = OPT.step{jj}.dual.lambda2;
                  OPT.phase{3}{jj}.var.dual.mu_delq = OPT.step{jj}.dual.mu_delq;
                end
              else
                for jj = 1:S
                  for ii = 1:ns-1
                    OPT.phase{3}{jj}.var.primal.delq(:,ii) = OPT.phase{3}{jj}.var.primal.q(:,ii+1) - OPT.phase{3}{jj}.var.primal.q(:,ii);
                  end
                  OPT.phase{3}{jj}.var.primal.delq(:,ns) = OPT.phase{3}{jj}.var.primal.q(:,1) - OPT.controlmove_q0;
                end
              end
            end
    end
    
end

OPT.steptime = toc;
