function [Scale,scaledmatrix]=scale(A)


%Input: symmetric matrix A
%Output: scaling vector R such that diag(R)*A*diag(R) has row norm between
%0 or 2
%for each row



%test if A is square matrix
a=size(A);
if a(1)~=a(2)
    error('A is not square');
end
%test if A is symmetric
if norm(A-A')>1e-10
    error('A is not symmetric');
end
n=a(1);

%build matrix B such that Bij=exponent(aij)
[S,B]=log2(A);


%% initialize r
R=ones(n,1);
workB=B;
for i=1:n
    %extract submatrix 
    %Asub=A(1:i,1:i);
    %define the working matrix
    if i>1
        %add r to column
        workB(:,i-1)=R(i-1)*ones(n,1)+workB(:,i-1);
        %add r to row
        workB(i-1,:)=R(i-1)*ones(1,n)+workB(i-1,:);
    end
    
    %now if we multiply Ri at the row and the column we have:
    %new ith subrow=[workB(i,1:i-1)+ones*R(i),B(i,i)+2*R(i)]
    a=max(abs(workB(i,1:i-1)));
    b=workB(i,i);
    
    if b<2*a
        R(i)=-a;
    else
        if mod(b,2)==0
            R(i)=-b/2;
        else
            R(i)=(-b+1)/2;
        end
    end

    
end


%% test result
for i=1:n
    for j=1:n
        res(i,j)=R(i)+R(j);
    end
end
result=B+res;



%calculate Scale vector
Scale=pow2(R);
%test result
scaledmatrix=diag(Scale)*A*diag(Scale);
%test norms
for i=1:n
    colnorm(i)=max(scaledmatrix(:,i));
end
colnorm;
min(Scale)
end