% -*-matlab-*-
% Variables filled in OPT
%
% OPT.cond.b1
% OPT.cond.b2
% OPT.cond.b
% OPT.cond.bndlo1
% OPT.cond.bndup1
% OPT.cond.bndlo2
% OPT.cond.bndup2
% OPT.cond.c
%
% OPT.cond.C12
% OPT.cond.B11
% OPT.cond.B12
% OPT.cond.B22
% OPT.cond.B
%
% QP complete with OPT
% .cond.B        Hessian
% .cond.b        Gradient
% .cond.bndlo1
% .cond.bndup1
% .cond.C12      Inequalities
% .cond.bndlo2
% .cond.bndup2   Bounds
%
% Missing: Initial condition for Initial Value Embedding


function mli_condense_withblocks

global OPT SLV;

phase = OPT.currentphase;

if (phase == 1)
  OPT.condtime = 0;
  return;
end

if phase == 0
    pvars = OPT.phase{4}.var.primal;
else
    pvars = OPT.phase{phase}.var.primal;
end

tic

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nrd = nrds + (ns-1) * nrdi + nrde;

OPT.cond.bndlo1(:) = 0;
OPT.cond.bndup1(:) = 0;
OPT.cond.bndlo2(:) = 0;
OPT.cond.bndup2(:) = 0;
OPT.cond.c(:) = 0;
OPT.cond.b(:) = 0;
if nrd > 0
  OPT.cond.rdres(:) = 0;
end

vidx = reshape(1:ns*nsd,nsd,ns);
vidx2 = reshape(nsd+(1:ns*nq),nq,ns);

if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
  for ii=1:ns
    stride = nsd+nq;
    idx = (ii-1)*stride;
    tempmatrix = lbfgshess_matrix(OPT.hess.lbfgshess{ii});
    OPT.cond.H_full(idx+(1:stride),idx+(1:stride)) = tempmatrix;
    OPT.hess.ssblock{ii} = tempmatrix(1:nsd,1:nsd);
    OPT.hess.sqblock{ii} = tempmatrix(1:nsd,(nsd+1):stride);
    OPT.hess.qqblock{ii} = tempmatrix((nsd+1):stride,(nsd+1):stride);
  end
  tempmatrix = lbfgshess_matrix(OPT.hess.lbfgshess{ns+1});
  OPT.cond.H_full(ns*(nsd+nq)+(1:nsd),ns*(nsd+nq)+(1:nsd)) = tempmatrix;
  OPT.hess.ssblock{ns+1} = tempmatrix;
end

% step bounds
l_s = reshape(SLV.opt.lagObjModSd*OPT.bounds.sdLoB,(ns+1)*nsd,1);
u_s = reshape(SLV.opt.lagObjModSd*OPT.bounds.sdUpB,(ns+1)*nsd,1);
s_h = reshape(pvars.sd,(ns+1)*nsd,1);
l_q = reshape(OPT.bounds.qLoB,ns*nq,1);
u_q = reshape(OPT.bounds.qUpB,ns*nq,1);
q_h = reshape(pvars.q,ns*nq,1);

a1 = l_s - s_h;
a2 = u_s - s_h;

OPT.cond.bndlo1 = a1(nsd+1:end);
OPT.cond.bndup1 = a2(nsd+1:end);
OPT.cond.bndlo2 = l_q-q_h;
OPT.cond.bndup2 = u_q-q_h;

% Linear transformation of equalities - Building blocks C12

if (phase == 0) || (phase == 4)
% First block column of C12 and C12'*B11
  OPT.cond.cblocks.first{1} = OPT.eval.mat.Gx{1};
  OPT.cond.cblocksb.first{1} = OPT.cond.cblocks.first{1}' * ...
      OPT.hess.ssblock{2};

  for ii=2:ns
    OPT.cond.cblocks.first{ii} = OPT.eval.mat.Gx{ii} * ...
        OPT.cond.cblocks.first{ii-1};
    OPT.cond.cblocksb.first{ii} = OPT.cond.cblocks.first{ii}' * ...
        OPT.hess.ssblock{ii+1};
  end


  % Other block columns of C12 and C12'*B11
  for jj=1:ns
    OPT.cond.cblocks.mixed{jj,jj} = OPT.eval.mat.Gq{jj};
    OPT.cond.cblocksb.mixed{jj,jj} = ...
	OPT.eval.mat.Gq{jj}' * OPT.hess.ssblock{jj+1};

    for ii=(jj+1:ns)
      OPT.cond.cblocks.mixed{ii,jj} = OPT.eval.mat.Gx{ii} * ...
	  OPT.cond.cblocks.mixed{ii-1,jj};
      OPT.cond.cblocksb.mixed{ii,jj} = ...
	  OPT.cond.cblocks.mixed{ii,jj}' * OPT.hess.ssblock{ii+1};
    end
  end

end

% Linear transformation of equalities - right hand side
OPT.cond.c(1:nsd,1) = OPT.eval.res.c(:,1);

for ii=2:ns
  OPT.cond.c(vidx(:,ii),1) = OPT.eval.mat.Gx{ii} * ...
      OPT.cond.c(vidx(:,ii-1),1) + OPT.eval.res.c(:,ii);
end

% Linear transformation of inequalities - right hand side
if nrd > 0
  sidx = 0;
  if nrds > 0
    OPT.cond.rdres(sidx+(1:nrds),1) = -OPT.eval.res.rds;
    sidx = sidx + nrds;
  end

  if nrdi > 0
    for ii=1:ns-1
      offs = sidx + (ii-1)*nrdi;
      OPT.cond.rdres(offs+(1:nrdi),1) = ...
          -(OPT.eval.res.rdi(:,ii) + OPT.eval.mat.Jrdix{ii} * ...
            OPT.eval.res.c(:,ii));
    end
%     sidx = sidx + (ns-1)*nrdi;
  end

  if nrde > 0
    OPT.cond.rdres(offs+(1:nrde),1) = ...
        -(OPT.eval.res.rde + OPT.eval.mat.Jrdex * ...
          OPT.eval.res.c(:,ns));
  end
end

% Build Hessian 
if (phase == 0) || (phase == 4)
  
  % B = M'*B11*M + M'*B12 + (M'*B12)' + B22
  OPT.cond.B(:) = 0;

  % B22 part of B
  OPT.cond.B(1:nsd,1:nsd) = OPT.hess.ssblock{1};
  OPT.cond.B(1:nsd,nsd+(1:nq)) = OPT.hess.sqblock{1};
  OPT.cond.B(nsd+(1:nq),1:nsd) = OPT.hess.sqblock{1}';
  for ii=1:ns
    OPT.cond.B(vidx2(:,ii),vidx2(:,ii)) = OPT.hess.qqblock{ii};
  end

  % s0,s0 part of M'*B11*M
  hmat = zeros(nsd,nsd);
  for ii=1:ns 
    hmat = hmat + OPT.cond.cblocksb.first{ii} * OPT.cond.cblocks.first{ii};
  end
  OPT.cond.B(1:nsd,1:nsd) = OPT.cond.B(1:nsd,1:nsd) + hmat;

  % s0,q part of M'*B11*M
  hmat = zeros(nsd,nq);
  for ii=1:ns
    hmat(:) = 0;
    for kk=ii:ns
      hmat = hmat + OPT.cond.cblocksb.first{kk} * ...
             OPT.cond.cblocks.mixed{kk,ii};
    end
    OPT.cond.B(1:nsd, vidx2(:,ii)) = OPT.cond.B(1:nsd, vidx2(:,ii)) + hmat;
    OPT.cond.B(vidx2(:,ii), 1:nsd) = OPT.cond.B(vidx2(:,ii), 1:nsd) + hmat';
  end

  % q,q part of M'*B11*M
  hmat = zeros(nq,nq);
  for ii=1:ns
    for jj=ii:ns
      hmat(:) = 0;
      for kk=jj:ns
	hmat = hmat + OPT.cond.cblocksb.mixed{kk,ii} * ...
               OPT.cond.cblocks.mixed{kk,jj};
      end
      OPT.cond.B(vidx2(:,ii),vidx2(:,jj)) = ...
          OPT.cond.B(vidx2(:,ii),vidx2(:,jj)) + hmat;
      if (ii ~= jj)
 	OPT.cond.B(vidx2(:,jj),vidx2(:,ii)) = ...
            OPT.cond.B(vidx2(:,jj),vidx2(:,ii)) + hmat';
      end
    end
  end
  
  % M'*B12 and (M'*B12)' part
  for ii=1:ns-1
    hmat = OPT.cond.cblocks.first{ii}'*OPT.hess.sqblock{ii+1};
    OPT.cond.B(1:nsd,vidx2(:,ii+1)) = ...
        OPT.cond.B(1:nsd,vidx2(:,ii+1)) + hmat;
    OPT.cond.B(vidx2(:,ii+1),1:nsd) = ...
        OPT.cond.B(vidx2(:,ii+1),1:nsd) + hmat';
  end

  for ii=1:ns-1
    for jj=ii:ns-1
      hmat = OPT.cond.cblocks.mixed{jj,ii}'*OPT.hess.sqblock{jj+1}; 
      OPT.cond.B(vidx2(:,ii),vidx2(:,jj+1)) = ...
          OPT.cond.B(vidx2(:,ii),vidx2(:,jj+1)) + hmat;
      OPT.cond.B(vidx2(:,jj+1),vidx2(:,ii)) = ...
          OPT.cond.B(vidx2(:,jj+1),vidx2(:,ii)) + hmat';
    end
  end
end
% Build condensed gradient

% b2 part
OPT.cond.b(1:nsd,1) = OPT.eval.res.modgradx(:,1);
for ii=1:ns
  OPT.cond.b(vidx2(:,ii),1) = OPT.eval.res.modgradq(:,ii);
end

% C12' * b1 part +  C12' * B11 * c part
for ii=1:ns
  OPT.cond.b(1:nsd,1) = OPT.cond.b(1:nsd,1) + ...
      OPT.cond.cblocks.first{ii}' * OPT.eval.res.modgradx(:,ii+1) + ...
      OPT.cond.cblocksb.first{ii} * OPT.cond.c(vidx(:,ii));
end

for jj=1:ns
  for ii=jj:ns
    OPT.cond.b(vidx2(:,jj),1) = OPT.cond.b(vidx2(:,jj),1) + ...
        OPT.cond.cblocks.mixed{ii,jj}' * OPT.eval.res.modgradx(:,ii+1) + ...
        OPT.cond.cblocksb.mixed{ii,jj} * OPT.cond.c(vidx(:,ii));
  end
end

% B12' * c part
for ii=1:ns-1
   OPT.cond.b(vidx2(:,ii+1),1) = OPT.cond.b(vidx2(:,ii+1),1) + ...
       OPT.hess.sqblock{ii+1}' * OPT.cond.c(vidx(:,ii));
end

% Condensing matrix C12 (only for QP, not used in Condensing)
if (phase == 0) || (phase == 4)
  OPT.cond.C12 = zeros(ns*nsd,nsd+ns*nq);
  for ii=1:ns
    OPT.cond.C12(vidx(:,ii),1:nsd) = OPT.cond.cblocks.first{ii};  
  end
  
  for ii=1:ns
    for jj=1:ii
      OPT.cond.C12(vidx(:,ii),vidx2(:,jj)) = ...
	  OPT.cond.cblocks.mixed{ii,jj};  
    end
  end
end

% Constraint matrix for QP
if (phase == 0) || (phase == 4)
  if nrd > 0
    OPT.cond.rdmat = zeros(nrd, nsd+ns*nq);
    sidx = 0;

    if nrds > 0
      OPT.cond.rdmat(sidx+(1:nrds),1:nsd) = OPT.eval.mat.Jrdsx;
      OPT.cond.rdmat(sidx+(1:nrds),nsd+(1:nq)) = OPT.eval.mat.Jrdsq;
      sidx = sidx + nrds;
    end

    if nrdi > 0
      for ii=1:ns-1
	xoffs=sidx+(ii-1)*nrdi; 
	OPT.cond.rdmat(xoffs+(1:nrdi),1:nsd) = ...
            OPT.eval.mat.Jrdix{ii} * OPT.cond.cblocks.first{ii};
	for jj=1:ii
	  yoffs = nsd+(jj-1)*nq;
	  OPT.cond.rdmat(xoffs+(1:nrdi),yoffs+(1:nq)) = ...
              OPT.eval.mat.Jrdix{ii} * OPT.cond.cblocks.mixed{ii,jj};
	end
	OPT.cond.rdmat(xoffs+(1:nrdi),nsd+ii*nq+(1:nq)) = ...
            OPT.eval.mat.Jrdiq{ii};
      end
      sidx = sidx + (ns-1) * nrdi;
    end

    if nrde > 0
	OPT.cond.rdmat(sidx+(1:nrde),1:nsd) = ...
            OPT.eval.mat.Jrdex * OPT.cond.cblocks.first{ns};
	for jj=1:ns
	  yoffs = nsd+(jj-1)*nq;
	  OPT.cond.rdmat(sidx+(1:nrde),yoffs+(1:nq)) = ...
              OPT.eval.mat.Jrdex * OPT.cond.cblocks.mixed{ns,jj};
	end
    end
  end
end


% Bounds
% OPT.condbndlo1 <= C12 * Deltaw2 <= OPT.condbndup1
% OPT.condbndlo2 <= Deltaw2 <= OPT.condbndup2
OPT.cond.bndlo1 = OPT.cond.bndlo1 - OPT.cond.c;
OPT.cond.bndup1 = OPT.cond.bndup1 - OPT.cond.c;

OPT.condtime = toc;
