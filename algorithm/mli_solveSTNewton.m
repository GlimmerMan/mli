function [ deltalam, nreg ] = mli_solveSTNewton( M, G, methodflag )
%SOLVENEWTON solve the Newton system to get step 
%   various methods

global ST

switch methodflag
    case 1
        % regularization and Cholesky decomposition
        
        %             [R,q] = chol(-M);
        %             if q == 0
        %                 q = ST.na + 1;
        %             end
        %             D = diag(R);
        %             beta = 1e-3;
        %             idx = abs(D) < beta^2;
        %             D(idx) = beta;
        %             R = triu(R,1) + diag(D);
        %             deltalam = [R\(R'\G(1:q-1)); -1e-5*G(q:end)];
        %             nreg = [ST.na - (q - 1), sum(idx)];
        
        [R,q] = chol(-M);
        D = diag(R);
        beta = 1e-3;
        idx = abs(D) < beta^2;
        if q == 0
            q = ST.na + 1;
        end
        if q ~= 0 || any(idx)
            idx = [idx; logical(ones(ST.na-(q-1),1))];
            [R,qq] = chol(-M + 1e-2 * diag(idx));
            assert(qq == 0)
        else
        end
        deltalam = R \ (R' \ G);
        nreg = ST.na - (q - 1);
        
  % case 2-5: iterative methods
        
    case 9
        % direct solve (fails, if M semidef)
        
        deltalam = -M \ G;
        nreg = 0;
        
    otherwise
        
        error('Method %i to solve the Newton system does not exist.',methodflag);
        
end

