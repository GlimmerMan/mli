function mli_scensave(ppath, logging)

global SCEN OPT EST ST SLV 

switch logging
  case 0
    % nothing
  case 1
    %check if resultfolder exists
    if ~exist([ ppath, '/results/'],'dir')
      mkdir([ ppath, '/results/']);
    end
    if OPT.currentsample==1
      %create filename
      dt = datestr(now,'dd-mm-yyyy-HH:MM:SS.FFF-');
      filename_string=[dt,SCEN.name,'.mat'];
      SCEN.resultfile = [ ppath, '/results/',filename_string];
    end
    if OPT.currentphase==0 || OPT.currentphase==9
      currentphase=4;
    else
      currentphase=OPT.currentphase;
    end
    
    if exist(SCEN.resultfile, 'file')
      load(SCEN.resultfile,'predicted_history');
      predicted_history{OPT.currentsample} = ...
        OPT.phase{currentphase}.var;
      save(SCEN.resultfile,'predicted_history','SCEN','-append');
    else
      predicted_history{OPT.currentsample} = ...
        OPT.phase{currentphase}.var;
      save(SCEN.resultfile,'predicted_history','SCEN');
    end
     
  case 2
    if ~exist([ ppath, '/results/'],'dir')
      mkdir([ ppath, '/results/']);
    end
    if OPT.currentsample==1
      OPT.folder_string = [ppath, '/results/',datestr(now,'dd-mm-yyyy-HH:MM:SS.FFF-')];
      mkdir(OPT.folder_string);
    end
    
    file_string = sprintf('it%06d.mat', OPT.currentsample);
    save([OPT.folder_string,'/',file_string]);  
end


