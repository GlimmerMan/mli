function mli_scaleQP

global OPT;

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;

dimR1 = nsd + (nq*ns);
dimR2 = nsd * ns;

% Initialize iterating matrices
Hhat = OPT.qp.H;
Ahat = OPT.qp.A;

% Initialize scaling factors
R = ones(dimR1+dimR2,1);

% Create helper space
h = zeros(dimR1+dimR2,1);

while 1
    
    % Row norms of composed matrix
    h(1:dimR1) = max(max(abs(Hhat),[],2), max(abs(Ahat),[],1)');
    h(dimR1+(1:dimR2)) = max(abs(Ahat),[],2);
    
    % h_i == F_i * 2^E_i, F_i in [0.5,1), E_i integer 
    [F,E] = log2(h);
    
    % Compute scaling factors
    must_scale = 0;
    for i=1:(dimR1+dimR2)
       if E(i) < 0
           h(i) = pow2(1.0, floor((1-E(i))/2));            
           must_scale = 1;
       elseif E(i) > 1
           h(i) = pow2(1.0, -floor(E(i)/2));
           must_scale = 1;
       else
           h(i) = 1.0;
       end
    end
    
    % If no scale is needed we are done
    if must_scale == 0
       break; 
    end
    
    % Scale composed matrix
    for i = 1:dimR1
        Hhat(i,:) = h(i) * Hhat(i,:);
        Hhat(:,i) = h(i) * Hhat(:,i);
        
        Ahat(:,i) = h(i) * Ahat(:,i);
        
        R(i) = h(i) * R(i);
    end
    
    for i = 1:dimR2
        Ahat(i,:) = h(dimR1+i) * Ahat(i,:);
        
        R(dimR1+i) = h(dimR1+i) * R(dimR1+i);
    end    
end

OPT.qp.R1 = diag(R(1:dimR1));
OPT.qp.R2 = diag(R(dimR1+(1:dimR2)));

end

