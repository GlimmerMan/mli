% -*-matlab-*-
function est_evaluate_growinghorizon

global EST;

% The following parts are evaluated:
%
% - matching conditions and sensitivity matrices
% - measurement function values and Jacobians
% - least squares residuals with exception of last one 
%   (the measurement is still unknown)
%
% This allows to set up the uncondensed system almost completely.
% We evaluate only the first cursamp nodes (growing horizon).
% Only D iterations in growing horizon phase.


nsd = EST.dims.nsd; 
nq = EST.dims.nq; 
np = EST.dims.np;
nlsqs = EST.dims.lsqfcn_s;
nlsqi = EST.dims.lsqfcn_i; 
nlsqe = EST.dims.lsqfcn_e;

cursamp = EST.currentsample;
pvars = EST.var.primal;

if EST.fixed_p
    sensdirs = [zeros(1,nsd);
                eye(nsd),
                zeros(nq+np,nsd)];
else
     sensdirs = [zeros(1,nsd+np);
                eye(nsd), zeros(nsd,np)  
                zeros(nq,nsd+np)
                zeros(np,nsd), eye(np)];
end

if ~EST.disable_arrival
    if EST.fixed_p
        EST.eval.res.arrivalCost = ...
            EST.P * [pvars.sd(:,1) - EST.xbar];
    else
        EST.eval.res.arrivalCost = ...
            EST.P * [pvars.sd(:,1) - EST.xbar; 
                     pvars.p - EST.pbar];
    end
end

% Dynamical equations
for msn = 1:cursamp-1
    inp = [];
    inp.thoriz = [EST.discr.msgrid(msn) EST.discr.msgrid(msn+1)];
    inp.sd = pvars.sd(:,msn);
    inp.p = pvars.p;
    inp.q = pvars.qconst(:,msn);
    inp.sensdirs = sensdirs;
    outp = eval_dyn_D_gn_est(inp);
    EST.eval.res.xd(:,msn) = outp.x;
    EST.eval.res.c(:,msn) = outp.x - pvars.sd(:,msn+1);
    EST.eval.mat.Gx{msn} = outp.G(1:nsd,1:nsd);
    if ~EST.fixed_p
        EST.eval.mat.Gp{msn} = outp.G(1:nsd,nsd+(1:np));
    end
end
   
% measurement Function
msn = 1;
inp = [];
inp.t = EST.discr.msgrid(msn);
inp.sd = pvars.sd(:,msn);
inp.p = pvars.p;
inp.q = pvars.qconst(:,msn);
inp.sensdirs = sensdirs;
outp = eval_lsqfcn_D_gn_est(inp,'s');

EST.eval.res.meas(:,msn) = outp.lsqfcn;
EST.eval.mat.Jmeasx{msn} = outp.lsqjac(1:nlsqs,1:nsd);
if ~EST.fixed_p
    EST.eval.mat.Jmeasp{msn} = outp.lsqjac(1:nlsqs,nsd+(1:np));
end

for msn = 2:cursamp
    inp = [];
    inp.t = EST.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.p = pvars.p;
    inp.q = pvars.qconst(:,msn);
    inp.sensdirs = sensdirs;
    outp = eval_lsqfcn_D_gn_est(inp,'i');
    
    EST.eval.res.meas(:,msn) = outp.lsqfcn;
    EST.eval.mat.Jmeasx{msn} = outp.lsqjac(1:nlsqs,1:nsd);
    if ~EST.fixed_p
        EST.eval.mat.Jmeasp{msn} = outp.lsqjac(1:nlsqs,nsd+(1:np));
    end
end
   
% complete lsq terms for known measurements
for msn = 1:cursamp-1
    Vmsn =  diag(EST.V(:,msn));
    EST.eval.res.lsq(:,msn) = Vmsn * ...
        (EST.eval.res.meas(:,msn) - EST.measurements(:,msn));
end

%disp('End of evaluate_growinghorizon...');
%keyboard;
