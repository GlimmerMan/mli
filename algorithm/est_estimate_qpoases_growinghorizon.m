% -*-matlab-*-
function est_estimate_qpoases_growinghorizon

global EST;

nsd = EST.dims.nsd;
np = EST.dims.np;
nq = EST.dims.nq;
nlsqs = EST.dims.lsqfcn_s;
nlsqi = EST.dims.lsqfcn_i; 
nlsqe = EST.dims.lsqfcn_e;

cursamp = EST.currentsample;

if cursamp == 1
    EST.cond.Fcond(1:nlsqs,1) = diag(EST.V(:,cursamp)) * ...
         (EST.eval.res.meas(:,cursamp) - EST.eta);
else
    EST.cond.Fcond(1:nlsqs,1) = EST.eval.res.lsq(:,1);
    idx = nlsqs;
    for ii = 2:cursamp-1
        EST.cond.Fcond(idx+(1:nlsqi),1) = EST.eval.res.lsq(:,ii);
        idx = idx+nlsqi;
    end
    EST.cond.Fcond(idx+(1:nlsqi),1) = diag(EST.V(:,cursamp)) * ...
         (EST.eval.res.meas(:,cursamp) - EST.eta);
end

EST.cond.Fcond = EST.cond.Fcond + EST.cond.Fmod;

% Build Gradiend = Jcond'*Fcond (arrivalCost treated separately)
if ~EST.disable_arrival
    EST.qp.g = [EST.P'*EST.eval.res.arrivalCost + ...
                EST.cond.Jcond'*EST.cond.Fcond];
else
    EST.qp.g = EST.cond.Jcond'*EST.cond.Fcond;
end


% now the QP is complete
[x, obj, status, nWSRout, y] = qpOASES_sequence('m', EST.qp.qphandle, EST.qp.H, EST.qp.g, ...
                                                EST.qp.A, EST.qp.lb, ...
                                                EST.qp.ub, EST.qp.lbA, ...
                                                EST.qp.ubA);

EST.qp.solstep = x;

% step blowup
if cursamp == 1
    EST.step.primal.sd(:,1) =  x(1:nsd);
    if ~EST.fixed_p
        EST.step.primal.p = x(nsd+(1:np));
    end
else
    EST.step.primal.sd(:,1) = x(1:nsd);
    if ~EST.fixed_p
        EST.step.primal.p = x(nsd+(1:np));
    end
    sdstep = EST.qp.A(1:nsd*(cursamp-1),:) * EST.qp.solstep + ...
             reshape(EST.cond.c(:,1:cursamp-1),nsd*(cursamp-1),1);
    EST.step.primal.sd(:,2:cursamp) = reshape(sdstep, nsd, cursamp-1);
end

% return estimates
if cursamp == 1
    EST.sdest = EST.var.primal.sd(:,1) + EST.step.primal.sd(:,1);
    if ~EST.fixed_p
        EST.pest = EST.var.primal.p + EST.step.primal.p;
    end
    EST.covest = inv(EST.qp.H);
else
    EST.sdest = EST.var.primal.sd(:,cursamp) + EST.step.primal.sd(: ...
                                                      ,cursamp);
    if ~EST.fixed_p
        EST.pest = EST.var.primal.p + EST.step.primal.p;
        GN = [EST.cond.cblocksx{cursamp-1} EST.cond.cblocksp{cursamp-1}
              zeros(np,nsd) eye(np)];
    else
        GN = EST.cond.cblocksx{cursamp-1};
    end
    EST.covest = GN * inv(EST.qp.H) * GN';
end

if EST.with_chi2test
    EST.chi2test(cursamp) = 0;
end

%disp('End of estimate_qpoases_growinghorizon...');
%keyboard;
