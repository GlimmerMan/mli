function [x_scaled, y_scaled] = mli_rescaleQP(x_scaled, y_scaled)

global OPT;

R1 = OPT.qp.R1;
R2 = OPT.qp.R2;

% Rescale primal variables
x_scaled = R1 * x_scaled;

% Rescale dual variables
y_scaled(1:size(R1,1)) = inv(R1) * y_scaled(1:size(R1,1));
y_scaled(size(R1,1)+1:end) = R2 * y_scaled(size(R1,1)+1:end);

end

