function update_msgrid
global SCEN OPT
current_time=SCEN.thistory(SCEN.currentsample);
%determine first k such that k*OPT.discr.shootingwidth>=current time
k0=floor(current_time/OPT.discr.shootingwidth);
if norm((k0+1)*OPT.discr.shootingwidth-current_time)<1e-5
  OPT.discr.msgrid=[current_time;linspace((k0+2)*OPT.discr.shootingwidth,(k0+1+OPT.nsi)*OPT.discr.shootingwidth,OPT.nsi)'];
else
  OPT.discr.msgrid=[current_time;linspace((k0+1)*OPT.discr.shootingwidth,(k0+OPT.nsi)*OPT.discr.shootingwidth,OPT.nsi)'];
  
end

end