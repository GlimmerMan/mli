% -*-matlab-*-
function mli_initialize_opt

global SCEN OPT ST;

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;

% Phase initialization
OPT.phasehistory = zeros(1,SCEN.steps);
OPT.iterhistory = zeros(1,SCEN.steps);
for ii = 1:length(OPT.ctrl.usephases)
  phase = OPT.ctrl.usephases(ii);
  itcount = OPT.ctrl.phase_iter(ii);
  freq = OPT.ctrl.phasefreq(ii);
  OPT.phasehistory(freq:freq:end) = phase;
  OPT.iterhistory(freq:freq:end) = itcount;
end
if OPT.initial_iterations <= 0  && OPT.phasehistory(1) ~= 9
  OPT.phasehistory(1) = 0;
end

if isfield(OPT.ctrl,'phase_mod')
  OPT.phasehistory(OPT.ctrl.phase_mod(:,1)) = OPT.ctrl.phase_mod(:,2);
end

if SCEN.use_st == 0
    
    OPT.phase{1}.ref = init_vars;
    OPT.phase{2}.ref = init_vars;
    
    OPT.phase{2}.data = init_data(2);
    OPT.phase{3}.data = init_data(3);
    OPT.phase{4}.data = init_data(4);
    
    OPT.eval = init_data(4);
    
    OPT.phase{2}.var = init_vars;
    OPT.phase{3}.var = init_vars;
    OPT.phase{4}.var = init_vars;
    
    nelim = ns * nsd;
    nremain = nsd + ns * nq;
    nrd = nrds + (ns-1)* nrdi + nrde;
    
    OPT.hess.sdsdblock = mat2cell(zeros(nsd,nsd,ns+1), nsd, nsd, ones(ns+1,1));
    OPT.hess.sdqblock = mat2cell(zeros(nsd,nq,ns), nsd, nq, ones(ns,1));
    OPT.hess.qqblock = mat2cell(zeros(nq,nq,ns), nq, nq, ones(ns,1));
    
    if OPT.use_condensing
        
        OPT.cond.bndlo1 = zeros(nelim, 1);
        OPT.cond.bndup1 = zeros(nelim, 1);
        OPT.cond.bndlo2 = zeros(nremain, 1);
        OPT.cond.bndup2 = zeros(nremain, 1);
        
        if nrd > 0
            OPT.cond.rdres = zeros(nrd, 1);
            OPT.cond.rdmat = zeros(nrd, nremain);
        end
        
        OPT.cond.C12 = zeros(nelim, nremain);
        OPT.cond.c = zeros(nelim, 1);
        OPT.cond.b = zeros(nremain, 1);
        OPT.cond.B = zeros(nremain);
        OPT.cond.H_full = zeros(nelim+nremain);
        
        % Condensed QP variables
        OPT.qp.H = zeros(nremain);
        OPT.qp.g = zeros(nremain, 1);
        OPT.qp.A = zeros(nelim + nrd, nremain);
        OPT.qp.ineqlb = zeros(nelim + nrd, 1);
        OPT.qp.inequb = zeros(nelim + nrd, 1);
        OPT.qp.lb = zeros(nremain, 1);
        OPT.qp.ub = zeros(nremain, 1);
        OPT.qp.solstep = zeros(nremain, 1);
        OPT.qp.soleqmult = zeros(nsd, 1);
        OPT.qp.solineqmult = zeros(nelim + nrd, 1);
        OPT.qp.solbndmult = zeros(ns * nq, 1);
        if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
          if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
            OPT.qp.H = zeros(nremain+(ns-1)*nq+nq);
            OPT.qp.g = zeros(nremain+(ns-1)*nq+nq, 1);
            OPT.qp.A = zeros(nelim+nrd+2*(ns-1)*nq+2*nq, nremain+(ns-1)*nq+nq);
            OPT.qp.ineqlb = zeros(nelim + nrd + 2*(ns-1)*nq+2*nq, 1);
            OPT.qp.inequb = zeros(nelim + nrd + 2*(ns-1)*nq+2*nq, 1);
            OPT.qp.lb = zeros(nremain+(ns-1)*nq+nq, 1);
            OPT.qp.ub = zeros(nremain+(ns-1)*nq+nq, 1);
          else % using only OPT.controlmove_alpha
            OPT.qp.A = zeros(nelim+nrd+(ns-1)*nq+nq, nremain);
            OPT.qp.ineqlb = zeros(nelim + nrd + (ns-1)*nq+nq, 1);
            OPT.qp.inequb = zeros(nelim + nrd + (ns-1)*nq+nq, 1);
          end
        end
    else
        % Uncondensed QP variables
        OPT.qp.H = zeros(nelim+nremain);
        OPT.qp.g = zeros(nelim+nremain, 1);
        OPT.qp.A = zeros(nelim+nrd, nelim+nremain);
        OPT.qp.ineqlb = zeros(nelim + nrd, 1);
        OPT.qp.inequb = zeros(nelim + nrd, 1);
        OPT.qp.lb = zeros(nelim+nremain, 1);
        OPT.qp.ub = zeros(nelim+nremain, 1);
        OPT.qp.solstep = zeros(nelim+nremain, 1);
        OPT.qp.soleqmult = zeros(nsd+nelim, 1);
        OPT.qp.solineqmult = zeros(nrd, 1);
        OPT.qp.solbndmult = zeros(nelim+nremain-nsd, 1);

        if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
          if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
            OPT.qp.H = zeros(nelim+nremain+(ns-1)*nq+nq);
            OPT.qp.g = zeros(nelim+nremain+(ns-1)*nq+nq, 1);
            OPT.qp.A = zeros(nelim+nrd+2*(ns-1)*nq+2*nq, nelim+nremain+(ns-1)*nq+nq);
            OPT.qp.ineqlb = zeros(nelim + nrd + 2*(ns-1)*nq+2*nq, 1);
            OPT.qp.inequb = zeros(nelim + nrd + 2*(ns-1)*nq+2*nq, 1);
            OPT.qp.lb = zeros(nelim+nremain+(ns-1)*nq+nq, 1);
            OPT.qp.ub = zeros(nelim+nremain+(ns-1)*nq+nq, 1);
          else % using only OPT.controlmove_alpha
            OPT.qp.A = zeros(nelim+nrd+(ns-1)*nq+nq, nelim+nremain);
            OPT.qp.ineqlb = zeros(nelim + nrd + (ns-1)*nq+nq, 1);
            OPT.qp.inequb = zeros(nelim + nrd + (ns-1)*nq+nq, 1);
          end
        end
    end
    
    % initialization call to qpOASES to get handle
    [QP, x, fval, exitflag, iter, lambda] = qpOASES_sequence( 'i',OPT.qp.H, ...
        OPT.qp.g, OPT.qp.A, ...
        OPT.qp.lb, OPT.qp.ub, ...
        OPT.qp.ineqlb, ...
        OPT.qp.inequb);
    OPT.qp.qphandle = QP;
    
    
    % Current state input - Feedback output
    OPT.deltas0 = zeros(nsd, 1);
    OPT.feedbackdelta = zeros(nq, 1);
    
    % (Expanded) variable steps and new multipliers
    OPT.step.primal.q = zeros(nq, ns);
    OPT.step.primal.sd = zeros(nsd, ns+1);
    if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
      OPT.step.primal.delq = zeros(nq, ns-1+1);
      if isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')
        OPT.step.dual.lambda1 = zeros(nq, ns-1+1);
        OPT.step.dual.lambda2 = zeros(nq, ns-1+1);
        OPT.step.dual.mu_delq = zeros(nq, ns-1+1);
        OPT.step.dualstep.lambda1 = zeros(nq, ns-1+1);
        OPT.step.dualstep.lambda2 = zeros(nq, ns-1+1);
        OPT.step.dualstep.mu_delq = zeros(nq, ns-1+1);
      end
    end
    OPT.step.dual.lambda = zeros(nsd, ns);
    OPT.step.dual.mu_sd = zeros(nsd, ns+1);
    OPT.step.dual.mu_q = zeros(nq, ns);
    if nrds > 0
        OPT.step.dual.mu_rds = zeros(nrds, 1);
    end
    if nrdi > 0
        OPT.step.dual.mu_rdi = zeros(nrdi, ns-1);
    end
    if nrde > 0
        OPT.step.dual.mu_rde = zeros(nrde, 1);
    end
    OPT.step.dualstep.lambda = zeros(nsd, ns);
    OPT.step.dualstep.mu_sd = zeros(nsd, ns+1);
    OPT.step.dualstep.mu_q = zeros(nq, ns);
    if nrds > 0
        OPT.step.dualstep.mu_rds = zeros(nrds, 1);
    end
    if nrdi > 0
        OPT.step.dualstep.mu_rdi = zeros(nrdi, ns-1);
    end
    if nrde > 0
        OPT.step.dualstep.mu_rde = zeros(nrde, 1);
    end
    
%     mli_solveNLP;
    if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
      OPT.hess.lbfgshess = cell(ns+1,1);
      for ii = 1:ns
        OPT.hess.lbfgshess{ii} = lbfgshess_init(nsd+nq,20,OPT.lbfgs_gamma);
      end
      OPT.hess.lbfgshess{ns+1} = lbfgshess_init(nsd,20,OPT.lbfgs_gamma);
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
        OPT.hess.lbfgsCMRhess = cell(ns,1);
        for ii = 1:ns
          OPT.hess.lbfgsCMRhess{ii} = lbfgshess_init(nq,20,OPT.lbfgs_gamma);
        end
      end
    end
    
    if isfield(OPT, 'solve_initial_nlp') && OPT.solve_initial_nlp > 0
        SCEN.currentsample = 1;
        user_opt_x_from_scen;
        mli_solveNLP;
    end
        
    if OPT.initial_iterations > 0
        fprintf('\nPerforming %d initial iterations...\n\n', OPT.initial_iterations);
        for ii = 1:OPT.initial_iterations;
            OPT.currentsample = -1;
            SCEN.currentsample = 1;
            if ii == 1
                OPT.currentphase = 0;
            else
                OPT.currentphase = 4;
                
            end
            if isfield(OPT, 'lbfgshess') && OPT.lbfgshess && ii > 1
              
            else
              mli_evaluate;
            end
            mli_calchess;
            if OPT.use_condensing
                mli_condense_withblocks;
            end
            user_opt_x_from_scen;
            if OPT.use_condensing
                mli_solveQP;
                mli_blowup;
            else
                mli_solveuncondensedQP;
                mli_blowup_nocond;
            end
            mli_stepcalc;
            
            if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
              mli_preeval;
              if SCEN.currentsample > 1
                mli_prepare_lbfgsupdate;
              end
            end
      
            if (ii == 1)
                mli_printinfo(1);
            else
                mli_printinfo(0);
            end
            if isfield(OPT, 'levmar') && isfield(OPT, 'levmarred')
                OPT.levmar = OPT.levmarred * OPT.levmar;
            end
        end
        
        
        OPT.phase{3}.var = OPT.phase{4}.var;
        OPT.phase{2}.var = OPT.phase{4}.var;
        OPT.phase{2}.ref = OPT.phase{4}.var;
        OPT.phase{1}.ref = OPT.phase{4}.var;
        
        if isfield(OPT, 'levmar') && isfield(OPT, 'levmarred')
            OPT.levmar = 0;
        end
        
        
        fprintf('\nInitialization completed.\n\n');
    end
    
    OPT.sigmaB = zeros(1,SCEN.steps);
    OPT.sigmaC = zeros(1,SCEN.steps);    
    
else % ST case

  S = ST.S; % number of scenarios
  
  nelim = ns * nsd;
  nremain = nsd + ns * nq;
  nrd = nrds + (ns-1)* nrdi + nrde;
  
  OPT.hess = cell(S,1);
  OPT.phase{1} = cell(S,1);
  OPT.phase{2} = cell(S,1);
  OPT.phase{3} = cell(S,1);
  OPT.phase{4} = cell(S,1);
  OPT.eval = cell(S,1);
  OPT.qp = cell(S,1);
  OPT.step = cell(S,1);
  OPT.deltas0 = cell(S,1);
  OPT.feedbackdelta = zeros(nq, 1);
  %     OPT.feedbackdelta = cell(S,1);
  QP = cell(S,1);
  x = cell(S,1);
  fval = cell(S,1);
  exitflag = cell(S,1);
  iter = cell(S,1);
  lambda = cell(S,1);
  
  % Loop over all scenarios
  for jj = 1:S
    
    OPT.phase{1}{jj}.ref = init_vars;
    OPT.phase{2}{jj}.ref = init_vars;
    
    OPT.phase{2}{jj}.data = init_data(2);
    OPT.phase{3}{jj}.data = init_data(3);
    OPT.phase{4}{jj}.data = init_data(4);
    
    OPT.eval{jj} = init_data(4);
    
    OPT.phase{2}{jj}.var = init_vars;
    OPT.phase{3}{jj}.var = init_vars;
    OPT.phase{4}{jj}.var = init_vars;
    
    
    
    OPT.hess{jj}.sdsdblock = mat2cell(zeros(nsd,nsd,ns+1), nsd, nsd, ones(ns+1,1));
    OPT.hess{jj}.sdqblock = mat2cell(zeros(nsd,nq,ns), nsd, nq, ones(ns,1));
    OPT.hess{jj}.qqblock = mat2cell(zeros(nq,nq,ns), nq, nq, ones(ns,1));
  end
  
  if OPT.use_condensing
    for jj = 1:S
      
      OPT.cond{jj}.bndlo1 = zeros(nelim, 1);
      OPT.cond{jj}.bndup1 = zeros(nelim, 1);
      OPT.cond{jj}.bndlo2 = zeros(nremain, 1);
      OPT.cond{jj}.bndup2 = zeros(nremain, 1);
      
      if nrd > 0
        OPT.cond{jj}.rdres = zeros(nrd, 1);
        OPT.cond{jj}.rdmat = zeros(nrd, nremain);
      end
      
      OPT.cond{jj}.C12 = zeros(nelim, nremain);
      OPT.cond{jj}.c = zeros(nelim, 1);
      OPT.cond{jj}.b = zeros(nremain, 1);
      OPT.cond{jj}.B = zeros(nremain);
      OPT.cond{jj}.H_full = zeros(nelim+nremain);
      
      % Condensed QP variables
      OPT.qp{jj}.H = zeros(nremain);
      OPT.qp{jj}.g = zeros(nremain, 1);
      OPT.qp{jj}.A = zeros(nelim + nrd, nremain);
      OPT.qp{jj}.ineqlb = zeros(nelim + nrd, 1);
      OPT.qp{jj}.inequb = zeros(nelim + nrd, 1);
      OPT.qp{jj}.lb = zeros(nremain, 1);
      OPT.qp{jj}.ub = zeros(nremain, 1);
      OPT.qp{jj}.solstep = zeros(nremain, 1);
      OPT.qp{jj}.soleqmult = zeros(nsd, 1);
      OPT.qp{jj}.solineqmult = zeros(nelim + nrd, 1);
      OPT.qp{jj}.solbndmult = zeros(ns * nq, 1);
    end
    if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        for jj = 1:S
          OPT.qp{jj}.H = zeros(nremain+(ns-1)*nq+nq);
          OPT.qp{jj}.g = zeros(nremain+(ns-1)*nq+nq, 1);
          OPT.qp{jj}.A = zeros(nelim+nrd+2*(ns-1)*nq+2*nq, nremain+(ns-1)*nq+nq);
          OPT.qp{jj}.ineqlb = zeros(nelim + nrd + 2*(ns-1)*nq+2*nq, 1);
          OPT.qp{jj}.inequb = zeros(nelim + nrd + 2*(ns-1)*nq+2*nq, 1);
          OPT.qp{jj}.lb = zeros(nremain+(ns-1)*nq+nq, 1);
          OPT.qp{jj}.ub = zeros(nremain+(ns-1)*nq+nq, 1);
        end
      else % using only OPT.controlmove_alpha
        for jj = 1:S
          OPT.qp{jj}.A = zeros(nelim+nrd+(ns-1)*nq+nq, nremain);
          OPT.qp{jj}.ineqlb = zeros(nelim + nrd + (ns-1)*nq+nq, 1);
          OPT.qp{jj}.inequb = zeros(nelim + nrd + (ns-1)*nq+nq, 1);
        end
      end
    end
        
  else
    
    %         Uncondensed QP variables
    for jj = 1:S
      OPT.qp{jj}.A = zeros(nelim+nrd, nelim+nremain);
      OPT.qp{jj}.ineqlb = zeros(nelim + nrd, 1);
      OPT.qp{jj}.inequb = zeros(nelim + nrd, 1);
      OPT.qp{jj}.H = zeros(nelim+nremain);
      OPT.qp{jj}.g = zeros(nelim+nremain, 1);
      OPT.qp{jj}.lb = zeros(nelim+nremain, 1);
      OPT.qp{jj}.ub = zeros(nelim+nremain, 1);
      OPT.qp{jj}.solstep = zeros(nelim+nremain, 1);
      OPT.qp{jj}.soleqmult = zeros(nsd+nelim, 1);
      OPT.qp{jj}.solineqmult = zeros(nrd, 1);
      OPT.qp{jj}.solbndmult = zeros(nelim+nremain-nsd, 1);
    end
    
    if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        for jj = 1:S
          OPT.qp{jj}.H = zeros(nelim+nremain+(ns-1)*nq+nq);
          OPT.qp{jj}.g = zeros(nelim+nremain+(ns-1)*nq+nq, 1);
          OPT.qp{jj}.A = zeros(nelim+nrd+2*(ns-1)*nq+2*nq, nelim+nremain+(ns-1)*nq+nq);
          OPT.qp{jj}.ineqlb = zeros(nelim + nrd + 2*(ns-1)*nq+2*nq, 1);
          OPT.qp{jj}.inequb = zeros(nelim + nrd + 2*(ns-1)*nq+2*nq, 1);
          OPT.qp{jj}.lb = zeros(nelim+nremain+(ns-1)*nq+nq, 1);
          OPT.qp{jj}.ub = zeros(nelim+nremain+(ns-1)*nq+nq, 1);
        end
      else % using only OPT.controlmove_alpha
        for jj = 1:S
          OPT.qp{jj}.A = zeros(nelim+nrd+(ns-1)*nq+nq, nelim+nremain);
          OPT.qp{jj}.ineqlb = zeros(nelim + nrd + (ns-1)*nq+nq, 1);
          OPT.qp{jj}.inequb = zeros(nelim + nrd + (ns-1)*nq+nq, 1);
        end
      end
    end


  end
  
  % initialization call to qpOASES to get handle
  
  for jj = 1:S
    
    [QP{jj}, x{jj}, fval{jj}, exitflag{jj}, iter{jj}, lambda{jj}] = qpOASES_sequence( 'i',OPT.qp{jj}.H, ...
      OPT.qp{jj}.g, OPT.qp{jj}.A, ...
      OPT.qp{jj}.lb, OPT.qp{jj}.ub, ...
      OPT.qp{jj}.ineqlb, ...
      OPT.qp{jj}.inequb);
    OPT.qp{jj}.qphandle = QP{jj};
    
    
    % Current state input - Feedback output
    OPT.deltas0{jj} = zeros(nsd, 1);
    %         OPT.feedbackdelta{jj} = zeros(nq, 1);
    
    % (Expanded) variable steps and new multipliers
    OPT.step{jj}.primal.q = zeros(nq, ns);
    OPT.step{jj}.primal.sd = zeros(nsd, ns+1);
    if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
      OPT.step{jj}.primal.delq = zeros(nq, ns-1+1);
      if isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')
        OPT.step{jj}.dual.lambda1 = zeros(nq, ns-1+1);
        OPT.step{jj}.dual.lambda2 = zeros(nq, ns-1+1);
        OPT.step{jj}.dual.mu_delq = zeros(nq, ns-1+1);
        OPT.step{jj}.dualstep.lambda1 = zeros(nq, ns-1+1);
        OPT.step{jj}.dualstep.lambda2 = zeros(nq, ns-1+1);
        OPT.step{jj}.dualstep.mu_delq = zeros(nq, ns-1+1);
      end
    end
    OPT.step{jj}.dual.lambda = zeros(nsd, ns);
    OPT.step{jj}.dual.mu_sd = zeros(nsd, ns+1);
    OPT.step{jj}.dual.mu_q = zeros(nq, ns);
    if nrds > 0
      OPT.step{jj}.dual.mu_rds = zeros(nrds, 1);
    end
    if nrdi > 0
      OPT.step{jj}.dual.mu_rdi = zeros(nrdi, ns-1);
    end
    if nrde > 0
      OPT.step{jj}.dual.mu_rde = zeros(nrde, 1);
    end
    OPT.step{jj}.dualstep.lambda = zeros(nsd, ns);
    OPT.step{jj}.dualstep.mu_sd = zeros(nsd, ns+1);
    OPT.step{jj}.dualstep.mu_q = zeros(nq, ns);
    if nrds > 0
      OPT.step{jj}.dualstep.mu_rds = zeros(nrds, 1);
    end
    if nrdi > 0
      OPT.step{jj}.dualstep.mu_rdi = zeros(nrdi, ns-1);
    end
    if nrde > 0
      OPT.step{jj}.dualstep.mu_rde = zeros(nrde, 1);
    end
    
    if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
      OPT.hess{jj}.lbfgshess = cell(ns+1,1);
      for ii = 1:ns
        OPT.hess{jj}.lbfgshess{ii} = lbfgshess_init(nsd+nq,20,OPT.lbfgs_gamma);
      end
      OPT.hess{jj}.lbfgshess{ns+1} = lbfgshess_init(nsd,20,OPT.lbfgs_gamma);
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
        OPT.hess{jj}.lbfgsCMRhess = cell(ns,1);
        for ii = 1:ns
          OPT.hess{jj}.lbfgsCMRhess{ii} = lbfgshess_init(nq,20,OPT.lbfgs_gamma);
        end
      end
    end
  end
  
  if OPT.initial_iterations > 0
    fprintf('\nQP Initial iterations not supported for ST case.\n\n');
    %         fprintf('\nInitialization completed.\n\n');
  end
  
  OPT.sigmaB = zeros(1,SCEN.steps);
  OPT.sigmaC = zeros(1,SCEN.steps);
  
end


