function fcn = eval_nlp_obj(vars)

global SCEN OPT ST

if SCEN.use_st == 0
    fcn = @eval;
else
    fcn = @evalST;
end


ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
nmfcn = OPT.dims.mfcn;
nlfcn = OPT.dims.lfcn;
CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;

  function [obj] = eval(x)
    
    vars.primal.sd = reshape(x(1:nsd*(ns+1)), nsd, ns+1);
    vars.primal.q = reshape(x(nsd*(ns+1)+(1:nq*ns)), nq, ns);
    vars.objective.sigma = -1;
    if CMR
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        vars.primal.delq = reshape(x((nsd+nq)*ns+nsd+(1:(nq*(ns-1+1)))), nq, ns-1+1);
      else
        for ii = 1:ns-1
          vars.primal.primal.delq(:,ii) = vars.primal.q(:,ii+1) - vars.primal.q(:,ii);
        end
        vars.primal.delq(:,ns) = vars.primal.q(:,1) - OPT.controlmove_q0;
      end
    end
    
    OPT.nlp.data = mli_evalNLP(vars);
    
    obj = 0;
    
    for msn=1:ns
      
      if nlfcn > 0
        
        obj = obj + OPT.nlp.data.res.lfcn(:,msn);
      
      end
      
    end
    
    if nmfcn > 0 && nlfcn <= 0
      if isfield(OPT,'periodicity')
        %self tracking part (objective)
        for ii=1:OPT.periodicity.transient
          obj=obj+OPT.periodicity.alpha*(OPT.periodicity.discount^ii)*((OPT.nlp.data.res.mfun{ii}.end-OPT.nlp.data.res.mfun{ii}.start)-(OPT.nlp.data.res.mfun{ii+OPT.periodicity.periodic}.end-OPT.nlp.data.res.mfun{ii+OPT.periodicity.periodic}.start))^2;
        end
        %add one state tracking term at the end of the transient horizon
        if OPT.periodicity.transient>=1
          %add one state tracking term at the end of the transient horizon
          state_diff=vars.primal.sd(:,OPT.periodicity.transient)-vars.primal.sd(:,OPT.periodicity.transient+OPT.periodicity.periodic);
          obj=obj+state_diff'*OPT.periodicity.beta.sd'*OPT.periodicity.beta.sd*state_diff;
          %add one control tracking term at the end of the transient horizon
          control_diff=vars.primal.q(:,OPT.periodicity.transient)-vars.primal.q(:,OPT.periodicity.transient+OPT.periodicity.periodic);
          obj=obj+control_diff'*OPT.periodicity.beta.q'*OPT.periodicity.beta.q*control_diff;
        end
        %economic output of periodic part
        for ii=OPT.periodicity.transient+1:OPT.periodicity.transient+OPT.periodicity.periodic
          obj=obj+(OPT.nlp.data.res.mfun{ii}.end-OPT.nlp.data.res.mfun{ii}.start);
        end
      else
        obj = obj + OPT.nlp.data.res.mfun;
      end
    end
    
     if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
       vars.primal.delq = reshape(x((nsd+nq)*ns+nsd+(1:nq*(ns-1+1))),nq,ns-1+1);
       if isfield(OPT, 'controlmove_R')
         for kk = 1:ns-1
           obj = obj + vars.primal.delq(:,kk)' * OPT.controlmove_R * vars.primal.delq(:,kk);
         end
         obj = obj + vars.primal.delq(:,ns)' * OPT.controlmove_R0 * vars.primal.delq(:,ns);
       end
       if isfield(OPT, 'controlmove_c')
         obj = obj + OPT.controlmove_c' * sum(vars.primal.delq(:,1:ns-1),2);
         obj = obj + OPT.controlmove_c0' * vars.primal.delq(:,ns);
       end
     end
  end

  function [obj] = evalST(x)
    
    S = ST.S;
    for jj = 1:S
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
        nz = (nsd+nq)*ns+nsd+nq*(ns-1+1);
        offset = (jj-1)*nz;
      else
        nz = (nsd+nq)*ns+nsd;
        offset = (jj-1)*nz;
      end
      vars{jj}.primal.sd = reshape(x(offset+(1:nsd*(ns+1))), nsd, ns+1);
      vars{jj}.primal.q = reshape(x(offset+(nsd*(ns+1)+(1:nq*ns))), nq, ns);
      vars{jj}.objective.sigma = -1;
      vars{jj}.primal.z_ipopt = x(offset+(1:nz))';
      if CMR
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
          vars{jj}.primal.delq = reshape(x(offset+((nsd+nq)*ns+nsd+(1:(nq*(ns-1+1))))), nq, ns-1+1);
        else
          for ii = 1:ns-1
            vars{jj}.primal.delq(:,ii) = vars{jj}.primal.q(:,ii+1) - vars{jj}.primal.q(:,ii);
          end
          vars{jj}.primal.delq(:,ns) = vars{jj}.primal.q(:,1) - OPT.controlmove_q0;
        end
      end
    end
    
    OPT.nlp.data = mli_evalNLP(vars);
    
    obj_scen = zeros(S,1);
    for jj = 1:S
      for msn=1:ns
        
        if nlfcn > 0
          
          obj_scen(jj) = obj_scen(jj) + OPT.nlp.data{jj}.res.lfcn(:,msn);
          
        end
        
      end
      
      if nmfcn > 0 && nlfcn <= 0
        obj_scen(jj) = obj_scen(jj) + OPT.nlp.data{jj}.res.mfun;
      end
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
        offset = (jj-1)*((nsd+nq)*ns+nsd+nq*(ns-1+1));
        vars{jj}.primal.delq = reshape(x(offset+((nsd+nq)*ns+nsd+(1:nq*(ns-1+1)))),nq,ns-1+1);
        if isfield(OPT, 'controlmove_R')
          for kk = 1:ns-1
            obj_scen(jj) = obj_scen(jj) + vars{jj}.primal.delq(:,kk)' * OPT.controlmove_R * vars{jj}.primal.delq(:,kk);
          end
          obj_scen(jj) = obj_scen(jj) + vars{jj}.primal.delq(:,ns)' * OPT.controlmove_R0 * vars{jj}.primal.delq(:,ns);
        end
        if isfield(OPT, 'controlmove_c')
          obj_scen(jj) = obj_scen(jj) + OPT.controlmove_c' * sum(vars{jj}.primal.delq,2);
          obj_scen(jj) = obj_scen(jj) + OPT.controlmove_c' * sum(vars{jj}.primal.delq(:,1:ns-1),2);
          obj_scen(jj) = obj_scen(jj) + OPT.controlmove_c0' * vars{jj}.primal.delq(:,ns);
        end
      end
    end
    obj = ST.weights*obj_scen;
  end

end