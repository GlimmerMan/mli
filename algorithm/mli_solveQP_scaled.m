function [x,status,y] = mli_solveQP_scaled(options)

global OPT;

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;

dimR1 = nsd + (nq*ns);
dimR2 = nsd * ns;

if 0
    
    A=[[OPT.qp.H,OPT.qp.A'];[OPT.qp.A,zeros(dimR2,dimR2)]];
    [r,~]=scaleqp(A);

    R1=diag(r(1:dimR1));
    R2=diag(r(dimR1+1:end));
    
end
if 1
    
    mli_scaleQP;
    
end

R1 = OPT.qp.R1;
R2 = OPT.qp.R2;

% Scale objective components
OPT.qp.Hs = R1 * OPT.qp.H * R1;
OPT.qp.gs = R1 * OPT.qp.g;

% Scale simple bounds
OPT.qp.lbs = inv(R1) * OPT.qp.lb;
OPT.qp.ubs = inv(R1) * OPT.qp.ub;

% Scale constraint matrix and its bounds
OPT.qp.As = R2 * OPT.qp.A * R1;
OPT.qp.ineqlbs = R2 * OPT.qp.ineqlb;
OPT.qp.inequbs = R2 * OPT.qp.inequb;

% Solve scaled QP
[x, ~, status, ~, y] = qpOASES_sequence('m', OPT.qp.qphandle,...
    OPT.qp.Hs, OPT.qp.gs,OPT.qp.As,...
    OPT.qp.lbs,OPT.qp.ubs,...
    OPT.qp.ineqlbs,OPT.qp.inequbs,...
    options);

% Rescale primal variables
x = R1 * x;

% Rescale dual variables
y(1:dimR1) = inv(R1) * y(1:dimR1);
y(dimR1+(1:dimR2)) = R2 * y(dimR1+(1:dimR2));

end

