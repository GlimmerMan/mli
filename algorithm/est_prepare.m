% -*-matlab-*-
function est_prepare

global EST;

cursamp = EST.currentsample;
ns = EST.dims.nshoot;

if cursamp <= ns
    est_evaluate_growinghorizon;
    est_gnhess_growinghorizon;
    %disp('Growing horizon phase: finished preparation');
else
    est_evaluate;
    est_gnhess;
end




