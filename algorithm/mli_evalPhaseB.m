% -*-matlab-*-
function [data] = mli_evalPhaseB(vars,ref)

global OPT

tic


ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd; 
nq = OPT.dims.nq; 
np = OPT.dims.np;
nlsqs = OPT.dims.lsqfcn_s;
nlsqi = OPT.dims.lsqfcn_i; 
nlsqe = OPT.dims.lsqfcn_e;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nmfcn = OPT.dims.mfcn;
nlfcn = OPT.dims.lfcn;

pvars = vars.primal;

sddiff = pvars.sd - ref.primal.sd;
qdiff = pvars.q - ref.primal.q;

%%% Start node
msn = 1;

% dynamical equations
inp = [];
inp.thoriz = [OPT.discr.msgrid(msn) OPT.discr.msgrid(msn+1)];
inp.sd = pvars.sd(:,msn);
inp.q = pvars.q(:,msn);
inp.p = pvars.pconst;

outp = eval_dyn_B(inp);

data.res.xd(:,msn) = outp.x;
data.res.c(:,msn) = outp.x - pvars.sd(:,msn+1);

data.res.modgradx(:,msn) = OPT.eval.res.refgradx(:,msn) + ...
OPT.hess.ssblock{msn} * sddiff(:,msn) + OPT.hess.sqblock{msn} * qdiff(:,msn);  

data.res.modgradq(:,msn) = OPT.eval.res.refgradq(:,msn) + ...
OPT.hess.sqblock{msn}' * sddiff(:,msn) + OPT.hess.qqblock{msn} * qdiff(:,msn);

if nlfcn > 0
    data.res.lfcn(msn) = outp.lfcn;
end

% least-squares objective
if nlsqs > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn);
  inp.p = pvars.pconst;

  outp = eval_lsqfcn_B(inp,'s');

  data.res.lsqfcn_s = outp.lsqfcn;
end

% decoupled node constraint
if nrds > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn);
  inp.p = pvars.pconst;

  outp = eval_rdfcn_B(inp,'s');
  
  data.res.rds = outp.rdfcn;
end

%%% Interior nodes
for msn=2:ns
% dynamical equations
  inp = [];
  inp.thoriz = [OPT.discr.msgrid(msn) OPT.discr.msgrid(msn+1)];
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn);
  inp.p = pvars.pconst;
  
  outp = eval_dyn_B(inp);
  
  data.res.xd(:,msn) = outp.x;
  data.res.c(:,msn) = outp.x - pvars.sd(:,msn+1);
  
  data.res.modgradx(:,msn) = OPT.eval.res.refgradx(:,msn) + ...
  OPT.hess.ssblock{msn} * sddiff(:,msn) + OPT.hess.sqblock{msn} * qdiff(:,msn);  
  
  data.res.modgradq(:,msn) = OPT.eval.res.refgradq(:,msn) + ...
  OPT.hess.sqblock{msn}' * sddiff(:,msn) + OPT.hess.qqblock{msn} * qdiff(:,msn);

  if nlfcn > 0
    data.res.lfcn(msn) = outp.lfcn;
  end

% least-squares objective
  if nlsqi > 0
    inp = [];
    inp.t = OPT.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.q = pvars.q(:,msn);
    inp.p = pvars.pconst;
    
    outp = eval_lsqfcn_B(inp,'i');
    
    data.res.lsqfcn_i(:,msn-1) = outp.lsqfcn;
  end

% decoupled node constraint
  if nrdi > 0
    inp = [];
    inp.t = OPT.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.q = pvars.q(:,msn);
    inp.p = pvars.pconst;
    
    outp = eval_rdfcn_B(inp,'i');
    
    data.res.rdi(:,msn-1) = outp.rdfcn;
  end
 
end

%%% End node
msn = ns + 1;

% least-squares objective
if nlsqe > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn-1);
  inp.p = pvars.pconst;
  
  outp = eval_lsqfcn_B(inp,'e');
  
  data.res.lsqfcn_e = outp.lsqfcn;
end

% decoupled node constraint
if nrde > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn);
  inp.p = pvars.pconst;
  
  outp = eval_rdfcn_B(inp,'e');
  
  data.res.rde = outp.rdfcn;
end

% Mayer objective
if nmfcn > 0 && nlfcn <= 0
    inp = [];
    inp.t = OPT.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.q = pvars.q(:,msn-1);
    inp.p = pvars.pconst;
    
    outp = eval_mfcn_B(inp);
    
    data.res.mfun = outp.mfcn;
end

data.res.modgradx(:,msn) = OPT.eval.res.refgradx(:,msn) + ...
OPT.hess.ssblock{msn} * sddiff(:,msn);

OPT.evaltime = toc;



