% -*-matlab-*-
function mli_dataTransferD

global SCEN OPT ST
    
nmfcn = OPT.dims.mfcn;
nlfcn = OPT.dims.lfcn;
nlsqs = OPT.dims.lsqfcn_s;
nlsqi = OPT.dims.lsqfcn_i;
nlsqe = OPT.dims.lsqfcn_e;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;

if SCEN.use_st == 0
    % >=B data
    if nlfcn > 0
      OPT.eval.res.lfcn = OPT.phase{4}.data.res.lfcn;
    end

    if nmfcn > 0 && nlfcn <= 0
        OPT.eval.res.mfun = OPT.phase{4}.data.res.mfun;
    end
    
    if nlsqs > 0
        OPT.eval.res.lsqfcn_s = OPT.phase{4}.data.res.lsqfcn_s;
    end
    if nlsqi > 0
        OPT.eval.res.lsqfcn_i = OPT.phase{4}.data.res.lsqfcn_i;
    end
    if nlsqe > 0
        OPT.eval.res.lsqfcn_e = OPT.phase{4}.data.res.lsqfcn_e;
    end
    
    if nrds > 0
        OPT.eval.res.rds = OPT.phase{4}.data.res.rds;
    end
    if nrdi > 0
        OPT.eval.res.rdi = OPT.phase{4}.data.res.rdi;
    end
    if nrde > 0
        OPT.eval.res.rde = OPT.phase{4}.data.res.rde;
    end
    
    OPT.eval.res.xd = OPT.phase{4}.data.res.xd;
    OPT.eval.res.c = OPT.phase{4}.data.res.c;
    OPT.eval.res.modgradx = OPT.phase{4}.data.res.modgradx;
    OPT.eval.res.modgradq = OPT.phase{4}.data.res.modgradq;
    if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1 && (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
      OPT.eval.res.modgraddelq = OPT.phase{4}.data.res.modgraddelq;
    end
    % >=C data
    if  nlfcn > 0
	    OPT.eval.res.dlfcn = OPT.phase{4}.data.res.dlfcn;
    end

    if nmfcn > 0 && nlfcn <= 0
        OPT.eval.res.mgrad = OPT.phase{4}.data.res.mgrad;
    end
    
    if nlsqs > 0
        OPT.eval.res.lsqsgradx = OPT.phase{4}.data.res.lsqsgradx;
        OPT.eval.res.lsqsgradq = OPT.phase{4}.data.res.lsqsgradq;
    end
    if nlsqi > 0
        OPT.eval.res.lsqigradx = OPT.phase{4}.data.res.lsqigradx;
        OPT.eval.res.lsqigradq = OPT.phase{4}.data.res.lsqigradq;
    end
    if nlsqe > 0
        OPT.eval.res.lsqegradx = OPT.phase{4}.data.res.lsqegradx;
    end
    
    if nrds > 0
        OPT.eval.res.muJrdsx = OPT.phase{4}.data.res.muJrdsx;
        OPT.eval.res.muJrdsq = OPT.phase{4}.data.res.muJrdsq;
    end
    if nrdi > 0
        OPT.eval.res.muJrdix = OPT.phase{4}.data.res.muJrdix;
        OPT.eval.res.muJrdiq = OPT.phase{4}.data.res.muJrdiq;
    end
    if nrde > 0
        OPT.eval.res.muJrdex = OPT.phase{4}.data.res.muJrdex;
    end
    
    OPT.eval.res.lambdaGx = OPT.phase{4}.data.res.lambdaGx;
    OPT.eval.res.lambdaGq = OPT.phase{4}.data.res.lambdaGq;
    OPT.eval.res.refgradx = OPT.phase{4}.data.res.refgradx;
    OPT.eval.res.refgradq = OPT.phase{4}.data.res.refgradq;
    OPT.eval.res.laggradx = OPT.phase{4}.data.res.laggradx;
    OPT.eval.res.laggradq = OPT.phase{4}.data.res.laggradq;
    if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1 && (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
      OPT.eval.res.laggraddelq = OPT.phase{4}.data.res.laggraddelq;
      OPT.eval.res.refgraddelq = OPT.phase{4}.data.res.refgraddelq;
    end
    
    % >=D data
    OPT.eval.mat.Gx = OPT.phase{4}.data.mat.Gx;
    OPT.eval.mat.Gq = OPT.phase{4}.data.mat.Gq;
    
    if nrds > 0
        OPT.eval.mat.Jrdsx = OPT.phase{4}.data.mat.Jrdsx;
        OPT.eval.mat.Jrdsq = OPT.phase{4}.data.mat.Jrdsq;
    end
    if nrdi > 0
        OPT.eval.mat.Jrdix = OPT.phase{4}.data.mat.Jrdix;
        OPT.eval.mat.Jrdiq = OPT.phase{4}.data.mat.Jrdiq;
    end
    if nrde > 0
        OPT.eval.mat.Jrdex = OPT.phase{4}.data.mat.Jrdex;
    end
    
    switch OPT.algo.hessian
        
        case 'exact'
            
            if nmfcn > 0 && nlfcn <= 0
                OPT.eval.mat.mhess = OPT.phase{4}.data.mat.mhess;
            end
            if nlsqs > 0
                OPT.eval.mat.Jlsqsx = OPT.phase{4}.data.mat.Jlsqsx;
                OPT.eval.mat.Jlsqsq = OPT.phase{4}.data.mat.Jlsqsq;
                OPT.eval.mat.Hlsqsxx = OPT.phase{4}.data.mat.Hlsqsxx;
                OPT.eval.mat.Hlsqsxq = OPT.phase{4}.data.mat.Hlsqsxq;
                OPT.eval.mat.Hlsqsqq = OPT.phase{4}.data.mat.Hlsqsqq;
            end
            if nlsqi > 0
                OPT.eval.mat.Jlsqix = OPT.phase{4}.data.mat.Jlsqix;
                OPT.eval.mat.Jlsqiq = OPT.phase{4}.data.mat.Jlsqiq;
                OPT.eval.mat.Hlsqixx = OPT.phase{4}.data.mat.Hlsqixx;
                OPT.eval.mat.Hlsqixq = OPT.phase{4}.data.mat.Hlsqixq;
                OPT.eval.mat.Hlsqiqq = OPT.phase{4}.data.mat.Hlsqiqq;
            end
            if nlsqe > 0
                OPT.eval.mat.Jlsqex = OPT.phase{4}.data.mat.Jlsqex;
                OPT.eval.mat.Hlsqexx = OPT.phase{4}.data.mat.Hlsqexx;
            end
            
            OPT.eval.mat.lambdaGxx = OPT.phase{4}.data.mat.lambdaGxx;
            OPT.eval.mat.lambdaGxq = OPT.phase{4}.data.mat.lambdaGxq;
            OPT.eval.mat.lambdaGqq = OPT.phase{4}.data.mat.lambdaGqq;
            
            if nrds > 0
                OPT.eval.mat.muJrdsxx = OPT.phase{4}.data.mat.muJrdsxx;
                OPT.eval.mat.muJrdsxq = OPT.phase{4}.data.mat.muJrdsxq;
                OPT.eval.mat.muJrdsqq = OPT.phase{4}.data.mat.muJrdsqq;
            end
            if nrdi > 0
                OPT.eval.mat.muJrdixx = OPT.phase{4}.data.mat.muJrdixx;
                OPT.eval.mat.muJrdixq = OPT.phase{4}.data.mat.muJrdixq;
                OPT.eval.mat.muJrdiqq = OPT.phase{4}.data.mat.muJrdiqq;
            end
            if nrde > 0
                OPT.eval.mat.muJrdexx = OPT.phase{4}.data.mat.muJrdexx;
            end
            
        case 'gn'
            
            if nlsqs > 0
                OPT.eval.mat.Jlsqsx = OPT.phase{4}.data.mat.Jlsqsx;
                OPT.eval.mat.Jlsqsq = OPT.phase{4}.data.mat.Jlsqsq;
            end
            if nlsqi > 0
                OPT.eval.mat.Jlsqix = OPT.phase{4}.data.mat.Jlsqix;
                OPT.eval.mat.Jlsqiq = OPT.phase{4}.data.mat.Jlsqiq;
            end
            if nlsqe > 0
                OPT.eval.mat.Jlsqex = OPT.phase{4}.data.mat.Jlsqex;
            end               
    end

else
    S = ST.S;
    if nmfcn > 0 && nlfcn <= 0
        for jj = 1:S
        OPT.eval{jj}.res.mfun = OPT.phase{4}{jj}.data.res.mfun;
        end
    end
    
    if nlsqs > 0
        for jj = 1:S
        OPT.eval{jj}.res.lsqfcn_s = OPT.phase{4}{jj}.data.res.lsqfcn_s;
        end
    end
    if nlsqi > 0
        for jj = 1:S
        OPT.eval{jj}.res.lsqfcn_i = OPT.phase{4}{jj}.data.res.lsqfcn_i;
        end
    end
    if nlsqe > 0
        for jj = 1:S
        OPT.eval{jj}.res.lsqfcn_e = OPT.phase{4}{jj}.data.res.lsqfcn_e;
        end
    end
    
    if nrds > 0
        for jj = 1:S
        OPT.eval{jj}.res.rds = OPT.phase{4}{jj}.data.res.rds;
        end
    end
    if nrdi > 0
        for jj = 1:S
        OPT.eval{jj}.res.rdi = OPT.phase{4}{jj}.data.res.rdi;
        end
    end
    if nrde > 0
        for jj = 1:S
        OPT.eval{jj}.res.rde = OPT.phase{4}{jj}.data.res.rde;
        end
    end
    
    for jj = 1:S
    OPT.eval{jj}.res.xd = OPT.phase{4}{jj}.data.res.xd;
    OPT.eval{jj}.res.c = OPT.phase{4}{jj}.data.res.c;
    OPT.eval{jj}.res.modgradx = OPT.phase{4}{jj}.data.res.modgradx;
    OPT.eval{jj}.res.modgradq = OPT.phase{4}{jj}.data.res.modgradq;
    end
    
    % >=C data
    if nmfcn > 0 && nlfcn <= 0
        for jj = 1:S
        OPT.eval{jj}.res.mgrad = OPT.phase{4}{jj}.data.res.mgrad;
        end
    end
    
    if nlsqs > 0
        for jj = 1:S
        OPT.eval{jj}.res.lsqsgradx = OPT.phase{4}{jj}.data.res.lsqsgradx;
        OPT.eval{jj}.res.lsqsgradq = OPT.phase{4}{jj}.data.res.lsqsgradq;
        end
    end
    if nlsqi > 0
        for jj = 1:S
        OPT.eval{jj}.res.lsqigradx = OPT.phase{4}{jj}.data.res.lsqigradx;
        OPT.eval{jj}.res.lsqigradq = OPT.phase{4}{jj}.data.res.lsqigradq;
        end
    end
    if nlsqe > 0
        for jj = 1:S
        OPT.eval{jj}.res.lsqegradx = OPT.phase{4}{jj}.data.res.lsqegradx;
        end
    end
    
    if nrds > 0
        for jj = 1:S
        OPT.eval{jj}.res.muJrdsx = OPT.phase{4}{jj}.data.res.muJrdsx;
        OPT.eval{jj}.res.muJrdsq = OPT.phase{4}{jj}.data.res.muJrdsq;
        end
    end
    if nrdi > 0
        for jj = 1:S
        OPT.eval{jj}.res.muJrdix = OPT.phase{4}{jj}.data.res.muJrdix;
        OPT.eval{jj}.res.muJrdiq = OPT.phase{4}{jj}.data.res.muJrdiq;
        end
    end
    if nrde > 0
        for jj = 1:S
        OPT.eval{jj}.res.muJrdex = OPT.phase{4}{jj}.data.res.muJrdex;
        end
    end
    
    for jj = 1:S
    OPT.eval{jj}.res.lambdaGx = OPT.phase{4}{jj}.data.res.lambdaGx;
    OPT.eval{jj}.res.lambdaGq = OPT.phase{4}{jj}.data.res.lambdaGq;
    OPT.eval{jj}.res.refgradx = OPT.phase{4}{jj}.data.res.refgradx;
    OPT.eval{jj}.res.refgradq = OPT.phase{4}{jj}.data.res.refgradq;
    OPT.eval{jj}.res.laggradx = OPT.phase{4}{jj}.data.res.laggradx;
    OPT.eval{jj}.res.laggradq = OPT.phase{4}{jj}.data.res.laggradq;

    
    % >=D data
    OPT.eval{jj}.mat.Gx = OPT.phase{4}{jj}.data.mat.Gx;
    OPT.eval{jj}.mat.Gq = OPT.phase{4}{jj}.data.mat.Gq;
    end
    
    if nrds > 0
        for jj = 1:S
        OPT.eval{jj}.mat.Jrdsx = OPT.phase{4}{jj}.data.mat.Jrdsx;
        OPT.eval{jj}.mat.Jrdsq = OPT.phase{4}{jj}.data.mat.Jrdsq;
        end
    end
    if nrdi > 0
        for jj = 1:S
        OPT.eval{jj}.mat.Jrdix = OPT.phase{4}{jj}.data.mat.Jrdix;
        OPT.eval{jj}.mat.Jrdiq = OPT.phase{4}{jj}.data.mat.Jrdiq;
        end
    end
    if nrde > 0
        for jj = 1:S
        OPT.eval{jj}.mat.Jrdex = OPT.phase{4}{jj}.data.mat.Jrdex;
        end
    end
    
    switch OPT.algo.hessian
        
        case 'exact'
            
            if nmfcn > 0 && nlfcn <= 0
                for jj = 1:S
                OPT.eval{jj}.mat.mhess = OPT.phase{4}{jj}.data.mat.mhess;
                end
            end
            if nlsqs > 0
                for jj = 1:S
                OPT.eval{jj}.mat.Jlsqsx = OPT.phase{4}{jj}.data.mat.Jlsqsx;
                OPT.eval{jj}.mat.Jlsqsq = OPT.phase{4}{jj}.data.mat.Jlsqsq;
                OPT.eval{jj}.mat.Hlsqsxx = OPT.phase{4}{jj}.data.mat.Hlsqsxx;
                OPT.eval{jj}.mat.Hlsqsxq = OPT.phase{4}{jj}.data.mat.Hlsqsxq;
                OPT.eval{jj}.mat.Hlsqsqq = OPT.phase{4}{jj}.data.mat.Hlsqsqq;
                end
            end
            if nlsqi > 0
                for jj = 1:S
                OPT.eval{jj}.mat.Jlsqix = OPT.phase{4}{jj}.data.mat.Jlsqix;
                OPT.eval{jj}.mat.Jlsqiq = OPT.phase{4}{jj}.data.mat.Jlsqiq;
                OPT.eval{jj}.mat.Hlsqixx = OPT.phase{4}{jj}.data.mat.Hlsqixx;
                OPT.eval{jj}.mat.Hlsqixq = OPT.phase{4}{jj}.data.mat.Hlsqixq;
                OPT.eval{jj}.mat.Hlsqiqq = OPT.phase{4}{jj}.data.mat.Hlsqiqq;
                end
            end
            if nlsqe > 0
                for jj = 1:S
                OPT.eval{jj}.mat.Jlsqex = OPT.phase{4}{jj}.data.mat.Jlsqex;
                OPT.eval{jj}.mat.Hlsqexx = OPT.phase{4}{jj}.data.mat.Hlsqexx;
                end
            end
            
            for jj = 1:S
            OPT.eval{jj}.mat.lambdaGxx = OPT.phase{4}{jj}.data.mat.lambdaGxx;
            OPT.eval{jj}.mat.lambdaGxq = OPT.phase{4}{jj}.data.mat.lambdaGxq;
            OPT.eval{jj}.mat.lambdaGqq = OPT.phase{4}{jj}.data.mat.lambdaGqq;
            end
            
            if nrds > 0
                for jj = 1:S
                OPT.eval{jj}.mat.muJrdsxx = OPT.phase{4}{jj}.data.mat.muJrdsxx;
                OPT.eval{jj}.mat.muJrdsxq = OPT.phase{4}{jj}.data.mat.muJrdsxq;
                OPT.eval{jj}.mat.muJrdsqq = OPT.phase{4}{jj}.data.mat.muJrdsqq;
                end
            end
            if nrdi > 0
                for jj = 1:S
                OPT.eval{jj}.mat.muJrdixx = OPT.phase{4}{jj}.data.mat.muJrdixx;
                OPT.eval{jj}.mat.muJrdixq = OPT.phase{4}{jj}.data.mat.muJrdixq;
                OPT.eval{jj}.mat.muJrdiqq = OPT.phase{4}{jj}.data.mat.muJrdiqq;
                end
            end
            if nrde > 0
                for jj = 1:S
                OPT.eval{jj}.mat.muJrdexx = OPT.phase{4}{jj}.data.mat.muJrdexx;
                end
            end
            
        case 'gn'
            
            if nlsqs > 0
                for jj = 1:S
                OPT.eval{jj}.mat.Jlsqsx = OPT.phase{4}{jj}.data.mat.Jlsqsx;
                OPT.eval{jj}.mat.Jlsqsq = OPT.phase{4}{jj}.data.mat.Jlsqsq;
                end
            end
            if nlsqi > 0
                for jj = 1:S
                OPT.eval{jj}.mat.Jlsqix = OPT.phase{4}{jj}.data.mat.Jlsqix;
                OPT.eval{jj}.mat.Jlsqiq = OPT.phase{4}{jj}.data.mat.Jlsqiq;
                end
            end
            if nlsqe > 0
                for jj = 1:S
                OPT.eval{jj}.mat.Jlsqex = OPT.phase{4}{jj}.data.mat.Jlsqex;
                end
            end
 
    end
    
end
