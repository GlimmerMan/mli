function [outp] = eval_dyn_C_est(inp)
    
    global SLV
    
    thoriz = inp.thoriz;    
    sd = inp.sd;
    q = inp.q;
    p = inp.p;    
    lambda = inp.lambda;
      
    nsd = length(sd);
 
    int = SLV.est.int;
    
    solvind('setTapeNumber', int, 1);
    solvind('setTimeHorizon',int, thoriz);
    solvind('setInitVals', int, [sd;q;p]);
    solvind('setForwardTaylorCoefficients', int, []);
    solvind('setAdjointTaylorCoefficients', int, 1, 0, 1, lambda);

    status = solvind('evaluate', int);
    if status ~= 0 
        error('integration failed');
    end
    
    sol = solvind('getSolution', int);
    outp.x = sol(1:nsd);
         
    help = solvind('getAdjSens', int);
    outp.lag = help(2:end);
    

    
