function mli_initialize_solvind

global SLV OPT EST SCEN;

solvind('reset');

% Reality setup
solvind('importDynamicModelLib', SCEN.model.file );
SLV.real.model = solvind('createDynamicModel', SCEN.model.name, ...
    SCEN.model.optionstr );
dims = solvind('getDims', SLV.real.model);
ctrlList = cell(1, dims.nu);
strCtrl = 'Piecewise_Constant';
for i = 1:dims.nu
    ctrlList{i} = strCtrl;
end
SLV.real.ctrls = solvind('createControlVector',  ctrlList );
SLV.real.int = solvind('createIntegrator', SCEN.discr.int_name );
solvind('setModel', SLV.real.int, SLV.real.model);
solvind('setControlVector', SLV.real.int, SLV.real.ctrls);
solvind('setRelTol', SLV.real.int, SCEN.discr.int_tol);
solvind('setNumOfTimeTrafoParams', SLV.real.int, 0);
solvind('setMaxBDFOrder', SLV.real.int, 6);
solvind('setTapeStorageMode', SLV.real.int, 'off');
solvind('setPrintLevel', SLV.real.int, 0);

% Controller setup
if SCEN.use_controller
    solvind('importDynamicModelLib', OPT.model.file );
    SLV.opt.model = solvind('createDynamicModel', OPT.model.name, ...
        OPT.model.optionstr );
    dims = solvind('getDims', SLV.opt.model);
    ctrlList = cell(1, dims.nu);
    strCtrl = 'Piecewise_Constant';
    for i = 1:dims.nu
        ctrlList{i} = strCtrl;
    end
    SLV.opt.ctrls = solvind('createControlVector',  ctrlList );
    SLV.opt.int = solvind('createIntegrator', OPT.discr.int_name );
    solvind('setModel', SLV.opt.int, SLV.opt.model);
    solvind('setControlVector', SLV.opt.int, SLV.opt.ctrls);
    solvind('setRelTol', SLV.opt.int, OPT.discr.int_tol);
    solvind('setNumOfTimeTrafoParams', SLV.opt.int, 0);
    solvind('setMaxBDFOrder', SLV.opt.int, OPT.discr.int_maxorder);
    solvind('setTapeStorageMode', SLV.opt.int, 'values');
    solvind('setMaxIntSteps', SLV.opt.int, OPT.discr.int_maxstep);
    solvind('setPrintLevel', SLV.opt.int, OPT.discr.int_plevel);
    %	solvind('setCorrectorAccuracyFactor', SLV.opt.int, 0);
    %	solvind('setCorrectorAbsoluteAccuracy', SLV.opt.int, 1e-13);
    
    SLV.opt.evl = solvind('createIntegratorEvaluator', SLV.opt.model, ...
        SLV.opt.ctrls);
    solvind('setOption', SLV.opt.evl, 'PreferADDerivatives', 'on');
    solvind('useTimeTrafo', SLV.opt.evl, 'no');
end

if SCEN.use_estimator
    solvind('importDynamicModelLib', EST.model.file );
    SLV.est.model = solvind('createDynamicModel', EST.model.name, ...
        EST.model.optionstr );
    dims = solvind('getDims', SLV.est.model);
    ctrlList = cell(1, dims.nu);
    strCtrl = 'Piecewise_Constant';
    for i = 1:dims.nu
        ctrlList{i} = strCtrl;
    end
    SLV.est.ctrls = solvind('createControlVector',  ctrlList );
    SLV.est.int = solvind('createIntegrator', EST.discr.int_name );
    solvind('setModel', SLV.est.int, SLV.est.model);
    solvind('setControlVector', SLV.est.int, SLV.est.ctrls);
    solvind('setRelTol', SLV.est.int, EST.discr.int_tol);
    solvind('setNumOfTimeTrafoParams', SLV.est.int, 0);
    solvind('setMaxBDFOrder', SLV.est.int, EST.discr.int_maxorder);
    solvind('setTapeStorageMode', SLV.est.int, 'values');
    solvind('setMaxIntSteps', SLV.est.int, EST.discr.int_maxstep);
    solvind('setPrintLevel', SLV.est.int, EST.discr.int_plevel);
    
    SLV.est.evl = solvind('createIntegratorEvaluator', SLV.est.model, ...
        SLV.est.ctrls);
    solvind('setOption', SLV.est.evl, 'PreferADDerivatives', 'on');
    solvind('useTimeTrafo', SLV.est.evl, 'no');
end

