function fcn = eval_nlp_cst_jac(vars)
global SCEN OPT ST

if SCEN.use_st == 0
    fcn = @eval;
else
    fcn = @evalST;
end

ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd;
nq = OPT.dims.nq;

shift = nsd*(nsd+nq) + nsd;
nnz = ns * shift;

%periodicity
if isfield(OPT,'periodicity')
  nnz=nnz+2*OPT.dims.periodicity*nsd;
end

CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;

ridx = zeros(nnz,1);
cidx = zeros(nnz,1);
vals = zeros(nnz,1);

for ii=1:ns
  
  xidx = (ii-1)*nsd;
  qidx = nsd*(ns+1) + (ii-1)*nq;
  
  ridx((ii-1)*shift+(1:shift)) = xidx + ...
    [kron(ones(nsd,1),(1:nsd)'); ...
    kron(ones(nq,1),(1:nsd)'); ...
    (1:nsd)'];
  
  cidx((ii-1)*shift+(1:shift)) = ...
    [xidx + kron((1:nsd)',ones(nsd,1)); ...
    qidx + kron((1:nq)',ones(nsd,1)); ...
    (xidx+nsd) + (1:nsd)'];
  
end

% periodicity
if isfield(OPT,'periodicity')
  xidx_end=ns*nsd;
  ridx(ns*shift+(1:OPT.dims.periodicity*nsd))=ns*nsd+[kron(ones(nsd,1),(1:OPT.dims.periodicity)')];
  cidx(ns*shift+(1:OPT.dims.periodicity*nsd))=xidx_end+[kron((1:nsd)',ones(OPT.dims.periodicity,1))];
  xidx_start=(ns-OPT.periodicity.periodic)*nsd;
  ridx(ns*shift+OPT.dims.periodicity*nsd+(1:OPT.dims.periodicity*nsd))=ns*nsd+[kron(ones(nsd,1),(1:OPT.dims.periodicity)')];
  cidx(ns*shift+OPT.dims.periodicity*nsd+(1:OPT.dims.periodicity*nsd))=xidx_start+[kron((1:nsd)',ones(OPT.dims.periodicity,1))];
end

  function [cst] = eval(x)
    
    vars.primal.sd = reshape(x(1:nsd*(ns+1)), nsd, ns+1);
    vars.primal.q = reshape(x(nsd*(ns+1)+(1:nq*ns)), nq, ns);
    vars.objective.sigma = -1;
    if CMR
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        vars.primal.delq = reshape(x((nsd+nq)*ns+nsd+(1:(nq*(ns-1+1)))), nq, ns-1+1);
      else
        for ii = 1:ns-1
          vars.primal.primal.delq(:,ii) = vars.primal.q(:,ii+1) - vars.primal.q(:,ii);
        end
        vars.primal.delq(:,ns) = vars.primal.q(:,1) - OPT.controlmove_q0;
      end
    end
    
    OPT.nlp.data = mli_evalNLP(vars);
    
    for msn=1:ns
      vals((msn-1)*shift+(1:shift)) = ...
        [reshape(OPT.nlp.data.mat.Gx{msn}, nsd*nsd, 1); ...
        reshape(OPT.nlp.data.mat.Gq{msn}, nsd*nq, 1); ...
        -ones(nsd, 1)];
    end

    if isfield(OPT,'periodicity')      
      % periodicity
      nnz_periodicity=2*OPT.dims.periodicity*nsd;
      vals_periodicity=zeros(nnz_periodicity,1);
      vals_periodicity(1:OPT.dims.periodicity*nsd)=reshape(OPT.nlp.data.periodicity.G_end,OPT.dims.periodicity*nsd,1);
      vals_periodicity(OPT.dims.periodicity*nsd+(1:OPT.dims.periodicity*nsd))=-reshape(OPT.nlp.data.periodicity.G_start,OPT.dims.periodicity*nsd,1);
      vals(ns*shift+(1:nnz_periodicity))=vals_periodicity;
    end


    if CMR
      xoffs = ns*nsd; %+nrds+nrdi+nrde;
      yoff1 = (ns+1)*nsd;
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        nz = ns*(nsd+nq)+nsd + (ns-1+1)*nq;
        nc = nsd*ns+2*(ns-1+1)*nq;
        xoffs2 = xoffs + (ns-1+1)*nq;
        yoff2 = ns*(nsd+nq)+nsd;
        A_CMR = sparse(nc,nz);
        for ii = 1:ns-1
          yidx = yoff1 + (ii-1)*nq+(1:nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yidx) = speye(nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yidx+nq) = -speye(nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yoff2+(ii-1)*nq+(1:nq)) = speye(nq);
          A_CMR(xoffs2+(ii-1)*nq+(1:nq),yidx) = -speye(nq);
          A_CMR(xoffs2+(ii-1)*nq+(1:nq),yidx+nq) = speye(nq);
          A_CMR(xoffs2+(ii-1)*nq+(1:nq),yoff2+(ii-1)*nq+(1:nq)) = speye(nq);
        end
        A_CMR(xoffs+(ns-1)*nq+(1:nq),yoff1+(1:nq)) = -speye(nq);
        A_CMR(xoffs+(ns-1)*nq+(1:nq),yoff2+(ns-1)*nq+(1:nq)) = speye(nq);
        A_CMR(xoffs2+(ns-1)*nq+(1:nq),yoff1+(1:nq)) = speye(nq);
        A_CMR(xoffs2+(ns-1)*nq+(1:nq),yoff2+(ns-1)*nq+(1:nq)) = speye(nq);
      else
        nz = ns*(nsd+nq)+nsd;
        nc = nsd*ns+(ns-1+1)*nq;
        A_CMR = sparse(nc,nz);
        for ii = 1:ns-1
          yidx = yoff1 + (ii-1)*nq+(1:nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yidx) = speye(nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yidx+nq) = -speye(nq);
        end
        A_CMR(xoffs+(ns-1)*nq+(1:nq),yoff1+(1:nq)) = -speye(nq);
      end
    else
      nz = ns*(nsd+nq)+nsd;
      nc = nsd*ns;
      % periodicity
      if isfield(OPT,'periodicity')
        nc=nc+OPT.dims.periodicity;
      end

      A_CMR = sparse(nc,nz);
    end
    
    cst = sparse(ridx, cidx, vals, nc, nz) + A_CMR;
  end

  function [cst] = evalST(x)
    
    S = ST.S;
    for jj = 1:S
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
        nz = (nsd+nq)*ns+nsd+nq*(ns-1+1);
        offset = (jj-1)*nz;
      else
        nz = (nsd+nq)*ns+nsd;
        offset = (jj-1)*nz;
      end
      vars{jj}.primal.sd = reshape(x(offset+(1:nsd*(ns+1))), nsd, ns+1);
      vars{jj}.primal.q = reshape(x(offset+(nsd*(ns+1)+(1:nq*ns))), nq, ns);
      vars{jj}.objective.sigma = -1;
      vars{jj}.primal.z_ipopt = x(offset+(1:nz))';
      if CMR
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
          vars{jj}.primal.delq = reshape(x(offset+((nsd+nq)*ns+nsd+(1:(nq*(ns-1+1))))), nq, ns-1+1);
        else
          for ii = 1:ns-1
            vars{jj}.primal.delq(:,ii) = vars{jj}.primal.q(:,ii+1) - vars{jj}.primal.q(:,ii);
          end
          vars{jj}.primal.delq(:,ns) = vars{jj}.primal.q(:,1) - OPT.controlmove_q0;
        end
      end
    end
    
    OPT.nlp.data = mli_evalNLP(vars);

    rowindex = [];
    columnindex = [];
    vals = [];
    vals_scen = zeros(nnz,1);
     
    if CMR
      xoffs = ns*nsd; %+nrds+nrdi+nrde;
      yoff1 = (ns+1)*nsd;
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        nz = ns*(nsd+nq)+nsd + (ns-1+1)*nq;
        nc = nsd*ns+2*(ns-1+1)*nq;
        xoffs2 = xoffs + (ns-1+1)*nq;
        yoff2 = ns*(nsd+nq)+nsd;
        A_CMR = sparse(nc,nz);
        for ii = 1:ns-1
          yidx = yoff1 + (ii-1)*nq+(1:nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yidx) = speye(nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yidx+nq) = -speye(nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yoff2+(ii-1)*nq+(1:nq)) = speye(nq);
          A_CMR(xoffs2+(ii-1)*nq+(1:nq),yidx) = -speye(nq);
          A_CMR(xoffs2+(ii-1)*nq+(1:nq),yidx+nq) = speye(nq);
          A_CMR(xoffs2+(ii-1)*nq+(1:nq),yoff2+(ii-1)*nq+(1:nq)) = speye(nq);
        end
        A_CMR(xoffs+(ns-1)*nq+(1:nq),yoff1+(1:nq)) = -speye(nq);
        A_CMR(xoffs+(ns-1)*nq+(1:nq),yoff2+(ns-1)*nq+(1:nq)) = speye(nq);
        A_CMR(xoffs2+(ns-1)*nq+(1:nq),yoff1+(1:nq)) = speye(nq);
        A_CMR(xoffs2+(ns-1)*nq+(1:nq),yoff2+(ns-1)*nq+(1:nq)) = speye(nq);
      else
        nz = ns*(nsd+nq)+nsd;
        nc = nsd*ns+(ns-1+1)*nq;
        A_CMR = sparse(nc,nz);
        for ii = 1:ns-1
          yidx = yoff1 + (ii-1)*nq+(1:nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yidx) = speye(nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yidx+nq) = -speye(nq);
        end
        A_CMR(xoffs+(ns-1)*nq+(1:nq),yoff1+(1:nq)) = -speye(nq);
      end
    else
      nz = ns*(nsd+nq)+nsd;
      nc = nsd*ns;
      A_CMR = [];
    end
    
    [r_CMR,c_CMR,v_CMR] = find(A_CMR);
    
    for jj = 1:S
      for msn=1:ns
        vals_scen((msn-1)*shift+(1:shift)) = ...
          [reshape(OPT.nlp.data{jj}.mat.Gx{msn}, nsd*nsd, 1); ...
          reshape(OPT.nlp.data{jj}.mat.Gq{msn}, nsd*nq, 1); ...
          -ones(nsd, 1)];
      end
      
      vals = [vals;vals_scen;v_CMR];
      if CMR
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
          offset = (jj-1)*(nsd*ns+2*(ns-1+1)*nq);
          ridx_scen = ridx + offset;
          cidx_scen = cidx + (jj-1)*nz;
          r_CMR_scen = r_CMR + offset;
          c_CMR_scen = c_CMR + (jj-1)*nz;
        else
          offset = (jj-1)*(nsd*ns+(ns-1+1)*nq);
          ridx_scen = ridx + offset;
          cidx_scen = cidx + (jj-1)*nz;
          r_CMR_scen = r_CMR + offset;
          c_CMR_scen = c_CMR + (jj-1)*nz;
        end
      else
        ridx_scen = ridx + (jj-1)*(nsd*ns);
        cidx_scen = cidx + (jj-1)*(nsd*(ns+1)+nq*ns);
        r_CMR_scen = r_CMR;
        c_CMR_scen = c_CMR;
      end
      
      rowindex = [rowindex;ridx_scen;r_CMR_scen];
      columnindex = [columnindex;cidx_scen;c_CMR_scen];
    end
    cst1 = sparse(rowindex, columnindex, vals);
    
    % Add non-anticipativity constraints
    cst = [cst1; ST.Jna_ipopt];
    
  end

end