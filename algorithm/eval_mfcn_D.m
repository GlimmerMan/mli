function [outp] = eval_mfcn_D(inp)
    
    global SLV
    
    sensdirs = inp.sensdirs;

    args.t = inp.t;    
    args.xd = inp.sd;
    args.qph = [inp.q;inp.p];    
    ndf = size(sensdirs,2);
    args.fwdTCOrder = 1;
    args.nFwdTC = ndf;
    args.fwdTC = sensdirs;
    args.nAdjTC = 1;
    args.adjTC = kron(ones(1,ndf), [1 0]);

    evl = SLV.opt.evl;
    
    args.rhs = solvind('evaluateFcn', evl, 'mfcn', args);
    result = solvind('evaluateDenseDer', evl, 'mfcn', args);

    outp.mfcn = result.rhs;
    outp.grad = result.propFwdTC(:);
    outp.H = result.propAdjTC(2:end,2:2:end);
    