% -*-matlab-*-
function mli_solveuncondensedQP_quadprog

global OPT;

OPT.condtime = 0;

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nrd = nrds + (ns-1) * nrdi + nrde;

phase = OPT.currentphase;
switch phase

  case {0,4}
    pvars = OPT.phase{4}.var.primal;
    
  case 1
    pvars = OPT.phase{1}.ref.primal;
    
  case 2
    pvars = OPT.phase{2}.var.primal;
    
  case 3
    pvars = OPT.phase{3}.var.primal;
     
end

tic
if (phase==0) || (phase==4)
    % Hessematrix
    for ii=1:ns
        stride = nsd+nq;
        idx = (ii-1)*stride;
        OPT.qp.H(idx+(1:stride),idx+(1:stride)) = ...
            [OPT.hess.ssblock{ii} OPT.hess.sqblock{ii}
             OPT.hess.sqblock{ii}' OPT.hess.qqblock{ii}];
    end
    OPT.qp.H(ns*(nsd+nq)+(1:nsd),ns*(nsd+nq)+(1:nsd)) = OPT.hess.ssblock{ns+1};

    % Constraint matrix (matching and ipc)
    for ii=1:ns
        xidx = (ii-1)*nsd;
        yidx = (ii-1)*(nsd+nq);
        OPT.qp.A(xidx+(1:nsd),yidx+(1:nsd+nq+nsd)) = ...
            [OPT.eval.mat.Gx{ii}, OPT.eval.mat.Gq{ii}, -eye(nsd)];
    end
    if nrd > 0
        xidx = ns*nsd;
        yidx = 0;
        if nrds > 0
            OPT.qp.A(xidx+(1:nrds),yidx+(1:nsd+nq)) = ...
                [OPT.eval.mat.Jrdsx, OPT.eval.mat.Jrdsq];
            xidx = xidx+nrds;
        end
        if nrdi > 0
            for ii = 1:ns-1
                yidx = yidx + (nsd+nq);
                OPT.qp.A(xidx+(1:nrdi),yidx+(1:nsd+nq)) = ...
                    [OPT.eval.mat.Jrdix{ii}, OPT.eval.mat.Jrdiq{ii}];
                xidx = xidx + nrdi;
            end
        end
        if nrde > 0
            yidx = ns*(nsd+nq);
            OPT.qp.A(xidx+(1:nrde),yidx+(1:nsd)) = OPT.eval.mat.Jrdex;
        end
    end
end

if (phase ~= 1)
    % Gradient
    stride = nsd+nq;
    
    for ii=1:ns
        idx = (ii-1)*stride;
        OPT.qp.g(idx+(1:stride),1) = ...
	[OPT.eval.res.modgradx(:,ii)
	 OPT.eval.res.modgradq(:,ii)];
    end
    OPT.qp.g(ns*(nsd+nq)+(1:nsd),1) = OPT.eval.res.modgradx(:,ns+1);
    
    % RHS (matching and ipc)
    for ii=1:ns
        OPT.qp.ineqlb((ii-1)*nsd+(1:nsd),1) = -OPT.eval.res.c(:,ii);
        OPT.qp.inequb((ii-1)*nsd+(1:nsd),1) = -OPT.eval.res.c(:,ii);
    end
    if nrd > 0
        bigbnd = 1e12;
        xoffs = ns*nsd;
        if nrds > 0
            OPT.qp.ineqlb(xoffs+(1:nrds)) = -OPT.eval.res.rds;
            OPT.qp.inequb(xoffs+(1:nrds)) = bigbnd*ones(size(OPT.eval.res.rds));
            xoffs = xoffs + nrds;
        end
        if nrdi > 0
            for jj=1:ns-1
                OPT.qp.ineqlb(xoffs+(1:nrdi)) = -OPT.eval.res.rdi(:,jj);
                OPT.qp.inequb(xoffs+(1:nrdi)) = bigbnd*ones(size(OPT.eval.res.rdi(:,jj)));
                xoffs = xoffs + nrdi;
            end
        end
        if nrde > 0
            OPT.qp.ineqlb(xoffs+(1:nrde)) = -OPT.eval.res.rde;
            OPT.qp.inequb(xoffs+(1:nrde)) = bigbnd*ones(size(OPT.eval.res.rde));
        end
    end
    
    % Bounds
    for ii=1:ns
        idx = (ii-1)*stride+nsd;
        OPT.qp.lb(idx+(1:stride),1) = ...
	[OPT.bounds.qLoB(:,ii) - pvars.q(:,ii)
         OPT.bounds.sdLoB(:,ii+1) - pvars.sd(:,ii+1)];
        
        OPT.qp.ub(idx+(1:stride),1) = ...
	[OPT.bounds.qUpB(:,ii) - pvars.q(:,ii)
         OPT.bounds.sdUpB(:,ii+1) - pvars.sd(:,ii+1)];
    end
end

OPT.deltas0 = OPT.x0(:) - pvars.sd(:,1);
OPT.qp.lb(1:nsd) = OPT.deltas0;
OPT.qp.ub(1:nsd) = OPT.deltas0;

%%% QP solution
Aeq = [eye(nsd) zeros(nsd,ns*(nsd+nq)); OPT.qp.A(1:ns*nsd,:)];
beq = [OPT.deltas0;OPT.qp.ineqlb(1:ns*nsd)];
if nrd > 0
  Aineq = [OPT.qp.A(ns*nsd+1:end,:)
	   -OPT.qp.A(ns*nsd+1:end,:)];
  bineq = [OPT.qp.inequb(ns*nsd+1:end,1)
	   -OPT.qp.ineqlb(ns*nsd+1:end,1)];
else
  Aineq = [];
  bineq = [];
end

opts = optimset('Algorithm', 'active-set');
opts = optimset(opts, 'Diagnostics', 'off');
opts = optimset(opts, 'Display', 'off');
opts = optimset(opts, 'MaxIter', 1e4);

[x, obj, status, output, y] = quadprog(OPT.qp.H, OPT.qp.g, Aineq, ...
                                       bineq, Aeq, beq, OPT.qp.lb, OPT.qp.ub, [], opts); 


if (status <= 0)
  status
  output
  error('QP solution failed!');
end

if (phase ~= 1)
    OPT.qp.solstep = x; 

    y.ineqlin = reshape(y.ineqlin, length(y.ineqlin)/2, 2);
    yineq = y.ineqlin(:,1) - y.ineqlin(:,2);
    ybnd = y.lower - y.upper;
    
    %  multipliers of (q_0,sd_1,...,q_N-1,sd_N) bounds
    OPT.qp.solbndmult = ybnd(nsd+1:end);
   
    % multipliers of sd_0 bound and matching
    OPT.qp.soleqmult = [-y.eqlin];
                        
    % multipliers of ipc
    OPT.qp.solineqmult = yineq;
end


% Feedback update
OPT.feedbackdelta = x(nsd+(1:nq));
OPT.qptime = toc;

