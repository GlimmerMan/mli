% -*-matlab-*-
function est_gnhess

global EST;

ns = EST.dims.nshoot;
nsd = EST.dims.nsd;
np = EST.dims.np;
nq = EST.dims.nq;
nlsqs = EST.dims.lsqfcn_s;
nlsqi = EST.dims.lsqfcn_i; 
nlsqe = EST.dims.lsqfcn_e;


% We start by building the condensing blocks and the 
% condensed matching conditions. This allows to substitute
% (Delta s_1, ..., Delta s_N) by C*(Delta s_0, Delta p) + c_cond.

% In Level B and C (without shift), the condensing blocks are kept. 
% The matching conditions need to be condensed.

% CBlocks
if EST.currentphase == 4
    EST.cond.cblocksx{1} = EST.eval.mat.Gx{1};
    if ~EST.fixed_p
        EST.cond.cblocksp{1} = EST.eval.mat.Gp{1};
    end
    for ii = 2:ns
        EST.cond.cblocksx{ii} = EST.eval.mat.Gx{ii} * ...
            EST.cond.cblocksx{ii-1};
        if ~EST.fixed_p
            EST.cond.cblocksp{ii} = EST.eval.mat.Gx{ii} * ...
                EST.cond.cblocksp{ii-1} + EST.eval.mat.Gp{ii};
        end
    end
end

% condensed rhs
EST.cond.c(:,1) = EST.eval.res.c(:,1);
for ii = 2:ns
    EST.cond.c(:,ii) = EST.eval.mat.Gx{ii} * ...
        EST.cond.c(:,ii-1) + EST.eval.res.c(:,ii);
end

% The condensed objective Jacobian is J*[I 0;C;0 I]

if EST.currentphase == 4
    % condensed J
    if ~EST.fixed_p
        EST.cond.Jcond(1:nlsqs,1:(nsd+np)) = ...
            diag(EST.V(:,1)) * [EST.eval.mat.Jmeasx{1}, EST.eval.mat.Jmeasp{1}];
        idx = nlsqs;
        for ii = 2:ns
            EST.cond.Jcond(idx+(1:nlsqi),1:(nsd+np)) = diag(EST.V(:,ii)) * ...
                [EST.eval.mat.Jmeasx{ii}*EST.cond.cblocksx{ii-1}, ...
                 EST.eval.mat.Jmeasx{ii}*EST.cond.cblocksp{ii-1} ...
                 + EST.eval.mat.Jmeasp{ii}];
            idx = idx + nlsqi;
        end
        EST.cond.Jcond(idx+(1:nlsqe),1:(nsd+np)) = diag(EST.V(:,ns+1)) * ...
            [EST.eval.mat.Jmeasx{ns+1}*EST.cond.cblocksx{ns}, ...
             EST.eval.mat.Jmeasx{ns+1}*EST.cond.cblocksp{ns} ...
             + EST.eval.mat.Jmeasp{ns+1}];
    else
        EST.cond.Jcond(1:nlsqs,1:nsd) = ...
            diag(EST.V(:,1)) * [EST.eval.mat.Jmeasx{1}];
        idx = nlsqs;
        for ii = 2:ns
            EST.cond.Jcond(idx+(1:nlsqi),1:nsd) = diag(EST.V(:,ii)) * ...
                [EST.eval.mat.Jmeasx{ii}*EST.cond.cblocksx{ii-1}];
            idx = idx + nlsqi;
        end
        EST.cond.Jcond(idx+(1:nlsqe),1:nsd) = diag(EST.V(:,ns+1)) * ...
            [EST.eval.mat.Jmeasx{ns+1}*EST.cond.cblocksx{ns}];
    end
end


% The condensing modification of F is J*[0;c_cond;0] 
EST.cond.Fmod(1:nlsqs,1) = 0.0;
idx = nlsqs;
for ii=2:ns
    EST.cond.Fmod(idx+(1:nlsqi),1) = diag(EST.V(:,ii)) * ...
        EST.eval.mat.Jmeasx{ii} * EST.cond.c(:,ii-1);
    idx = idx + nlsqi;
end
EST.cond.Fmod(idx+(1:nlsqe),1) = diag(EST.V(:,ii)) * ...
    EST.eval.mat.Jmeasx{ns+1} * EST.cond.c(:,ns);

% modification of condensed gradient 
EST.cond.gradmod = EST.cond.Jcond' * EST.cond.Fmod;

% modgradp and modgradx(:,ns+1) not (fully) available yet!!
EST.cond.grad(1:nsd) = EST.eval.res.modgradx(:,1);
if ~EST.fixed_p
    EST.cond.grad(nsd+(1:np)) = 0.0;
end
for ii=1:ns-1
    EST.cond.grad(1:nsd) = EST.cond.grad(1:nsd) + EST.cond.cblocksx{ii}'* ...
        EST.eval.res.modgradx(:,ii+1);
    if ~EST.fixed_p
        EST.cond.grad(nsd+(1:np)) = EST.cond.grad(nsd+(1:np)) + EST.cond.cblocksp{ii}'* ...
            EST.eval.res.modgradx(:,ii+1);
    end
end


% Now everything is available for the condensed constrained linear
% least-squares problem. However, we want to formulate it as a
% QP. Therefore, we build the Hessian and the constraint Jacobian
% and residual. The gradient will be completed as soon as the last
% measurement becomes known. 

% condensed Hessian
if ~EST.disable_arrival
    EST.qp.H = EST.P'*EST.P + EST.cond.Jcond'*EST.cond.Jcond;
else
    EST.qp.H = EST.cond.Jcond'*EST.cond.Jcond;
end

% constraint Jacobian (transformed bounds)
if EST.currentphase == 4
    idx = 0;
    for ii = 1:ns
        if ~EST.fixed_p
            EST.qp.A(idx+(1:nsd), 1:(nsd+np)) = [EST.cond.cblocksx{ii} ...
                                EST.cond.cblocksp{ii}];
        else
            EST.qp.A(idx+(1:nsd), 1:nsd) = EST.cond.cblocksx{ii};
        end
        idx = idx+nsd;
    end
end

% constraint rhs
idx = 0;
for ii = 1:ns
    EST.qp.lbA(idx+(1:nsd)) = [EST.bounds.sdLoB(:,ii+1) - ...
                        EST.var.primal.sd(:,ii+1) - EST.cond.c(:,ii)];
    EST.qp.ubA(idx+(1:nsd)) = [EST.bounds.sdUpB(:,ii+1) - ...
                        EST.var.primal.sd(:,ii+1) - EST.cond.c(:,ii)]; 
    idx = idx+nsd;
end

% bounds
if ~EST.fixed_p
    EST.qp.lb = [EST.bounds.sdLoB(:,1) - EST.var.primal.sd(:,1)
                 EST.bounds.pLoB - EST.var.primal.p];
    EST.qp.ub = [EST.bounds.sdUpB(:,1) - EST.var.primal.sd(:,1)
                 EST.bounds.pUpB - EST.var.primal.p];
else
    EST.qp.lb = EST.bounds.sdLoB(:,1) - EST.var.primal.sd(:,1);
    EST.qp.ub = EST.bounds.sdUpB(:,1) - EST.var.primal.sd(:,1);
end

%disp('End of gnhess...');
%keyboard;


