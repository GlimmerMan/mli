function [outp] = eval_mfcn_B(inp)
    
    global SLV
       
    args.t = inp.t;    
    args.xd = inp.sd;
    args.qph = [inp.q;inp.p];    

    evl = SLV.opt.evl;
    
    args.rhs = solvind('evaluateFcn', evl, 'mfcn', args);
    outp.mfcn = args.rhs;


    