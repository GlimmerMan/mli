function mli_printinfo(forceheader, delta)

global OPT;

nmfcn = OPT.dims.mfcn;
nlfcn = OPT.dims.lfcn;

nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
ns = OPT.dims.nshoot;

ii = OPT.currentsample;
phase = OPT.currentphase;

% Objective function

obj = 0;
if nlfcn > 0
  obj = obj + OPT.eval.res.lfcn * ones(size(OPT.eval.res.lfcn))';
end
if nmfcn > 0 && nlfcn <= 0
    if isfield(OPT,'periodicity')
      %objective has to be implemented
    else
      obj = obj + OPT.eval.res.mfun;
    end
end
if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
  if isfield(OPT, 'controlmove_c')
    obj = obj + OPT.controlmove_c' * sum(OPT.phase{4}.var.primal.delq(:,1:ns-1),2);
    obj = obj + OPT.controlmove_c0' * OPT.phase{4}.var.primal.delq(:,ns);
  end
  if isfield(OPT, 'controlmove_R')
    for kk = 1:ns-1
      obj = obj + OPT.phase{4}.var.primal.delq(:,kk)' * OPT.controlmove_R * OPT.phase{4}.var.primal.delq(:,kk);
    end
    obj = obj + OPT.phase{4}.var.primal.delq(:,ns)' * OPT.controlmove_R0 * OPT.phase{4}.var.primal.delq(:,ns);
  end
end

ipc = [];
if nrds > 0
    ipc = [ipc;OPT.eval.res.rds(:)];
end
if nrdi > 0
    ipc = [ipc;OPT.eval.res.rdi(:)];
end
if nrde > 0
    ipc = [ipc;OPT.eval.res.rde(:)];
end

% violated matching and interior point constraints
% bounds and initial condition should always be satisfied due to linearity
if ~isempty(ipc)
    feasn = norm([OPT.eval.res.c(:);min(ipc,zeros(size(ipc)))]);
else
    feasn = norm(OPT.eval.res.c(:));
end


pstep = [OPT.step.primal.sd(:);OPT.step.primal.q(:)];

dstep = [OPT.step.dualstep.lambda(:);OPT.step.dualstep.mu_sd(:);...
    OPT.step.dualstep.mu_q(:)];
if nrds > 0
    dstep = [dstep;OPT.step.dualstep.mu_rds(:)];
end
if nrdi > 0
    dstep = [dstep;OPT.step.dualstep.mu_rdi(:)];
end
if nrde > 0
    dstep = [dstep;OPT.step.dualstep.mu_rde(:)];
end

if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1 && (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
  pstep = [pstep; OPT.step.primal.delq(:)];
  dstep = [dstep; OPT.step.dualstep.mu_delq(:); OPT.step.dualstep.lambda1(:); OPT.step.dualstep.lambda2(:)];
end

pstepn = norm(pstep);
dstepn = norm(dstep);
stepn = norm([pstep;dstep]);

laggradn = norm([OPT.eval.res.laggradx(:);OPT.eval.res.laggradq(:)]);

if nargin == 1
    
    if (forceheader ~= 0)
        fprintf('\n%5s %2s %9s %9s %9s %9s %9s %9s %9s %9s\n', ...
            'iter', 'ph', '|feas|', '|pstep|', '|dstep|', ...
            '|step|', '|laggr|', 'evltime', 'condtime', 'qptime');
    end
    
    fprintf('%5d %2d %9.2e %9.2e %9.2e %9.2e %9.2e %9.2e %9.2e %9.2e\n', ...
        ii, phase, feasn, pstepn, dstepn, stepn, laggradn, ...
        OPT.evaltime, OPT.condtime, OPT.qptime);
else
    if (forceheader ~= 0)
        fprintf('\n%5s %2s %9s %9s %9s %9s %9s %9s %9s %9s %9s\n', ...
            'iter', 'ph', '|feas|', '|pstep|', '|dstep|', ...
            '|step|', '|laggr|', 'evltime', 'condtime', 'qptime', 'estdelta');
    end
    
    fprintf('%5d %2d %9.2e %9.2e %9.2e %9.2e %9.2e %9.2e %9.2e %9.2e %9.2e\n', ...
        ii, phase, feasn, pstepn, dstepn, stepn, laggradn, ...
        OPT.evaltime, OPT.condtime, OPT.qptime, delta);
end