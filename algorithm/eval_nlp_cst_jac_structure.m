function fcn = eval_nlp_cst_jac_structure(vars)
global SCEN OPT ST

if SCEN.use_st == 0
    fcn = @eval;
else
    fcn = @evalST;
end

ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd;
nq = OPT.dims.nq;

shift = nsd*(nsd+nq) + nsd;
nnz = ns * shift;
%periodicity
if isfield(OPT,'periodicity')
    nnz=nnz+2*OPT.dims.periodicity*nsd;
end

CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;

ridx = zeros(nnz,1);
cidx = zeros(nnz,1);
vals = ones(nnz,1);

for msn=1:ns
  
  xidx = (msn-1)*nsd;
  qidx = nsd*(ns+1) + (msn-1)*nq;
  
  ridx((msn-1)*shift+(1:shift)) = xidx + ...
    [kron(ones(nsd,1),(1:nsd)'); ...
    kron(ones(nq,1),(1:nsd)'); ...
    (1:nsd)'];
  
  cidx((msn-1)*shift+(1:shift)) = ...
    [xidx + kron((1:nsd)',ones(nsd,1)); ...
    qidx + kron((1:nq)',ones(nsd,1)); ...
    (xidx+nsd) + (1:nsd)'];
  
end
% periodicity
if isfield(OPT,'periodicity')
  xidx_end=ns*nsd;
  ridx(ns*shift+(1:OPT.dims.periodicity*nsd))=msn*nsd+[kron(ones(nsd,1),(1:OPT.dims.periodicity)')];
  cidx(ns*shift+(1:OPT.dims.periodicity*nsd))=xidx_end+[kron((1:nsd)',ones(OPT.dims.periodicity,1))];
  xidx_start=(ns-OPT.periodicity.periodic)*nsd;
  ridx(ns*shift+OPT.dims.periodicity*nsd+(1:OPT.dims.periodicity*nsd))=msn*nsd+[kron(ones(nsd,1),(1:OPT.dims.periodicity)')];
  cidx(ns*shift+OPT.dims.periodicity*nsd+(1:OPT.dims.periodicity*nsd))=xidx_start+[kron((1:nsd)',ones(OPT.dims.periodicity,1))];
end

  function [cst] = eval(x)
    
    if CMR
      xoffs = ns*nsd;%+nrds+nrdi+nrde;
      yoff1 = (ns+1)*nsd;     
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        nz = ns*(nsd+nq)+nsd + (ns-1+1)*nq;
        nc = nsd*ns+2*(ns-1+1)*nq;
        xoffs2 = xoffs + (ns-1+1)*nq;
        yoff2 = ns*(nsd+nq)+nsd;
        A_CMR = sparse(nc,nz);
        for ii = 1:ns-1
          yidx = yoff1 + (ii-1)*nq+(1:nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yidx) = speye(nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yidx+nq) = speye(nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yoff2+(ii-1)*nq+(1:nq)) = speye(nq);
          A_CMR(xoffs2+(ii-1)*nq+(1:nq),yidx) = speye(nq);
          A_CMR(xoffs2+(ii-1)*nq+(1:nq),yidx+nq) = speye(nq);
          A_CMR(xoffs2+(ii-1)*nq+(1:nq),yoff2+(ii-1)*nq+(1:nq)) = speye(nq);
        end
        A_CMR(xoffs+(ns-1)*nq+(1:nq),yoff1+(1:nq)) = -speye(nq);
        A_CMR(xoffs+(ns-1)*nq+(1:nq),yoff2+(ns-1)*nq+(1:nq)) = speye(nq);
        A_CMR(xoffs2+(ns-1)*nq+(1:nq),yoff1+(1:nq)) = speye(nq);
        A_CMR(xoffs2+(ns-1)*nq+(1:nq),yoff2+(ns-1)*nq+(1:nq)) = speye(nq);
      else
        nz = ns*(nsd+nq)+nsd;
        nc = nsd*ns+(ns-1+1)*nq;
        A_CMR = sparse(nc,nz);
        for ii = 1:ns-1
          yidx = yoff1 + (ii-1)*nq+(1:nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yidx) = speye(nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yidx+nq) = speye(nq);
        end
        A_CMR(xoffs+(ns-1)*nq+(1:nq),yoff1+(1:nq)) = speye(nq);
      end
    else

      nz = ns*(nsd+nq)+nsd;


      nc = nsd*ns;
      if isfield(OPT,'periodicity')
        nc=nc+OPT.dims.periodicity;
      end
      A_CMR = sparse(nc,nz);
    end
    
    cst = sparse(ridx, cidx, vals, nc, nz) + A_CMR;
  end

  function [cst] = evalST(x)
    S = ST.S;
    rowindex = [];
    columnindex = [];
    vals = [];
    vals_scen = ones(nnz,1);
    
    if CMR
      xoffs = ns*nsd; %+nrds+nrdi+nrde;
      yoff1 = (ns+1)*nsd;
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        nz = ns*(nsd+nq)+nsd + (ns-1+1)*nq;
        nc = nsd*ns+2*(ns-1+1)*nq;
        xoffs2 = xoffs + (ns-1+1)*nq;
        yoff2 = ns*(nsd+nq)+nsd;
        A_CMR = sparse(nc,nz);
        for ii = 1:ns-1
          yidx = yoff1 + (ii-1)*nq+(1:nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yidx) = speye(nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yidx+nq) = speye(nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yoff2+(ii-1)*nq+(1:nq)) = speye(nq);
          A_CMR(xoffs2+(ii-1)*nq+(1:nq),yidx) = speye(nq);
          A_CMR(xoffs2+(ii-1)*nq+(1:nq),yidx+nq) = speye(nq);
          A_CMR(xoffs2+(ii-1)*nq+(1:nq),yoff2+(ii-1)*nq+(1:nq)) = speye(nq);
        end
        A_CMR(xoffs+(ns-1)*nq+(1:nq),yoff1+(1:nq)) = speye(nq);
        A_CMR(xoffs+(ns-1)*nq+(1:nq),yoff2+(ns-1)*nq+(1:nq)) = speye(nq);
        A_CMR(xoffs2+(ns-1)*nq+(1:nq),yoff1+(1:nq)) = speye(nq);
        A_CMR(xoffs2+(ns-1)*nq+(1:nq),yoff2+(ns-1)*nq+(1:nq)) = speye(nq);
      else
        nz = ns*(nsd+nq)+nsd;
        nc = nsd*ns+(ns-1+1)*nq;
        A_CMR = sparse(nc,nz);
        for ii = 1:ns-1
          yidx = yoff1 + (ii-1)*nq+(1:nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yidx) = speye(nq);
          A_CMR(xoffs+(ii-1)*nq+(1:nq),yidx+nq) = speye(nq);
        end
        A_CMR(xoffs+(ns-1)*nq+(1:nq),yoff1+(1:nq)) = speye(nq);
      end
    else
      nz = ns*(nsd+nq)+nsd;
      nc = nsd*ns;
      A_CMR = [];
    end
    
    [r_CMR,c_CMR] = find(A_CMR);
    v_CMR = ones(size(r_CMR));
      
    for jj = 1:S
      
      vals = [vals;vals_scen;v_CMR];
      if CMR
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
          offset = (jj-1)*(nsd*ns+2*(ns-1+1)*nq);
          ridx_scen = ridx + offset;
          cidx_scen = cidx + (jj-1)*nz;
          r_CMR_scen = r_CMR + offset;
          c_CMR_scen = c_CMR + (jj-1)*nz;
        else
          offset = (jj-1)*(nsd*ns+(ns-1+1)*nq);
          ridx_scen = ridx + offset;
          cidx_scen = cidx + (jj-1)*nz;
          r_CMR_scen = r_CMR + offset;
          c_CMR_scen = c_CMR + (jj-1)*nz;
        end
      else
        ridx_scen = ridx + (jj-1)*(nsd*ns);
        cidx_scen = cidx + (jj-1)*(nsd*(ns+1)+nq*ns);
        r_CMR_scen = r_CMR;
        c_CMR_scen = c_CMR;
      end
      
      rowindex = [rowindex;ridx_scen;r_CMR_scen];
      columnindex = [columnindex;cidx_scen;c_CMR_scen];
    end
    cst1 = sparse(rowindex, columnindex, vals);
    
    % Add non-anticipativity constraints
    cst = [cst1; abs(ST.Jna_ipopt)];
    
%     nC = size(ST.C_ipopt{1});
%     ncst1 = size(cst1);
%     for jj = 1:S-1
%         nozrows = sum(ST.C_ipopt{jj},2) > 0;
%         nC(jj,:) = size(ST.C_ipopt{jj}(nozrows,:));
%         [i,k,v] = find([ST.C_ipopt{jj}(nozrows,:), ST.E_ipopt{jj+1}(nozrows,:)]);
%         if ~iscolumn(i)
%           i = i';
%           k = k';
%           v = v';
%         end
%         rowindex = [rowindex; ncst1(1)+sum(nC(1:jj-1,1))+i];
%         columnindex = [columnindex; sum(nC(1:jj-1,2))+k];
%         vals = [vals;v];
%     end
%     cst = sparse(rowindex, columnindex, vals);   
  end
end