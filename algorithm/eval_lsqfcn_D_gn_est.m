function [outp] = eval_lsqfcn_D_gn_est(inp, nodetype)
    
    global SLV
    
    sensdirs = inp.sensdirs;
    args.t = inp.t;    
    args.xd = inp.sd;
    args.qph = [inp.q;inp.p];    

    ndf = size(sensdirs,2);
    args.fwdTCOrder = 1;
    args.nFwdTC = ndf;
    args.fwdTC = sensdirs;
    args.nAdjTC = 0;
    args.adjTC = [];

    evl = SLV.est.evl;
    
    switch nodetype
      case 's'
	args.rhs = solvind('evaluateFcn', evl, 'lsqfcn_s', args);
	result = solvind('evaluateDenseDer', evl, 'lsqfcn_s', args);
	
      case 'i'
	args.rhs = solvind('evaluateFcn', evl, 'lsqfcn_i', args);
	result = solvind('evaluateDenseDer', evl, 'lsqfcn_i', args);
	
      case 'e'
	args.rhs = solvind('evaluateFcn', evl, 'lsqfcn_e', args);
	result = solvind('evaluateDenseDer', evl, 'lsqfcn_e', args);
	
      otherwise
	error('eval_lsqfcn_D: Invalid nodetype');
	
    end

    outp.lsqfcn = result.rhs;
    outp.lsqjac = result.propFwdTC;
    outp.lsqgrad = outp.lsqjac' * outp.lsqfcn;
