% -*-matlab-*-
function [var] = zero_vars

global OPT

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
np = OPT.dims.np;
ns = OPT.dims.nshoot;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;

var.primal.sd = zeros(nsd, ns+1);
var.primal.q = zeros(nq, ns);
var.primal.pconst = zeros(np, 1);
    
var.dual.lambda = zeros(nsd, ns);
var.dual.mu_sd = zeros(nsd, ns+1);
var.dual.mu_q = zeros(nq, ns);

if nrds > 0
    var.dual.mu_rds = zeros(nrds, 1);
end

if nrdi > 0
    var.dual.mu_rdi = zeros(nrdi, ns-1);
end

if nrde > 0
    var.dual.mu_rde = zeros(nrde, 1);
end
