function [outp] = eval_lsqfcn_D(inp, nodetype)
    
    global SLV
    
    sensdirs = inp.sensdirs;
    ndf = size(sensdirs,2);
    args.t = inp.t;    
    args.xd = inp.sd;
    args.qph = [inp.q;inp.p];    

    evl = SLV.opt.evl;
    
    switch nodetype
      case 's'
	args.rhs = solvind('evaluateFcn', evl, 'lsqfcn_s', args);
	args.fwdTCOrder = 1;
	args.nFwdTC = ndf;
	args.fwdTC = sensdirs;
	args.nAdjTC = 1;
	args.adjTC = kron(ones(1,ndf), [args.rhs zeros(size(args.rhs))]);
	result = solvind('evaluateDenseDer', evl, 'lsqfcn_s', args);
	
      case 'i'
	args.rhs = solvind('evaluateFcn', evl, 'lsqfcn_i', args);
	args.fwdTCOrder = 1;
	args.nFwdTC = ndf;
	args.fwdTC = sensdirs;
	args.nAdjTC = 1;
	args.adjTC = kron(ones(1,ndf), [args.rhs zeros(size(args.rhs))]);
	result = solvind('evaluateDenseDer', evl, 'lsqfcn_i', args);
	
      case 'e'
	args.rhs = solvind('evaluateFcn', evl, 'lsqfcn_e', args);
	args.fwdTCOrder = 1;
	args.nFwdTC = ndf;
	args.fwdTC = sensdirs;
	args.nAdjTC = 1;
	args.adjTC = kron(ones(1,ndf), [args.rhs zeros(size(args.rhs))]);
	result = solvind('evaluateDenseDer', evl, 'lsqfcn_e', args);
	
      otherwise
	error('eval_lsqfcn_D: Invalid nodetype');
	
    end

    outp.lsqfcn = result.rhs;
    outp.lsqjac = result.propFwdTC;
    outp.lsqgrad = result.propAdjTC(2:end,1);
    outp.lsq2nd = result.propAdjTC(2:end,2:2:end);
