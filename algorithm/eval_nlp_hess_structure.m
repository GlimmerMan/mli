function fcn = eval_nlp_hess_structure(vars)
global SCEN OPT ST

if SCEN.use_st == 0
    fcn = @eval;
else
    fcn = @evalST;
end

ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
nmfcn = OPT.dims.mfcn;
nlfcn = OPT.dims.lfcn;

shift = nsd*nsd + 2*nsd*nq + nq*nq;
nnz = ns*shift;
if nmfcn > 0 && nlfcn <= 0 && ~isfield(OPT,'periodicity')
  nnz = nnz + nsd*nsd;
end

CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;

ridx = zeros(nnz,1);
cidx = zeros(nnz,1);
vals = ones(nnz,1);

for msn=1:ns
  
  xidx = (msn-1)*nsd;
  qidx = nsd*(ns+1) + (msn-1)*nq;
  
  ridx((msn-1)*shift+(1:shift)) = ...
    [xidx + kron(ones(nsd,1),(1:nsd)'); ...
    xidx + kron(ones(nq,1),(1:nsd)'); ...
    qidx + kron(ones(nsd,1),(1:nq)'); ...
    qidx + kron(ones(nq,1),(1:nq)')];
  
  cidx((msn-1)*shift+(1:shift)) = ...
    [xidx + kron((1:nsd)',ones(nsd,1)); ...
    qidx + kron((1:nq)',ones(nsd,1)); ...
    xidx + kron((1:nsd)',ones(nq,1)); ...
    qidx + kron((1:nq)',ones(nq,1))];
  
end

if nmfcn > 0 && nlfcn <= 0
  if isfield(OPT,'periodicity')
    H_per=zeros((nsd+nq)*ns+nsd,(nsd+nq)*ns+nsd);
    for ii=1:OPT.periodicity.transient
      %parts of the hessian corresponding to (Li-Li+p)^2
      %Set up projection matrix correspponding to (Li-Li+p)^2
      P=zeros((nsd+nq)*ns+nsd,1);
      %the part of the transient interval
      P((ii-1)*nsd+(1:2*nsd),1)=ones(2*nsd,1);
      %the part of the periodic interval
      P((ii-1+OPT.periodicity.periodic)*nsd+(1:2*nsd),1)=ones(2*nsd,1);
      %modify the hessian with the discount factor
      H_per=H_per+P*P';
    end
            
            
    if OPT.periodicity.transient>=1
      %add term for 2. derivatives wrt state and control deviation
      %state
      P_state=zeros((nsd+nq)*ns+nsd,1);
      %the state part of the transient interval
      P_state((OPT.periodicity.transient-1)*nsd+(1:nsd),1)=ones(nsd,1);
      %the state part of the periodic interval
      P_state((OPT.periodicity.transient-1+OPT.periodicity.periodic)*nsd+(1:nsd),1)=ones(nsd,1);
      %modify the hessian
      H_per=H_per+P_state*P_state';
      %control
      P_control=zeros((nsd+nq)*ns+nsd,1);
      %the state part of the transient interval
      P_control((ns+1)*nsd+(OPT.periodicity.transient-1)*nq+(1:nq),1)=ones(nq,1);
      %the state part of the periodic interval
      P_control((ns+1)*nsd+(OPT.periodicity.transient-1+OPT.periodicity.periodic)*nq+(1:nq),1)=ones(nq,1);
      %modify the hessian
      H_per=H_per+P_control*P_control';
    end
  else
    xidx = ns*nsd;
    ridx(ns*shift+(1:nsd*nsd)) = xidx + kron(ones(nsd,1),(1:nsd)');
    cidx(ns*shift+(1:nsd*nsd)) = xidx + kron((1:nsd)',ones(nsd,1));
  end
end

  function [cst] = eval()
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
      if isfield(OPT, 'controlmove_R')
        [r,c] = find(OPT.controlmove_R);
        xoff = ns*(nsd+nq)+nsd;
        for ii=1:ns-1
          idx = xoff + (ii-1)*nq;
          ridx = [ridx; r + idx];
          cidx = [cidx; c + idx];
          vals = [vals; ones(size(r))];
        end
        [r,c] = find(OPT.controlmove_R0);
        idx = xoff + (ns-1)*nq;
        ridx = [ridx; r + idx];
        cidx = [cidx; c + idx];
        vals = [vals; ones(size(r))];
      end
      nz = ns*(nsd+nq)+nsd + (ns-1+1)*nq;
    else
      nz = ns*(nsd+nq)+nsd;
    end
    if isfield(OPT,'periodicity')
      %add structure parts of normal and periodic part and extract the structure
      H_aux=sparse(ridx, cidx, vals, nz, nz)+sparse(H_per);
      [r_aux,c_aux]=find(H_aux);
      nnz_aux=numel(r_aux);
      hess=sparse(r_aux,c_aux,ones(nnz_aux,1));
      cst = tril(hess);
    else
      cst = tril(sparse(ridx, cidx, vals, nz, nz));
    end

  end

  function [cst] = evalST()
    S = ST.S;    
    rowindex = [];
    columnindex = [];
    values = [];
    
    for jj = 1:S
      r_CMR = [];
      c_CMR = [];
      v_CMR = [];
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
        if isfield(OPT, 'controlmove_R')
          [r,c] = find(OPT.controlmove_R);
          xoff = ns*(nsd+nq)+nsd;
          for ii=1:ns-1
            idx = xoff + (ii-1)*nq;
            r_CMR = [r_CMR; r + idx];
            c_CMR = [c_CMR; c + idx];
            v_CMR = [v_CMR; ones(size(r))];
          end
          [r,c] = find(OPT.controlmove_R0);
          idx = xoff + (ns-1)*nq;
          ridx = [ridx; r + idx];
          cidx = [cidx; c + idx];
          vals = [vals; ones(size(r))];
        end
        nz = ns*(nsd+nq)+nsd + (ns-1+1)*nq;
      else
        nz = ns*(nsd+nq)+nsd;
      end
      
      ridx_scen = [ridx; r_CMR] + (jj-1)*nz;
      cidx_scen = [cidx; c_CMR] + (jj-1)*nz;
      vidx_scen = [vals; v_CMR];
      
      rowindex = [rowindex; ridx_scen];
      columnindex = [columnindex; cidx_scen];
      values = [values; vidx_scen];
    end
    
    cst = tril(sparse(rowindex, columnindex, values, S*nz, S*nz));
  end

end
