% -*-matlab-*-
function mli_prepare_lbfgsupdate

global SCEN OPT ST

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;

s = cell(ns+1,1);
y = cell(ns+1,1);


if SCEN.use_st == 0
  [laggradx,laggradq] = mli_assemble_laggrad(OPT.phase{4}.var.dual,OPT.phase{4}.data);
  [laggradx_old,laggradq_old] = mli_assemble_laggrad(OPT.phase{4}.var.dual,OPT.phase{4}.dataold);
  for ii = 1:ns
    s{ii} = [OPT.step.primal.sd(:,ii);OPT.step.primal.q(:,ii)];
    y{ii} = [laggradx(:,ii)-laggradx_old(:,ii); laggradq(:,ii)-laggradq_old(:,ii)];
    
    OPT.hess.lbfgshess{ii} = lbfgshess_update(OPT.hess.lbfgshess{ii}, s{ii}, y{ii});
  end
  
  s{ns+1} = OPT.step.primal.sd(:,ns+1);
  y{ns+1} = laggradx(:,ns+1)-laggradx_old(:,ns+1);
  
  OPT.hess.lbfgshess{ns+1} = lbfgshess_update(OPT.hess.lbfgshess{ns+1}, s{ns+1}, y{ns+1});

   if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
     sdelq = cell(ns,1);
     ydelq = cell(ns,1);
     laggraddelq = mli_assemble_laggraddelq(OPT.phase{4}.var.dual,OPT.phase{4}.data);
     laggraddelq_old = mli_assemble_laggraddelq(OPT.phase{4}.var.dual,OPT.phase{4}.dataold);
     for ii = 1:ns
       sdelq{ii} = OPT.step.primal.delq(:,ii);
       ydelq{ii} = laggraddelq(:,ii)-laggraddelq_old(:,ii);
       OPT.hess.lbfgsCMRhess{ii} = lbfgshess_update(OPT.hess.lbfgsCMRhess{ii}, sdelq{ii}, ydelq{ii});
     end
   end
else % ST case
  S = ST.S;
  for jj = 1:S
    [laggradx,laggradq] = mli_assemble_laggrad(OPT.phase{4}{jj}.var.dual,OPT.phase{4}{jj}.data);
    [laggradx_old,laggradq_old] = mli_assemble_laggrad(OPT.phase{4}{jj}.var.dual,OPT.phase{4}{jj}.dataold);
    for ii = 1:ns
      s{ii} = [OPT.step{jj}.primal.sd(:,ii);OPT.step{jj}.primal.q(:,ii)];
      y{ii} = [laggradx(:,ii)-laggradx_old(:,ii); laggradq(:,ii)-laggradq_old(:,ii)];
      
      OPT.hess{jj}.lbfgshess{ii} = lbfgshess_update(OPT.hess{jj}.lbfgshess{ii}, s{ii}, y{ii});
    end
    
    s{ns+1} = OPT.step{jj}.primal.sd(:,ns+1);
    y{ns+1} = laggradx(:,ns+1)-laggradx_old(:,ns+1);
    
    OPT.hess{jj}.lbfgshess{ns+1} = lbfgshess_update(OPT.hess{jj}.lbfgshess{ns+1}, s{ns+1}, y{ns+1});
     
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
      sdelq = cell(ns,1);
      ydelq = cell(ns,1);
      laggraddelq = mli_assemble_laggraddelq(OPT.phase{4}{jj}.var.dual,OPT.phase{4}{jj}.data);
      laggraddelq_old = mli_assemble_laggraddelq(OPT.phase{4}{jj}.var.dual,OPT.phase{4}{jj}.dataold);
      for ii = 1:ns
        sdelq{ii} = OPT.step{jj}.primal.delq(:,ii);
        ydelq{ii} = laggraddelq(:,ii)-laggraddelq_old(:,ii);
        OPT.hess{jj}.lbfgsCMRhess{ii} = lbfgshess_update(OPT.hess{jj}.lbfgsCMRhess{ii}, sdelq{ii}, ydelq{ii});
      end
    end
  end
end
