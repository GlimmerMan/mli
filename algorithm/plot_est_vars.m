% -*-matlab-*-
function plot_est_vars(nspf, ncpf, offset)

global EST;

offset_s = offset+100;
offset_u = offset+200;
offset_p = offset+300;

nsd = EST.dims.nsd;
nq = EST.dims.nq;
np = EST.dims.np;


t = EST.discr.msgrid;

% state plots
nsf = ceil(nsd/nspf);
nfm = 2;
nfn = ceil(nspf/nfm);

for ii = 1:nsf-1
   figure(ii+offset_s);
   for jj=1:nspf
       idx = (ii-1)*nspf+jj;
       x = EST.var.primal.sd(idx,:);
       subplot(nfn,nfm,jj);
       plot(t,x,'r*');
   end
end

figure(nsf+offset_s);
nup = nsd - (nsf-1)*nspf;
for jj=1:nup
    idx = (nsf-1)*nspf+jj;
    x = EST.var.primal.sd(idx,:);
    subplot(nfn,nfm,jj);
    plot(t,x,'r*');
end

% control plots
nsf = ceil(nq/ncpf);
nfm = 2;
nfn = ceil(ncpf/nfm);

for ii = 1:nsf-1
   figure(ii+offset_u);
   for jj=1:ncpf
       idx = (ii-1)*ncpf+jj;
       u = EST.var.primal.qconst(idx,:);
       u = [u u(end)];
       subplot(nfn,nfm,jj);
       stairs(t(1:end),u,'g');
   end
end

figure(nsf+offset_u);
nup = nq - (nsf-1)*ncpf;
for jj=1:nup
    idx = (nsf-1)*ncpf+jj;
    u = EST.var.primal.qconst(idx,:);
    u = [u u(end)];
    subplot(nfn,nfm,jj);
    stairs(t(1:end),u,'g');
end

% parameter plots
figure(1+offset_p);
p = EST.var.primal.p;
plot(p,'b*');
pmin = min(p);
pmax = max(p);
pabs = max(abs(pmin),abs(pmax));
axis([0 length(p)+1 pmin-0.1*pabs pmax+0.1*pabs]);
grid on

