function [ data ] = mli_check_if_mfcn_is_lfcn( vars )

global OPT SLV

data.lfcnState = 0;
  
msn = 1;

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
np = OPT.dims.np;

pvars = vars.primal;
sensdirs = [zeros(1,nsd+nq);eye(nsd+nq);zeros(np,nsd+nq)];

% Try to find a candidate state for lfcn state

inp = [];
inp.t = 0;
inp.sd = pvars.sd(:,msn);
inp.q = pvars.q(:,msn);
inp.p = pvars.pconst;
inp.sensdirs = sensdirs;

outp = eval_mfcn_D(inp);

mgrad = outp.grad(1:nsd);
mhess = outp.H(1:nsd,1:nsd);

nzsmgrad = find(mgrad);

% Check if we have a candidate state for lfcn state
if ~any(any(mhess)) && size(nzsmgrad, 1) == 1
  
  ndf = size(sensdirs,2);
  
  thoriz = [OPT.discr.msgrid(msn) OPT.discr.msgrid(msn+1)];
  sd = pvars.sd(:,msn);
  q = pvars.q(:,msn);
  p = pvars.pconst;
  
  int = SLV.opt.int;
  
  solvind('setTapeNumber', int, 1);
  solvind('setTimeHorizon',int, thoriz);
  solvind('setInitVals', int, [sd;q;p]);
  
  %solvind('activateFeature', int, 'Feature_Forward_Sensitivity_Error_Control');
  solvind('evaluate', int);
  %solvind('deactivateFeature', int, 'Feature_Forward_Sensitivity_Error_Control');

  solvind('setForwardTaylorCoefficients', int, ndf, 1, sensdirs);
      
  retval = solvind('forwardSensSweep', int);
    if retval ~= 0 
        error('forwardSensSweep failed');
    end
  
  fwdDers = solvind('getFwdSens', int);
  
  cmpVec = zeros(size(fwdDers, 1), 1);
  cmpVec(nzsmgrad, 1) = 1;
  
  dxdlfcn = fwdDers(:, nzsmgrad);
  
  if isequal(cmpVec, dxdlfcn)
    data.lfcnState = nzsmgrad;
  end
  
end

end % mli_check_if_mfcn_is_lfcn

