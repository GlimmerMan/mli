% -*-matlab-*-
function est_transition

global EST;
ns = EST.dims.nshoot;
nsd = EST.dims.nsd;
np = EST.dims.np;
nq = EST.dims.nq;
nlsqs = EST.dims.lsqfcn_s;
nlsqi = EST.dims.lsqfcn_i; 
nlsqe = EST.dims.lsqfcn_e;

cursamp = EST.currentsample;

% step blowup and update
if cursamp == 1
    EST.var.primal.sd(:,1) = EST.var.primal.sd(:,1) + ...
        EST.step.primal.sd(:,1);
    if ~EST.fixed_p
        EST.var.primal.p = EST.var.primal.p + EST.step.primal.p;
    end
elseif cursamp <= ns
    EST.var.primal.sd(:,1:cursamp) = EST.var.primal.sd(:,1:cursamp) ...
        + EST.step.primal.sd(:,1:cursamp); 
    if ~EST.fixed_p
        EST.var.primal.p = EST.var.primal.p + EST.step.primal.p;
    end
else
    EST.var.primal.sd = EST.var.primal.sd + EST.step.primal.sd;
    if ~EST.fixed_p
        EST.var.primal.p = EST.var.primal.p + EST.step.primal.p;
    end
end

% add new nodes in the growing horizon phase
if cursamp <= ns
    inp = [];
    inp.thoriz = [EST.discr.msgrid(cursamp) EST.discr.msgrid(cursamp+1)];
    inp.sd = EST.var.primal.sd(:,cursamp);
    inp.p = EST.var.primal.p;
    inp.q = EST.newctrl;
    outp = eval_dyn_B_est(inp);
    EST.var.primal.sd(:,cursamp+1) = outp.x;
end

% update of arrival cost
if ~EST.disable_arrival
    if cursamp > ns+1
        est_updateArrival;
    end
end



% Shifting of online data
if cursamp <= ns
   EST.var.primal.qconst(:, cursamp) = EST.newctrl;
   EST.measurements(:, cursamp) = EST.eta;
else
   EST.var.primal.qconst = [EST.var.primal.qconst(:,2:end) EST.newctrl];
   EST.measurements = [EST.measurements(:,2:end) EST.eta];
end

%disp('End of est_transition...');
%keyboard;