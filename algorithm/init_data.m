% -*-matlab-*-
function [data] = init_data(phase)

global OPT

% Dimensionen
nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
np = OPT.dims.np;
nmfcn = OPT.dims.mfcn;
nlfcn = OPT.dims.lfcn;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nlsqs = OPT.dims.lsqfcn_s;
nlsqi = OPT.dims.lsqfcn_i;
nlsqe = OPT.dims.lsqfcn_e;
ns = OPT.dims.nshoot;

% Phase B, C und D:
% LSQ und Mayerzielfunktion, Schliessbedingungen, Ungleichungen
if phase > 1
    if nlfcn > 0
      data.res.lfcn = zeros(1,ns);
    end
    
    if nmfcn > 0 && nlfcn <= 0
        data.res.mfun = 0;
    end
    
    if nlsqs > 0
        data.res.lsqfcn_s = zeros(nlsqs,1);
    end
    if nlsqi > 0
        data.res.lsqfcn_i = zeros(nlsqi,ns-1);
    end
    if nlsqe > 0
        data.res.lsqfcn_e = zeros(nlsqe,1);
    end
    
    if nrds > 0
        data.res.rds = zeros(nrds,1);
    end
    if nrdi > 0
        data.res.rdi = zeros(nrdi,ns-1);
    end
    if nrde > 0
        data.res.rde = zeros(nrde,1);
    end
    
    data.res.xd = zeros(nsd,ns);
    data.res.c  = zeros(nsd,ns);
    
    data.res.modgradx = zeros(nsd,ns+1);
    data.res.modgradq = zeros(nq,ns);
    if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1 && (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
      data.res.modgraddelq = zeros(nq,ns);
    end
end

% Phase C und D:
% LSQ und Mayergradienten, adjungierte Ableitungen
if phase > 2
    if nlfcn > 0
      data.res.dlfcn = zeros(nsd,ns);
    end
  
    if nmfcn > 0 && nlfcn <= 0
        data.res.mgrad = zeros(nsd,1);
    end
    
    if nlsqs > 0
        data.res.lsqsgradx = zeros(nsd,1);
        data.res.lsqsgradq = zeros(nq,1);
    end
    if nlsqi > 0
        data.res.lsqigradx = zeros(nsd,ns-1);
        data.res.lsqigradq = zeros(nq,ns-1);
    end
    if nlsqe > 0
        data.res.lsqegradx = zeros(nsd,1);
    end
    
    if nrds > 0
        data.res.muJrdsx = zeros(nsd,1);
        data.res.muJrdsq = zeros(nq,1);
    end
    if nrdi > 0
        data.res.muJrdix = zeros(nsd,ns-1);
        data.res.muJrdiq = zeros(nq,ns-1);
    end
    if nrde > 0
        data.res.muJrdex = zeros(nsd,1);
    end
    
    data.res.lambdaGx = zeros(nsd,ns);
    data.res.lambdaGq = zeros(nq,ns);
    
    data.res.refgradx = zeros(nsd,ns+1);
    data.res.refgradq = zeros(nq,ns);
    
    data.res.laggradx = zeros(nsd,ns+1);
    data.res.laggradq = zeros(nq,ns);
    
    if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1 && (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
      data.res.laggraddelq = zeros(nq,ns);
      data.res.refgraddelq = zeros(nq,ns);
      
      xoffs = 0;
      xoffs2 = xoffs + (ns-1)*nq+nq;
      yoff = ns*(nsd+nq)+nsd;
      stride  = nsd+nq;
      for ii = 1:ns-1
        yidx = (ii-1)*stride+((nsd+1):(nsd+nq));
        data.mat.Gdelq(xoffs+(ii-1)*nq+(1:nq),yidx) = eye(nq);
        data.mat.Gdelq(xoffs+(ii-1)*nq+(1:nq),yidx+stride) = -eye(nq);
        data.mat.Gdelq(xoffs+(ii-1)*nq+(1:nq),yoff+(ii-1)*nq+(1:nq)) = eye(nq);
        data.mat.Gdelq(xoffs2+(ii-1)*nq+(1:nq),yidx) = -eye(nq);
        data.mat.Gdelq(xoffs2+(ii-1)*nq+(1:nq),yidx+stride) = eye(nq);
        data.mat.Gdelq(xoffs2+(ii-1)*nq+(1:nq),yoff+(ii-1)*nq+(1:nq)) = eye(nq);
      end
      data.mat.Gdelq(xoffs+(ns-1)*nq+(1:nq),(nsd+1):(nsd+nq)) = -eye(nq);
      data.mat.Gdelq(xoffs+(ns-1)*nq+(1:nq),yoff+(ns-1)*nq+(1:nq)) = eye(nq);
      data.mat.Gdelq(xoffs2+(ns-1)*nq+(1:nq),(nsd+1):(nsd+nq)) = eye(nq);
      data.mat.Gdelq(xoffs2+(ns-1)*nq+(1:nq),yoff+(ns-1)*nq+(1:nq)) = eye(nq);
      
%       data.mat.Gdelq1 = data.mat.Gdelq(); % = eye
%       data.mat.Gdelq2 = data.mat.Gdelq(); % = eye
    end
end

% Phase D:
% Sensitivitaeten, Jacobimatrizen, Hessebloecke
if phase > 3
    data.mat.Gx = mat2cell(zeros(nsd,nsd,ns),nsd,nsd,ones(ns,1));
    data.mat.Gq = mat2cell(zeros(nsd,nq,ns),nsd,nq,ones(ns,1));
    
    if nrds > 0
        data.mat.Jrdsx = zeros(nrds,nsd);
        data.mat.Jrdsq = zeros(nrds,nq);
    end
    if nrdi > 0
        data.mat.Jrdix = mat2cell(zeros(nrdi,nsd,ns-1),nrdi,nsd,ones(ns-1,1));
        data.mat.Jrdiq = mat2cell(zeros(nrdi,nq,ns-1),nrdi,nq,ones(ns-1,1));
    end
    if nrde > 0
        data.mat.Jrdex = zeros(nrde,nsd);
    end
    
    switch OPT.algo.hessian
        
        case 'exact'
            
            if nmfcn > 0
                data.mat.mhess = zeros(nsd,nsd);
            end
            
            if nlsqs > 0
                % GN part
                data.mat.Jlsqsx = zeros(nlsqs,nsd);
                data.mat.Jlsqsq = zeros(nlsqs,nq);
                % "Zufallsanteil"
                data.mat.Hlsqsxx = zeros(nsd,nsd);
                data.mat.Hlsqsxq = zeros(nsd,nq);
                data.mat.Hlsqsqq = zeros(nq,nq);
            end
            if nlsqi > 0
                % GN part
                data.mat.Jlsqix = mat2cell(zeros(nlsqi,nsd,ns-1),nlsqi,nsd,ones(ns-1,1));
                data.mat.Jlsqiq = mat2cell(zeros(nlsqi,nq,ns-1),nlsqi,nq,ones(ns-1,1));
                % "Zufallsanteil"
                data.mat.Hlsqixx = mat2cell(zeros(nsd,nsd,ns-1),nsd,nsd,ones(ns-1,1));
                data.mat.Hlsqixq = mat2cell(zeros(nsd,nq,ns-1),nsd,nq,ones(ns-1,1));
                data.mat.Hlsqiqq = mat2cell(zeros(nq,nq,ns-1),nq,nq,ones(ns-1,1));
            end
            if nlsqe > 0
                % GN part
                data.mat.Jlsqex = zeros(nlsqe,nsd);
                % "Zufallsanteil"
                data.mat.Hlsqexx = zeros(nsd,nsd);
            end
            
            data.mat.lambdaGxx = mat2cell(zeros(nsd,nsd,ns),nsd,nsd,ones(ns,1));
            data.mat.lambdaGxq = mat2cell(zeros(nsd,nq,ns),nsd,nq,ones(ns,1));
            data.mat.lambdaGqq = mat2cell(zeros(nq,nq,ns),nq,nq,ones(ns,1));
            
            if nrds > 0
                data.mat.muJrdsxx = zeros(nsd,nsd);
                data.mat.muJrdsxq = zeros(nsd,nq);
                data.mat.muJrdsqq = zeros(nq,nq);
            end
            if nrdi > 0
                data.mat.muJrdixx = mat2cell(zeros(nsd,nsd,ns-1),nsd,nsd,ones(ns-1,1));
                data.mat.muJrdixq = mat2cell(zeros(nsd,nq,ns-1),nsd,nq,ones(ns-1,1));
                data.mat.muJrdiqq = mat2cell(zeros(nq,nq,ns-1),nq,nq,ones(ns-1,1));
            end
            if nrde > 0
                data.mat.muJrdexx = zeros(nsd,nsd);
            end
            
        case 'gn'
            
            if nlsqs > 0
                data.mat.Jlsqsx = zeros(nlsqs,nsd);
                data.mat.Jlsqsq = zeros(nlsqs,nq);
            end
            if nlsqi > 0
                data.mat.Jlsqix = mat2cell(zeros(nlsqi,nsd,ns-1),nlsqi,nsd,ones(ns-1,1));
                data.mat.Jlsqiq = mat2cell(zeros(nlsqi,nq,ns-1),nlsqi,nq,ones(ns-1,1));
            end
            if nlsqe > 0
                data.mat.Jlsqex = zeros(nlsqe,nsd);
            end
            
    end
end

