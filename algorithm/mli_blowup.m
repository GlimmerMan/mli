% -*-matlab-*-
% Variables filled in OPT
%
% OPT.qstep
% OPT.sstep
% OPT.lambda

function mli_blowup

global SCEN OPT ST

phase = OPT.currentphase;
if (phase == 0)
  phase = 4;
end

if (phase == 1)
  OPT.blowuptime = 0;
  return;
end

tic

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nrd = nrds + (ns-1) * nrdi + nrde;

if SCEN.use_st == 0
    % Step in q
    OPT.step.primal.q = reshape(OPT.qp.solstep(nsd+1:end,1),nq,ns);
    
    % delq
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
      for ii = 1:ns-1+1
        idx = (ii-1)*nq;
        OPT.step.primal.delq(:,ii) = OPT.qp.soldelq(idx+(1:nq));
      end
    end
    
    % Step in s0
    OPT.step.primal.sd(:,1) = OPT.qp.solstep(1:nsd,1);
    
    % Inequality multipliers
    if nrd > 0
        OPT.step.dual.mu_sd = reshape([OPT.qp.soleqmult;OPT.qp.solineqmult(1:ns*nsd)], nsd, ns+1);
        sidx = ns*nsd;
        if nrds > 0
            OPT.step.dual.mu_rds = OPT.qp.solineqmult(sidx+(1:nrds));
            sidx = sidx + nrds;
        end
        if nrdi > 0
            OPT.step.dual.mu_rdi = reshape(OPT.qp.solineqmult(sidx+(1:(ns-1)*nrdi)), nrdi, ns-1);
            sidx = sidx + (ns-1)*nrdi;
        end
        if nrde > 0
            OPT.step.dual.mu_rde = OPT.qp.solineqmult(sidx+(1:nrde));
        end
    else
        OPT.step.dual.mu_sd = reshape([OPT.qp.soleqmult;OPT.qp.solineqmult], nsd, ns+1);
    end
    
    OPT.step.dual.mu_q = reshape(OPT.qp.solbndmult, nq, ns);
    
    % Expand step in s1,..sN
    for ii=1:ns
        OPT.step.primal.sd(:,ii+1) = OPT.eval.mat.Gx{ii} * OPT.step.primal.sd(:,ii) + ...
            OPT.eval.mat.Gq{ii} * OPT.step.primal.q(:,ii) + OPT.eval.res.c(:,ii);
    end
    
    % Multipliers of QP after linear transformation before elimination
    if nrdi > 0
        for ii=1:ns-1
            nuestar(:,ii) = -OPT.hess.ssblock{ii+1} * OPT.step.primal.sd(:,ii+1) ...
                - OPT.hess.sqblock{ii+1} * OPT.step.primal.q(:,ii+1) - OPT.eval.res.modgradx(:,ii+1) ...
                + OPT.step.dual.mu_sd(:,ii+1) + OPT.eval.mat.Jrdix{ii}' * OPT.step.dual.mu_rdi(:,ii);
        end
    else
        for ii=1:ns-1
            nuestar(:,ii) = -OPT.hess.ssblock{ii+1} * OPT.step.primal.sd(:,ii+1) ...
                - OPT.hess.sqblock{ii+1} * OPT.step.primal.q(:,ii+1) - OPT.eval.res.modgradx(:,ii+1) ...
                + OPT.step.dual.mu_sd(:,ii+1);
        end
    end
    
    nuestar(:,ns) = -OPT.hess.ssblock{ns+1} * OPT.step.primal.sd(:,ns+1) - OPT.eval.res.modgradx(:,ns+1) ...
        + OPT.step.dual.mu_sd(:,ns+1);
    if nrde > 0
        nuestar(:,ns) = nuestar(:,ns) + OPT.eval.mat.Jrdex' * OPT.step.dual.mu_rde;
    end
    
    
    % Equality multipliers (lambda continuity; lambda initial cond.)
    OPT.step.dual.lambda(:,ns) = nuestar(:,ns);
    for ii=1:ns-1
        OPT.step.dual.lambda(:,ns-ii) = nuestar(:,ns-ii) + ...
            OPT.eval.mat.Gx{ns-ii+1}' * OPT.step.dual.lambda(:,ns-ii+1);
    end
    
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
      % New delq multipliers
      for ii=1:ns
        idx = (ii-1)*nq;
        OPT.step.dual.mu_delq(:,ii) = OPT.qp.solbndmult2(idx+(1:nq),1);
      end
      % New CMR multipliers
      OPT.step.dual.lambda1 = reshape(OPT.qp.solCMRmult(1:ns*nq), nq, ns);
      OPT.step.dual.lambda2 = reshape(OPT.qp.solCMRmult(ns*nq+(1:ns*nq)), nq, ns);
    end
    
    % Calculate actual steps in multipliers (new mult - old mult)
    OPT.step.dualstep.lambda = OPT.step.dual.lambda - OPT.phase{phase}.var.dual.lambda;
    OPT.step.dualstep.mu_sd = OPT.step.dual.mu_sd - OPT.phase{phase}.var.dual.mu_sd;
    OPT.step.dualstep.mu_q = OPT.step.dual.mu_q - OPT.phase{phase}.var.dual.mu_q;
    if nrds > 0
        OPT.step.dualstep.mu_rds = OPT.step.dual.mu_rds - OPT.phase{phase}.var.dual.mu_rds;
    end
    if nrdi > 0
        OPT.step.dualstep.mu_rdi = OPT.step.dual.mu_rdi - OPT.phase{phase}.var.dual.mu_rdi;
    end
    if nrde > 0
        OPT.step.dualstep.mu_rde = OPT.step.dual.mu_rde - OPT.phase{phase}.var.dual.mu_rde;
    end
    
    % Stepnorm
    pstep = [OPT.step.primal.sd(:);OPT.step.primal.q(:)];
    
    dstep = [OPT.step.dualstep.lambda(:);OPT.step.dualstep.mu_sd(:);OPT.step.dualstep.mu_q(:)];
    if nrds > 0
        dstep = [dstep;OPT.step.dualstep.mu_rds(:)];
    end
    if nrdi > 0
        dstep = [dstep;OPT.step.dualstep.mu_rdi(:)];
    end
    if nrde > 0
        dstep = [dstep;OPT.step.dualstep.mu_rde(:)];
    end
    
    OPT.step.stepnorm = norm([pstep;dstep]);

else %% ST
    S = ST.S;
    for jj = 1:S    
        % Step in q
        OPT.step{jj}.primal.q = reshape(OPT.qp{jj}.solstep(nsd+1:end,1),nq,ns);
        
        % Step in s0
        OPT.step{jj}.primal.sd(:,1) = OPT.qp{jj}.solstep(1:nsd,1);
    end
    
    % delq
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
      for jj = 1:S
        for ii = 1:ns-1+1
          idx = (ii-1)*nq;
          OPT.step{jj}.primal.delq(:,ii) = OPT.qp{jj}.soldelq(idx+(1:nq));
        end
      end
    end
    
    % Inequality multipliers
    if nrd > 0
        for jj = 1:S
            OPT.step{jj}.dual.mu_sd = reshape([OPT.qp{jj}.soleqmult;OPT.qp{jj}.solineqmult(1:ns*nsd)], nsd, ns+1);
            sidx = ns*nsd;
            if nrds > 0
                OPT.step{jj}.dual.mu_rds = OPT.qp{jj}.solineqmult(sidx+(1:nrds));
                sidx = sidx + nrds;
            end
            if nrdi > 0
                OPT.step{jj}.dual.mu_rdi = reshape(OPT.qp{jj}.solineqmult(sidx+(1:(ns-1)*nrdi)), nrdi, ns-1);
                sidx = sidx + (ns-1)*nrdi;
            end
            if nrde > 0
                OPT.step{jj}.dual.mu_rde = OPT.qp{jj}.solineqmult(sidx+(1:nrde));
            end
        end
    else
        for jj = 1:S
            OPT.step{jj}.dual.mu_sd = reshape([OPT.qp{jj}.soleqmult;OPT.qp{jj}.solineqmult], nsd, ns+1);
        end
    end
    
    for jj = 1:S
        OPT.step{jj}.dual.mu_q = reshape(OPT.qp{jj}.solbndmult, nq, ns);
    end
    
    % Expand step in s1,..sN
    for jj = 1:S
        for ii=1:ns
            OPT.step{jj}.primal.sd(:,ii+1) = OPT.eval{jj}.mat.Gx{ii} * OPT.step{jj}.primal.sd(:,ii) + ...
                OPT.eval{jj}.mat.Gq{ii} * OPT.step{jj}.primal.q(:,ii) + OPT.eval{jj}.res.c(:,ii);
        end
    end
    
    % Multipliers of QP after linear transformation before elimination
    nuestar = cell(1,S);
    if nrdi > 0
        for jj = 1:S
            for ii=1:ns-1
                nuestar{jj}(:,ii) = -OPT.hess{jj}.ssblock{ii+1} * OPT.step{jj}.primal.sd(:,ii+1) ...
                    - OPT.hess{jj}.sqblock{ii+1} * OPT.step{jj}.primal.q(:,ii+1) - OPT.eval{jj}.res.modgradx(:,ii+1) ...
                    + OPT.step{jj}.dual.mu_sd(:,ii+1) + OPT.eval{jj}.mat.Jrdix{ii}' * OPT.step{jj}.dual.mu_rdi(:,ii);
            end
        end
    else
        for jj = 1:S
            for ii=1:ns-1
                nuestar{jj}(:,ii) = -OPT.hess{jj}.ssblock{ii+1} * OPT.step{jj}.primal.sd(:,ii+1) ...
                    - OPT.hess{jj}.sqblock{ii+1} * OPT.step{jj}.primal.q(:,ii+1) - OPT.eval{jj}.res.modgradx(:,ii+1) ...
                    + OPT.step{jj}.dual.mu_sd(:,ii+1);
            end
        end
    end
    
    for jj = 1:S
        nuestar{jj}(:,ns) = -OPT.hess{jj}.ssblock{ns+1} * OPT.step{jj}.primal.sd(:,ns+1) - OPT.eval{jj}.res.modgradx(:,ns+1) ...
            + OPT.step{jj}.dual.mu_sd(:,ns+1);
    end
    if nrde > 0
        for jj = 1:S
            nuestar{jj}(:,ns) = nuestar{jj}(:,ns) + OPT.eval{jj}.mat.Jrdex' * OPT.step{jj}.dual.mu_rde;
        end
    end
    
    
    % Equality multipliers (lambda continuity; lambda initial cond.)
    for jj = 1:S
        OPT.step{jj}.dual.lambda(:,ns) = nuestar{jj}(:,ns);
        for ii=1:ns-1
            OPT.step{jj}.dual.lambda(:,ns-ii) = nuestar{jj}(:,ns-ii) + ...
                OPT.eval{jj}.mat.Gx{ns-ii+1}' * OPT.step{jj}.dual.lambda(:,ns-ii+1);
        end
    end
    
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
      % New delq multipliers
      for jj = 1:S
        for ii=1:ns
          idx = (ii-1)*nq;
          OPT.step{jj}.dual.mu_delq(:,ii) = OPT.qp{jj}.solbndmult2(idx+(1:nq),1);
        end
        % New CMR multipliers
        OPT.step{jj}.dual.lambda1 = reshape(OPT.qp{jj}.solCMRmult(1:ns*nq), nq, ns);
        OPT.step{jj}.dual.lambda2 = reshape(OPT.qp{jj}.solCMRmult(ns*nq+(1:ns*nq)), nq, ns);
      end
    end
    
    % Calculate actual steps in multipliers (new mult - old mult)
    for jj = 1:S
        OPT.step{jj}.dualstep.lambda = OPT.step{jj}.dual.lambda - OPT.phase{phase}{jj}.var.dual.lambda;
        OPT.step{jj}.dualstep.mu_sd = OPT.step{jj}.dual.mu_sd - OPT.phase{phase}{jj}.var.dual.mu_sd;
        OPT.step{jj}.dualstep.mu_q = OPT.step{jj}.dual.mu_q - OPT.phase{phase}{jj}.var.dual.mu_q;
    end
    if nrds > 0
        for jj = 1:S
            OPT.step{jj}.dualstep.mu_rds = OPT.step{jj}.dual.mu_rds - OPT.phase{phase}{jj}.var.dual.mu_rds;
        end
    end
    if nrdi > 0
        for jj = 1:S
            OPT.step{jj}.dualstep.mu_rdi = OPT.step{jj}.dual.mu_rdi - OPT.phase{phase}{jj}.var.dual.mu_rdi;
        end
    end
    if nrde > 0
        for jj = 1:S
            OPT.step{jj}.dualstep.mu_rde = OPT.step{jj}.dual.mu_rde - OPT.phase{phase}{jj}.var.dual.mu_rde;
        end
    end
    
    % Stepnorm
    pstep = cell(1,S);
    dstep = cell(1,S);
    
    for jj = 1:S
        pstep{jj} = [OPT.step{jj}.primal.sd(:);OPT.step{jj}.primal.q(:)];
        
        dstep{jj} = [OPT.step{jj}.dualstep.lambda(:);OPT.step{jj}.dualstep.mu_sd(:);OPT.step{jj}.dualstep.mu_q(:)];
        
        if nrds > 0
            dstep{jj} = [dstep{jj};OPT.step{jj}.dualstep.mu_rds(:)];
        end
        if nrdi > 0
            dstep{jj} = [dstep{jj};OPT.step{jj}.dualstep.mu_rdi(:)];
        end
        if nrde > 0
            dstep{jj} = [dstep{jj};OPT.step{jj}.dualstep.mu_rde(:)];
        end
        
        OPT.step{jj}.stepnorm = norm([pstep{jj};dstep{jj}]);
    end
end

OPT.blowuptime = toc;
OPT.ipopttime = 0;

