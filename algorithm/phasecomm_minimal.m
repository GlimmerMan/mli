% -*-matlab-*-
function phasecomm_minimal(phase)

global OPT;

switch phase
  case {0,4}
    if ismember(2, OPT.ctrl.usephases)
        OPT.phase{2}.ref = OPT.phase{4}.var;
    end
    if ismember(1, OPT.ctrl.usephases)
        OPT.phase{1}.ref = OPT.phase{4}.var;
    end
    
  case 1
    % Do nothing
    
  case 2
    if ismember(1, OPT.ctrl.usephases)
        OPT.phase{1}.ref = OPT.phase{2}.var;
    end
     
  case 3
    if ismember(2, OPT.ctrl.usephases)
        OPT.phase{2}.ref = OPT.phase{3}.var;
    end
    if ismember(1, OPT.ctrl.usephases)
        OPT.phase{1}.ref = OPT.phase{3}.var;
    end 
end
