function mli_printinfoST(forceheader, delta)

global OPT ST;

nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;
E = ST.Ec;
C = ST.Cc;
  
% if OPT.use_condensing
  nz = nsd + nq * ns;
% else
%   nz = nsd * (ns+1) + nq * ns;
% end


ii = OPT.currentsample;
phase = OPT.currentphase;

feas = [];
pstep = [];
dstep = [];
laggrad = [];
if phase ~= 9
  for jj = 1:ST.S
    ipc = [];
    if nrds > 0
      ipc = [ipc;OPT.eval{jj}.res.rds(:)];
    end
    if nrdi > 0
      ipc = [ipc;OPT.eval{jj}.res.rdi(:)];
    end
    if nrde > 0
      ipc = [ipc;OPT.eval{jj}.res.rde(:)];
    end
    
    % violated matching and interior point constraints
    % bounds and initial condition should always be satisfied due to linearity
    if ~isempty(ipc)
      feas = [feas; OPT.eval{jj}.res.c(:);min(ipc,zeros(size(ipc)))];
    else
      feas = [feas; OPT.eval{jj}.res.c(:)];
    end
    if jj < ST.S
      feas = [feas; E{jj+1}(:,(nsd+1):nz)*OPT.step{jj+1}.primal.q(:)-C{jj}(:,(nsd+1):nz)*OPT.step{jj}.primal.q(:)];
    end
    
    pstep = [pstep; OPT.step{jj}.primal.sd(:);OPT.step{jj}.primal.q(:)];
    
    dstep = [dstep; OPT.step{jj}.dualstep.lambda(:);OPT.step{jj}.dualstep.mu_sd(:);...
      OPT.step{jj}.dualstep.mu_q(:)];
    if nrds > 0
      dstep = [dstep;OPT.step{jj}.dualstep.mu_rds(:)];
    end
    if nrdi > 0
      dstep = [dstep;OPT.step{jj}.dualstep.mu_rdi(:)];
    end
    if nrde > 0
      dstep = [dstep;OPT.step{jj}.dualstep.mu_rde(:)];
    end
    if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1 && (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
      pstep = [pstep; OPT.step{jj}.primal.delq(:)];
      dstep = [dstep; OPT.step{jj}.dualstep.mu_delq(:); OPT.step{jj}.dualstep.lambda1(:); OPT.step{jj}.dualstep.lambda2(:)];
    end

    nonanticipativity = (C{jj}'-E{jj}')*ST.lambda;
    nonanticipativity = nonanticipativity(nsd+1:nz);

    laggrad = [laggrad; OPT.eval{jj}.res.laggradx(:);OPT.eval{jj}.res.laggradq(:) + nonanticipativity];
  end
end

feasn = norm(feas);
pstepn = norm(pstep);
dstepn = norm(dstep);
stepn = norm([pstep;dstep]);
laggradn = norm(laggrad);

if nargin == 1
    
    if (forceheader ~= 0)
        fprintf('\n%5s %2s %4s %9s %9s %9s %9s %9s %4s %4s %9s %9s %9s\n', ...
            'iter', 'ph', 'it_N', '|feas|', '|pstep|', '|dstep|', ...
            '|step|', '|laggr|', 'outR', 'inR', 'evltime', 'condtime', 'qptime');
    end

    
    fprintf('%5d %2d %4d %9.2e %9.2e %9.2e %9.2e %9.2e %4d %4d %9.2e %9.2e %9.2e\n', ...
        ii, phase, ST.it_N, feasn, pstepn, dstepn, stepn, laggradn, ...
        ST.reg(1:2), OPT.evaltime{1}, OPT.condtime{1}, OPT.qptime);
else
    if (forceheader ~= 0)
        fprintf('\n%5s %2s %4s %9s %9s %9s %9s %9s %9s %4s %4s %9s %9s %9s\n', ...
            'iter', 'ph', 'it_N', '|feas|', '|pstep|', '|dstep|', ...
            '|step|', '|laggr|', 'outR', 'inR', 'evltime', 'condtime', 'qptime', 'estdelta');
    end
    
    fprintf('%5d %2d %9.2g %9.2e %9.2e %9.2e %9.2e %9.2e %9.2e %9.2e %9.2e\n', ...
        ii, phase, feasn, pstepn, dstepn, stepn, laggradn, ...
        OPT.evaltime{1}, OPT.condtime{1}, OPT.qptime, delta);
end