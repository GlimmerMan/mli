function [outp] = eval_rdfcn_D_gn(inp, nodetype)
    
    global SLV
    
    sensdirs = inp.sensdirs;
    args.t = inp.t;    
    args.xd = inp.sd;
    args.qph = [inp.q;inp.p];    

    ndf = size(sensdirs,2);
    args.fwdTCOrder = 1;
    args.nFwdTC = ndf;
    args.fwdTC = sensdirs;
    args.nAdjTC = 0;
    args.adjTC = [];

    evl = SLV.opt.evl;
    
    switch nodetype
      case 's'
	args.rhs = solvind('evaluateFcn', evl, 'rdfcn_s', args);
	result = solvind('evaluateDenseDer', evl, 'rdfcn_s', args);
	
      case 'i'
	args.rhs = solvind('evaluateFcn', evl, 'rdfcn_i', args);
	result = solvind('evaluateDenseDer', evl, 'rdfcn_i', args);
	
      case 'e'
	args.rhs = solvind('evaluateFcn', evl, 'rdfcn_e', args);
	result = solvind('evaluateDenseDer', evl, 'rdfcn_e', args);
	
      otherwise
	error('eval_rdfcn_D: Invalid nodetype');
	
    end

    outp.rdfcn = result.rhs;
    outp.rdjac = result.propFwdTC;
    
