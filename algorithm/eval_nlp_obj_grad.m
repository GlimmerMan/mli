function fcn = eval_nlp_obj_grad(vars)
global SCEN OPT ST

if SCEN.use_st == 0
    fcn = @eval;
else
    fcn = @evalST;
end

ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
nmfcn = OPT.dims.mfcn;
nlfcn = OPT.dims.lfcn;
CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;

  function [obj] = eval(x)
    
    vars.primal.sd = reshape(x(1:nsd*(ns+1)), nsd, ns+1);
    vars.primal.q = reshape(x(nsd*(ns+1)+(1:nq*ns)), nq, ns);
    vars.objective.sigma = -1;
    if CMR
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        vars.primal.delq = reshape(x((nsd+nq)*ns+nsd+(1:(nq*(ns-1+1)))), nq, ns-1+1);
      else
        for ii = 1:ns-1
          vars.primal.primal.delq(:,ii) = vars.primal.q(:,ii+1) - vars.primal.q(:,ii);
        end
        vars.primal.delq(:,ns) = vars.primal.q(:,1) - OPT.controlmove_q0;
      end
    end
    
    OPT.nlp.data = mli_evalNLP(vars);
    
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
      obj = zeros((nsd+nq)*ns+nsd+nq*(ns-1+1),1);
    else
      obj = zeros((nsd+nq)*ns+nsd,1);
    end
    
    for msn=1:ns
      
      xidx = (msn-1)*nsd;
      qidx = nsd*(ns+1) + (msn-1)*nq;
      
%       if nlfcn > 0        
%         obj(xidx+(1:nsd)) = OPT.nlp.data.res.dlfcn(1:nsd,msn);
%         obj(qidx+(1:nq)) = OPT.nlp.data.res.dlfcn(nsd+(1:nq),msn);
%       end
      obj(xidx+(1:nsd)) = OPT.nlp.data.res.modgradx(:,msn);
      obj(qidx+(1:nq)) = OPT.nlp.data.res.modgradq(:,msn);
    end
    obj(ns*nsd+(1:nsd)) = OPT.nlp.data.res.modgradx(:,ns+1);
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
      delqidx = nsd*(ns+1) + nq*ns + (msn-1)*nq;
      for msn=1:ns
        obj(delqidx+(1:nq)) = OPT.nlp.data.res.modgraddelq(:,msn);
      end
    end
    
    if nmfcn > 0 && nlfcn <= 0
      if isfield(OPT,'periodicity')
        %objective is split in start parts corresponding to shooting nodes.
        for ii=1:OPT.periodicity.transient
          x1idx_start=(ii-1)*(nsd)+(1:nsd);
          x1idx_end=x1idx_start+(nsd);
          x2idx_start=(ii-1+OPT.periodicity.periodic)*(nsd)+(1:nsd);
          x2idx_end=x2idx_start+(nsd);
          %plus the discount factor
          eval=(OPT.periodicity.discount^ii)*((OPT.nlp.data.res.mfun{ii}.end-OPT.nlp.data.res.mfun{ii}.start)-(OPT.nlp.data.res.mfun{ii+OPT.periodicity.periodic}.end-OPT.nlp.data.res.mfun{ii+OPT.periodicity.periodic}.start));
          obj(x1idx_start)=obj(x1idx_start)-2*OPT.periodicity.alpha*eval*OPT.nlp.data.res.mgrad{ii}.start;
          obj(x1idx_end)=obj(x1idx_end)+2*OPT.periodicity.alpha*eval*OPT.nlp.data.res.mgrad{ii}.end;
          obj(x2idx_start)=obj(x2idx_start)+2*OPT.periodicity.alpha*eval*OPT.nlp.data.res.mgrad{ii+OPT.periodicity.periodic}.start;
          obj(x2idx_end)=obj(x2idx_end)-2*OPT.periodicity.alpha*eval*OPT.nlp.data.res.mgrad{ii+OPT.periodicity.periodic}.end;
        end
        
        
        if OPT.periodicity.transient>=1
          %add sensitivity wrt state and control track objectives
          %state part
          x1idx=(OPT.periodicity.transient-1)*(nsd)+(1:nsd);
          x2idx=(OPT.periodicity.transient+OPT.periodicity.periodic-1)*(nsd)+(1:nsd);
          eval=OPT.periodicity.beta.sd*(vars.primal.sd(:,OPT.periodicity.transient)-vars.primal.sd(:,OPT.periodicity.transient+OPT.periodicity.periodic));
          obj(x1idx)=obj(x1idx)+2*eval*OPT.periodicity.beta.sd';
          obj(x2idx)=obj(x2idx)-2*eval*OPT.periodicity.beta.sd';
          %control part
          q1idx=(ns+1)*nsd+(OPT.periodicity.transient-1)*(nq)+(1:nq);
          q2idx=(ns+1)*nsd+(OPT.periodicity.transient+OPT.periodicity.periodic-1)*(nq)+(1:nq);
          eval=OPT.periodicity.beta.q*(vars.primal.q(:,OPT.periodicity.transient)-vars.primal.q(:,OPT.periodicity.transient+OPT.periodicity.periodic));
          obj(q1idx)=obj(q1idx)+2*eval*OPT.periodicity.beta.q';
          obj(q2idx)=obj(q2idx)-2*eval*OPT.periodicity.beta.q';
        end
        for ii=OPT.periodicity.transient+1:OPT.periodicity.transient+OPT.periodicity.periodic
          xidx_start=(ii-1)*(nsd)+(1:nsd);
          xidx_end=xidx_start+(nsd);
          obj(xidx_start)=obj(xidx_start)-OPT.nlp.data.res.mgrad{ii}.start;
          obj(xidx_end)=obj(xidx_end)+OPT.nlp.data.res.mgrad{ii}.end;
        end
        
      else
        xidx = ns*nsd;
        obj(xidx+(1:nsd)) = OPT.nlp.data.res.mgrad;
      end
      
    end
%     
%     if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
%       vars.primal.delq = reshape(x((nsd+nq)*ns+nsd+(1:nq*(ns-1))),nq,ns-1);
%       if isfield(OPT, 'controlmove_c')
%         c = OPT.controlmove_c;
%       else
%         c = zeros(nq,1);
%       end
%       if isfield(OPT, 'controlmove_R')
%         R = OPT.controlmove_R;
%       else
%         R = zeros(nq,nq);
%       end
%       xoff = ns*(nsd+nq)+nsd;
%       for ii=1:ns-1
%         idx = xoff + (ii-1)*nq;
%         obj(idx+(1:nq),1) = c + 2 * R * vars.primal.delq(:,ii);
%       end
%     end
    
  end

  function [obj] = evalST(x)
    
    S = ST.S;
    for jj = 1:S
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
        nz = (nsd+nq)*ns+nsd+nq*(ns-1+1);
        offset = (jj-1)*nz;
      else
        nz = (nsd+nq)*ns+nsd;
        offset = (jj-1)*nz;
      end
      vars{jj}.primal.sd = reshape(x(offset+(1:nsd*(ns+1))), nsd, ns+1);
      vars{jj}.primal.q = reshape(x(offset+(nsd*(ns+1)+(1:nq*ns))), nq, ns);
      vars{jj}.objective.sigma = -1;
      vars{jj}.primal.z_ipopt = x(offset+(1:nz))';
      if CMR
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
          vars{jj}.primal.delq = reshape(x(offset+((nsd+nq)*ns+nsd+(1:(nq*(ns-1+1))))), nq, ns-1+1);
        else
          for ii = 1:ns-1
            vars{jj}.primal.delq(:,ii) = vars{jj}.primal.q(:,ii+1) - vars{jj}.primal.q(:,ii);
          end
          vars{jj}.primal.delq(:,ns) = vars{jj}.primal.q(:,1) - OPT.controlmove_q0;
        end
      end
    end
    
    OPT.nlp.data = mli_evalNLP(vars);
    
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
      obj = zeros(S*((nsd+nq)*ns+nsd+nq*(ns-1+1)),1);
    else
      obj = zeros(S*((nsd+nq)*ns+nsd),1);
    end
    
    for jj = 1:S
      
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
        offset = (jj-1)*((nsd+nq)*ns+nsd+nq*(ns-1+1));
      else
        offset = (jj-1)*((nsd+nq)*ns+nsd);
      end
      
      for msn=1:ns
        
        xidx = (msn-1)*nsd;
        qidx = nsd*(ns+1) + (msn-1)*nq;
        
        obj(offset+(xidx+(1:nsd))) = OPT.nlp.data{jj}.res.modgradx(:,msn);
        obj(offset+(qidx+(1:nq))) = OPT.nlp.data{jj}.res.modgradq(:,msn);
      end
      obj(offset+(ns*nsd+(1:nsd))) = OPT.nlp.data{jj}.res.modgradx(:,ns+1);
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
        delqidx = nsd*(ns+1) + nq*ns + (msn-1)*nq;
        for msn=1:ns
          obj(offset+(delqidx+(1:nq))) = OPT.nlp.data{jj}.res.modgraddelq(:,msn);
        end
      end
    
%       for msn=1:ns
%         
%         xidx = (msn-1)*nsd;
%         qidx = nsd*(ns+1) + (msn-1)*nq;
%         
%         if nlfcn > 0
%           obj(offset+(xidx+(1:nsd))) = OPT.nlp.data{jj}.res.dlfcn(1:nsd,msn);
%           obj(offset+(qidx+(1:nq))) = OPT.nlp.data{jj}.res.dlfcn(nsd+(1:nq),msn);
%         end
%         
%       end
%       
%       if nmfcn > 0 && nlfcn <= 0
%         
%         xidx = ns*nsd;
%         
%         obj(offset+(xidx+(1:nsd))) = OPT.nlp.data{jj}.res.mgrad;
%       end
%       if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
%         vars{jj}.primal.delq = reshape(x(offset+((nsd+nq)*ns+nsd+(1:nq*(ns-1)))),nq,ns-1);
%         if isfield(OPT, 'controlmove_c')
%           c = OPT.controlmove_c;
%         else
%           c = zeros(nq,1);
%         end
%         if isfield(OPT, 'controlmove_R')
%           R = OPT.controlmove_R;
%         else
%           R = zeros(nq,nq);
%         end
%         xoff = ns*(nsd+nq)+nsd;
%         for ii=1:ns-1
%           idx = xoff + (ii-1)*nq;
%           obj(offset+idx+(1:nq),1) = c + 2 * R * vars{jj}.primal.delq(:,ii);
%         end
%       end
    end
    
  end

end