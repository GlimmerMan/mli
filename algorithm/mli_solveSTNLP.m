
function mli_solveSTNLP

global OPT SCEN SLV ST;

tic

ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd; 
nq = OPT.dims.nq;

nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nlsqs = OPT.dims.lsqfcn_s;
nlsqi = OPT.dims.lsqfcn_i;
nlsqe = OPT.dims.lsqfcn_e;

CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;

S = ST.S;

if nrds > 0 || nrdi > 0 || nrde > 0
   error('Decoupled node constraints currently not implemented for Ipopt.') 
end

if nlsqs > 0 || nlsqi > 0 || nlsqe > 0
   error('Least squares objective currently not implemented for Ipopt.')
end

OPT.nlp.data = init_nlp_data;

OPT.nlp.cache.pvars = [];
OPT.nlp.cache.dvars = [];
OPT.nlp.cache.ovars = [];
OPT.nlp.cache.data = init_nlp_data;

vars = cell(S,1);
pvars = cell(S,1);

for jj=1:S
    vars{jj} = OPT.phase{4}{jj}.var;
    pvars{jj} = vars{jj}.primal;
end

% SCEN.currentsample = 1; % only at init
user_opt_x_from_scen;

% Set initial values of NLP variables
if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
  x0 = zeros(1,S*((nsd+nq)*ns+nsd+nq*(ns-1)));
  for jj = 1:S
    offset = (jj-1)*((nsd+nq)*ns+nsd+nq*(ns-1+1));
    x0(offset+(1:nsd*(ns+1))) = reshape(pvars{jj}.sd,[],1);
    x0(offset+(nsd*(ns+1)+(1:nq*ns))) = reshape(pvars{jj}.q,[],1);
    x0(offset+((nsd+nq)*ns+nsd+(1:nq*(ns-1+1)))) = reshape(pvars{jj}.delq,[],1);
    x0(offset+(1:nsd)) = SLV.opt.lagObjModSd * OPT.x0(:); % init val at MPC time = 0
  end
else
  x0 = zeros(1,S*((nsd+nq)*ns+nsd));
  for jj = 1:S
    offset = (jj-1)*((nsd+nq)*ns+nsd);
    x0(offset+(1:nsd*(ns+1))) = reshape(pvars{jj}.sd,[],1);
    x0(offset+(nsd*(ns+1)+(1:nq*ns))) = reshape(pvars{jj}.q,[],1);
    x0(offset+(1:nsd)) = SLV.opt.lagObjModSd * OPT.x0(:); % init val at MPC time = 0
  end
end

% Set lower and upper bound values of NLP variables
if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
  bigbnd = 1e12;
  options.lb = zeros(1,S*((nsd+nq)*ns+nsd+nq*(ns-1+1)));
  options.ub = zeros(1,S*((nsd+nq)*ns+nsd+nq*(ns-1+1)));
  for jj = 1:S
    offset = (jj-1)*((nsd+nq)*ns+nsd+nq*(ns-1+1));
    options.lb(offset+(1:nsd*(ns+1))) = reshape(SLV.opt.lagObjModSd*OPT.bounds.sdLoB,[],1);
    options.lb(offset+(nsd*(ns+1)+(1:nq*ns))) = reshape(OPT.bounds.qLoB,[],1);
    options.lb(offset+((nsd+nq)*ns+nsd+(1:nq*(ns-1+1)))) = -bigbnd*ones(nq*(ns-1+1),1);
    options.lb(offset+(1:nsd)) = x0(offset+(1:nsd));
    options.ub(offset+(1:nsd*(ns+1))) = reshape(SLV.opt.lagObjModSd*OPT.bounds.sdUpB,[],1);
    options.ub(offset+(nsd*(ns+1)+(1:nq*ns))) = reshape(OPT.bounds.qUpB,[],1);
    options.ub(offset+((nsd+nq)*ns+nsd+(1:nq*(ns-1+1)))) = [repmat(OPT.controlmove_alpha',1,(ns-1)),OPT.controlmove_alpha0'];
    options.ub(offset+(1:nsd)) = x0(offset+(1:nsd));    
  end
else
  options.lb = zeros(1,S*((nsd+nq)*ns+nsd));
  options.ub = zeros(1,S*((nsd+nq)*ns+nsd));
  for jj = 1:S
    offset = (jj-1)*((nsd+nq)*ns+nsd);
    options.lb(offset+(1:nsd*(ns+1))) = reshape(SLV.opt.lagObjModSd*OPT.bounds.sdLoB,[],1);
    options.lb(offset+(nsd*(ns+1)+(1:nq*ns))) = reshape(OPT.bounds.qLoB,[],1);
    options.lb(offset+(1:nsd)) = x0(offset+(1:nsd));
    options.ub(offset+(1:nsd*(ns+1))) = reshape(SLV.opt.lagObjModSd*OPT.bounds.sdUpB,[],1);
    options.ub(offset+(nsd*(ns+1)+(1:nq*ns))) = reshape(OPT.bounds.qUpB,[],1);
    options.ub(offset+(1:nsd)) = x0(offset+(1:nsd));
  end
end

% Set non-anticipativity matrices
% reordering of E, C due to s,q ordering introduced in solve_NLP
idx = []; % index for variable ordering in solve_NLP

if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
for kk = 1:ns
  offset = (kk-1)*(nsd+nq+nq);
  idx = [idx,offset+(1:nsd)];
end
offset = (ns-1)*(nsd+nq+nq)+(nsd+nq);
idx = [idx,offset+(1:nsd)];
else
  for kk = 1:ns+1
    offset = (kk-1)*(nsd+nq);
    idx = [idx,offset+(1:nsd)];
  end 
end
for kk = 1:ns
  if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
    offset = kk*nsd + (kk-1)*(nq+nq);
  else
    offset = kk*nsd + (kk-1)*nq;
  end
  idx = [idx,offset+(1:nq)];
end
if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
  for kk = 1:ns-1+1
    offset = kk*(nsd+nq) + (kk-1)*nq;
    idx = [idx,offset+(1:nq)];
  end
end

ST.E_ipopt = cell(S,1);
ST.C_ipopt = cell(S,1);
for jj = 1:S
  ST.E_ipopt{jj} = ST.E{jj}(:,idx);
  ST.C_ipopt{jj} = ST.C{jj}(:,idx);
end

% Non-anticipativity Jacobian block
rowindex = [];
columnindex = [];
vals = [];
nC = zeros(S-1,2);
for jj = 1:S-1
  nozrows = sum(ST.C_ipopt{jj},2) > 0;
  nC(jj,:) = size(ST.C_ipopt{jj}(nozrows,:));
  [i,k,v] = find([ST.C_ipopt{jj}(nozrows,:), -ST.E_ipopt{jj+1}(nozrows,:)]);
  if ~iscolumn(i)
    i = i';
    k = k';
    v = v';
  end
  rowindex = [rowindex; sum(nC(1:jj-1,1)) + i];
  columnindex = [columnindex; sum(nC(1:jj-1,2)) + k];
  vals = [vals; v];
end
nrs = max(rowindex);
if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
  ncs = S*(ns*(nsd+nq)+nsd + (ns-1+1)*nq);
else
ncs = S*((nsd+nq)*ns+nsd);
end

ST.Jna_ipopt = sparse(rowindex, columnindex, vals, nrs, ncs);
    
% Set lower and upper bound values of constraints
if CMR
  if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
    options.cl = zeros(1, S*(nsd*ns+2*(ns-1+1)*nq)+nrs);
    options.cu = [repmat([zeros(1, nsd*ns), inf*ones(1,2*(ns-1+1)*nq)],1,S),zeros(1,nrs)];
  else
    options.cl = [repmat([zeros(1, nsd*ns), repmat(-OPT.controlmove_alpha',1,(ns-1)), -OPT.controlmove_alpha0'],1,S), zeros(1,nrs)];
    options.cu = [repmat([zeros(1, nsd*ns), repmat(OPT.controlmove_alpha',1,(ns-1)), OPT.controlmove_alpha0'],1,S), zeros(1,nrs)];
  end
else
  options.cl = zeros(1, S*nsd*ns + nrs);
  options.cu = zeros(1, S*nsd*ns + nrs);
end

% Set the IPOPT options.
% options.ipopt.hessian_approximation = 'limited-memory';
options.ipopt.mu_strategy           = 'adaptive';
% tol_S = floor(log10(S));
options.ipopt.tol                   = 1e-3;
% options.ipopt.derivative_test       = 'second-order';
options.ipopt.fixed_variable_treatment = 'make_constraint';
options.ipopt.linear_solver = 'ma57';

% The callback functions. %!
funcs.objective         = eval_nlp_obj(vars);
funcs.gradient          = eval_nlp_obj_grad(vars);
funcs.constraints       = eval_nlp_cst(vars);
funcs.jacobian          = eval_nlp_cst_jac(vars);
funcs.jacobianstructure = eval_nlp_cst_jac_structure(vars);
funcs.hessian           = eval_nlp_hess(vars);
funcs.hessianstructure  = eval_nlp_hess_structure(vars);
            
% Run IPOPT.
try
  [ipopt_output, x, info] = evalc('ipopt(x0, funcs, options)');
catch
  error('Exception in Ipopt')
  keyboard
end
fid = fopen('ipopt.out', 'w');
fprintf(fid, ipopt_output);
fclose(fid);

J = ceil(ST.S/2);
if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
  offset = (J-1)*((nsd+nq)*ns+nsd+nq*(ns-1+1));
else
  offset = (J-1)*((nsd+nq)*ns+nsd);
end

OPT.feedbackq = x(offset+(nsd*(ns+1)+(1:nq)));
% OPT.feedbackdelta = feedbackq - OPT.phase{4}{J}.var.primal.q(:,1); % be careful
OPT.ipopttime = info.cpu;

for jj=1:S
    if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
      OPT.phase{4}{jj}.varold = OPT.phase{4}{jj}.var;
    end
    
    % write result to phase variable
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
      offset = (jj-1)*((nsd+nq)*ns+nsd+nq*(ns-1+1));
    else
      offset = (jj-1)*((nsd+nq)*ns+nsd);
    end
    OPT.phase{4}{jj}.var.primal.sd = reshape(x(offset+(1:nsd*(ns+1))), nsd, ns+1);
    OPT.phase{4}{jj}.var.primal.q = reshape(x(offset+(nsd*(ns+1)+(1:nq*ns))), nq, ns);
    
    if CMR
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        OPT.phase{4}{jj}.var.primal.delq = reshape(x(offset+((nsd+nq)*ns+nsd+(1:(nq*(ns-1+1))))), nq, ns-1+1);
      else
        for ii = 1:ns-1
          OPT.phase{4}{jj}.var.primal.delq(:,ii) = OPT.phase{4}{jj}.var.primal.q(:,ii+1) - OPT.phase{4}{jj}.var.primal.q(:,ii);
        end
        OPT.phase{4}{jj}.var.primal.delq(:,ns) = OPT.phase{4}{jj}.var.primal.q(:,1) - OPT.controlmove_q0;
      end
    end
    
    % administr. dual variables
    if CMR
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        cst_offset = (jj-1)*(nsd*ns+2*(ns-1+1)*nq);
        OPT.phase{4}.var.dual.lambda = reshape(-info.lambda(cst_offset+(1:nsd*ns)), nsd, ns);
        OPT.phase{4}.var.dual.lambda1 = reshape(-info.lambda(cst_offset+(nsd*ns+(1:nq*ns))), nq, ns);
        OPT.phase{4}.var.dual.lambda2 = reshape(-info.lambda(cst_offset+(nsd*ns+nq*ns+(1:nq*ns))), nq, ns);
        
        ybnd = info.zl - info.zu;
        OPT.phase{4}.var.dual.mu_sd = reshape(ybnd(offset+(1:nsd*(ns+1))), nsd, ns+1);
        OPT.phase{4}.var.dual.mu_q = reshape(ybnd(offset+(nsd*(ns+1)+(1:nq*ns))), nq, ns);
        OPT.phase{4}.var.dual.mu_delq = reshape(ybnd(offset+(nsd*(ns+1)+nq*ns+(1:(nq*ns-1+1)))), nq, ns);
      else
        ybnd = info.zl - info.zu;
        OPT.phase{4}{jj}.var.dual.mu_sd = reshape(ybnd(offset+(1:nsd*(ns+1))), nsd, ns+1);
        OPT.phase{4}{jj}.var.dual.mu_q = reshape(ybnd(offset+(nsd*(ns+1)+(1:nq*ns))), nq, ns);
        
        cst_offset = (jj-1)*(nsd*ns+(ns-1+1)*nq);
        OPT.phase{4}{jj}.var.dual.lambda = reshape(-info.lambda(cst_offset+(1:nsd*ns)), nsd, ns);
      end
    else   
      ybnd = info.zl - info.zu;
      OPT.phase{4}{jj}.var.dual.mu_sd = reshape(ybnd(offset+(1:nsd*(ns+1))), nsd, ns+1);
      OPT.phase{4}{jj}.var.dual.mu_q = reshape(ybnd(offset+(nsd*(ns+1)+(1:nq*ns))), nq, ns);
      
      cst_offset = (jj-1)*(nsd*ns);
      OPT.phase{4}{jj}.var.dual.lambda = reshape(-info.lambda(cst_offset+(1:nsd*ns)), nsd, ns);
    end
end

OPT.initime = toc;

end
