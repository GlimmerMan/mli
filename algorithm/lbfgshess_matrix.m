function B = lbfgshess_matrix(Hess)
% B = LBFGSHESS_MATRIX(Hess)  Compose and return L-BFGS Hessian matrix
% approximation.
% 

% Authors: Andreas Potschka, Leonard Wirsching
% Date: Dec 3, 2009

B = Hess.gamma * eye(size(Hess.s,1));

% compact representation of L-BFGS matrix
l = Hess.l;
if l == 0
	return
end
S = Hess.s(:,1:l);
Y = Hess.y(:,1:l);
L = S' * Y;
D = diag(L);
L = tril(L, -1);

M = [ Hess.gamma * S'*S, L; L', -diag(D) ];
SY = [ Hess.gamma*S, Y ];

B = B - SY * (M \ SY');

return

% alternatively
[L,D,P,S] = ldl(M);
SY = L \ (P' * (S \ SY));
LSopts.POSDEF = false;
LSopts.SYM = true;
B = B - SY * linsolve(D, SY', LSopts);

