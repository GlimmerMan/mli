function mli_solvescenQP_nocond(jj,lambda,flag)

global OPT ST

options = OPT.qpoptions;
nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;
CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;

tscenqp = tic;

switch flag
    case 'm'
        
        % regularize Hessian
%         already in mli_solveSTQP
%         [V,E] = eig(0.5*(HH+HH'));
        
        [x{jj}, ST.objj(jj), status{jj}, nWSRout{jj}, y{jj}] = qpOASES_sequence('m', ...
            OPT.qp{jj}.qphandle, ...
            OPT.qp{jj}.H, OPT.qp{jj}.g + (ST.C{jj}-ST.E{jj})'*lambda, ...
            OPT.qp{jj}.A, OPT.qp{jj}.lb, ...
            OPT.qp{jj}.ub, OPT.qp{jj}.ineqlb, ...
            OPT.qp{jj}.inequb, ...
            options);

        % on error, try again from scratch
        if status{jj} ~= 0
            qpOASES_sequence('c', OPT.qp{jj}.qphandle);
            [OPT.qp{jj}.qphandle, x{jj}, ST.objj(jj), status{jj}, nWSRout{jj}, y{jj}] = ...
                qpOASES_sequence('i', ...
                OPT.qp{jj}.H, OPT.qp{jj}.g + (ST.C{jj}-ST.E{jj})'*lambda, ...
                OPT.qp{jj}.A, OPT.qp{jj}.lb, ...
                OPT.qp{jj}.ub, OPT.qp{jj}.ineqlb, ...
                OPT.qp{jj}.inequb, ...
                options);
        end

    case 'h'
        [x{jj}, ST.objj(jj), status{jj}, nWSRout{jj}, y{jj}] = qpOASES_sequence('h', ...
            OPT.qp{jj}.qphandle, ...
            OPT.qp{jj}.g + (ST.C{jj}-ST.E{jj})'*lambda, ...
            OPT.qp{jj}.lb, OPT.qp{jj}.ub, OPT.qp{jj}.ineqlb, ...
            OPT.qp{jj}.inequb, options);       
end

if (status{jj} ~= 0)
    status{jj}
    keyboard
%     error('QP solution failed! Scenario number %i',jj);
end


if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
  xoff = ns*(nsd+nq) + nsd;
  OPT.qp{jj}.solstep = x{jj}(1:xoff);
  OPT.qp{jj}.soldelq = x{jj}((xoff+1):end);
else
  OPT.qp{jj}.solstep = x{jj};
end

if any(isnan(y{jj}))
    disp('NAN in QP Multipliers!');
    keyboard;
end

if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
  OPT.qp{jj}.solbndmult = y{jj}(nsd+(1:ns*(nsd+nq)));
  OPT.qp{jj}.solbndmult2 = y{jj}(nsd+ns*(nsd+nq)+(1:ns*nq));
  xoffs = ns*(nsd+nq) + nsd + ns*nq;
  OPT.qp{jj}.soleqmult = [y{jj}(1:nsd);y{jj}(xoffs+(1:ns*nsd))];
  OPT.qp{jj}.solCMRmult = y{jj}(xoffs+ns*nsd+(1:2*ns*nq));
  
else
  %  multipliers of (q_0,sd_1,...,q_N-1,sd_N) bounds
  OPT.qp{jj}.solbndmult = y{jj}(nsd+(1:ns*(nsd+nq)));
  
  % multipliers of sd_0 bound and matching
  xoffs = ns*(nsd+nq) + nsd;
  
  OPT.qp{jj}.soleqmult = [y{jj}(1:nsd);y{jj}(xoffs+(1:ns*nsd))];
end

% multipliers of ipc
xoffs = xoffs + ns*nsd;
OPT.qp{jj}.solineqmult = y{jj}(xoffs+1:end);

OPT.scenqptime{jj} = toc(tscenqp);
OPT.scenqpcumtime{jj} = OPT.scenqpcumtime{jj}+OPT.scenqptime{jj};