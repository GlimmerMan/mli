function fcn = eval_nlp_cst(vars)
global SCEN OPT ST

if SCEN.use_st == 0
    fcn = @eval;
else
    fcn = @evalST;
end

ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;

  function [cst] = eval(x)
    
    vars.primal.sd = reshape(x(1:nsd*(ns+1)), nsd, ns+1);
    vars.primal.q = reshape(x(nsd*(ns+1)+(1:nq*ns)), nq, ns);
    vars.objective.sigma = -1;
    if CMR
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        vars.primal.delq = reshape(x((nsd+nq)*ns+nsd+(1:(nq*(ns-1+1)))), nq, ns-1+1);
      else
        for ii = 1:ns-1
          vars.primal.primal.delq(:,ii) = vars.primal.q(:,ii+1) - vars.primal.q(:,ii);
        end
        vars.primal.delq(:,ns) = vars.primal.q(:,1) - OPT.controlmove_q0;
      end
    end
    
    OPT.nlp.data = mli_evalNLP(vars);
    

    if isfield(OPT,'periodicity')
      cst=zeros(nsd*ns+OPT.dims.periodicity,1);
    else
      cst=zeros(nsd*ns,1);
    end

    for msn=1:ns      
      cst((msn-1)*nsd+(1:nsd)) = OPT.nlp.data.res.c(:,msn);
    end
    if isfield(OPT,'periodicity')
      % add periodicity constraint
      cst(nsd*ns+(1:OPT.dims.periodicity))=OPT.nlp.data.res.periodicity;
    end
    
    if CMR
      xoffs = ns*nsd; %+nrds+nrdi+nrde;
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        vars.primal.delq = reshape(x(nsd*(ns+1)+nq*ns+(1:nq*(ns-1+1))), nq, ns-1+1);
        xoffs2 = xoffs + (ns-1+1)*nq;
        for ii = 1:ns-1
          cst(xoffs+(ii-1)*nq+(1:nq)) = vars.primal.q(:,ii)-vars.primal.q(:,ii+1)+vars.primal.delq(:,ii);
          cst(xoffs2+(ii-1)*nq+(1:nq)) = vars.primal.q(:,ii)-vars.primal.q(:,ii+1)+vars.primal.delq(:,ii);
        end
        cst(xoffs+(ns-1)*nq+(1:nq)) = OPT.controlmove_q0-vars.primal.q(:,1)+vars.primal.delq(:,ns);
        cst(xoffs2+(ns-1)*nq+(1:nq)) = OPT.controlmove_q0-vars.primal.q(:,1)+vars.primal.delq(:,ns);
      else
        for ii = 1:ns-1
          cst(xoffs+(ii-1)*nq+(1:nq)) = vars.primal.q(:,ii)-vars.primal.q(:,ii+1);
        end
        cst(xoffs+(ns-1)*nq+(1:nq)) = OPT.controlmove_q0-vars.primal.q(:,1);
      end
    end
    
  end

  function [cst] = evalST(x)
    
    S = ST.S;
    for jj = 1:S
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
        nz = (nsd+nq)*ns+nsd+nq*(ns-1+1);
        offset = (jj-1)*nz;
      else
        nz = (nsd+nq)*ns+nsd;
        offset = (jj-1)*nz;
      end
      vars{jj}.primal.sd = reshape(x(offset+(1:nsd*(ns+1))), nsd, ns+1);
      vars{jj}.primal.q = reshape(x(offset+(nsd*(ns+1)+(1:nq*ns))), nq, ns);
      vars{jj}.objective.sigma = -1;
      vars{jj}.primal.z_ipopt = x(offset+(1:nz))';
      if CMR
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
          vars{jj}.primal.delq = reshape(x(offset+((nsd+nq)*ns+nsd+(1:(nq*(ns-1+1))))), nq, ns-1+1);
        else
          for ii = 1:ns-1
            vars{jj}.primal.delq(:,ii) = vars{jj}.primal.q(:,ii+1) - vars{jj}.primal.q(:,ii);
          end
          vars{jj}.primal.delq(:,ns) = vars{jj}.primal.q(:,1) - OPT.controlmove_q0;
        end
      end
    end
    
    OPT.nlp.data = mli_evalNLP(vars);
    
    if CMR
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
          cst = zeros(S*(nsd*ns+2*(ns-1+1)*nq),1);
        else
          cst = zeros(S*(nsd*ns+(ns-1+1)*nq),1);
        end
      else
        %     cst = zeros(S*nsd*ns + (S-1)*ST.na,1);
        cst = zeros(S*nsd*ns,1);
    end
    
    for jj = 1:S
      if CMR
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
          offset = (jj-1)*(nsd*ns+2*(ns-1+1)*nq);
        else
          offset = (jj-1)*(nsd*ns+(ns-1+1)*nq);
        end
      else
        offset = (jj-1)*(nsd*ns);
      end
      
      for msn=1:ns
        cst(offset+((msn-1)*nsd+(1:nsd))) = OPT.nlp.data{jj}.res.c(:,msn);
      end
      
      if CMR
        xoffs = ns*nsd; %+nrds+nrdi+nrde;
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
          vars{jj}.primal.delq = reshape(x(offset+nsd*(ns+1)+nq*ns+(1:nq*(ns-1+1))), nq, ns-1+1);
          xoffs2 = xoffs + (ns-1+1)*nq;
          for ii = 1:ns-1
            cst(offset+xoffs+(ii-1)*nq+(1:nq)) = vars{jj}.primal.q(:,ii)-vars{jj}.primal.q(:,ii+1)+vars{jj}.primal.delq(:,ii);
            cst(offset+xoffs2+(ii-1)*nq+(1:nq)) = vars{jj}.primal.q(:,ii)-vars{jj}.primal.q(:,ii+1)+vars{jj}.primal.delq(:,ii);
          end
          cst(offset+xoffs+(ns-1)*nq+(1:nq)) = OPT.controlmove_q0-vars{jj}.primal.q(:,1)+vars{jj}.primal.delq(:,ns);
          cst(offset+xoffs2+(ns-1)*nq+(1:nq)) = OPT.controlmove_q0-vars{jj}.primal.q(:,1)+vars{jj}.primal.delq(:,ns);
        else
          for ii = 1:ns-1
            cst(offset+xoffs+(ii-1)*nq+(1:nq)) = vars{jj}.primal.q(:,ii)-vars{jj}.primal.q(:,ii+1);
          end
          cst(offset+xoffs+(ns-1)*nq+(1:nq)) = OPT.controlmove_q0-vars{jj}.primal.q(:,1);
        end    
      end
    end
    
    % non-anticipativity constraints
    for jj = 1:S-1
%     offset = S*nsd*ns + (jj-1)*ST.na;
%       cst(offset+(1:ST.na)) = OPT.nlp.data{jj}.res.noac;
      cst = [cst;OPT.nlp.data{jj}.res.noac];
    end
  end

end