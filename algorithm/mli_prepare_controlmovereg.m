% -*-matlab-*-
function mli_prepare_controlmovereg
global SCEN OPT ST;

sample = OPT.currentsample;
prevsample = max(sample-1,1);
samplingratio = (SCEN.sampling(sample)-SCEN.sampling(prevsample))/(OPT.discr.msgrid(2)-OPT.discr.msgrid(1));
if isfinite(samplingratio) && samplingratio ~= 0
  OPT.controlmove_alpha0 = OPT.controlmove_alpha*samplingratio;
  if isfield(OPT, 'controlmove_c')
    OPT.controlmove_c0 = OPT.controlmove_c * samplingratio;
  end
  if isfield(OPT, 'controlmove_R')
    OPT.controlmove_R0 = OPT.controlmove_R * samplingratio^2;
  end
else
  OPT.controlmove_alpha0 = OPT.controlmove_alpha;
  if isfield(OPT, 'controlmove_c')
    OPT.controlmove_c0 = OPT.controlmove_c;
  end
  if isfield(OPT, 'controlmove_R')
    OPT.controlmove_R0 = OPT.controlmove_R;
  end
end

OPT.samplingratio = samplingratio;

if SCEN.use_st == 0
  OPT.controlmove_q0 = OPT.phase{4}.var.primal.q(:,1);
else
  J = ceil(ST.S/2);
  OPT.controlmove_q0 = OPT.phase{4}{J}.var.primal.q(:,1);
end

end