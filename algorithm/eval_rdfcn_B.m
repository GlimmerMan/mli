function [outp] = eval_rdfcn_B(inp, nodetype)
    
    global SLV
       
    args.t = inp.t;    
    args.xd = inp.sd;
    args.qph = [inp.q;inp.p];    

    evl = SLV.opt.evl;
    
    switch nodetype
      case 's'
	args.rhs = solvind('evaluateFcn', evl, 'rdfcn_s', args);

      case 'i'
	args.rhs = solvind('evaluateFcn', evl, 'rdfcn_i', args);
	
      case 'e'
	args.rhs = solvind('evaluateFcn', evl, 'rdfcn_e', args);
	
      otherwise
	error('eval_rdfcn_B: Invalid nodetype');
	
    end

    outp.rdfcn = args.rhs;


    
