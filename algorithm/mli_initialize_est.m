% -*-matlab-*-
function mli_initialize_est
    
global EST SCEN
    
nsd = EST.dims.nsd;
nq = EST.dims.nq;
np = EST.dims.np;
ns = EST.dims.nshoot;
nlsqs = EST.dims.lsqfcn_s;
nlsqi = EST.dims.lsqfcn_i; 
nlsqe = EST.dims.lsqfcn_e;

%% variable initialization
EST.var.primal.sd = zeros(nsd,ns+1);
EST.var.primal.sd(:,1) = EST.ini.xbar;
EST.var.primal.p = EST.ini.pbar;
EST.var.primal.qconst = zeros(nq,ns);

% used in mode C
EST.var.dual.lambda = zeros(nsd,ns);


%% step initialization
EST.step.primal.sd = zeros(nsd,ns+1);
if ~EST.fixed_p
    EST.step.primal.p = zeros(np,1);
end

%% initialization of estimator tuning terms

% initialization of arrival cost terms
if ~EST.disable_arrival
    EST.xbar = EST.ini.xbar;
    if ~EST.fixed_p
        EST.pbar = EST.ini.pbar;
    end
    EST.P = sqrtm(inv(EST.ini.xpcov));
end

% artificial state noise to downweight arrival cost
EST.W = sqrtm(inv(EST.statecov));

% measurement covariances 
EST.V = (1./sqrt(diag(EST.meascov))) * ones(1,ns+1);


%% Initialization of algorithmic data

% condensing data
EST.cond.cblocksx = mat2cell(zeros(nsd,nsd,ns), nsd, nsd, ones(ns,1));
if ~EST.fixed_p
    EST.cond.cblocksp = mat2cell(zeros(nsd,np,ns), nsd, np, ...
                                 ones(ns,1));
end
EST.cond.c = zeros(nsd, ns);
EST.cond.Fcond = zeros(nlsqs + (ns-1)*nlsqi + nlsqe,1);
EST.cond.Fmod = zeros(nlsqs + (ns-1)*nlsqi + nlsqe,1);
if EST.fixed_p
    EST.cond.Jcond = zeros(nlsqs + (ns-1)*nlsqi + nlsqe, nsd);
    EST.cond.gradmod = zeros(nsd,1);
    EST.cond.grad =  zeros(nsd,1);
else
    EST.cond.Jcond = zeros(nlsqs + (ns-1)*nlsqi + nlsqe, nsd+np);
    EST.cond.gradmod = zeros(nsd+np,1);
    EST.cond.grad =  zeros(nsd+np,1);
end


% function and derivative evaluation data
EST.eval.res.xd = zeros(nsd, ns);
EST.eval.res.c  = zeros(nsd, ns);

% Here we assume that nlsqs=nlsqi=nlsqe
EST.eval.res.meas = zeros(nlsqs, ns+1);
EST.eval.res.lsq  = zeros(nlsqs, ns+1);

if ~EST.disable_arrival
    if EST.fixed_p
        EST.eval.res.arrivalCost = zeros(nsd, 1);
    else
        EST.eval.res.arrivalCost = zeros(nsd+np, 1);
    end
end

EST.eval.res.modgradx = zeros(nsd,ns+1);
EST.eval.res.modgradp = zeros(np,1);

EST.eval.res.lambdaGx = zeros(nsd,ns);
EST.eval.res.lambdaGp = zeros(np,ns);

EST.eval.mat.Gx = mat2cell(zeros(nsd,nsd,ns),nsd,nsd,ones(ns,1));
if ~EST.fixed_p
    EST.eval.mat.Gp = mat2cell(zeros(nsd,np,ns),nsd,np,ones(ns,1));
end

% Here we assume that nlsqs=nlsqi=nlsqe
EST.eval.mat.Jmeasx = mat2cell(zeros(nlsqs,nsd,ns+1),nlsqs,nsd,ones(ns+1,1));
if ~EST.fixed_p
    EST.eval.mat.Jmeasp = mat2cell(zeros(nlsqs,np,ns+1),nlsqs,np,ones(ns+1,1));
end

% QP data
if EST.fixed_p
    EST.qp.H = zeros(nsd, nsd);
    EST.qp.g = zeros(nsd, 1);
    EST.qp.A = zeros(ns*nsd, nsd);
    EST.qp.lbA = zeros(ns*nsd, 1);
    EST.qp.ubA = zeros(ns*nsd, 1);
    EST.qp.lb = zeros(nsd, 1);
    EST.qp.ub = zeros(nsd, 1);
    EST.qp.solstep = zeros(nsd, 1);
else
    EST.qp.H = zeros(nsd+np, nsd+np);
    EST.qp.g = zeros(nsd+np, 1);
    EST.qp.A = zeros(ns*nsd, nsd+np);
    EST.qp.lbA = zeros(ns*nsd, 1);
    EST.qp.ubA = zeros(ns*nsd, 1);
    EST.qp.lb = zeros(nsd+np, 1);
    EST.qp.ub = zeros(nsd+np, 1);
    EST.qp.solstep = zeros(nsd+np);
end

% initialization call to qpOASES to get handle
[QP, x, fval, exitflag, iter, lambda] = qpOASES_sequence( 'i',EST.qp.H, ...
                                                  EST.qp.g, EST.qp.A, ...
                                                  EST.qp.lb, EST.qp.ub, ...
                                                  EST.qp.lbA, ...
                                                  EST.qp.ubA);
EST.qp.qphandle = QP;

%% initialization of online data
EST.measurements = zeros(nlsqs, ns);
EST.eta = zeros(nlsqs, 1);
EST.newctrl = zeros(nq, 1);

%% initialization of results (estimate and covariance)
EST.sdest = zeros(nsd, 1);
if EST.fixed_p
    EST.covest = zeros(nsd, nsd);
else
    EST.pest = zeros(np, 1);
    EST.covest = zeros(nsd+np, nsd+np);
end

%% other initialization
if EST.with_chi2test
    EST.chi2test = zeros(SCEN.steps,1);
end

disp('End of EST initialization.');
%keyboard;