function [outp] = eval_rcfcn_i_D(inp)


global SLV OPT

int = SLV.opt.int;
sensdirs = inp.sensdirs;

%auxiliary direction for second derivative
lambda=zeros(OPT.dims.periodicity,1);

ndf = size(sensdirs,2);

args.t = inp.t;
args.xd = inp.sd;
%what does qph mean??
args.qph = [inp.q];
ndf = size(sensdirs,2);
args.fwdTCOrder = 1;
args.nFwdTC = ndf;
args.fwdTC = sensdirs;
args.nAdjTC = 1;
adjDirs = kron(ones(1,ndf), [lambda,zeros(size(lambda))]);

args.adjTC =adjDirs;

evl = SLV.opt.evl;

args.rhs = solvind('evaluateFcn', evl, 'rcfcn_i', args);
result = solvind('evaluateDenseDer', evl, 'rcfcn_i', args);

outp.eval = result.rhs;
%have to calculate the Jacobian of the function wrt to the state
G = result.propFwdTC;
H = result.propAdjTC(2:end,2:2:end);
%only dependant of the state
outp.G=G(:,1:size(args.xd,1));
outp.H=H(1:size(args.xd,1),1:size(args.xd,1));
