% -*-matlab-*-
function est_evalPhaseC

global EST;

ns = EST.dims.nshoot;
nsd = EST.dims.nsd; 
nq = EST.dims.nq; 
np = EST.dims.np;
nlsqs = EST.dims.lsqfcn_s;
nlsqi = EST.dims.lsqfcn_i; 
nlsqe = EST.dims.lsqfcn_e;

pvars = EST.var.primal;
dvars = EST.var.dual;

% dynamic equations and adjoint derivatives
for msn = 1:ns
    inp = [];
    inp.thoriz = [EST.discr.msgrid(msn) EST.discr.msgrid(msn+1)];
    inp.sd = pvars.sd(:,msn);
    inp.p = pvars.p;
    inp.q = pvars.qconst(:,msn);
    inp.lambda = dvars.lambda(:,msn);
    
    outp = eval_dyn_C_est(inp);
    EST.eval.res.xd(:,msn) = outp.x;
    EST.eval.res.c(:,msn) = outp.x - pvars.sd(:,msn+1);
    EST.eval.res.lambdaGx(:,msn) = outp.lag(1:nsd);
    if ~EST.fixed_p
        EST.eval.res.lambdaGp(:,msn) = outp.lag(nsd+(1:np));
    end
end
    
% measurement function residuals and gradients
msn = 1;
inp = [];
inp.t = EST.discr.msgrid(msn);
inp.sd = pvars.sd(:,msn);
inp.p = pvars.p;
inp.q = pvars.qconst(:,msn);
outp = eval_lsqfcn_C_est(inp,'s',EST.measurements(:,msn),EST.V(:,msn));

EST.eval.res.meas(:,msn) = outp.lsqfcn;
EST.eval.res.lsq(:,msn) = outp.residual;
EST.eval.res.modgradx(:,msn) = outp.lsqgrad(1:nsd);
EST.eval.res.modgradx(:,msn) = EST.eval.res.modgradx(:,msn) - ...
    EST.eval.res.lambdaGx(:,msn) + EST.eval.mat.Gx{msn}'* ...
    dvars.lambda(:,msn);

if ~EST.fixed_p
    EST.eval.res.modgradp(:) = 0.0;
    EST.eval.res.modgradp = EST.eval.res.modgradp + ...
        outp.lsqgrad(nsd+(1:np));
    EST.eval.res.modgradp = EST.eval.res.modgradp - ...
        EST.eval.res.lambdaGp(:,msn) + EST.eval.mat.Gp{msn}'* ...
        dvars.lambda(:,msn);
end

for msn = 2:ns
    inp = [];
    inp.t = EST.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.p = pvars.p;
    inp.q = pvars.qconst(:,msn);
    outp = eval_lsqfcn_C_est(inp,'i',EST.measurements(:,msn),EST.V(:,msn));
    
    EST.eval.res.meas(:,msn) = outp.lsqfcn;
    EST.eval.res.lsq(:,msn) = outp.residual;
    EST.eval.res.modgradx(:,msn) = outp.lsqgrad(1:nsd);
    EST.eval.res.modgradx(:,msn) = EST.eval.res.modgradx(:,msn) - ...
        EST.eval.res.lambdaGx(:,msn) + EST.eval.mat.Gx{msn}'* ...
        dvars.lambda(:,msn);
    if ~EST.fixed_p
        EST.eval.res.modgradp = EST.eval.res.modgradp + ...
            outp.lsqgrad(nsd+(1:np));
        EST.eval.res.modgradp = EST.eval.res.modgradp - ...
            EST.eval.res.lambdaGp(:,msn) + EST.eval.mat.Gp{msn}'* ...
            dvars.lambda(:,msn);
    end
end

% we cannot compute the gradient yet since the last measurement is
% missing and so the last residual (=adjDir) can not be computed!
msn = ns+1;
inp = [];
inp.t = EST.discr.msgrid(msn);
inp.sd = pvars.sd(:,msn);
inp.p = pvars.p;
inp.q = pvars.qconst(:,msn-1);
outp = eval_lsqfcn_C_est(inp,'e');
EST.eval.res.meas(:,msn) = outp.lsqfcn;
    
%disp('End of evalPhaseC...');
%keyboard;
