% -*-matlab-*-

% arrivalIn: struct with members
% P_l, V_l, H_x, H_p, W, X_x, X_p
% xbar_l, pbar_l, eta_l, ht, xt
%
% arrivalOut: struct with members
% P_new, xbar_new, pbar_new
%
function est_updateArrival

global EST;
nsd = EST.dims.nsd;
np = EST.dims.np;
nq = EST.dims.nq;
nlsqs = EST.dims.lsqfcn_s;
nxp = nsd+np;

if ~EST.fixed_p
    sensdirs = [zeros(1,nsd+np);
                eye(nsd), zeros(nsd,np)  
                zeros(nq,nsd+np)
                zeros(np,nsd), eye(np)];
else
    sensdirs = [zeros(1,nsd);
                eye(nsd)
                zeros(nq+np,nsd)];
end

P_l = EST.P; 
V_l = diag(EST.V(:,1)); 
W   = EST.W;
xbar_l = EST.xbar;
if ~EST.fixed_p
    pbar_l = EST.pbar;
end
eta_l  = EST.measurements(:,1);

inp = [];
inp.thoriz = [EST.discr.msgrid(1) EST.discr.msgrid(2)];
inp.sd = EST.var.primal.sd(:,1);
inp.p = EST.var.primal.p;
inp.q = EST.var.primal.qconst(:,1);
inp.sensdirs = sensdirs;
outp = eval_dyn_D_gn_est(inp);
xd = outp.x;
X_x = outp.G(1:nsd,1:nsd);
xt  = xd - X_x*EST.var.primal.sd(:,1);
if ~EST.fixed_p
    X_p = outp.G(1:nsd,nsd+(1:np));
    xt  = xt - X_p*EST.var.primal.p;
end

inp = [];
inp.t = EST.discr.msgrid(1);
inp.sd = EST.var.primal.sd(:,1);
inp.p = EST.var.primal.p;
inp.q = EST.var.primal.qconst(:,1);
inp.sensdirs = sensdirs;
outp = eval_lsqfcn_D_gn_est(inp,'s');
hh = outp.lsqfcn;
H_x = outp.lsqjac(1:nlsqs,1:nsd);
ht = hh - H_x*EST.var.primal.sd(:,1);
if ~EST.fixed_p
    H_p = outp.lsqjac(1:nlsqs,nsd+(1:np));
    ht = ht - H_p*EST.var.primal.p; 
end

if ~EST.fixed_p
    O1 = zeros(nxp);
    O2 = zeros(nlsqs, nxp);
    O3 = zeros(np, nsd);
    II = eye(np);
    
    hmat = [X_x X_p
            O3  II];
    
    lsqmat = [P_l           , O1
              -V_l*[H_x H_p], O2 
              -W * hmat     , W];
    
    lsqrhs = [-P_l * [xbar_l;pbar_l]
              V_l * (eta_l - ht)
              -W * [xt;zeros(np,1)]];
else
    O1 = zeros(nsd);
    O2 = zeros(nlsqs, nsd);
    
    hmat = X_x;
    
    lsqmat = [P_l      , O1
              -V_l*H_x , O2 
              -W * hmat, W];
    
    lsqrhs = [-P_l * xbar_l
              V_l * (eta_l - ht)
              -W * xt];
end

[Q, R] = qr(lsqmat);

if ~EST.fixed_p
    r = Q' * lsqrhs;
    r2 = r(nxp+(1:nxp),1);
    R2 = R(nxp+(1:nxp), nxp+(1:nxp)); 
    v = R2 \ r2;
    
    EST.P = R2;
    EST.xbar = -v(1:nsd);
    EST.pbar = -v(nsd+(1:np));
else
    r = Q' * lsqrhs;
    r2 = r(nsd+(1:nsd),1);
    R2 = R(nsd+(1:nsd), nsd+(1:nsd)); 
    v = R2 \ r2;
    
    EST.P = R2;
    EST.xbar = -v(1:nsd);
end


