% -*-matlab-*-
% Variables filled in OPT
%
% OPT.deltas0
% OPT.qpHess
% OPT.qpGrad
% OPT.qpIneqLB
% OPT.qpIneqUB
% OPT.qpLB
% OPT.qpUB
% OPT.qpSolStep
% OPT.qpSolEqMult
% OPT.qpSolIneqMult
% OPT.qpSolBndMult
% OPT.feedbackdelta

function mli_solveQP

global OPT SLV;

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nrd = nrds + (ns-1) * nrdi + nrde;

CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;

tic

phase = OPT.currentphase;

switch (phase)
    case 1
        OPT.deltas0 = SLV.opt.lagObjModSd*OPT.x0(:) -...
            OPT.phase{1}.ref.primal.sd(:,1);
    case {0,4}
        OPT.deltas0 = SLV.opt.lagObjModSd*OPT.x0(:) -...
            OPT.phase{4}.var.primal.sd(:,1);
    case 2
        OPT.deltas0 = SLV.opt.lagObjModSd*OPT.x0(:) - ...
            OPT.phase{2}.var.primal.sd(:,1);
    case 3
        OPT.deltas0 = SLV.opt.lagObjModSd*OPT.x0(:) - ...
            OPT.phase{3}.var.primal.sd(:,1);
end

% Gradient
OPT.qp.g = OPT.cond.b;
if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
  switch phase   
    case {0,4}
      pvars = OPT.phase{4}.var.primal;     
    case 1
      pvars = OPT.phase{1}.ref.primal;    
    case 2
      pvars = OPT.phase{2}.var.primal; 
    case 3
      pvars = OPT.phase{3}.var.primal;   
  end
  xoff = nsd+ns*nq;
  for ii=1:ns-1
    idx = xoff + (ii-1)*nq;
    OPT.qp.g(idx+(1:nq),1) = OPT.eval.res.modgraddelq(:,ii);
  end
  idx = xoff + (ns-1)*nq;
  OPT.qp.g(idx+(1:nq),1) = OPT.eval.res.modgraddelq(:,ns);
end

% RHS
if nrd > 0
    bigbnd = 1e12*ones(size(OPT.cond.rdres));
    OPT.qp.ineqlb = [OPT.cond.bndlo1;OPT.cond.rdres];
    OPT.qp.inequb = [OPT.cond.bndup1;bigbnd];
else
    OPT.qp.ineqlb = OPT.cond.bndlo1;
    OPT.qp.inequb = OPT.cond.bndup1;
end
if CMR
  xoffs = ns * nsd + nrd;
  alpha = OPT.controlmove_alpha;
  if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
    xoffs2 = xoffs + (ns-1)*nq+nq;
    bigbnd = 1e12;
    for ii = 1:ns-1
      OPT.qp.ineqlb(xoffs+(ii-1)*nq+(1:nq)) = pvars.q(:,ii+1) - pvars.q(:,ii);
      OPT.qp.inequb(xoffs+(ii-1)*nq+(1:nq)) = bigbnd*ones(nq,1);
      OPT.qp.ineqlb(xoffs2+(ii-1)*nq+(1:nq)) = pvars.q(:,ii) - pvars.q(:,ii+1);
      OPT.qp.inequb(xoffs2+(ii-1)*nq+(1:nq)) = bigbnd*ones(nq,1);
    end
    OPT.qp.ineqlb(xoffs+(ns-1)*nq+(1:nq)) = pvars.q(:,1) - OPT.controlmove_q0;
    OPT.qp.inequb(xoffs+(ns-1)*nq+(1:nq)) = bigbnd*ones(nq,1);
    OPT.qp.ineqlb(xoffs2+(ns-1)*nq+(1:nq)) = OPT.controlmove_q0 - pvars.q(:,1);
    OPT.qp.inequb(xoffs2+(ns-1)*nq+(1:nq)) = bigbnd*ones(nq,1);
    
  else
    for ii = 1:ns-1
      OPT.qp.ineqlb(xoffs+(ii-1)*nq+(1:nq)) = -alpha;
      OPT.qp.inequb(xoffs+(ii-1)*nq+(1:nq)) = alpha;
    end
    OPT.qp.ineqlb(xoffs+(ns-1)*nq+(1:nq)) = -OPT.controlmove_alpha0;
    OPT.qp.inequb(xoffs+(ns-1)*nq+(1:nq)) = OPT.controlmove_alpha0;
  end
end

% Bounds
OPT.qp.lb = [OPT.deltas0;OPT.cond.bndlo2];
OPT.qp.ub = [OPT.deltas0;OPT.cond.bndup2];
if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
  xoff = nsd + ns*nq;
  for ii=1:ns-1
    idx = xoff + (ii-1)*nq;
    OPT.qp.lb(idx+(1:nq)) = -bigbnd*ones(nq,1);
    OPT.qp.ub(idx+(1:nq)) = OPT.controlmove_alpha;
  end
  idx = xoff + (ns-1)*nq;
  OPT.qp.lb(idx+(1:nq)) = -bigbnd*ones(nq,1);
  OPT.qp.ub(idx+(1:nq)) = OPT.controlmove_alpha0;
end

%options = qpOASES_options('MPC');
options = qpOASES_options('reliable');
options = qpOASES_options(options,'maxIter', 1e6);

switch (phase)
    case {1,2,3,4}
        maxCpuTime = -1;
        if isfield(OPT, 'maxCpuTime')
            maxCpuTime = OPT.maxCpuTime;
        end
        if maxCpuTime > 0
            options = qpOASES_options(options, 'maxCpuTime', maxCpuTime);
        end
end

switch (phase)
    case 0
        OPT.qp.H = OPT.cond.B;
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
          if isfield(OPT, 'controlmove_R')
            R = OPT.controlmove_R;
          else
            R = zeros(nq,nq);
          end
          xoff = nsd + ns*nq;
          for ii=1:ns-1
            idx = xoff + (ii-1)*nq;
            OPT.qp.H(idx+(1:nq),idx+(1:nq)) = 4 * R;
          end
          idx = xoff + (ns-1)*nq;
          OPT.qp.H(idx+(1:nq),idx+(1:nq)) = 4 * OPT.controlmove_R0;
        end
        if nrd > 0
            OPT.qp.A = [OPT.cond.C12;OPT.cond.rdmat];
        else
            OPT.qp.A = OPT.cond.C12;
        end
        if CMR
          %!
        end
        
        if 0
            
            [x,status,y] = mli_solveQP_scaled(options);
        
        else
            [x, ~, status, ~, y] = qpOASES_sequence('m', OPT.qp.qphandle, ...
                OPT.qp.H, OPT.qp.g, ...
                OPT.qp.A, OPT.qp.lb, ...
                OPT.qp.ub, OPT.qp.ineqlb, ...
                OPT.qp.inequb,options);
        
        end
        
    case 4
      if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
        OPT.qp.H = OPT.cond.B;
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
          xoff = ns*nq+nsd;
          for ii=1:ns
            idx = xoff + (ii-1)*nq;
            OPT.qp.H(idx+(1:nq),idx+(1:nq)) = lbfgshess_matrix(OPT.hess.lbfgsCMRhess{ii});
          end
        end
      else
        OPT.qp.H = OPT.cond.B;
%         OPT.qp.H = eye(size(OPT.cond.B)); % the absolute regularization
        % other regularizations %!
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
          if isfield(OPT, 'controlmove_R')
            R = OPT.controlmove_R;
          else
            R = zeros(nq,nq);
          end
          xoff = nsd + ns*nq;
          for ii=1:ns-1
            idx = xoff + (ii-1)*nq;
            OPT.qp.H(idx+(1:nq),idx+(1:nq)) = 4 * R;
          end
          idx = xoff + (ns-1)*nq;
          OPT.qp.H(idx+(1:nq),idx+(1:nq)) = 4 * OPT.controlmove_R0;
        end
      end
      
        if nrd > 0
            OPT.qp.A = [OPT.cond.C12;OPT.cond.rdmat];
        else
            OPT.qp.A = OPT.cond.C12;
        end
        if CMR
          xoffs = ns * nsd + nrd;
          if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
            xoffs2 = xoffs + (ns-1)*nq+nq;
            yoff = nsd + ns*nq;
            for ii = 1:ns-1
              yidx = nsd+(ii-1)*nq+(1:nq);
              OPT.qp.A(xoffs+(ii-1)*nq+(1:nq),yidx) = eye(nq);
              OPT.qp.A(xoffs+(ii-1)*nq+(1:nq),yidx+nq) = -eye(nq);
              OPT.qp.A(xoffs+(ii-1)*nq+(1:nq),yoff+(ii-1)*nq+(1:nq)) = eye(nq);
              OPT.qp.A(xoffs2+(ii-1)*nq+(1:nq),yidx) = -eye(nq);
              OPT.qp.A(xoffs2+(ii-1)*nq+(1:nq),yidx+nq) = eye(nq);
              OPT.qp.A(xoffs2+(ii-1)*nq+(1:nq),yoff+(ii-1)*nq+(1:nq)) = eye(nq);
            end
            OPT.qp.A(xoffs+(ns-1)*nq+(1:nq),nsd+(1:nq)) = -eye(nq);
            OPT.qp.A(xoffs+(ns-1)*nq+(1:nq),yoff+(ns-1)*nq+(1:nq)) = eye(nq);
            OPT.qp.A(xoffs2+(ns-1)*nq+(1:nq),nsd+(1:nq)) = eye(nq);
            OPT.qp.A(xoffs2+(ns-1)*nq+(1:nq),yoff+(ns-1)*nq+(1:nq)) = eye(nq);
          else
            for ii = 1:ns-1
              yidx = nsd+(ii-1)*nq+(1:nq);
              OPT.qp.A(xoffs+(ii-1)*nq+(1:nq),yidx) = eye(nq);
              OPT.qp.A(xoffs+(ii-1)*nq+(1:nq),yidx+nq) = -eye(nq);
            end
            OPT.qp.A(xoffs+(ns-1)*nq+(1:nq),nsd+(1:nq)) = -eye(nq);
          end
        end
        
        try
            if isfield(OPT, 'levmar') && isfield(OPT, 'levmarred')
                OPT.qp.H = OPT.qp.H + OPT.levmar * eye(size(OPT.qp.H));
            end
            
            if 0
            
                [x,status,y] = mli_solveQP_scaled(options);
        
            else
              
                [x, ~, status, ~, y] = qpOASES_sequence('m', OPT.qp.qphandle,...
                    OPT.qp.H, OPT.qp.g, ...
                    OPT.qp.A,OPT.qp.lb,...
                    OPT.qp.ub,OPT.qp.ineqlb,...
                    OPT.qp.inequb,options);
            
            end
            
        catch
            warning('QP could not be solved');
            keyboard
            error('QP could not be solved');
        end
        
    case {1,2,3}
        try
            [x, ~, status, ~, y] = qpOASES_sequence('h', ...
                OPT.qp.qphandle, OPT.qp.g, ...
                OPT.qp.lb, OPT.qp.ub,...
                OPT.qp.ineqlb,OPT.qp.inequb);
        catch
            error('QP could not be solved');
        end
end

% [KKTMat, KKTrhs, v, U, S, V, r, Kern, OPT.qp.Hred, OPT.qp.eigHred] = kktanalysis(OPT.qp, x, y);
% keyboard;
    
if (status ~= 0)
    %     status
    error('QP solution failed!');
    

end


% Feedback law
% g = [zeros(nsd,nq);eye(nq);zeros((ns-1)*nq,nq)];
% lb = zeros(length(OPT.qp.lb),nq);
% ub = zeros(length(OPT.qp.ub),nq);
% lbA = zeros(length(OPT.qp.ineqlb),nq);
% ubA = zeros(length(OPT.qp.inequb),nq);

% [invpart,lambda] = qpOASES_sequence( 'e',OPT.qp.qphandle,g,lb,ub,lbA,ubA );

% feedback = -lambda(1:nsd,:)';



if (phase ~= 1)
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
      xoff = nsd + ns*nq;
      OPT.qp.solstep = x(1:xoff);
      OPT.qp.soldelq = x((xoff+1):end);
    else
      OPT.qp.solstep = x;
    end
    
    % Constraint multipliers
    if any(isnan(y))
        disp('NANs in QP Multipliers!');
        keyboard
    end
    
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
      OPT.qp.soleqmult = y(1:nsd);
      OPT.qp.solbndmult = y(nsd+(1:ns*nq));
      OPT.qp.solbndmult2 = y(nsd+ns*nq+(1:ns*nq));
      xoffs = nsd + 2*ns*nq;
      OPT.qp.solineqmult = y(xoffs+(1:nsd*ns)); %! if nrd = 0, else more ineqmult
      OPT.qp.solCMRmult = y(xoffs+ns*nsd+(1:2*ns*nq));
    else
      OPT.qp.soleqmult = y(1:nsd);
      OPT.qp.solbndmult = y(nsd+(1:ns*nq));
      OPT.qp.solineqmult = y(nsd+ns*nq+(1:nsd*ns)); %! if nrd = 0, else more ineqmult 
    end
end


% Feedback update
OPT.feedbackdelta = x(nsd+(1:nq));
OPT.qptime = toc;

