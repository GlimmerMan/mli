function [outp] = eval_dyn_D_gn(inp)
    
    global SLV
    
    thoriz = inp.thoriz;    
    sd = inp.sd;
    q = inp.q;
    p = inp.p;    
    sensdirs = inp.sensdirs;

    ndf = size(sensdirs,2);	
    nsd = length(sd);

    int = SLV.opt.int;
    
    solvind('setTapeNumber', int, 1);
    solvind('setTimeHorizon',int, thoriz);
    solvind('setInitVals', int, [sd;q;p]);

    solvind('setForwardTaylorCoefficients', int, ndf, 1, sensdirs); 
    solvind('setAdjointTaylorCoefficients', int, []);

    solvind('activateFeature', int, 'Feature_Forward_Sensitivity_Error_Control');
    status = solvind('evaluate', int);
    solvind('deactivateFeature', int, 'Feature_Forward_Sensitivity_Error_Control');

    sol = solvind('getSolution', int);
    outp.x = sol(1:nsd);

    fwdDers = solvind('getFwdSens', int);
    outp.G = fwdDers;

    