function [step] = mli_varstep(var1,var2)
step.primal = var1.primal.sd(:) - var2.primal.sd(:);
step.primal = [step.primal;
	       var1.primal.q(:) - var2.primal.q(:)];

step.dual = var1.dual.lambda(:) - var2.dual.lambda(:);
step.dual = [step.dual;
	     var1.dual.mu_sd(:) - var2.dual.mu_sd(:)];
step.dual = [step.dual;
	     var1.dual.mu_q(:) - var2.dual.mu_q(:)];

if isfield(var1.dual, 'mu_rds')
    step.dual = [step.dual;
                 var1.dual.mu_rds(:) - var2.dual.mu_rds(:)];
end

if isfield(var1.dual, 'mu_rdi')
    step.dual = [step.dual;
                 var1.dual.mu_rdi(:) - var2.dual.mu_rdi(:)];
end

if isfield(var1.dual, 'mu_rde')
    step.dual = [step.dual;
                 var1.dual.mu_rde(:) - var2.dual.mu_rde(:)];
end
