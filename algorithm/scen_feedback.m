% -*-matlab-*-
function scen_feedback
    
global SCEN OPT ST

ii = SCEN.currentsample;

if SCEN.use_controller
    if SCEN.use_st == 0
    switch (OPT.currentphase)
      case 1
        SCEN.qhistory(:,ii) = OPT.phase{1}.ref.primal.q(:,1) + OPT.feedbackdelta(:);
      case {0, 4, 9}
        SCEN.qhistory(:,ii) = OPT.phase{4}.var.primal.q(:,1) + OPT.feedbackdelta(:);
      case 2
        SCEN.qhistory(:,ii) = OPT.phase{2}.var.primal.q(:,1) + OPT.feedbackdelta(:);
      case 3
        SCEN.qhistory(:,ii) = OPT.phase{3}.var.primal.q(:,1) + OPT.feedbackdelta(:);
      case 9
        SCEN.qhistory(:,ii) = OPT.feedbackq(:);
    end
    else
      J = ceil(ST.S/2);
      switch (OPT.currentphase)
        case {0, 4}
          SCEN.qhistory(:,ii) = OPT.phase{4}{J}.var.primal.q(:,1) + OPT.feedbackdelta(:);
        case 9
          SCEN.qhistory(:,ii) = OPT.feedbackq(:);
      end
    end
else
    SCEN.qhistory(:,ii)= SCEN.default_control(SCEN.xhistory(:,ii));
end
    
% if SCEN.withcontrollaw == 1
%   if MLI.currentphase == 1
%     SCEN.currentq = MLI.phaseA_q0ref;
%   else
%     SCEN.currentq = MLI.var.primal.q(:,1);
%   end
%   SCEN.FeedbackMat = MLI.FeedbackMat;
%   SCEN.FeedbackOff = MLI.FeedbackOff;
% end