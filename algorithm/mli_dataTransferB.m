% -*-matlab-*-
function mli_dataTransferB

global OPT;

nmfcn = OPT.dims.mfcn;
nlfcn = OPT.dims.lfcn;
nlsqs = OPT.dims.lsqfcn_s;
nlsqi = OPT.dims.lsqfcn_i; 
nlsqe = OPT.dims.lsqfcn_e;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;

% >=B data
if nlfcn > 0
  OPT.eval.res.lfcn = OPT.phase{2}.data.res.lfcn;
end

if nmfcn > 0 && nlfcn <= 0
  OPT.eval.res.mfun = OPT.phase{2}.data.res.mfun;
end

if nlsqs > 0
  OPT.eval.res.lsqfcn_s = OPT.phase{2}.data.res.lsqfcn_s;
end
if nlsqi > 0
  OPT.eval.res.lsqfcn_i = OPT.phase{2}.data.res.lsqfcn_i;
end
if nlsqe > 0
  OPT.eval.res.lsqfcn_e = OPT.phase{2}.data.res.lsqfcn_e;
end
	
if nrds > 0
  OPT.eval.res.rds = OPT.phase{2}.data.res.rds;
end
if nrdi > 0
  OPT.eval.res.rdi = OPT.phase{2}.data.res.rdi;
end
if nrde > 0
  OPT.eval.res.rde = OPT.phase{2}.data.res.rde;
end

OPT.eval.res.xd = OPT.phase{2}.data.res.xd;
OPT.eval.res.c = OPT.phase{2}.data.res.c;
OPT.eval.res.modgradx = OPT.phase{2}.data.res.modgradx;
OPT.eval.res.modgradq = OPT.phase{2}.data.res.modgradq;



