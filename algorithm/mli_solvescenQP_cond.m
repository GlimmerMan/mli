function mli_solvescenQP_cond(jj,lambda,flag)

global OPT ST

options = OPT.qpoptions;
nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nrd = nrds + (ns-1) * nrdi + nrde;
CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;

tscenqp = tic;

if 0 % solve scaled QP
    
dimR1 = nsd + (nq*ns);
dimR2 = nsd * ns;

% -------------------------- scale QP --------------
    
    % Initialize iterating matrices
    Hhat = OPT.qp{jj}.H;
    Ahat = OPT.qp{jj}.A;
    
    % Initialize scaling factors
    R = ones(dimR1+dimR2,1);
    
    % Create helper space
    h = zeros(dimR1+dimR2,1);
    must_scale = 1;
    
    while must_scale == 1
        
        % Row norms of composed matrix
        h(1:dimR1) = max(max(abs(Hhat),[],2), max(abs(Ahat),[],1)');
        h(dimR1+(1:dimR2)) = max(abs(Ahat),[],2);
        
        % h_i == F_i * 2^E_i, F_i in [0.5,1), E_i integer
        [F,E] = log2(h);
        
        % Compute scaling factors
        must_scale = 0;
        for i=1:(dimR1+dimR2)
            if E(i) < 0
                h(i) = pow2(1.0, floor((1-E(i))/2));
                must_scale = 1;
            elseif E(i) > 1
                h(i) = pow2(1.0, -floor(E(i)/2));
                must_scale = 1;
            else
                h(i) = 1.0;
            end
        end

        if must_scale == 1

        
        % Scale composed matrix
        for i = 1:dimR1
            Hhat(i,:) = h(i) * Hhat(i,:);
            Hhat(:,i) = h(i) * Hhat(:,i);
            
            Ahat(:,i) = h(i) * Ahat(:,i);
            
            R(i) = h(i) * R(i);
        end
        
        for i = 1:dimR2
            Ahat(i,:) = h(dimR1+i) * Ahat(i,:);
            
            R(dimR1+i) = h(dimR1+i) * R(dimR1+i);
        end
        end
    end
    
    OPT.qp{jj}.R1 = diag(R(1:dimR1));
    OPT.qp{jj}.R2 = diag(R(dimR1+(1:dimR2)));

%---------------------------------
    
    R1 = OPT.qp{jj}.R1;
    R2 = OPT.qp{jj}.R2;
    
    % Scale objective components
    OPT.qp{jj}.Hs = R1 * OPT.qp{jj}.H * R1;
    OPT.qp{jj}.gs = R1 * (OPT.qp{jj}.g + (ST.Cc{jj}-ST.Ec{jj})'*lambda);
    
    % Scale simple bounds
    OPT.qp{jj}.lbs = R1\OPT.qp{jj}.lb;
    OPT.qp{jj}.ubs = R1\OPT.qp{jj}.ub;
    
    % Scale constraint matrix and its bounds
    OPT.qp{jj}.As = R2 * OPT.qp{jj}.A * R1;
    OPT.qp{jj}.ineqlbs = R2 * OPT.qp{jj}.ineqlb;
    OPT.qp{jj}.inequbs = R2 * OPT.qp{jj}.inequb;
    
    % Solve scaled QP
        [x{jj}, ST.objj(jj), status{jj}, ~, y{jj}] = qpOASES_sequence('m', ...
        OPT.qp{jj}.qphandle,...
        OPT.qp{jj}.Hs, OPT.qp{jj}.gs , ...
        OPT.qp{jj}.As,OPT.qp{jj}.lbs,...
        OPT.qp{jj}.ubs,OPT.qp{jj}.ineqlbs,...
        OPT.qp{jj}.inequbs, ...
        options);
    
    % on error, try again from scratch
    if status{jj} ~= 0
        qpOASES_sequence('c', OPT.qp{jj}.qphandle);
        [OPT.qp{jj}.qphandle, x{jj}, ST.objj(jj), status{jj}, ~, y{jj}] = ...
            qpOASES_sequence('i', ...
            OPT.qp{jj}.Hs, OPT.qp{jj}.gs, ... % + (ST.Cc{jj}-ST.Ec{jj})'*lambda, ...
            OPT.qp{jj}.As, OPT.qp{jj}.lbs, ...
            OPT.qp{jj}.ubs, OPT.qp{jj}.ineqlbs, ...
            OPT.qp{jj}.inequbs, ...
            options);
    end
    
    % Rescale primal variables
    x{jj} = R1 * x{jj};
    
    % Rescale dual variables
    y{jj}(1:dimR1) = R1\y{jj}(1:dimR1);
    y{jj}(dimR1+(1:dimR2)) = R2 * y{jj}(dimR1+(1:dimR2));
    
else % no scaling of condensed QP
    
        % regularize Hessian
%         already in mli_solveSTcondQP
%         [V,E] = eig(0.5*(HH+HH'));

%         OPT.qp{jj}.H = 0.5 * (OPT.qp{jj}.H+OPT.qp{jj}.H');
    
        if 1 % solve scenario QP with qpOASES
        [x{jj}, ST.objj(jj), status{jj}, ~, y{jj}] = qpOASES_sequence('m', ...
            OPT.qp{jj}.qphandle,...
            OPT.qp{jj}.H, OPT.qp{jj}.g + (ST.Cc{jj}-ST.Ec{jj})'*lambda, ...
            OPT.qp{jj}.A,OPT.qp{jj}.lb,...
            OPT.qp{jj}.ub,OPT.qp{jj}.ineqlb,...
            OPT.qp{jj}.inequb, ...
            options);
    
        % on error, try again from scratch
        if status{jj} ~= 0
            qpOASES_sequence('c', OPT.qp{jj}.qphandle);
            [OPT.qp{jj}.qphandle, x{jj}, ST.objj(jj), status{jj}, ~, y{jj}] = ...
                qpOASES_sequence('i', ...
                OPT.qp{jj}.H, OPT.qp{jj}.g + (ST.Cc{jj}-ST.Ec{jj})'*lambda, ...
                OPT.qp{jj}.A, OPT.qp{jj}.lb, ...
                OPT.qp{jj}.ub, OPT.qp{jj}.ineqlb, ...
                OPT.qp{jj}.inequb, ...
                options);
        end

        if (status{jj} ~= 0)
            status{jj}
            fprintf('QP solution failed! Scenario number %i\n',jj);
            keyboard
        end
    
        else % try gurobi   %! CMR not supported
        
        OPT.qp{jj}.model.Q = sparse(0.5*OPT.qp{jj}.H);
        OPT.qp{jj}.model.obj = OPT.qp{jj}.g + (ST.Cc{jj}-ST.Ec{jj})'*lambda;
        OPT.qp{jj}.model.A = sparse([OPT.qp{jj}.A; OPT.qp{jj}.A]);
        OPT.qp{jj}.model.sense = char([ones(size(OPT.qp{jj}.ineqlb))*'>'; ones(size(OPT.qp{jj}.inequb))*'<']);
        OPT.qp{jj}.model.rhs = [OPT.qp{jj}.ineqlb; OPT.qp{jj}.inequb];
        OPT.qp{jj}.model.lb = OPT.qp{jj}.lb;
        OPT.qp{jj}.model.ub = OPT.qp{jj}.ub;
        params.Presolve = 2;
        params.TimeLimit = 100;
        params.OutputFlag = 0;
        result = gurobi(OPT.qp{jj}.model, params);
        
        if ~strcmp(result.status,'OPTIMAL')
            fprintf('QP solution failed! Scenario number %i\n',jj);
            keyboard
        end
        
        x{jj} = result.x;
        y{jj} = result.pi;
        ST.objj(jj) = result.objval;
        end
end

if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
  xoff = nsd + ns*nq;
  OPT.qp{jj}.solstep = x{jj}(1:xoff);
  OPT.qp{jj}.soldelq = x{jj}((xoff+1):end);
else
  OPT.qp{jj}.solstep = x{jj};
end

% Constraint multipliers
if any(isnan(y{jj}))
    disp('NANs in QP Multipliers!');
    keyboard
end

if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
  OPT.qp{jj}.soleqmult = y{jj}(1:nsd);
  OPT.qp{jj}.solbndmult = y{jj}(nsd+(1:ns*nq));
  OPT.qp{jj}.solbndmult2 = y{jj}(nsd+ns*nq+(1:ns*nq));
  xoffs = nsd + 2*ns*nq;
  OPT.qp{jj}.solineqmult = y{jj}(xoffs+(1:nsd*ns)); %! if nrd = 0, else more ineqmult
  OPT.qp{jj}.solCMRmult = y{jj}(xoffs+ns*nsd+(1:2*ns*nq));
else
  OPT.qp{jj}.soleqmult = y{jj}(1:nsd);
  OPT.qp{jj}.solbndmult = y{jj}(nsd+(1:ns*nq));
  OPT.qp{jj}.solineqmult = y{jj}(nsd+ns*nq+(1:nsd*ns)); %! if nrd = 0, else more ineqmult
end

OPT.scenqptime{jj} = toc(tscenqp);
OPT.scenqpcumtime{jj} = OPT.scenqpcumtime{jj}+OPT.scenqptime{jj};