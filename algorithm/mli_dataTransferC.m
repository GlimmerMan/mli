% -*-matlab-*-
function mli_dataTransferC

global OPT;

nmfcn = OPT.dims.mfcn;
nlsqs = OPT.dims.lsqfcn_s;
nlsqi = OPT.dims.lsqfcn_i; 
nlsqe = OPT.dims.lsqfcn_e;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nlfcn = OPT.dims.lfcn;

% >=B data
if nlfcn > 0
  OPT.eval.res.lfcn = OPT.phase{3}.data.res.lfcn;
end

if nmfcn > 0 && nlfcn <= 0
  OPT.eval.res.mfun = OPT.phase{3}.data.res.mfun;
end

if nlsqs > 0
  OPT.eval.res.lsqfcn_s = OPT.phase{3}.data.res.lsqfcn_s;
end
if nlsqi > 0
  OPT.eval.res.lsqfcn_i = OPT.phase{3}.data.res.lsqfcn_i;
end
if nlsqe > 0
  OPT.eval.res.lsqfcn_e = OPT.phase{3}.data.res.lsqfcn_e;
end
	
if nrds > 0
  OPT.eval.res.rds = OPT.phase{3}.data.res.rds;
end
if nrdi > 0
  OPT.eval.res.rdi = OPT.phase{3}.data.res.rdi;
end
if nrde > 0
  OPT.eval.res.rde = OPT.phase{3}.data.res.rde;
end

OPT.eval.res.xd = OPT.phase{3}.data.res.xd;
OPT.eval.res.c = OPT.phase{3}.data.res.c;
OPT.eval.res.modgradx = OPT.phase{3}.data.res.modgradx;
OPT.eval.res.modgradq = OPT.phase{3}.data.res.modgradq;

% >=C data
if nlfcn > 0
  OPT.eval.res.dlfcn = OPT.phase{3}.data.res.dlfcn;
end

if nmfcn > 0 && nlfcn <= 0
  OPT.eval.res.mgrad = OPT.phase{3}.data.res.mgrad;
end

if nlsqs > 0
  OPT.eval.res.lsqsgradx = OPT.phase{3}.data.res.lsqsgradx;
  OPT.eval.res.lsqsgradq = OPT.phase{3}.data.res.lsqsgradq;
end
if nlsqi > 0
  OPT.eval.res.lsqigradx = OPT.phase{3}.data.res.lsqigradx;
  OPT.eval.res.lsqigradq = OPT.phase{3}.data.res.lsqigradq;
end
if nlsqe > 0
  OPT.eval.res.lsqegradx = OPT.phase{3}.data.res.lsqegradx;
end

if nrds > 0
  OPT.eval.res.muJrdsx = OPT.phase{3}.data.res.muJrdsx;
  OPT.eval.res.muJrdsq = OPT.phase{3}.data.res.muJrdsq;
end
if nrdi > 0
  OPT.eval.res.muJrdix = OPT.phase{3}.data.res.muJrdix;
  OPT.eval.res.muJrdiq = OPT.phase{3}.data.res.muJrdiq;
end
if nrde > 0
  OPT.eval.res.muJrdex = OPT.phase{3}.data.res.muJrdex;
end

OPT.eval.res.lambdaGx = OPT.phase{3}.data.res.lambdaGx;
OPT.eval.res.lambdaGq = OPT.phase{3}.data.res.lambdaGq;
OPT.eval.res.refgradx = OPT.phase{3}.data.res.refgradx;
OPT.eval.res.refgradq = OPT.phase{3}.data.res.refgradq;
OPT.eval.res.laggradx = OPT.phase{3}.data.res.laggradx;
OPT.eval.res.laggradq = OPT.phase{3}.data.res.laggradq;

