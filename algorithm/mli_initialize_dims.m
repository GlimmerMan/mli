function mli_initialize_dims

global SCEN OPT EST SLV

% test SCEN
scendims = solvind('getDims', SLV.real.int);

SCEN.dims.nx = scendims.nxd;
SCEN.dims.nq = scendims.nq;
SCEN.dims.np = scendims.np;


% test OPT
if SCEN.use_controller
    optdims = solvind('getDims', SLV.opt.int);
    outdims = solvind('getOutputDims', SLV.opt.evl);
    
    OPT.dims.nsd = optdims.nxd;
    OPT.dims.nq = optdims.nq;
    OPT.dims.np = optdims.np;
    OPT.dims.nshoot = length(OPT.discr.msgrid)-1;
    OPT.dims.mfcn = outdims.mfcn;
    OPT.dims.lfcn = 0;
    OPT.dims.lsqfcn_s = outdims.lsqfcn_s;
    OPT.dims.lsqfcn_i = outdims.lsqfcn_i;
    OPT.dims.lsqfcn_e = outdims.lsqfcn_e;
    OPT.dims.rdfcn_s =  outdims.rdfcn_s;
    OPT.dims.rdfcn_i =  outdims.rdfcn_i;
    OPT.dims.rdfcn_e = outdims.rdfcn_e;
    
    SLV.opt.lagObjModSd = speye(OPT.dims.nsd);
    
    if 0 % OPT.dims.mfcn > 0
        tmpVars = init_vars;
        outp = mli_check_if_mfcn_is_lfcn(tmpVars);
        
        if outp.lfcnState > 0
            OPT.dims.lfcn = 1;
            OPT.dims.nsd = optdims.nxd-1;
            OPT.dims.lfcnState = outp.lfcnState;
            
            SLV.opt.lagObjModSd(OPT.dims.lfcnState,:) = [];
            
            SLV.opt.lagObjModSens = speye(1+optdims.nxd+optdims.nq+optdims.np);
            SLV.opt.lagObjModSens(:,1+OPT.dims.lfcnState) = [];
            
            SLV.opt.lagObjModLambda = speye(optdims.nxd);
            SLV.opt.lagObjModLambda(:,OPT.dims.lfcnState) = [];
            
            SLV.opt.lagObjLambda = -1;
        end
    end
    
    
end

% test EST
if SCEN.use_estimator
    estdims = solvind('getDims', SLV.est.int);
    estout = solvind('getOutputDims', SLV.est.evl);
    
    EST.dims.nshoot = length(EST.discr.msgrid)-1;
    EST.dims.nsd = estdims.nxd;
    EST.dims.nq = estdims.nq;
    EST.dims.np = estdims.np;
    EST.dims.lsqfcn_s = estout.lsqfcn_s;
    EST.dims.lsqfcn_i = estout.lsqfcn_i;
    EST.dims.lsqfcn_e = estout.lsqfcn_e;
    
    
end
