function problem_initialize
    
    global SCEN OPT

    scenario_initialize;
    
    if ~isfield(SCEN,'start')
      SCEN.start = 1;
    end
    
    if ~isfield(SCEN,'use_st')
      SCEN.use_st = 0;
    end
    
    if SCEN.use_st > 0
      tree_initialize;
    end
    
    if SCEN.use_controller
        controller_initialize;
    end
    
    if SCEN.use_estimator
        estimator_initialize;
    end
    
    
    
