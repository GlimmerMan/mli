% -*-matlab-*-
function est_evaluate

global EST;

% The following parts are evaluated:
%
% - matching conditions and sensitivity matrices
% - measurement function values and Jacobians
% - least squares residuals with exception of last one 
%   (the measurement is still unknown)
%
% This allows to set up the uncondensed system almost completely.


pvars = EST.var.primal;

if ~EST.disable_arrival
    if EST.fixed_p
        EST.eval.res.arrivalCost = ...
            EST.P * [pvars.sd(:,1) - EST.xbar];
    else
        EST.eval.res.arrivalCost = ...
            EST.P * [pvars.sd(:,1) - EST.xbar; 
                     pvars.p - EST.pbar];
    end
end

switch EST.currentphase
    
  case 2
    est_evalPhaseB;
  
  case 3
    est_evalPhaseC;
    
  case 4
    est_evalPhaseD;
    
end
    