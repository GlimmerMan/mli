function [outp] = eval_dyn_B(inp)
    
    global SLV OPT
    
    thoriz = inp.thoriz;    
    sd = inp.sd;
    q = inp.q;
    p = inp.p;  
    
    if OPT.dims.lfcn > 0
      sd = SLV.opt.lagObjModSd'*sd;
    end

    nsd = length(sd);
       
    int = SLV.opt.int;
    
    solvind('setTapeNumber', int, 1);
    solvind('setTimeHorizon',int, thoriz);
    solvind('setInitVals', int, [sd;q;p]);
    solvind('setForwardTaylorCoefficients', int, []);
    solvind('setAdjointTaylorCoefficients', int, []);
    status = solvind('evaluate', int);
    if status ~= 0 
        error('integration failed');
    end
    
    sol = solvind('getSolution', int);
    outp.x = SLV.opt.lagObjModSd*sol;
    
    if OPT.dims.lfcn > 0
      outp.lfcn = sol(OPT.dims.lfcnState);
    end

