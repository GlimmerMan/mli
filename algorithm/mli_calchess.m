% -*-matlab-*-
function mli_calchess
    
    global SCEN OPT ST
    
    ns = OPT.dims.nshoot;
    nsd = OPT.dims.nsd;
    nq = OPT.dims.nq;
    nlsqs = OPT.dims.lsqfcn_s;
    nlsqi = OPT.dims.lsqfcn_i; 
    nlsqe = OPT.dims.lsqfcn_e;
    nrds = OPT.dims.rdfcn_s;
    nrdi = OPT.dims.rdfcn_i;
    nrde = OPT.dims.rdfcn_e;
    nmfcn = OPT.dims.mfcn;
    nlfcn = OPT.dims.lfcn;
    
    if SCEN.use_st == 0
        switch (OPT.currentphase)
            
            case {1,2,3}
                OPT.hesstime = 0;
                return;
                
            case {4, 9}
                tic
                switch OPT.algo.hessian
                    
                    case 'exact'
                        
                        if nlsqs > 0
                            OPT.hess.ssblock{1} = OPT.eval.mat.Hlsqsxx;
                            OPT.hess.qqblock{1} = OPT.eval.mat.Hlsqsqq;
                            OPT.hess.sqblock{1} = OPT.eval.mat.Hlsqsxq;
                            
                            OPT.hess.ssblock{1} = OPT.hess.ssblock{1} + ...
                                OPT.eval.mat.Jlsqsx' * OPT.eval.mat.Jlsqsx;
                            OPT.hess.qqblock{1} = OPT.hess.qqblock{1} + ...
                                OPT.eval.mat.Jlsqsq' * OPT.eval.mat.Jlsqsq;
                            OPT.hess.sqblock{1} = OPT.hess.sqblock{1} + ...
                                OPT.eval.mat.Jlsqsx' * OPT.eval.mat.Jlsqsq;
                        else
                            OPT.hess.ssblock{1} = zeros(nsd,nsd);
                            OPT.hess.qqblock{1} = zeros(nq,nq);
                            OPT.hess.sqblock{1} = zeros(nsd,nq);
                        end
                        
                        OPT.hess.ssblock{1} = OPT.hess.ssblock{1} - OPT.eval.mat.lambdaGxx{1};
                        OPT.hess.qqblock{1} = OPT.hess.qqblock{1} - OPT.eval.mat.lambdaGqq{1};
                        OPT.hess.sqblock{1} = OPT.hess.sqblock{1} - OPT.eval.mat.lambdaGxq{1};
                        
                        if nrds > 0
                            OPT.hess.ssblock{1} = OPT.hess.ssblock{1} - ...
                                OPT.eval.mat.muJrdsxx;
                            OPT.hess.qqblock{1} = OPT.hess.qqblock{1} - ...
                                OPT.eval.mat.muJrdsqq;
                            OPT.hess.sqblock{1} = OPT.hess.sqblock{1} - ...
                                OPT.eval.mat.muJrdsxq;
                        end
                        
                        for msn=2:ns
                            if nlsqi > 0
                                OPT.hess.ssblock{msn} = OPT.eval.mat.Hlsqixx{msn-1};
                                OPT.hess.qqblock{msn} = OPT.eval.mat.Hlsqiqq{msn-1};
                                OPT.hess.sqblock{msn} = OPT.eval.mat.Hlsqixq{msn-1};
                                
                                OPT.hess.ssblock{msn} = OPT.hess.ssblock{msn} + ...
                                    OPT.eval.mat.Jlsqix{msn-1}' * OPT.eval.mat.Jlsqix{msn-1};
                                OPT.hess.qqblock{msn} = OPT.hess.qqblock{msn} + ...
                                    OPT.eval.mat.Jlsqiq{msn-1}' * OPT.eval.mat.Jlsqiq{msn-1};
                                OPT.hess.sqblock{msn} = OPT.hess.sqblock{msn} + ...
                                    OPT.eval.mat.Jlsqix{msn-1}' * OPT.eval.mat.Jlsqiq{msn-1};
                            else
                                OPT.hess.ssblock{msn} = zeros(nsd,nsd);
                                OPT.hess.qqblock{msn} = zeros(nq,nq);
                                OPT.hess.sqblock{msn} = zeros(nsd,nq);
                            end
                            
                            OPT.hess.ssblock{msn} =  OPT.hess.ssblock{msn} - OPT.eval.mat.lambdaGxx{msn};
                            OPT.hess.qqblock{msn} =  OPT.hess.qqblock{msn} - OPT.eval.mat.lambdaGqq{msn};
                            OPT.hess.sqblock{msn} =  OPT.hess.sqblock{msn} - OPT.eval.mat.lambdaGxq{msn};
                            
                            if nrdi > 0
                                OPT.hess.ssblock{msn} = OPT.hess.ssblock{msn} - ...
                                    OPT.eval.mat.muJrdixx{msn-1};
                                OPT.hess.qqblock{msn} = OPT.hess.qqblock{msn} - ...
                                    OPT.eval.mat.muJrdiqq{msn-1};
                                OPT.hess.sqblock{msn} = OPT.hess.sqblock{msn} - ...
                                    OPT.eval.mat.muJrdixq{msn-1};
                            end
                        end
                        
                        msn = ns + 1;
                        if nmfcn > 0 && nlfcn <= 0
                            OPT.hess.ssblock{msn} = OPT.eval.mat.mhess;
                        else
                            OPT.hess.ssblock{msn} = zeros(nsd,nsd);
                        end
                        
                        if nlsqe > 0
                            OPT.hess.ssblock{msn} = OPT.hess.ssblock{msn} + OPT.eval.mat.Hlsqexx;
                            OPT.hess.ssblock{msn} = OPT.hess.ssblock{msn} + ...
                                OPT.eval.mat.Jlsqex' * OPT.eval.mat.Jlsqex;
                        end
                        
                        if nrde > 0
                            OPT.hess.ssblock{msn} = OPT.hess.ssblock{msn} - ...
                                OPT.eval.mat.muJrdexx;
                        end
                        OPT.hesstime = toc;
                        
                    case 'gn'
                        
                        OPT.hess.ssblock{1} = zeros(nsd,nsd);
                        OPT.hess.qqblock{1} = zeros(nq,nq);
                        OPT.hess.sqblock{1} = zeros(nsd,nq);
                        
                        if nlsqs > 0
                            OPT.hess.ssblock{1} = OPT.hess.ssblock{1} + ...
                                OPT.eval.mat.Jlsqsx' * OPT.eval.mat.Jlsqsx;
                            OPT.hess.qqblock{1} = OPT.hess.qqblock{1} + ...
                                OPT.eval.mat.Jlsqsq' * OPT.eval.mat.Jlsqsq;
                            OPT.hess.sqblock{1} = OPT.hess.sqblock{1} + ...
                                OPT.eval.mat.Jlsqsx' * OPT.eval.mat.Jlsqsq;
                        end
                        
                        
                        for msn=2:ns
                            OPT.hess.ssblock{msn} = zeros(nsd,nsd);
                            OPT.hess.qqblock{msn} = zeros(nq,nq);
                            OPT.hess.sqblock{msn} = zeros(nsd,nq);
                            
                            if nlsqi > 0
                                OPT.hess.ssblock{msn} = OPT.hess.ssblock{msn} + ...
                                    OPT.eval.mat.Jlsqix{msn-1}' * ...
                                    OPT.eval.mat.Jlsqix{msn-1};
                                OPT.hess.qqblock{msn} = OPT.hess.qqblock{msn} + ...
                                    OPT.eval.mat.Jlsqiq{msn-1}' * ...
                                    OPT.eval.mat.Jlsqiq{msn-1};
                                OPT.hess.sqblock{msn} = OPT.hess.sqblock{msn} + ...
                                    OPT.eval.mat.Jlsqix{msn-1}' * ...
                                    OPT.eval.mat.Jlsqiq{msn-1};
                            end
                        end
                        
                        msn = ns + 1;
                        OPT.hess.ssblock{msn} = zeros(nsd,nsd);
                        if nlsqe > 0
                            OPT.hess.ssblock{msn} = OPT.hess.ssblock{msn} + ...
                                OPT.eval.mat.Jlsqex' * OPT.eval.mat.Jlsqex;
                        end
                        OPT.hesstime = toc;
                        
                end
                
            case 0
                tic
                switch OPT.algo.hessian
                    
                    case 'exact'
                        
                        for msn=1:ns
                            OPT.hess.ssblock{msn} = eye(nsd);
                            OPT.hess.qqblock{msn} = eye(nq);
                            OPT.hess.sqblock{msn} = zeros(nsd,nq);
                        end
                        
                        msn = ns + 1;
                        OPT.hess.ssblock{msn} = eye(nsd);
                        OPT.hesstime = toc;
                        
                    case 'gn'
                        
                        OPT.hess.ssblock{1} = zeros(nsd,nsd);
                        OPT.hess.qqblock{1} = zeros(nq,nq);
                        OPT.hess.sqblock{1} = zeros(nsd,nq);
                        
                        if nlsqs > 0
                            OPT.hess.ssblock{1} = OPT.hess.ssblock{1} + ...
                                OPT.eval.mat.Jlsqsx' * OPT.eval.mat.Jlsqsx;
                            OPT.hess.qqblock{1} = OPT.hess.qqblock{1} + ...
                                OPT.eval.mat.Jlsqsq' * OPT.eval.mat.Jlsqsq;
                            OPT.hess.sqblock{1} = OPT.hess.sqblock{1} + ...
                                OPT.eval.mat.Jlsqsx' * OPT.eval.mat.Jlsqsq;
                        end
                        
                        
                        for msn=2:ns
                            OPT.hess.ssblock{msn} = zeros(nsd,nsd);
                            OPT.hess.qqblock{msn} = zeros(nq,nq);
                            OPT.hess.sqblock{msn} = zeros(nsd,nq);
                            
                            if nlsqi > 0
                                OPT.hess.ssblock{msn} = OPT.hess.ssblock{msn} + ...
                                    OPT.eval.mat.Jlsqix{msn-1}' * ...
                                    OPT.eval.mat.Jlsqix{msn-1};
                                OPT.hess.qqblock{msn} = OPT.hess.qqblock{msn} + ...
                                    OPT.eval.mat.Jlsqiq{msn-1}' * ...
                                    OPT.eval.mat.Jlsqiq{msn-1};
                                OPT.hess.sqblock{msn} = OPT.hess.sqblock{msn} + ...
                                    OPT.eval.mat.Jlsqix{msn-1}' * ...
                                    OPT.eval.mat.Jlsqiq{msn-1};
                            end
                        end
                        
                        msn = ns + 1;
                        OPT.hess.ssblock{msn} = zeros(nsd,nsd);
                        if nlsqe > 0
                            OPT.hess.ssblock{msn} = OPT.hess.ssblock{msn} + ...
                                OPT.eval.mat.Jlsqex' * OPT.eval.mat.Jlsqex;
                        end
                        OPT.hesstime = toc;
                end
                
        end
        
    else %%%%%%%%%%%%%%%%%%% with ST
        S = ST.S;
        switch (OPT.currentphase)
            
            case {1,2,3}
                OPT.hesstime = 0;
                return;
                
            case {4,9}
                tic
                switch OPT.algo.hessian
                    
                    case 'exact'
                        
                        if nlsqs > 0
                            for jj = 1:S
                                OPT.hess{jj}.ssblock{1} = OPT.eval{jj}.mat.Hlsqsxx;
                                OPT.hess{jj}.qqblock{1} = OPT.eval{jj}.mat.Hlsqsqq;
                                OPT.hess{jj}.sqblock{1} = OPT.eval{jj}.mat.Hlsqsxq;
                                
                                OPT.hess{jj}.ssblock{1} = OPT.hess{jj}.ssblock{1} + ...
                                    OPT.eval{jj}.mat.Jlsqsx' * OPT.eval{jj}.mat.Jlsqsx;
                                OPT.hess{jj}.qqblock{1} = OPT.hess{jj}.qqblock{1} + ...
                                    OPT.eval{jj}.mat.Jlsqsq' * OPT.eval{jj}.mat.Jlsqsq;
                                OPT.hess{jj}.sqblock{1} = OPT.hess{jj}.sqblock{1} + ...
                                    OPT.eval{jj}.mat.Jlsqsx' * OPT.eval{jj}.mat.Jlsqsq;
                            end
                        else
                            for jj = 1:S
                                OPT.hess{jj}.ssblock{1} = zeros(nsd,nsd);
                                OPT.hess{jj}.qqblock{1} = zeros(nq,nq);
                                OPT.hess{jj}.sqblock{1} = zeros(nsd,nq);
                            end
                        end
                        
                        for jj = 1:S
                            OPT.hess{jj}.ssblock{1} = OPT.hess{jj}.ssblock{1} - OPT.eval{jj}.mat.lambdaGxx{1};
                            OPT.hess{jj}.qqblock{1} = OPT.hess{jj}.qqblock{1} - OPT.eval{jj}.mat.lambdaGqq{1};
                            OPT.hess{jj}.sqblock{1} = OPT.hess{jj}.sqblock{1} - OPT.eval{jj}.mat.lambdaGxq{1};
                        end
                        
                        if nrds > 0
                            for jj = 1:S
                                OPT.hess{jj}.ssblock{1} = OPT.hess{jj}.ssblock{1} - ...
                                    OPT.eval{jj}.mat.muJrdsxx;
                                OPT.hess{jj}.qqblock{1} = OPT.hess{jj}.qqblock{1} - ...
                                    OPT.eval{jj}.mat.muJrdsqq;
                                OPT.hess{jj}.sqblock{1} = OPT.hess{jj}.sqblock{1} - ...
                                    OPT.eval{jj}.mat.muJrdsxq;
                            end
                        end
                        
                        for msn=2:ns
                            if nlsqi > 0
                                for jj = 1:S
                                    OPT.hess{jj}.ssblock{msn} = OPT.eval{jj}.mat.Hlsqixx{msn-1};
                                    OPT.hess{jj}.qqblock{msn} = OPT.eval{jj}.mat.Hlsqiqq{msn-1};
                                    OPT.hess{jj}.sqblock{msn} = OPT.eval{jj}.mat.Hlsqixq{msn-1};
                                    
                                    OPT.hess{jj}.ssblock{msn} = OPT.hess{jj}.ssblock{msn} + ...
                                        OPT.eval{jj}.mat.Jlsqix{msn-1}' * OPT.eval{jj}.mat.Jlsqix{msn-1};
                                    OPT.hess{jj}.qqblock{msn} = OPT.hess{jj}.qqblock{msn} + ...
                                        OPT.eval{jj}.mat.Jlsqiq{msn-1}' * OPT.eval{jj}.mat.Jlsqiq{msn-1};
                                    OPT.hess{jj}.sqblock{msn} = OPT.hess{jj}.sqblock{msn} + ...
                                        OPT.eval{jj}.mat.Jlsqix{msn-1}' * OPT.eval{jj}.mat.Jlsqiq{msn-1};
                                end
                            else
                                for jj = 1:S
                                    OPT.hess{jj}.ssblock{msn} = zeros(nsd,nsd);
                                    OPT.hess{jj}.qqblock{msn} = zeros(nq,nq);
                                    OPT.hess{jj}.sqblock{msn} = zeros(nsd,nq);
                                end
                            end
                            
                            for jj = 1:S
                                OPT.hess{jj}.ssblock{msn} =  OPT.hess{jj}.ssblock{msn} - OPT.eval{jj}.mat.lambdaGxx{msn};
                                OPT.hess{jj}.qqblock{msn} =  OPT.hess{jj}.qqblock{msn} - OPT.eval{jj}.mat.lambdaGqq{msn};
                                OPT.hess{jj}.sqblock{msn} =  OPT.hess{jj}.sqblock{msn} - OPT.eval{jj}.mat.lambdaGxq{msn};
                            end
                            
                            if nrdi > 0
                                for jj = 1:S
                                    OPT.hess{jj}.ssblock{msn} = OPT.hess{jj}.ssblock{msn} - ...
                                        OPT.eval{jj}.mat.muJrdixx{msn-1};
                                    OPT.hess{jj}.qqblock{msn} = OPT.hess{jj}.qqblock{msn} - ...
                                        OPT.eval{jj}.mat.muJrdiqq{msn-1};
                                    OPT.hess{jj}.sqblock{msn} = OPT.hess{jj}.sqblock{msn} - ...
                                        OPT.eval{jj}.mat.muJrdixq{msn-1};
                                end
                            end
                        end
                        
                        msn = ns + 1;
                        if nmfcn > 0 && nlfcn <= 0
                            for jj = 1:S
                                OPT.hess{jj}.ssblock{msn} = OPT.eval{jj}.mat.mhess;
                            end
                        else
                            for jj = 1:S
                                OPT.hess{jj}.ssblock{msn} = zeros(nsd,nsd);
                            end
                        end
                        
                        if nlsqe > 0
                            for jj = 1:S
                                OPT.hess{jj}.ssblock{msn} = OPT.hess{jj}.ssblock{msn} + OPT.eval{jj}.mat.Hlsqexx;
                                OPT.hess{jj}.ssblock{msn} = OPT.hess{jj}.ssblock{msn} + ...
                                    OPT.eval{jj}.mat.Jlsqex' * OPT.eval{jj}.mat.Jlsqex;
                            end
                        end
                        
                        if nrde > 0
                            for jj = 1:S
                                OPT.hess{jj}.ssblock{msn} = OPT.hess{jj}.ssblock{msn} - ...
                                    OPT.eval{jj}.mat.muJrdexx;
                            end
                        end
                        OPT.hesstime = toc;
                        
                    case 'gn'
                        
                        for jj = 1:S
                            OPT.hess{jj}.ssblock{1} = zeros(nsd,nsd);
                            OPT.hess{jj}.qqblock{1} = zeros(nq,nq);
                            OPT.hess{jj}.sqblock{1} = zeros(nsd,nq);
                        end
                        
                        if nlsqs > 0
                            for jj = 1:S
                                OPT.hess{jj}.ssblock{1} = OPT.hess{jj}.ssblock{1} + ...
                                    OPT.eval{jj}.mat.Jlsqsx' * OPT.eval{jj}.mat.Jlsqsx;
                                OPT.hess{jj}.qqblock{1} = OPT.hess{jj}.qqblock{1} + ...
                                    OPT.eval{jj}.mat.Jlsqsq' * OPT.eval{jj}.mat.Jlsqsq;
                                OPT.hess{jj}.sqblock{1} = OPT.hess{jj}.sqblock{1} + ...
                                    OPT.eval{jj}.mat.Jlsqsx' * OPT.eval{jj}.mat.Jlsqsq;
                            end
                        end
                        
                        
                        for msn=2:ns
                            for jj = 1:S
                                OPT.hess{jj}.ssblock{msn} = zeros(nsd,nsd);
                                OPT.hess{jj}.qqblock{msn} = zeros(nq,nq);
                                OPT.hess{jj}.sqblock{msn} = zeros(nsd,nq);
                            end
                            
                            if nlsqi > 0
                                for jj = 1:S
                                    OPT.hess{jj}.ssblock{msn} = OPT.hess{jj}.ssblock{msn} + ...
                                        OPT.eval{jj}.mat.Jlsqix{msn-1}' * ...
                                        OPT.eval{jj}.mat.Jlsqix{msn-1};
                                    OPT.hess{jj}.qqblock{msn} = OPT.hess{jj}.qqblock{msn} + ...
                                        OPT.eval{jj}.mat.Jlsqiq{msn-1}' * ...
                                        OPT.eval{jj}.mat.Jlsqiq{msn-1};
                                    OPT.hess{jj}.sqblock{msn} = OPT.hess{jj}.sqblock{msn} + ...
                                        OPT.eval{jj}.mat.Jlsqix{msn-1}' * ...
                                        OPT.eval{jj}.mat.Jlsqiq{msn-1};
                                end
                            end
                        end
                        
                        msn = ns + 1;
                        for jj = 1:S
                            OPT.hess{jj}.ssblock{msn} = zeros(nsd,nsd);
                        end
                        if nlsqe > 0
                            for jj = 1:S
                                OPT.hess{jj}.ssblock{msn} = OPT.hess{jj}.ssblock{msn} + ...
                                    OPT.eval{jj}.mat.Jlsqex' * OPT.eval{jj}.mat.Jlsqex;
                            end
                        end
                        OPT.hesstime = toc;
                        
                end
                
            case 0
                tic
                switch OPT.algo.hessian
                    
                    case 'exact'
                        
                        for msn=1:ns
                            for jj = 1:S
                                OPT.hess{jj}.ssblock{msn} = eye(nsd);
                                OPT.hess{jj}.qqblock{msn} = eye(nq);
                                OPT.hess{jj}.sqblock{msn} = zeros(nsd,nq);
                            end
                        end
                        
                        msn = ns + 1;
                        for jj = 1:S
                            OPT.hess{jj}.ssblock{msn} = eye(nsd);
                        end
                        OPT.hesstime = toc;
                        
                    case 'gn'
                        
                        for jj = 1:S
                            OPT.hess{jj}.ssblock{1} = zeros(nsd,nsd);
                            OPT.hess{jj}.qqblock{1} = zeros(nq,nq);
                            OPT.hess{jj}.sqblock{1} = zeros(nsd,nq);
                        end
                        
                        if nlsqs > 0
                            for jj = 1:S
                                OPT.hess{jj}.ssblock{1} = OPT.hess{jj}.ssblock{1} + ...
                                    OPT.eval{jj}.mat.Jlsqsx' * OPT.eval{jj}.mat.Jlsqsx;
                                OPT.hess{jj}.qqblock{1} = OPT.hess{jj}.qqblock{1} + ...
                                    OPT.eval{jj}.mat.Jlsqsq' * OPT.eval{jj}.mat.Jlsqsq;
                                OPT.hess{jj}.sqblock{1} = OPT.hess{jj}.sqblock{1} + ...
                                    OPT.eval{jj}.mat.Jlsqsx' * OPT.eval{jj}.mat.Jlsqsq;
                            end
                        end
                        
                        
                        for msn=2:ns
                            for jj = 1:S
                                OPT.hess{jj}.ssblock{msn} = zeros(nsd,nsd);
                                OPT.hess{jj}.qqblock{msn} = zeros(nq,nq);
                                OPT.hess{jj}.sqblock{msn} = zeros(nsd,nq);
                            end
                            
                            if nlsqi > 0
                                for jj = 1:S
                                    OPT.hess{jj}.ssblock{msn} = OPT.hess{jj}.ssblock{msn} + ...
                                        OPT.eval{jj}.mat.Jlsqix{msn-1}' * ...
                                        OPT.eval{jj}.mat.Jlsqix{msn-1};
                                    OPT.hess{jj}.qqblock{msn} = OPT.hess{jj}.qqblock{msn} + ...
                                        OPT.eval{jj}.mat.Jlsqiq{msn-1}' * ...
                                        OPT.eval{jj}.mat.Jlsqiq{msn-1};
                                    OPT.hess{jj}.sqblock{msn} = OPT.hess{jj}.sqblock{msn} + ...
                                        OPT.eval{jj}.mat.Jlsqix{msn-1}' * ...
                                        OPT.eval{jj}.mat.Jlsqiq{msn-1};
                                end
                            end
                        end
                        
                        msn = ns + 1;
                        for jj = 1:S
                            OPT.hess{jj}.ssblock{msn} = zeros(nsd,nsd);
                        end
                        if nlsqe > 0
                            for jj = 1:S
                                OPT.hess{jj}.ssblock{msn} = OPT.hess{jj}.ssblock{msn} + ...
                                    OPT.eval{jj}.mat.Jlsqex' * OPT.eval{jj}.mat.Jlsqex;
                            end
                        end
                        OPT.hesstime = toc;
                end
                
        end
        
    end
