% -*-matlab-*-
function [data] = init_nlp_data()

global SCEN OPT ST

% Dimensionen
nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
nmfcn = OPT.dims.mfcn;
nlfcn = OPT.dims.lfcn;
ns = OPT.dims.nshoot;

if SCEN.use_st == 0
    
data.res.xd = zeros(nsd,ns);
data.res.c  = zeros(nsd,ns);

if nlfcn > 0
  data.res.lfcn = zeros(1,ns);
  data.res.dlfcn = zeros(nsd+nq,ns);
end

data.mat.Gx = mat2cell(zeros(nsd,nsd,ns),nsd,nsd,ones(ns,1));
data.mat.Gq = mat2cell(zeros(nsd,nq,ns),nsd,nq,ones(ns,1));
data.mat.lambdaGxx = mat2cell(zeros(nsd,nsd,ns),nsd,nsd,ones(ns,1));
data.mat.lambdaGxq = mat2cell(zeros(nsd,nq,ns),nsd,nq,ones(ns,1));
data.mat.lambdaGqq = mat2cell(zeros(nq,nq,ns),nq,nq,ones(ns,1));

if nmfcn > 0 && nlfcn <= 0
  data.res.mfun = 0;
  data.res.mgrad = zeros(nsd,1);
  data.mat.mhess = zeros(nsd,nsd);
end

else % ST case
   S = ST.S;
   for jj = 1:S
       data.res{jj}.xd = zeros(nsd,ns);
       data.res{jj}.c  = zeros(nsd,ns);
       
       if nlfcn > 0
           data.res{jj}.lfcn = zeros(1,ns);
           data.res{jj}.dlfcn = zeros(nsd+nq,ns);
       end
       
       data.mat{jj}.Gx = mat2cell(zeros(nsd,nsd,ns),nsd,nsd,ones(ns,1));
       data.mat{jj}.Gq = mat2cell(zeros(nsd,nq,ns),nsd,nq,ones(ns,1));
       data.mat{jj}.lambdaGxx = mat2cell(zeros(nsd,nsd,ns),nsd,nsd,ones(ns,1));
       data.mat{jj}.lambdaGxq = mat2cell(zeros(nsd,nq,ns),nsd,nq,ones(ns,1));
       data.mat{jj}.lambdaGqq = mat2cell(zeros(nq,nq,ns),nq,nq,ones(ns,1));
       
       if nmfcn > 0 && nlfcn <= 0
           data.res{jj}.mfun = 0;
           data.res{jj}.mgrad = zeros(nsd,1);
           data.mat{jj}.mhess = zeros(nsd,nsd);
       end
   end  
end
end
