% -*-matlab-*-
function phasecomm_maximal(phase)

global SCEN OPT ST

if SCEN.use_st == 0
    switch phase
        case {0,4}
            if ismember(3, OPT.ctrl.usephases)
                OPT.phase{3}.var = OPT.phase{4}.var;
            end
            if ismember(2, OPT.ctrl.usephases)
                OPT.phase{2}.var = OPT.phase{4}.var;
                OPT.phase{2}.ref = OPT.phase{4}.var;
            end
            if ismember(1, OPT.ctrl.usephases)
                OPT.phase{1}.ref = OPT.phase{4}.var;
            end
            
        case 1
            % Do nothing
            
        case 2
            if ismember(4, OPT.ctrl.usephases)
                OPT.phase{4}.var = OPT.phase{2}.var;
            end
            if ismember(3, OPT.ctrl.usephases)
                OPT.phase{3}.var = OPT.phase{2}.var;
            end
            if ismember(1, OPT.ctrl.usephases)
                OPT.phase{1}.ref = OPT.phase{2}.var;
            end
            
        case 3
            if ismember(4, OPT.ctrl.usephases)
                OPT.phase{4}.var = OPT.phase{3}.var;
            end
            if ismember(2, OPT.ctrl.usephases)
                OPT.phase{2}.var = OPT.phase{3}.var;
                OPT.phase{2}.ref = OPT.phase{3}.var;
            end
            if ismember(1, OPT.ctrl.usephases)
                OPT.phase{1}.ref = OPT.phase{3}.var;
            end
    end
else % ST
    S = ST.S;
    switch phase
        case {0,4}
            for jj = 1:S
                if ismember(3, OPT.ctrl.usephases)
                    OPT.phase{3}{jj}.var = OPT.phase{4}{jj}.var;
                end
                if ismember(2, OPT.ctrl.usephases)
                    OPT.phase{2}{jj}.var = OPT.phase{4}{jj}.var;
                    OPT.phase{2}{jj}.ref = OPT.phase{4}{jj}.var;
                end
                if ismember(1, OPT.ctrl.usephases)
                    OPT.phase{1}{jj}.ref = OPT.phase{4}{jj}.var;
                end
            end
            
        case 1
            % Do nothing
            
        case 2
            for jj = 1:S
                if ismember(4, OPT.ctrl.usephases)
                    OPT.phase{4}{jj}.var = OPT.phase{2}{jj}.var;
                end
                if ismember(3, OPT.ctrl.usephases)
                    OPT.phase{3}{jj}.var = OPT.phase{2}{jj}.var;
                end
                if ismember(1, OPT.ctrl.usephases)
                    OPT.phase{1}{jj}.ref = OPT.phase{2}{jj}.var;
                end
            end
        case 3
            for jj = 1:S
                if ismember(4, OPT.ctrl.usephases)
                    OPT.phase{4}{jj}.var = OPT.phase{3}{jj}.var;
                end
                if ismember(2, OPT.ctrl.usephases)
                    OPT.phase{2}{jj}.var = OPT.phase{3}{jj}.var;
                    OPT.phase{2}{jj}.ref = OPT.phase{3}{jj}.var;
                end
                if ismember(1, OPT.ctrl.usephases)
                    OPT.phase{1}{jj}.ref = OPT.phase{3}{jj}.var;
                end
            end
    end
    
end
