function [outp] = eval_dyn_D(inp)
    
    global SLV OPT
    
    thoriz = inp.thoriz;    
    sd = inp.sd;
    q = inp.q;
    p = inp.p;    
    lambda = inp.lambda;
    sensdirs = inp.sensdirs;

    if OPT.dims.lfcn > 0
      sd = SLV.opt.lagObjModSd'*sd;      
      
      lambda = SLV.opt.lagObjModLambda * lambda;
      
      sensdirs = SLV.opt.lagObjModSens * sensdirs;
    end
    
    ndf = size(sensdirs,2);	
    
    adjDirs = repmat([lambda,zeros(size(lambda))], 1, ndf);

    int = SLV.opt.int;
    
    solvind('setTapeNumber', int, 1);
   
    % Integration
    solvind('setTimeHorizon',int, thoriz);
    solvind('setInitVals', int, [sd;q;p]);

%    solvind('activateFeature', int, 'Feature_Forward_Sensitivity_Error_Control');
    status = solvind('evaluate', int);
    if status ~= 0 
        error('integration failed');
    end
%    solvind('deactivateFeature', int, 'Feature_Forward_Sensitivity_Error_Control');

    sol = solvind('getSolution', int);
    outp.x = SLV.opt.lagObjModSd*sol;

    % Forward Sweep
    solvind('setForwardTaylorCoefficients', int, ndf, 1, sensdirs); 

    retval = solvind('forwardSensSweep', int);
    if retval ~= 0 
        error('forwardSensSweep failed');
    end
    
    fwdDers = solvind('getFwdSens', int);
    outp.G = SLV.opt.lagObjModSd*fwdDers;
    if OPT.dims.lfcn > 0
      outp.dlfcn = fwdDers(OPT.dims.lfcnState,:);
    end
    
    % Backward Sweep
    solvind('setAdjointTaylorCoefficients', int, 1, ndf, 2, adjDirs);

    retval = solvind('backwardSensSweep', int);
    if retval ~= 0 
        error('backwardSensSweep failed');
    end

    help = solvind('getAdjSens', int);
    outp.lag = help(2:end,1);
    if OPT.dims.lfcn > 0
      outp.lag(OPT.dims.lfcnState) = [];
    end    
    
    fwd_adj_hess = help(2:end,2:2:end);
    if OPT.dims.lfcn > 0
        P = SLV.opt.lagObjModSens(2:end,2:end);
        outp.H = P' * fwd_adj_hess;
    else
        outp.H = fwd_adj_hess;
    end
    
    % lfcn
    % This seems to work. Unclear if this is the fastest/most elegant
    % solution!?
    
    if OPT.dims.lfcn > 0
        lambda = zeros(size(lambda));
        lambda(OPT.dims.lfcnState) = SLV.opt.lagObjLambda;
        adjDirs = kron(ones(1,ndf), [lambda,zeros(size(lambda))]);
        solvind('setAdjointTaylorCoefficients', int, 1, ndf, 2, adjDirs);

        retval = solvind('backwardSensSweep', int);
    
        if retval ~= 0 
            error('backwardSensSweep failed');
        end
        
        help = solvind('getAdjSens', int);
        fwd_adj_hess = help(2:end,2:2:end); 
        P = SLV.opt.lagObjModSens(2:end,2:end);
        outp.H = outp.H + P' * fwd_adj_hess; 
    end

    if ~all(isfinite(sol)) || ~all(isfinite(fwdDers(:))) || ~all(isfinite(help(:))) 
      fprintf('Warning: Integration solution/derivatives are not finite!\n');
      keyboard;
    end

