% -*-matlab-*-
function [laggradx,laggradq] = mli_assemble_laggrad(dvars,data)

% dvars data: who is who
global OPT

ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd; 
nq = OPT.dims.nq; 

nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nlsqs = OPT.dims.lsqfcn_s;
nlsqe = OPT.dims.lsqfcn_e;
nmfcn = OPT.dims.mfcn;
nlfcn = OPT.dims.lfcn;

laggradx = zeros(nsd,ns+1);
laggradq = zeros(nq,ns+1);

%%% Start Node
msn = 1;

% laggradx(:,msn) = - data.res.lambdaGx(:,msn) - dvars.mu_sd(:,msn);
% laggradq(:,msn) = - data.res.lambdaGq(:,msn) - dvars.mu_q(:,msn);
laggradx(:,msn) = - data.mat.Gx{msn}'*dvars.lambda(:,msn) - dvars.mu_sd(:,msn);
laggradq(:,msn) = - data.mat.Gq{msn}'*dvars.lambda(:,msn) - dvars.mu_q(:,msn);

if nrds > 0
  laggradx(:,msn) = laggradx(:,msn) ...
      - data.res.muJrdsx;
  laggradq(:,msn) = laggradq(:,msn) ...
      - data.res.muJrdsq;
end

if nlsqs > 0
  laggradx(:,msn) = laggradx(:,msn) ...
      + data.res.lsqsgradx;
  laggradq(:,msn) = laggradq(:,msn) ...
      + data.res.lsqsgradq;
  
  data.res.modgradx(:,msn) = data.res.modgradx(:,msn) ... 
      + data.res.lsqsgradx;
  data.res.modgradq(:,msn) = data.res.modgradq(:,msn) ... 
      + data.res.lsqsgradq;
  
  data.res.refgradx(:,msn) = data.res.refgradx(:,msn) ... 
      + data.res.lsqsgradx;
  data.res.refgradq(:,msn) = data.res.refgradq(:,msn) ... 
      + data.res.lsqsgradq;
end

if nlfcn > 0
    laggradx(:,msn) = laggradx(:,msn) ...
        + data.res.dlfcn(1:nsd,msn);
    laggradq(:,msn) = laggradq(:,msn) ...
        + data.res.dlfcn(nsd+(1:nq),msn);
    
    data.res.modgradx(:,msn) = data.res.modgradx(:,msn) ...
        + data.res.dlfcn(1:nsd,msn);
    data.res.modgradq(:,msn) = data.res.modgradq(:,msn) ...
        + data.res.dlfcn(nsd+(1:nq),msn);
    
    data.res.refgradx(:,msn) = data.res.refgradx(:,msn) ...
        + data.res.dlfcn(1:nsd,msn);
    data.res.refgradq(:,msn) = data.res.refgradq(:,msn) ...
        + data.res.dlfcn(nsd+(1:nq),msn);
end

%%% Interior Nodes
for msn=2:ns

%   laggradx(:,msn) = dvars.lambda(:,msn-1) - data.res.lambdaGx(:,msn) ...
%       - dvars.mu_sd(:,msn);
%   laggradq(:,msn) = - data.res.lambdaGq(:,msn) - dvars.mu_q(:,msn);
  laggradx(:,msn) = dvars.lambda(:,msn-1) - data.mat.Gx{msn}'*dvars.lambda(:,msn) - dvars.mu_sd(:,msn);
laggradq(:,msn) = - data.mat.Gq{msn}'*dvars.lambda(:,msn) - dvars.mu_q(:,msn);

  if nrdi > 0
      laggradx(:,msn) = laggradx(:,msn) ...
          - data.res.muJrdix(:,msn-1);
      laggradq(:,msn) = laggradq(:,msn) ...
          - data.res.muJrdiq(:,msn-1);
  end
  
  if nlsqs > 0
      laggradx(:,msn) = laggradx(:,msn) ...
          + data.res.lsqigradx(:,msn-1);
      laggradq(:,msn) = laggradq(:,msn) ...
          + data.res.lsqigradq(:,msn-1);
  
      data.res.modgradx(:,msn) = data.res.modgradx(:,msn) ... 
          + data.res.lsqigradx(:,msn-1);
      data.res.modgradq(:,msn) = data.res.modgradq(:,msn) ... 
          + data.res.lsqigradq(:,msn-1);
  
      data.res.refgradx(:,msn) = data.res.refgradx(:,msn) ... 
          + data.res.lsqigradx(:,msn-1);
      data.res.refgradq(:,msn) = data.res.refgradq(:,msn) ... 
          + data.res.lsqigradq(:,msn-1);
  end

  if nlfcn > 0
    laggradx(:,msn) = laggradx(:,msn) ...
      + data.res.dlfcn(1:nsd,msn);
    laggradq(:,msn) = laggradq(:,msn) ...
      + data.res.dlfcn(nsd+(1:nq),msn);
    
    data.res.modgradx(:,msn) = data.res.modgradx(:,msn) ...
      + data.res.dlfcn(1:nsd,msn);
    data.res.modgradq(:,msn) = data.res.modgradq(:,msn) ...
      + data.res.dlfcn(nsd+(1:nq),msn);
    
    data.res.refgradx(:,msn) = data.res.refgradx(:,msn) ...
      + data.res.dlfcn(1:nsd,msn);
    data.res.refgradq(:,msn) = data.res.refgradq(:,msn) ...
      + data.res.dlfcn(nsd+(1:nq),msn);
  end
end

%%% End node (depends only on s, not on q)
msn = ns + 1;
data.res.modgradx(:,msn) = zeros(nsd,1);
data.res.refgradx(:,msn) = zeros(nsd,1);
  
% Lagrange gradient
laggradx(:,msn) = dvars.lambda(:,msn-1) ...
    - dvars.mu_sd(:,msn);

if nrde > 0
    laggradx(:,msn) = laggradx(:,msn) ...
        - data.res.muJrdex;
end

if nlsqe > 0
    laggradx(:,msn) = laggradx(:,msn) ...
        + data.res.lsqegradx;
    
    data.res.modgradx(:,msn) = data.res.modgradx(:,msn) ... 
        + data.res.lsqegradx;
    
    data.res.refgradx(:,msn) = data.res.refgradx(:,msn) ... 
        + data.res.lsqegradx;
end

if nmfcn > 0 && nlfcn <= 0
    laggradx(:,msn) = laggradx(:,msn) ...
        + data.res.mgrad; 

    data.res.modgradx(:,msn) = data.res.modgradx(:,msn) ... 
        + data.res.mgrad;
    
    data.res.refgradx(:,msn) = data.res.refgradx(:,msn) ... 
        + data.res.mgrad;
end

    
