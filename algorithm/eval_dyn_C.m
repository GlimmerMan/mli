function [outp] = eval_dyn_C(inp)
    
    global SLV OPT
    
    thoriz = inp.thoriz;
    sd = inp.sd;
    q = inp.q;
    p = inp.p;    
    lambda = inp.lambda;
    
    adj_dirs = lambda;
    nAdjTC = 1;
    
    if OPT.dims.lfcn > 0
      sd = SLV.opt.lagObjModSd'*sd;      
      
      lambda = SLV.opt.lagObjModLambda * lambda;
      lambda_lfcn = zeros(size(lambda));
      lambda_lfcn(OPT.dims.lfcnState) = SLV.opt.lagObjLambda;

      adj_dirs = [lambda, -lambda_lfcn];
      nAdjTC = 2;
    end
    
    int = SLV.opt.int;
    
    solvind('setTapeNumber', int, 1);
    solvind('setTimeHorizon',int, thoriz);
    solvind('setInitVals', int, [sd;q;p]);
    solvind('setForwardTaylorCoefficients', int, []);
    solvind('setAdjointTaylorCoefficients', int, nAdjTC, 0, 1, adj_dirs);
    
    

    status = solvind('evaluate', int);
    if status ~= 0 
        error('integration failed');
    end
    
    sol = solvind('getSolution', int);
    outp.x = SLV.opt.lagObjModSd*sol;
    if OPT.dims.lfcn > 0
      outp.lfcn = sol(OPT.dims.lfcnState);
    end
    
         
    help = solvind('getAdjSens', int);
    outp.lag = help(2:end,1);
    
    if OPT.dims.lfcn > 0
      outp.dlfcn = help(2:end,2);
      outp.dlfcn(OPT.dims.lfcnState) = [];
      outp.lag(OPT.dims.lfcnState) = [];
    end