% -*-matlab-*-
% Variables filled in OPT
%
% OPT.cond.b1
% OPT.cond.b2
% OPT.cond.b
% OPT.cond.bndlo1
% OPT.cond.bndup1
% OPT.cond.bndlo2
% OPT.cond.bndup2
% OPT.cond.c
%
% OPT.cond.C12
% OPT.cond.B11
% OPT.cond.B12
% OPT.cond.B22
% OPT.cond.B
%
% QP complete with OPT
% .cond.B        Hessian
% .cond.b        Gradient
% .cond.bndlo1
% .cond.bndup1
% .cond.C12      Inequalities
% .cond.bndlo2
% .cond.bndup2   Bounds
%
% Missing: Initial condition for Initial Value Embedding


function mli_condenseST_withblocks

global OPT SLV ST

S = ST.S;
phase = OPT.currentphase;
pvars = cell(S,1);

if (phase == 1)
  OPT.condtime = 0;
  return;
end

if phase == 0
    for jj = 1:S
        pvars{jj} = OPT.phase{4}{jj}.var.primal;
    end
else
    for jj = 1:S
        pvars{jj} = OPT.phase{phase}{jj}.var.primal;
    end
end

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nrd = nrds + (ns-1) * nrdi + nrde;

vidx = reshape(1:ns*nsd,nsd,ns);
vidx2 = reshape(nsd+(1:ns*nq),nq,ns);

for jj = 1:S

% regularize Hessian block
%     for ii = 1:ns
%         H_temp = [OPT.hess{jj}.ssblock{ii} OPT.hess{jj}.sqblock{ii}
%                  OPT.hess{jj}.sqblock{ii}' OPT.hess{jj}.qqblock{ii}];
%         [V,E] = eig(H_temp);
%         D = real(diag(E));
%         DD = max(D,1e-6*max(abs(D)));
%         H_conv = V*diag(DD)/V;
%         OPT.hess{jj}.ssblock{ii} = H_conv(1:nsd,1:nsd);
%         OPT.hess{jj}.sqblock{ii} = H_conv(1:nsd,nsd+1:end);
%         OPT.hess{jj}.qqblock{ii} = H_conv(nsd+1:end,nsd+1:end);
%     end

    tic
    condjj = OPT.cond{jj};
    evaljj = OPT.eval{jj};
    hessjj = OPT.hess{jj};
    
    condjj.bndlo1(:) = 0;
    condjj.bndup1(:) = 0;
    condjj.bndlo2(:) = 0;
    condjj.bndup2(:) = 0;
    condjj.c(:) = 0;
    condjj.b(:) = 0;
    if nrd > 0
        condjj.rdres(:) = 0;
    end
    
    % regularize Hessian block
    OPT.qp{jj}.nreg = 0;
    OPT.qp{jj}.maxreg = 0;
    if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
      for ii=1:ns
        stride = nsd+nq;
        idx = (ii-1)*stride;
        tempmatrix = lbfgshess_matrix(hessjj.lbfgshess{ii});
        condjj.H_full(idx+(1:stride),idx+(1:stride)) = tempmatrix;
        hessjj.ssblock{ii} = tempmatrix(1:nsd,1:nsd);
        hessjj.sqblock{ii} = tempmatrix(1:nsd,(nsd+1):stride);
        hessjj.qqblock{ii} = tempmatrix((nsd+1):stride,(nsd+1):stride);
      end
      tempmatrix = lbfgshess_matrix(hessjj.lbfgshess{ns+1});
      condjj.H_full(ns*(nsd+nq)+(1:nsd),ns*(nsd+nq)+(1:nsd)) = tempmatrix;
      hessjj.ssblock{ns+1} = tempmatrix;
%     else 
%       for ii=1:ns
%         stride = nsd+nq;
%         idx = (ii-1)*stride;
%         
%         H_temp = [hessjj.ssblock{ii} hessjj.sqblock{ii}
%           hessjj.sqblock{ii}' hessjj.qqblock{ii}];
%         [V,E] = eig(0.5*(H_temp+H_temp'));
%         D = diag(E);
%         ep = 1e-6*max(abs(D));
%         DD = max(D,ep);
%         tempmatrix = V*diag(DD)/V;
%         hessjj.ssblock{ii} = tempmatrix(1:nsd,1:nsd);
%         hessjj.sqblock{ii} = tempmatrix(1:nsd,(nsd+1):stride);
%         hessjj.qqblock{ii} = tempmatrix((nsd+1):stride,(nsd+1):stride);
%         OPT.qp{jj}.nreg = OPT.qp{jj}.nreg + sum(diag(E) < ep);
%         maxreg = max(DD-D);
%         OPT.qp{jj}.maxreg = max(maxreg,OPT.qp{jj}.maxreg);
%       end
%       H_temp = hessjj.ssblock{ns+1};
%       [V,E] = eig(0.5*(H_temp+H_temp'));
%       D = diag(E);
%       ep = 0e-6*max(abs(D));
%       DD = max(D,ep);
%       hessjj.ssblock{ns+1} = V*diag(DD)/V;
%       OPT.qp{jj}.nreg = OPT.qp{jj}.nreg + sum(diag(E) < ep);
%       maxreg = max(DD-D);
%       OPT.qp{jj}.maxreg = max(maxreg,OPT.qp{jj}.maxreg);
    end
    
    % step bounds
    l_s = reshape(SLV.opt.lagObjModSd*OPT.bounds.sdLoB,(ns+1)*nsd,1);
    u_s = reshape(SLV.opt.lagObjModSd*OPT.bounds.sdUpB,(ns+1)*nsd,1);
    l_q = reshape(OPT.bounds.qLoB,ns*nq,1);
    u_q = reshape(OPT.bounds.qUpB,ns*nq,1);
    s_h = cell(S,1);
    q_h = cell(S,1);
    a1 = cell(S,1);
    a2 = cell(S,1);
    
    
    s_h{jj} = reshape(pvars{jj}.sd,(ns+1)*nsd,1);
    q_h{jj} = reshape(pvars{jj}.q,ns*nq,1);
    
    a1{jj} = l_s - s_h{jj};
    a2{jj} = u_s - s_h{jj};
    
    condjj.bndlo1 = a1{jj}(nsd+1:end);
    condjj.bndup1 = a2{jj}(nsd+1:end);
    condjj.bndlo2 = l_q-q_h{jj};
    condjj.bndup2 = u_q-q_h{jj};
    
    
    % Linear transformation of equalities - Building blocks C12
    
    if (phase == 0) || (phase == 4)
        
        % First block column of C12 and C12'*B11
        condjj.cblocks.first{1} = evaljj.mat.Gx{1};
        condjj.cblocksb.first{1} = condjj.cblocks.first{1}' * ...
            hessjj.ssblock{2};
        
        for ii=2:ns
            condjj.cblocks.first{ii} = evaljj.mat.Gx{ii} * ...
                condjj.cblocks.first{ii-1};
            condjj.cblocksb.first{ii} = condjj.cblocks.first{ii}' * ...
                hessjj.ssblock{ii+1};
        end
        
        
        % Other block columns of C12 and C12'*B11
        for kk=1:ns
            condjj.cblocks.mixed{kk,kk} = evaljj.mat.Gq{kk};
            condjj.cblocksb.mixed{kk,kk} = ...
                evaljj.mat.Gq{kk}' * hessjj.ssblock{kk+1};
            
            for ii=(kk+1:ns)
                condjj.cblocks.mixed{ii,kk} = evaljj.mat.Gx{ii} * ...
                    condjj.cblocks.mixed{ii-1,kk};
                condjj.cblocksb.mixed{ii,kk} = ...
                    condjj.cblocks.mixed{ii,kk}' * hessjj.ssblock{ii+1};
            end
        end
        
    end
    
    
    % Linear transformation of equalities - right hand side
    condjj.c(1:nsd,1) = evaljj.res.c(:,1);
    
    for ii=2:ns
        condjj.c(vidx(:,ii),1) = evaljj.mat.Gx{ii} * ...
            condjj.c(vidx(:,ii-1),1) + evaljj.res.c(:,ii);
    end
    
    % Linear transformation of inequalities - right hand side
    if nrd > 0
        sidx = 0;
        if nrds > 0
            condjj.rdres(sidx+(1:nrds),1) = -evaljj.res.rds;
            sidx = sidx + nrds;
        end
        
        if nrdi > 0
            for ii=1:ns-1
                offs = sidx + (ii-1)*nrdi;
                condjj.rdres(offs+(1:nrdi),1) = ...
                    -(evaljj.res.rdi(:,ii) + evaljj.mat.Jrdix{ii} * ...
                    evaljj.res.c(:,ii));
            end
            %     sidx = sidx + (ns-1)*nrdi;
        end
        
        if nrde > 0
            condjj.rdres(offs+(1:nrde),1) = ...
                -(evaljj.res.rde + evaljj.mat.Jrdex * ...
                evaljj.res.c(:,ns));
        end
    end
    
    
    % Build Hessian
    if (phase == 0) || (phase == 4)
        
        % B = M'*B11*M + M'*B12 + (M'*B12)' + B22
        condjj.B(:) = 0;
        
        % B22 part of B
        condjj.B(1:nsd,1:nsd) = hessjj.ssblock{1};
        condjj.B(1:nsd,nsd+(1:nq)) = hessjj.sqblock{1};
        condjj.B(nsd+(1:nq),1:nsd) = hessjj.sqblock{1}';
        for ii=1:ns
            condjj.B(vidx2(:,ii),vidx2(:,ii)) = hessjj.qqblock{ii};
        end
        
        % s0,s0 part of M'*B11*M
        hmat = zeros(nsd,nsd);
        for ii=1:ns
            hmat = hmat + condjj.cblocksb.first{ii} * condjj.cblocks.first{ii};
        end
        condjj.B(1:nsd,1:nsd) = condjj.B(1:nsd,1:nsd) + hmat;
        
        % s0,q part of M'*B11*M
        hmat = zeros(nsd,nq);
        for ii=1:ns
            hmat(:) = 0;
            for kk=ii:ns
                hmat = hmat + condjj.cblocksb.first{kk} * ...
                    condjj.cblocks.mixed{kk,ii};
            end
            condjj.B(1:nsd, vidx2(:,ii)) = condjj.B(1:nsd, vidx2(:,ii)) + hmat;
            condjj.B(vidx2(:,ii), 1:nsd) = condjj.B(vidx2(:,ii), 1:nsd) + hmat';
        end
        
        % q,q part of M'*B11*M
        if 0
%         hmat = zeros(nq,nq);
        cblocksmixed = cell(1,ns);
        
        for ll = 1:ns
            cblocksmixed{ll} = cell2mat(condjj.cblocks.mixed(:,ll));
        end
        for ii=1:ns
            blsize = size(condjj.cblocksb.mixed{1,1});
            cblsize = [blsize(1),((ns-ii)+1)*blsize(2)];
            cblocksbmixed = reshape(cell2mat(condjj.cblocksb.mixed(:,ii))',cblsize); % ll:ns
   
            for ll=ii:ns
%                 cblocksmixed = cell2mat(condjj.cblocks.mixed(:,ll)); % ll:ns,ll
                hmat = cblocksbmixed(:,blsize(2)*(ll-ii)+1:end)*cblocksmixed{ll};
%                 hmat(:) = 0;                
%                 for kk=ll:ns
%                     hmat = hmat + condjj.cblocksb.mixed{kk,ii} * ...
%                         condjj.cblocks.mixed{kk,ll};
%                 end
                condjj.B(vidx2(:,ii),vidx2(:,ll)) = ...
                    condjj.B(vidx2(:,ii),vidx2(:,ll)) + hmat;
                if (ii ~= ll)
                    condjj.B(vidx2(:,ll),vidx2(:,ii)) = ...
                        condjj.B(vidx2(:,ll),vidx2(:,ii)) + hmat';
                end
            end
        end
        else
          % old q,q part of M'*B11*M
          hmat = zeros(nq,nq);
          for ii=1:ns
            for ll=ii:ns
              hmat(:) = 0;
              for kk=ll:ns
                hmat = hmat + condjj.cblocksb.mixed{kk,ii} * ...
                  condjj.cblocks.mixed{kk,ll};
              end
              condjj.B(vidx2(:,ii),vidx2(:,ll)) = ...
                condjj.B(vidx2(:,ii),vidx2(:,ll)) + hmat;
              if (ii ~= ll)
                condjj.B(vidx2(:,ll),vidx2(:,ii)) = ...
                  condjj.B(vidx2(:,ll),vidx2(:,ii)) + hmat';
              end
            end
          end
        end
        
        % M'*B12 and (M'*B12)' part
        for ii=1:ns-1
            hmat = condjj.cblocks.first{ii}'*hessjj.sqblock{ii+1};
            condjj.B(1:nsd,vidx2(:,ii+1)) = ...
                condjj.B(1:nsd,vidx2(:,ii+1)) + hmat;
            condjj.B(vidx2(:,ii+1),1:nsd) = ...
                condjj.B(vidx2(:,ii+1),1:nsd) + hmat';
        end
        
        for ii=1:ns-1
            for ll=ii:ns-1
                hmat = condjj.cblocks.mixed{ll,ii}'*hessjj.sqblock{ll+1};
                condjj.B(vidx2(:,ii),vidx2(:,ll+1)) = ...
                    condjj.B(vidx2(:,ii),vidx2(:,ll+1)) + hmat;
                condjj.B(vidx2(:,ll+1),vidx2(:,ii)) = ...
                    condjj.B(vidx2(:,ll+1),vidx2(:,ii)) + hmat';
            end
        end
    end
    
    % Build condensed gradient
    
    % b2 part
    condjj.b(1:nsd,1) = evaljj.res.modgradx(:,1);
    for ii=1:ns
        condjj.b(vidx2(:,ii),1) = evaljj.res.modgradq(:,ii);
    end
    
    % C12' * b1 part +  C12' * B11 * c part
    for ii=1:ns
        condjj.b(1:nsd,1) = condjj.b(1:nsd,1) + ...
            condjj.cblocks.first{ii}' * evaljj.res.modgradx(:,ii+1) + ...
            condjj.cblocksb.first{ii} * condjj.c(vidx(:,ii));
    end
    
    for ll=1:ns
        for ii=ll:ns
            condjj.b(vidx2(:,ll),1) = condjj.b(vidx2(:,ll),1) + ...
                condjj.cblocks.mixed{ii,ll}' * evaljj.res.modgradx(:,ii+1) + ...
                condjj.cblocksb.mixed{ii,ll} * condjj.c(vidx(:,ii));
        end
    end
    
    % B12' * c part
    for ii=1:ns-1
        condjj.b(vidx2(:,ii+1),1) = condjj.b(vidx2(:,ii+1),1) + ...
            hessjj.sqblock{ii+1}' * condjj.c(vidx(:,ii));
    end
    
    
    % Condensing matrix C12 (only for QP, not used in Condensing)
    if (phase == 0) || (phase == 4)
        
        condjj.C12 = zeros(ns*nsd,nsd+ns*nq);
        for ii=1:ns
            condjj.C12(vidx(:,ii),1:nsd) = condjj.cblocks.first{ii};
        end
        
        for ii=1:ns
            for ll=1:ii
                condjj.C12(vidx(:,ii),vidx2(:,ll)) = ...
                    condjj.cblocks.mixed{ii,ll};
            end
        end
        
    end
    
    % Constraint matrix for QP
    if (phase == 0) || (phase == 4)
        
        if nrd > 0
            condjj.rdmat = zeros(nrd, nsd+ns*nq);
            sidx = 0;
            
            if nrds > 0
                condjj.rdmat(sidx+(1:nrds),1:nsd) = evaljj.mat.Jrdsx;
                condjj.rdmat(sidx+(1:nrds),nsd+(1:nq)) = evaljj.mat.Jrdsq;
                sidx = sidx + nrds;
            end
            
            if nrdi > 0
                for ii=1:ns-1
                    xoffs=sidx+(ii-1)*nrdi;
                    condjj.rdmat(xoffs+(1:nrdi),1:nsd) = ...
                        evaljj.mat.Jrdix{ii} * condjj.cblocks.first{ii};
                    for ll=1:ii
                        yoffs = nsd+(ll-1)*nq;
                        condjj.rdmat(xoffs+(1:nrdi),yoffs+(1:nq)) = ...
                            evaljj.mat.Jrdix{ii} * condjj.cblocks.mixed{ii,ll};
                    end
                    condjj.rdmat(xoffs+(1:nrdi),nsd+ii*nq+(1:nq)) = ...
                        evaljj.mat.Jrdiq{ii};
                end
                sidx = sidx + (ns-1) * nrdi;
            end
            
            if nrde > 0
                condjj.rdmat(sidx+(1:nrde),1:nsd) = ...
                    evaljj.mat.Jrdex * condjj.cblocks.first{ns};
                for ll=1:ns
                    yoffs = nsd+(ll-1)*nq;
                    condjj.rdmat(sidx+(1:nrde),yoffs+(1:nq)) = ...
                        evaljj.mat.Jrdex * condjj.cblocks.mixed{ns,ll};
                end
            end
        end
        
    end
    
    
    % Bounds
    % OPT.condbndlo1 <= C12 * Deltaw2 <= OPT.condbndup1
    % OPT.condbndlo2 <= Deltaw2 <= OPT.condbndup2
    
    condjj.bndlo1 = condjj.bndlo1 - condjj.c;
    condjj.bndup1 = condjj.bndup1 - condjj.c;
    OPT.cond{jj} = condjj;
    condjj = 0;
    OPT.condtime{jj} = toc;
end

