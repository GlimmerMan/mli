% -*-matlab-*-
function mli_preeval

global SCEN OPT ST

% step must be computed before

if SCEN.use_st == 0
  switch OPT.currentphase
    case {0, 4, 9}
      OPT.phase{4}.dataold = OPT.phase{4}.data;
      
    case 2
      OPT.phase{2}.dataold = OPT.phase{2}.data;
      
    case 3
      OPT.phase{3}.dataold = OPT.phase{3}.data;
      
    case {1}
      return;
  end
else % ST case
  S = ST.S;
  switch OPT.currentphase
    case {0, 4, 9}
      for jj = 1:S
        OPT.phase{4}{jj}.dataold = OPT.phase{4}{jj}.data;
      end
    case 2
      for jj = 1:S
        OPT.phase{2}{jj}.dataold = OPT.phase{2}{jj}.data;
      end
    case 3
      for jj = 1:S
        OPT.phase{3}{jj}.dataold = OPT.phase{3}{jj}.data;
      end
    case {1}
      return;
  end
end
  
mli_evaluate;
