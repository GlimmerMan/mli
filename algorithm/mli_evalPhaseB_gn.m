% -*-matlab-*-
function [data] = mli_evalPhaseB_gn(vars,ref)

global OPT

tic

ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd; 
nq = OPT.dims.nq; 
np = OPT.dims.np;
nlsqs = OPT.dims.lsqfcn_s;
nlsqi = OPT.dims.lsqfcn_i; 
nlsqe = OPT.dims.lsqfcn_e;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;

pvars = vars.primal;

sddiff = pvars.sd - ref.primal.sd;
qdiff = pvars.q - ref.primal.q;

%%% Start node
msn = 1;

% dynamical equations
inp = [];
inp.thoriz = [OPT.discr.msgrid(msn) OPT.discr.msgrid(msn+1)];
inp.sd = pvars.sd(:,msn);
inp.q = pvars.q(:,msn);
inp.p = pvars.pconst;

outp = eval_dyn_B(inp);

data.res.xd(:,msn) = outp.x;
data.res.c(:,msn) = outp.x - pvars.sd(:,msn+1);

% least-squares objective
if nlsqs > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn);
  inp.p = pvars.pconst;

  outp = eval_lsqfcn_B(inp,'s');

  data.res.lsqfcn_s = outp.lsqfcn;
end

% decoupled node constraint
if nrds > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn);
  inp.p = pvars.pconst;

  outp = eval_rdfcn_B(inp,'s');
  
  data.res.rds = outp.rdfcn;
end

data.res.modgradx(:,msn) = zeros(nsd,1);
data.res.modgradq(:,msn) = zeros(nq,1);

if nlsqs > 0
  data.res.modgradx(:,msn) = data.res.modgradx(:,msn) + OPT.eval.mat.Jlsqsx' * data.res.lsqfcn_s;  
  data.res.modgradq(:,msn) = data.res.modgradq(:,msn) + OPT.eval.mat.Jlsqsq' * data.res.lsqfcn_s;  
end

%%% Interior nodes
for msn=2:ns
% dynamical equations
  inp = [];
  inp.thoriz = [OPT.discr.msgrid(msn) OPT.discr.msgrid(msn+1)];
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn);
  inp.p = pvars.pconst;
  
  outp = eval_dyn_B(inp);
  
  data.res.xd(:,msn) = outp.x;
  data.res.c(:,msn) = outp.x - pvars.sd(:,msn+1);
  
% least-squares objective
  if nlsqi > 0
    inp = [];
    inp.t = OPT.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.q = pvars.q(:,msn);
    inp.p = pvars.pconst;
    
    outp = eval_lsqfcn_B(inp,'i');
    
    data.res.lsqfcn_i(:,msn-1) = outp.lsqfcn;
  end

% decoupled node constraint
  if nrdi > 0
    inp = [];
    inp.t = OPT.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.q = pvars.q(:,msn);
    inp.p = pvars.pconst;
    
    outp = eval_rdfcn_B(inp,'i');
    
    data.res.rdi(:,msn-1) = outp.rdfcn;
  end

  data.res.modgradx(:,msn) = zeros(nsd,1);
  data.res.modgradq(:,msn) = zeros(nq,1);
  
  if nlsqs > 0
    data.res.modgradx(:,msn) = data.res.modgradx(:,msn) ...
        + OPT.eval.mat.Jlsqix{msn-1}' * data.res.lsqfcn_i(:,msn-1);  
    data.res.modgradq(:,msn) = data.res.modgradq(:,msn) ...
        + OPT.eval.mat.Jlsqiq{msn-1}' * data.res.lsqfcn_i(:,msn-1);  
  end
  
end

%%% End node
msn = ns + 1;

% least-squares objective
if nlsqe > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn-1);
  inp.p = pvars.pconst;
  
  outp = eval_lsqfcn_B(inp,'e');
  
  data.res.lsqfcn_e = outp.lsqfcn;
end

% decoupled node constraint
if nrde > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn-1);
  inp.p = pvars.pconst;
  
  outp = eval_rdfcn_B(inp,'e');
  
  data.res.rde = outp.rdfcn;
end
 
data.res.modgradx(:,msn) = zeros(nsd,1);

if nlsqs > 0
  data.res.modgradx(:,msn) = data.res.modgradx(:,msn) + OPT.eval.mat.Jlsqex' *  data.res.lsqfcn_e;  
end


OPT.evaltime = toc;



