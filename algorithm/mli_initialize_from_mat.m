function mli_initialize_from_mat(matfilename)

global OPT SCEN ST;

if ~exist(matfilename, 'file')
  error(['The specified file ' matfilename ' does not exist.'])
end

load(matfilename)
mli_initialize_solvind;
SCEN.start = SCEN.currentsample + 1;

if SCEN.use_st == 0
  [QP, x, fval, exitflag, iter, lambda] = qpOASES_sequence( 'i',OPT.qp.H, ...
    OPT.qp.g, OPT.qp.A, ...
    OPT.qp.lb, OPT.qp.ub, ...
    OPT.qp.ineqlb, ...
    OPT.qp.inequb);
  OPT.qp.qphandle = QP;
else
  S = ST.S; 
  for jj = 1:S   
    [QP{jj}, x{jj}, fval{jj}, exitflag{jj}, iter{jj}, lambda{jj}] = qpOASES_sequence( 'i',OPT.qp{jj}.H, ...
      OPT.qp{jj}.g, OPT.qp{jj}.A, ...
      OPT.qp{jj}.lb, OPT.qp{jj}.ub, ...
      OPT.qp{jj}.ineqlb, ...
      OPT.qp{jj}.inequb);
    OPT.qp{jj}.qphandle = QP{jj}; 
  end
end

msg = ['Specify the options that you want to be different from the file ', matfilename, ', then hit F5.'];
fprintf(msg);
keyboard

% These could be options one would like to specify:
%
% OPT.phasehistory(:) = 4;
% OPT.iterhistory(:) = 1;
% OPT.phasehistory([1,5,16]) = 9; % solve with ipopt
% OPT.use_condensing = 1;
% OPT.lbfgshess = 0;
% etc.
end
