% -*-matlab-*-
function mli_blowup_nocond

global SCEN OPT ST

phase = OPT.currentphase;
if (phase == 0)
    phase = 4;
end

if (phase == 1)
    OPT.blowuptime = 0;
    return;
end

tic

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nrd = nrds + (ns-1) * nrdi + nrde;

if SCEN.use_st == 0
    % Variable step
    for ii=1:ns
        idx = (ii-1)*(nsd+nq);
        OPT.step.primal.sd(:,ii) = OPT.qp.solstep(idx+(1:nsd),1);
        OPT.step.primal.q(:,ii) = OPT.qp.solstep(idx+nsd+(1:nq),1);
    end
    OPT.step.primal.sd(:,ns+1) = OPT.qp.solstep(ns*(nsd+nq)+(1:nsd),1);  
    
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
      % delq
      for ii = 1:ns-1+1
        idx = (ii-1)*nq;
        OPT.step.primal.delq(:,ii) = OPT.qp.soldelq(idx+(1:nq));
      end
      % New bound multipliers
      OPT.step.dual.mu_sd(:,1) = OPT.qp.soleqmult(1:nsd);
      for ii=1:ns
        idx = (ii-1)*(nsd+nq);
        OPT.step.dual.mu_q(:,ii) = OPT.qp.solbndmult(idx+(1:nq),1);
        OPT.step.dual.mu_sd(:,ii+1) = OPT.qp.solbndmult(idx+nq+(1:nsd),1);
      end
      % New matching multipliers
      OPT.step.dual.lambda = reshape(OPT.qp.soleqmult(nsd+1:end), nsd, ...
        ns);
      % New delq multipliers
      for ii=1:ns
        idx = (ii-1)*nq;
        OPT.step.dual.mu_delq(:,ii) = OPT.qp.solbndmult2(idx+(1:nq),1);
      end
      % New CMR multipliers
      OPT.step.dual.lambda1 = reshape(OPT.qp.solCMRmult(1:ns*nq), nq, ns);
      OPT.step.dual.lambda2 = reshape(OPT.qp.solCMRmult(ns*nq+(1:ns*nq)), nq, ns);
    else
      % New bound multipliers
      OPT.step.dual.mu_sd(:,1) = OPT.qp.soleqmult(1:nsd);
      for ii=1:ns
        idx = (ii-1)*(nsd+nq);
        OPT.step.dual.mu_q(:,ii) = OPT.qp.solbndmult(idx+(1:nq),1);
        OPT.step.dual.mu_sd(:,ii+1) = OPT.qp.solbndmult(idx+nq+(1:nsd),1);
      end
      
      % New matching multipliers
      OPT.step.dual.lambda = reshape(OPT.qp.soleqmult(nsd+1:end), nsd, ...
        ns);
    end
    % New IPC multipliers
    if nrd > 0
        sidx = 0;
        if nrds > 0
            OPT.step.dual.mu_rds = OPT.qp.solineqmult(sidx+(1:nrds));
            sidx = sidx + nrds;
        end
        if nrdi > 0
            OPT.step.dual.mu_rdi = reshape(OPT.qp.solineqmult(sidx+(1:(ns-1)*nrdi)), nrdi, ns-1);
            sidx = sidx + (ns-1)*nrdi;
        end
        if nrde > 0
            OPT.step.dual.mu_rde = OPT.qp.solineqmult(sidx+(1:nrde));
        end
    end
    
    % Calculate actual steps in multipliers (new mult - old mult)
    OPT.step.dualstep.lambda = OPT.step.dual.lambda - OPT.phase{phase}.var.dual.lambda;
    OPT.step.dualstep.mu_sd = OPT.step.dual.mu_sd - OPT.phase{phase}.var.dual.mu_sd;
    OPT.step.dualstep.mu_q = OPT.step.dual.mu_q - OPT.phase{phase}.var.dual.mu_q;
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
    OPT.step.dualstep.mu_delq = OPT.step.dual.mu_delq - OPT.phase{phase}.var.dual.mu_delq;
    OPT.step.dualstep.lambda1 = OPT.step.dual.lambda1 - OPT.phase{phase}.var.dual.lambda1;
    OPT.step.dualstep.lambda2 = OPT.step.dual.lambda2 - OPT.phase{phase}.var.dual.lambda2;
    end
    if nrds > 0
        OPT.step.dualstep.mu_rds = OPT.step.dual.mu_rds - OPT.phase{phase}.var.dual.mu_rds;
    end
    if nrdi > 0
        OPT.step.dualstep.mu_rdi = OPT.step.dual.mu_rdi - OPT.phase{phase}.var.dual.mu_rdi;
    end
    if nrde > 0
        OPT.step.dualstep.mu_rde = OPT.step.dual.mu_rde - OPT.phase{phase}.var.dual.mu_rde;
    end
    
    % Stepnorm
    pstep = [OPT.step.primal.sd(:);OPT.step.primal.q(:)];
    
    dstep = [OPT.step.dualstep.lambda(:);OPT.step.dualstep.mu_sd(:);OPT.step.dualstep.mu_q(:)];
    if nrds > 0
        dstep = [dstep;OPT.step.dualstep.mu_rds(:)];
    end
    if nrdi > 0
        dstep = [dstep;OPT.step.dualstep.mu_rdi(:)];
    end
    if nrde > 0
        dstep = [dstep;OPT.step.dualstep.mu_rde(:)];
    end
    
    OPT.step.stepnorm = norm([pstep;dstep]);
    
else %% ST
    
    S = ST.S;
    % Variable step
    for ii=1:ns
        idx = (ii-1)*(nsd+nq);
        for jj = 1:S
            OPT.step{jj}.primal.sd(:,ii) = OPT.qp{jj}.solstep(idx+(1:nsd),1);
            OPT.step{jj}.primal.q(:,ii) = OPT.qp{jj}.solstep(idx+nsd+(1:nq),1);
        end
    end
    for jj = 1:S
        OPT.step{jj}.primal.sd(:,ns+1) = OPT.qp{jj}.solstep(ns*(nsd+nq)+(1:nsd),1);
    end

    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
      % delq
      for jj = 1:S
        for ii = 1:ns-1+1
          idx = (ii-1)*nq;
          OPT.step{jj}.primal.delq(:,ii) = OPT.qp{jj}.soldelq(idx+(1:nq));
        end
        
        % New bound multipliers
        OPT.step{jj}.dual.mu_sd(:,1) = OPT.qp{jj}.soleqmult(1:nsd);
        for ii=1:ns
          idx = (ii-1)*(nsd+nq);
          OPT.step{jj}.dual.mu_q(:,ii) = OPT.qp{jj}.solbndmult(idx+(1:nq),1);
          OPT.step{jj}.dual.mu_sd(:,ii+1) = OPT.qp{jj}.solbndmult(idx+nq+(1:nsd),1);
        end
        % New matching multipliers
        OPT.step{jj}.dual.lambda = reshape(OPT.qp{jj}.soleqmult(nsd+1:end), nsd, ...
          ns);
        % New delq multipliers
        for ii=1:ns
          idx = (ii-1)*nq;
          OPT.step{jj}.dual.mu_delq(:,ii) = OPT.qp{jj}.solbndmult2(idx+(1:nq),1);
        end
        % New CMR multipliers
        OPT.step{jj}.dual.lambda1 = reshape(OPT.qp{jj}.solCMRmult(1:ns*nq), nq, ns);
        OPT.step{jj}.dual.lambda2 = reshape(OPT.qp{jj}.solCMRmult(ns*nq+(1:ns*nq)), nq, ns);
      end
    else
      % New bound multipliers
      for jj = 1:S
        OPT.step{jj}.dual.mu_sd(:,1) = OPT.qp{jj}.soleqmult(1:nsd);
        for ii=1:ns
          idx = (ii-1)*(nsd+nq);
          OPT.step{jj}.dual.mu_q(:,ii) = OPT.qp{jj}.solbndmult(idx+(1:nq),1);
          OPT.step{jj}.dual.mu_sd(:,ii+1) = OPT.qp{jj}.solbndmult(idx+nq+(1:nsd),1);
        end
        
        % New matching multipliers
        OPT.step{jj}.dual.lambda = reshape(OPT.qp{jj}.soleqmult(nsd+1:end), nsd, ...
          ns);
      end
    end
    % New IPC multipliers
    if nrd > 0
        sidx = 0;
        if nrds > 0
            for jj = 1:S
                OPT.step{jj}.dual.mu_rds = OPT.qp{jj}.solineqmult(sidx+(1:nrds));
                sidx = sidx + nrds;
            end
        end
        if nrdi > 0
            for jj = 1:S
                OPT.step{jj}.dual.mu_rdi = reshape(OPT.qp{jj}.solineqmult(sidx+(1:(ns-1)*nrdi)), nrdi, ns-1);
                sidx = sidx + (ns-1)*nrdi;
            end
        end
        if nrde > 0
            for jj = 1:S
                OPT.step{jj}.dual.mu_rde = OPT.qp{jj}.solineqmult(sidx+(1:nrde));
            end
        end
    end
    
    % Calculate actual steps in multipliers (new mult - old mult)
    for jj = 1:S
        OPT.step{jj}.dualstep.lambda = OPT.step{jj}.dual.lambda - OPT.phase{phase}{jj}.var.dual.lambda;
        OPT.step{jj}.dualstep.mu_sd = OPT.step{jj}.dual.mu_sd - OPT.phase{phase}{jj}.var.dual.mu_sd;
        OPT.step{jj}.dualstep.mu_q = OPT.step{jj}.dual.mu_q - OPT.phase{phase}{jj}.var.dual.mu_q;
        if nrds > 0
            OPT.step{jj}.dualstep.mu_rds = OPT.step{jj}.dual.mu_rds - OPT.phase{phase}{jj}.var.dual.mu_rds;
        end
        if nrdi > 0
            OPT.step{jj}.dualstep.mu_rdi = OPT.step{jj}.dual.mu_rdi - OPT.phase{phase}{jj}.var.dual.mu_rdi;
        end
        if nrde > 0
            OPT.step{jj}.dualstep.mu_rde = OPT.step{jj}.dual.mu_rde - OPT.phase{phase}{jj}.var.dual.mu_rde;
        end
    end
    
    % Stepnorm
    pstep = cell(1,S);
    dstep = cell(1,S);
    
    for jj = 1:S
        pstep{jj} = [OPT.step{jj}.primal.sd(:);OPT.step{jj}.primal.q(:)];
        
        dstep{jj} = [OPT.step{jj}.dualstep.lambda(:);OPT.step{jj}.dualstep.mu_sd(:);OPT.step{jj}.dualstep.mu_q(:)];
        if nrds > 0
            dstep{jj} = [dstep{jj};OPT.step{jj}.dualstep.mu_rds(:)];
        end
        if nrdi > 0
            dstep{jj} = [dstep{jj};OPT.step{jj}.dualstep.mu_rdi(:)];
        end
        if nrde > 0
            dstep{jj} = [dstep{jj};OPT.step{jj}.dualstep.mu_rde(:)];
        end
        
        OPT.step{jj}.stepnorm = norm([pstep{jj};dstep{jj}]);
    end
end

OPT.blowuptime = toc;
OPT.ipopttime = 0;

