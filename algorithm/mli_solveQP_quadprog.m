% -*-matlab-*-
% Variables filled in OPT
%
% OPT.deltas0
% OPT.qpHess
% OPT.qpGrad
% OPT.qpIneqLB
% OPT.qpIneqUB
% OPT.qpLB
% OPT.qpUB
% OPT.qpSolStep
% OPT.qpSolEqMult
% OPT.qpSolIneqMult
% OPT.qpSolBndMult
% OPT.feedbackdelta

function mli_solveQP_quadprog

global OPT;

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nrd = nrds + (ns-1) * nrdi + nrde;

tic

phase = OPT.currentphase;

switch (phase)
  case 1
    OPT.deltas0 = OPT.x0(:) - OPT.phase{1}.ref.primal.sd(:,1);
  case {0,4}
    OPT.deltas0 = OPT.x0(:) - OPT.phase{4}.var.primal.sd(:,1);
  case 2
    OPT.deltas0 = OPT.x0(:) - OPT.phase{2}.var.primal.sd(:,1);
  case 3
    OPT.deltas0 = OPT.x0(:) - OPT.phase{3}.var.primal.sd(:,1);
end

OPT.qp.g = OPT.cond.b;
if nrd > 0
  bigbnd = 1e12*ones(size(OPT.cond.rdres));
  OPT.qp.ineqlb = [OPT.cond.bndlo1;OPT.cond.rdres];
  OPT.qp.inequb = [OPT.cond.bndup1;bigbnd];
else
  OPT.qp.ineqlb = OPT.cond.bndlo1;
  OPT.qp.inequb = OPT.cond.bndup1;
end

OPT.qp.lb = [OPT.deltas0;OPT.cond.bndlo2];
OPT.qp.ub = [OPT.deltas0;OPT.cond.bndup2];

OPT.qp.H = 0.5*(OPT.cond.B+OPT.cond.B');
if nrd > 0
    OPT.qp.A = [OPT.cond.C12;OPT.cond.rdmat];
else
    OPT.qp.A = OPT.cond.C12;
end
A = [OPT.qp.A;-OPT.qp.A];
b = [OPT.qp.inequb;-OPT.qp.ineqlb];

opts = optimset('Algorithm', 'active-set');
opts = optimset(opts, 'Diagnostics', 'off');
opts = optimset(opts, 'Display', 'off');
opts = optimset(opts, 'MaxIter', 1e4);

[x, obj, status, output, y] = quadprog(OPT.qp.H, OPT.qp.g, A, ...
                                       b, [], [], OPT.qp.lb, ...
                                       OPT.qp.ub, [], opts);

if (status == 0)
    [x, obj, status, output, y] = quadprog(OPT.qp.H, OPT.qp.g, A, ...
                                       b, [], [], OPT.qp.lb, ...
                                       OPT.qp.ub, x, opts);
end

if (status < 0)
  status
  output
  error('QP solution failed!');
end


% Feedback law not implemented

if (phase ~= 1)
    OPT.qp.solstep = x; 
    % Constraint multipliers
    y.ineqlin = reshape(y.ineqlin, length(y.ineqlin)/2, 2);
    yineq = y.ineqlin(:,1) - y.ineqlin(:,2);
    ybnd = y.lower - y.upper;

    OPT.qp.soleqmult = ybnd(1:nsd);
    OPT.qp.solbndmult = ybnd(nsd+1:end);
    OPT.qp.solineqmult = yineq;
end


% Feedback update
OPT.feedbackdelta = x(nsd+(1:nq));
OPT.qptime = toc;

