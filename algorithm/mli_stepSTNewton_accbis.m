function alpha = mli_stepSTNewton_accbis

global OPT ST
% alpha = 1;
% return 

% accelerated bisection

alpha_min = 1e-4;
alpha_max = 1;
scal = 0.01;

it_max = 6;
it = 0;
df_alpha = 1;
eps_ls = 1e-3;
n_scen = ST.S;
fval = ST.obj;
z = cell(n_scen,1);

CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;

if OPT.use_condensing == 1
    for j = 1:n_scen
        mli_solvescenQP_cond(j,ST.lambda + alpha_max*ST.deltalam,'h');
    end
else
    for j = 1:n_scen
        mli_solvescenQP_nocond(j,ST.lambda + alpha_max*ST.deltalam,'h');
    end
end
fval_cand = ST.objj*ST.weights';

while fval_cand < fval && alpha_max > alpha_min
    alpha_max = scal*alpha_max;
    ST.Newton.G = zeros(size(ST.lambda));
    if OPT.use_condensing == 1
        for j = 1:n_scen
            mli_solvescenQP_cond(j,ST.lambda + alpha_max*ST.deltalam,'h');
            if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
              z{j} = [OPT.qp{j}.solstep; OPT.qp{j}.soldelq];
            else
              z{j} = OPT.qp{j}.solstep;
            end
            ST.Newton.G = ST.Newton.G + (ST.Cc{j}-ST.Ec{j})*z{j};
        end
    else
        for j = 1:n_scen
            mli_solvescenQP_nocond(j,ST.lambda + alpha_max*ST.deltalam,'h');
            if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
              z{j} = [OPT.qp{j}.solstep; OPT.qp{j}.soldelq];
            else
              z{j} = OPT.qp{j}.solstep;
            end
            ST.Newton.G = ST.Newton.G + (ST.C{j}-ST.E{j})*z{j};
        end
    end
    fval_cand = ST.objj*ST.weights';
end

alpha_max = min(alpha_max/scal,2-alpha_min);

while it < it_max
    
    it = it+1;
    alpha = (alpha_max+alpha_min)/2;
    ST.Newton.G = zeros(size(ST.lambda));
    if OPT.use_condensing == 1
        for j = 1:n_scen
            mli_solvescenQP_cond(j,ST.lambda + alpha*ST.deltalam,'h');
            if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
              z{j} = [OPT.qp{j}.solstep; OPT.qp{j}.soldelq];
            else
              z{j} = OPT.qp{j}.solstep;
            end
            ST.Newton.G = ST.Newton.G + (ST.Cc{j}-ST.Ec{j})*z{j};
        end
    else
        for j = 1:n_scen
            mli_solvescenQP_nocond(j,ST.lambda + alpha_max*ST.deltalam,'h');
            if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
              z{j} = [OPT.qp{j}.solstep; OPT.qp{j}.soldelq];
            else
              z{j} = OPT.qp{j}.solstep;
            end
            ST.Newton.G = ST.Newton.G + (ST.C{j}-ST.E{j})*z{j};
        end
    end
    df_alpha = ST.deltalam'*ST.Newton.G;
    if abs(df_alpha) <= eps_ls
        return
    else
        if df_alpha < 0
            alpha_max = alpha;
        else
            alpha_min = alpha;
            alpha_max = min(alpha_max,1);
            
        end
    end
end