% -*-matlab-*-
function [var] = init_vars

global OPT SLV

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;

if isfield(OPT.ini, 'loadpath')
    % initialize from loaded data
    % assumes full data
    load(OPT.ini.loadpath);
    var.primal.sd = svars.primal.sd;
    var.primal.q = svars.primal.q;
    var.primal.pconst = svars.primal.pconst;
    var.dual.lambda = svars.dual.lambda;
    var.dual.mu_sd = svars.dual.mu_sd;
    var.dual.mu_q = svars.dual.mu_q;
    if nrds > 0
        var.dual.mu_rds = svars.dual.mu_rds;
    end
    if nrdi > 0
        var.dual.mu_rdi = svars.dual.mu_rdi;
    end
    if nrde > 0
        var.dual.mu_rde = svars.dual.mu_rde;
    end
else
    % Initialize from specified data
    var.primal.q = OPT.ini.q;
    var.primal.pconst = OPT.ini.pconst;
    
    if ~isfield(OPT.ini,'lambda')
        var.dual.lambda = zeros(nsd, ns);
    else
        var.dual.lambda = OPT.ini.lambda;
    end
    
    if isfield(OPT,'periodicity')
        if ~isfield(OPT.ini,'periodicity')
            var.dual.periodicity=zeros(OPT.dims.periodicity,1);
        else
            var.dual.periodicity=OPT.ini.periodicity;
        end
    end
    
    if nrds > 0
        if ~isfield(OPT.ini,'mu_rds')
            var.dual.mu_rds = zeros(nrds, 1);
        else
            var.dual.mu_rds = OPT.ini.mu_rds;
        end
    end
    
    if nrdi > 0
        if ~isfield(OPT.ini,'mu_rdi')
            var.dual.mu_rdi = zeros(nrdi, ns-1);
        else
            var.dual.mu_rdi = OPT.ini.mu_rdi;
        end
    end
    
    if nrde > 0
        if ~isfield(OPT.ini,'mu_rde')
            var.dual.mu_rde = zeros(nrde, 1);
        else
            var.dual.mu_rde = OPT.ini.mu_rde;
        end
    end
    
    if ~isfield(OPT.ini,'mu_sd')
        var.dual.mu_sd = zeros(nsd, ns+1);
    else
        var.dual.mu_sd = OPT.ini.mu_sd;
    end
    
    if ~isfield(OPT.ini,'mu_q')
        var.dual.mu_q = zeros(nq, ns);
    else
        var.dual.mu_q = OPT.ini.mu_q;
    end
    
    lagObjModSd = SLV.opt.lagObjModSd;
    
    if OPT.ini.init_by_int == 0
        var.primal.sd = lagObjModSd*OPT.ini.sd;
    else
        var.primal.sd(:,1) = OPT.ini.sd(:,1);
        for ii=1:ns
            int = SLV.opt.int;
            solvind('setTimeHorizon', int, [OPT.discr.msgrid(ii)...
                OPT.discr.msgrid(ii+1)]);
            initvals = [var.primal.sd(:,ii)
                var.primal.q(:,ii)
                var.primal.pconst];
            solvind('setInitVals', int, initvals);
            
            status = solvind('evaluate', int);
            if status ~= 0
                error('init_vars: Integration for state initialization failed.');
            end
            
            sol = solvind('getSolution', int);
            solsd = sol;
            
            bnddiff = (OPT.bounds.sdUpB(:,ii+1)-solsd < 0);
            solsd(bnddiff) = OPT.bounds.sdUpB(bnddiff, ii+1);
            bnddiff = (solsd-OPT.bounds.sdLoB(:,ii+1) < 0);
            solsd(bnddiff) = OPT.bounds.sdLoB(bnddiff, ii+1);
            var.primal.sd(:,ii+1) = solsd;
        end
        
        var.primal.sd = lagObjModSd * var.primal.sd;
    end
    if isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1
      var.primal.delq = zeros(nq,ns-1+1);
      for ii = 1:ns-1
        var.primal.delq(:,ii) = var.primal.q(:,ii+1) - var.primal.q(:,ii);
      end
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        var.dual.mu_delq = zeros(nq, ns);
        var.dual.lambda1 = zeros(nq, ns);
        var.dual.lambda2 = zeros(nq, ns);
      end
    end
end