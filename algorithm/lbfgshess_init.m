function [Hess] =  lbfgshess_init(n, maxl, gamma)
% [Hess] =  LBFGSHESS_INIT(n, maxl, gamma)  initialize limited memory BFGS
% Hessian of dimension n with maxl memory slots and initial approximation
% gamma*eye(n) and return Hessian structure Hess.
% 

% Authors: Andreas Potschka, Leonard Wirsching
% Date: Dec 3, 2009

Hess.gamma = gamma;

Hess.s = zeros(n, maxl);
Hess.y = zeros(n, maxl);

Hess.actcol = 1;
Hess.l = 0;

% threshold for s'*y to guarantee positive definiteness
Hess.posdef_thres = 1e-10;
Hess.nskip = 0;

