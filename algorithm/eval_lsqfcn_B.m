function [outp] = eval_lsqfcn_B(inp, nodetype)
    
    global SLV
       
    args.t = inp.t;    
    args.xd = inp.sd;
    args.qph = [inp.q;inp.p];    

    evl = SLV.opt.evl;
    
    switch nodetype
      case 's'
	args.rhs = solvind('evaluateFcn', evl, 'lsqfcn_s', args);
	
      case 'i'
	args.rhs = solvind('evaluateFcn', evl, 'lsqfcn_i', args);
	
      case 'e'
	args.rhs = solvind('evaluateFcn', evl, 'lsqfcn_e', args);
	
      otherwise
	error('eval_lsqfcn_B: Invalid nodetype');
	
    end

    outp.lsqfcn = args.rhs;


    
