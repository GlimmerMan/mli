
function mli_solveNLP

global OPT SCEN SLV;

tic

ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd; 
nq = OPT.dims.nq;

nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nlsqs = OPT.dims.lsqfcn_s;
nlsqi = OPT.dims.lsqfcn_i;
nlsqe = OPT.dims.lsqfcn_e;

CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;

if nrds > 0 || nrdi > 0 || nrde > 0
   error('Decoupled node constraints currently not implemented for Ipopt.') 
end

if nlsqs > 0 || nlsqi > 0 || nlsqe > 0
   error('Least squares objective currently not implemented for Ipopt.')
end

OPT.nlp.data = init_nlp_data;

OPT.nlp.cache.pvars = [];
OPT.nlp.cache.dvars = [];
OPT.nlp.cache.ovars = [];
OPT.nlp.cache.data = init_nlp_data;

vars = OPT.phase{4}.var;
pvars = vars.primal;

% SCEN.currentsample = 1; % only at init
user_opt_x_from_scen;

% Set initial values of NLP variables
if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
  x0 = zeros(1,(nsd+nq)*ns+nsd+nq*(ns-1+1));
  x0(1:nsd*(ns+1)) = reshape(pvars.sd,[],1);
  x0(nsd*(ns+1)+(1:nq*ns)) = reshape(pvars.q,[],1);
  x0((nsd+nq)*ns+nsd+(1:nq*(ns-1+1))) = reshape(pvars.delq,[],1);
else
  x0 = zeros(1,(nsd+nq)*ns+nsd);
  x0(1:nsd*(ns+1)) = reshape(pvars.sd,[],1);
  x0(nsd*(ns+1)+(1:nq*ns)) = reshape(pvars.q,[],1);
end

x0(1:nsd) = SLV.opt.lagObjModSd * OPT.x0(:);
% x0(1:nsd) = SLV.opt.lagObjModSd * SCEN.xhistory(:,OPT.currentsample);

% Set lower and upper bound values of NLP variables
if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
  bigbnd = 1e12;
  options.lb = zeros(1,(nsd+nq)*ns+nsd+nq*(ns-1+1));
  options.lb(1:nsd*(ns+1)) = reshape(SLV.opt.lagObjModSd*OPT.bounds.sdLoB,[],1);
  options.lb(nsd*(ns+1)+(1:nq*ns)) = reshape(OPT.bounds.qLoB,[],1);
  options.lb((nsd+nq)*ns+nsd+(1:nq*(ns-1+1))) = -bigbnd*ones(nq*(ns-1+1),1);
  options.ub = zeros(1,(nsd+nq)*ns+nsd+nq*(ns-1+1));
  options.ub(1:nsd*(ns+1)) = reshape(SLV.opt.lagObjModSd*OPT.bounds.sdUpB,[],1);
  options.ub(nsd*(ns+1)+(1:nq*ns)) = reshape(OPT.bounds.qUpB,[],1);
  options.ub((nsd+nq)*ns+nsd+(1:nq*(ns-1+1))) = [repmat(OPT.controlmove_alpha',1,(ns-1)),OPT.controlmove_alpha0'];
else
  options.lb = zeros(1,(nsd+nq)*ns+nsd);
  options.lb(1:nsd*(ns+1)) = reshape(SLV.opt.lagObjModSd*OPT.bounds.sdLoB,[],1);
  options.lb(nsd*(ns+1)+(1:nq*ns)) = reshape(OPT.bounds.qLoB,[],1);  
  options.ub = zeros(1,(nsd+nq)*ns+nsd);
  options.ub(1:nsd*(ns+1)) = reshape(SLV.opt.lagObjModSd*OPT.bounds.sdUpB,[],1);
  options.ub(nsd*(ns+1)+(1:nq*ns)) = reshape(OPT.bounds.qUpB,[],1);
end

options.lb(1:nsd) = x0(1:nsd); 
options.ub(1:nsd) = x0(1:nsd);


% Set lower and upper bound values of constraints
if CMR
  if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
    options.cl = [zeros(1, nsd*ns), zeros(1,(ns-1)*nq),-OPT.controlmove_q0',zeros(1,(ns-1)*nq),OPT.controlmove_q0'];
    options.cu = [zeros(1, nsd*ns), inf*ones(1,2*(ns-1+1)*nq)];
  else
    options.cl = [zeros(1, nsd*ns), repmat(-OPT.controlmove_alpha',1,(ns-1)), -OPT.controlmove_alpha0'];
    options.cu = [zeros(1, nsd*ns), repmat(OPT.controlmove_alpha',1,(ns-1)), OPT.controlmove_alpha0'];
  end
else
  options.cl = zeros(1, nsd*ns);
  options.cu = zeros(1, nsd*ns);
end
% Set lower and upper bound values of constraints (shooting and
% periodicity)
if isfield(OPT,'periodicity')
  options.cl = [zeros(1, nsd*ns),OPT.periodicity.lb'];
  options.cu = [zeros(1, nsd*ns),OPT.periodicity.ub'];
end

% Set the IPOPT options.
% options.ipopt.hessian_approximation = 'limited-memory';
options.ipopt.mu_strategy           = 'adaptive';
options.ipopt.tol                   = 5*SCEN.discr.int_tol; % 1e-3
% options.ipopt.derivative_test       = 'second-order';
options.ipopt.fixed_variable_treatment = 'make_constraint';
options.ipopt.mu_init = 1e-2;
options.ipopt.max_iter = 1000;

% The callback functions.
funcs.objective         = eval_nlp_obj(vars);
funcs.gradient          = eval_nlp_obj_grad(vars);
funcs.constraints       = eval_nlp_cst(vars);
funcs.jacobian          = eval_nlp_cst_jac(vars);
funcs.jacobianstructure = eval_nlp_cst_jac_structure(vars);
funcs.hessian           = eval_nlp_hess(vars);
funcs.hessianstructure  = eval_nlp_hess_structure(vars);

% Run IPOPT.
try
  [ipopt_output, x, info] = evalc('ipopt(x0, funcs, options)');
catch
  error('Exception in Ipopt')
  keyboard
end
fid = fopen('ipopt.out', 'w');
fprintf(fid, ipopt_output);
fclose(fid);

OPT.feedbackq = x(nsd*(ns+1)+(1:nq))';
% OPT.feedbackdelta = x(nsd*(ns+1)+(1:nq))'-OPT.phase{4}.var.primal.q(:,1); % if not init, careful
OPT.ipopttime = info.cpu;

if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
  OPT.phase{4}.varold = OPT.phase{4}.var;
end

% administr. primal variables
OPT.phase{4}.var.primal.sd = reshape(x(1:nsd*(ns+1)), nsd, ns+1);
OPT.phase{4}.var.primal.q = reshape(x(nsd*(ns+1)+(1:nq*ns)), nq, ns);

if isfield(OPT,'periodicity')
  OPT.phase{4}.var.dual.periodicity=-info.lambda(nsd*ns+1:nsd*ns+OPT.dims.periodicity);
end

if CMR
  if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
    OPT.phase{4}.var.primal.delq = reshape(x((nsd+nq)*ns+nsd+(1:(nq*(ns-1+1)))), nq, ns-1+1);
  else
    for ii = 1:ns-1
      OPT.phase{4}.var.primal.delq(:,ii) = OPT.phase{4}.var.primal.q(:,ii+1) - OPT.phase{4}.var.primal.q(:,ii);
    end
    OPT.phase{4}.var.primal.delq(:,ns) = OPT.phase{4}.var.primal.q(:,1) - OPT.controlmove_q0;
  end
end

% administr. dual variables
if CMR
  if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
    OPT.phase{4}.var.dual.lambda = reshape(-info.lambda(1:nsd*ns), nsd, ns);
    OPT.phase{4}.var.dual.lambda1 = reshape(-info.lambda(nsd*ns+(1:nq*ns)), nq, ns);
    OPT.phase{4}.var.dual.lambda2 = reshape(-info.lambda(nsd*ns+nq*ns+(1:nq*ns)), nq, ns);
    
    ybnd = info.zl - info.zu;
    OPT.phase{4}.var.dual.mu_sd = reshape(ybnd(1:nsd*(ns+1)), nsd, ns+1);
    OPT.phase{4}.var.dual.mu_q = reshape(ybnd(nsd*(ns+1)+(1:nq*ns)), nq, ns);
    OPT.phase{4}.var.dual.mu_delq = reshape(ybnd(nsd*(ns+1)+nq*ns+(1:(nq*ns-1+1))), nq, ns);
    
    %   OPT.qp.solbndmult = y(nsd+(1:ns*(nsd+nq)));
    %   OPT.qp.solbndmult2 = y(nsd+ns*(nsd+nq)+(1:ns*nq));
    %   xoffs = ns*(nsd+nq) + nsd + ns*nq;
    %   OPT.qp.soleqmult = [y(1:nsd);y(xoffs+(1:ns*nsd))];
    %   OPT.qp.solCMRmult = y(xoffs+ns*nsd+(1:2*ns*nq));
  else
    OPT.phase{4}.var.dual.lambda = reshape(-info.lambda(1:nsd*ns), nsd, ns);
    
    ybnd = info.zl - info.zu;
    OPT.phase{4}.var.dual.mu_sd = reshape(ybnd(1:nsd*(ns+1)), nsd, ns+1);
    OPT.phase{4}.var.dual.mu_q = reshape(ybnd(nsd*(ns+1)+(1:nq*ns)), nq, ns);
  end
else
  OPT.phase{4}.var.dual.lambda = reshape(-info.lambda(1:nsd*ns), nsd, ns);
  
  ybnd = info.zl - info.zu;
  OPT.phase{4}.var.dual.mu_sd = reshape(ybnd(1:nsd*(ns+1)), nsd, ns+1);
  OPT.phase{4}.var.dual.mu_q = reshape(ybnd(nsd*(ns+1)+(1:nq*ns)), nq, ns);
end
OPT.initime = toc;

end
