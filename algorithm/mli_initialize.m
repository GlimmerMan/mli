function mli_initialize
    
    global SCEN;    

    mli_initialize_solvind;
    mli_initialize_dims;

    mli_initialize_scen;
    
    if SCEN.use_st > 0
        mli_create_tree;
    end
    
    if SCEN.use_controller
        mli_initialize_opt;
    end
    
    if SCEN.use_estimator
        mli_initialize_est;
    end
    

