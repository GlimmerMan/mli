% -*-matlab-*-
function mli_create_tree
    
global ST OPT
        
% create the scenario tree based on information in ST that was initialized
% before

m = ST.nup(2);
nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
% np = OPT.dims.np;
nshoot = OPT.dims.nshoot;
CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;
if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
  nz = nsd * (nshoot+1) + nq * nshoot + nq*(nshoot-1) + nq;
else
  nz = nsd * (nshoot+1) + nq * nshoot;
end
nd = ST.robust_horizon;
S = m^nd;

tmp = S - m.^[0:nd-1];
na = nq*sum(tmp);
ST.lambda = (1:na)'/na;

ST.S = S;
ST.na = na;

switch nd
    case 1
        ST.branches = cell(m,1);
        for jj = 1:m
            ST.branches{jj} = ST.uncertain_parameters(:,jj);
        end
        ST.Couple = zeros(m,1);
        for jj = 1:m-1
            ST.Couple(jj,1) = jj+1;
        end
    case 2
        ST.branches = cell(m^2,2);
        for jj = 1:m
            for ii = 1:m
                ST.branches{(jj-1)*m + ii,1} = ST.uncertain_parameters(:,jj);
                ST.branches{(jj-1)*m + ii,2} = ST.uncertain_parameters(:,ii);
            end
        end
        ST.Couple = zeros(m^2,2);
        for jj = 1:m^2-1
            ST.Couple(jj,1) = jj+1;
            if mod(jj,m) ~= 0
                ST.Couple(jj,2) = jj+1;
            end
        end
    case 3
        ST.branches = cell(m^3,3);
        for jj = 1:m
            for ii = 1:m
                for kk = 1:m
                    ST.branches{(jj-1)*m^2 + (ii-1)*m + kk,1} = ST.uncertain_parameters(:,jj);
                    ST.branches{(jj-1)*m^2 + (ii-1)*m + kk,2} = ST.uncertain_parameters(:,ii);
                    ST.branches{(jj-1)*m^2 + (ii-1)*m + kk,3} = ST.uncertain_parameters(:,kk);
                end
            end
        end
        ST.Couple = zeros(m^2,3);
        for jj = 1:m^3-1
            ST.Couple(jj,1) = jj+1;
            if mod(jj,m^2) ~= 0
                ST.Couple(jj,2) = jj+1;
            end
            if mod(jj,m) ~= 0
                ST.Couple(jj,3) = jj+1;
            end
        end
        
    otherwise
        error('Length of robust horizon not supported');
end

% tree reduction
if isfield(ST,'pruneprob')
    pruneprob = ST.pruneprob;
    [tree,pdf] = test_pen_scenariotree(ST.nup(2),nd,pruneprob);
    ST.weights = tree.scen_prob/sum(tree.scen_prob);
    del_idx = find(pdf<pruneprob);
    rem_idx = find(pdf>=pruneprob);
    ST.branches(del_idx,:) = [];
    S = size(tree.scen_prob,2);  
    ST.S = S;
%     hardcodepen65; % only working for penicillin reduction 729 -> 65
    hardcodepen29; % only working for penicillin reduction 81 -> 29
    na = ST.na;
end

% set C,E

clist = zeros(size(ST.Couple));
clist(find(ST.Couple)) = 1;

ST.C = cell(S,1);
ST.E = ST.C;

for jj = 1:S-1
    ST.C{jj} = zeros(na,nz);
    for kk = 1:nd
        if  ST.Couple(jj,kk) == jj+1
            % index = nq*sum(m^kk - m.^[0:kk]);
            % rows = index:(index+nq-1);
            switch kk
                case 1
                    index = sum(clist(1:jj,1));
                case 2
                    index = sum(clist(1:jj,2))+sum(clist(:,1));
                case 3
                    index = sum(clist(1:jj,3))+sum(clist(:,2))+sum(clist(:,1));
            end
            rows = nq*(index-1)+1:nq*index;
            cols = (kk*nsd + (kk-1)*nq)+1:(kk*nsd + kk*nq);
            ST.C{jj}(rows,cols) = eye(nq);
        end
    end
end

ST.C{S} = zeros(na,nz);
ST.E{1} = zeros(na,nz);

for jj = 2:S
    ST.E{jj} = ST.C{jj-1};
end

% if OPT.use_condensing == 1
    ST.Cc = cell(S,1);
    ST.Ec = cell(S,1);
    qidx = (nsd + (1:nq))'*(1:nshoot);
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
      delqidx = nsd * (nshoot+1) + nq * nshoot + (1:nq*(nshoot-1+1));
      idx = [1:nsd,qidx(:)',delqidx];
    else
      idx = [1:nsd,qidx(:)'];
    end
    for jj = 1:S
        ST.Cc{jj} = ST.C{jj}(:,idx);
        ST.Ec{jj} = ST.E{jj}(:,idx);
    end
% end

