% -*-matlab-*-
% Variables filled in OPT
%
% OPT.deltas0
% OPT.qpHess
% OPT.qpGrad
% OPT.qpIneqLB
% OPT.qpIneqUB
% OPT.qpLB
% OPT.qpUB
% OPT.qpSolStep
% OPT.qpSolEqMult
% OPT.qpSolIneqMult
% OPT.qpSolBndMult
% OPT.feedbackdelta

function mli_solveSTcondQP

global OPT SLV ST

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;
nz = nsd + nq * ns;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nrd = nrds + (ns-1) * nrdi + nrde;

CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;

tqp = tic;

S = ST.S;
phase = OPT.currentphase;
pvars = cell(S,1);
switch phase

    case {0,4}
        for jj = 1:S
            pvars{jj} = OPT.phase{4}{jj}.var.primal;
        end
        
    case 1
        for jj = 1:S
            pvars{jj} = OPT.phase{1}{jj}.ref.primal;
        end
        
    case 2
        for jj = 1:S
            pvars{jj} = OPT.phase{2}{jj}.var.primal;
        end
        
    case 3
        for jj = 1:S
            pvars{jj} = OPT.phase{3}{jj}.var.primal;
        end
        
end

for jj = 1:S
  
  OPT.deltas0{jj} = SLV.opt.lagObjModSd*OPT.x0(:) -...
    pvars{jj}.sd(:,1);

  %  Hessian                                      !% phases
  if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
    % sd and q part already done before condensing
    OPT.qp{jj}.H = OPT.cond{jj}.B;
    
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
      xoff = ns*nq+nsd;
      for ii=1:ns
        idx = xoff + (ii-1)*nq;
        OPT.qp{jj}.H(idx+(1:nq),idx+(1:nq)) = lbfgshess_matrix(OPT.hess{jj}.lbfgsCMRhess{ii});
      end
    end
  else   
    OPT.qp{jj}.H = OPT.cond{jj}.B;
    % regularize condensed Hessian

    HH = OPT.qp{jj}.H;
    [V,E] = eig(0.5*(HH+HH'));
    D = diag(E);
    ep = 0e-6*max(abs(D));
    DD = max(D,ep);
    OPT.qp{jj}.H = V*diag(DD)/V;
    OPT.qp{jj}.nreg = sum(diag(E) < ep);
    OPT.qp{jj}.maxreg = max(DD-D);
    
    % reg = max([0; -D]);
    % reg = 5e3;
    % OPT.qp{jj}.H = OPT.qp{jj}.H + reg * eye(size(OPT.qp{jj}.H));
    % if ~isfield(OPT, 'maxreg')
    %             OPT.maxreg = 0;
    %         end
    %         if reg > OPT.maxreg
    %             OPT.maxreg = reg;
    %             disp(reg)
    %         end
    
    %         OPT.qp{jj}.H = OPT.qp{jj}.H + 1e1*eye(size(OPT.qp{jj}.H));
    
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
      if isfield(OPT, 'controlmove_R')
        R = OPT.controlmove_R;
      else
        R = zeros(nq,nq);
      end
      xoff = nsd + ns*nq;
      for ii=1:ns-1
        idx = xoff + (ii-1)*nq;
        OPT.qp{jj}.H(idx+(1:nq),idx+(1:nq)) = 4 * R;
      end
      idx = xoff + (ns-1)*nq;
      OPT.qp{jj}.H(idx+(1:nq),idx+(1:nq)) = 4 * OPT.controlmove_R0;
    end
  end
  
  % Constraint matrix (matching and ipc)                  %! phases
  if nrd > 0
    OPT.qp{jj}.A = [OPT.cond{jj}.C12;OPT.cond{jj}.rdmat];
  else
    OPT.qp{jj}.A = OPT.cond{jj}.C12;
  end
  if CMR
    xoffs = ns * nsd + nrd;
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
      xoffs2 = xoffs + (ns-1)*nq+nq;
      yoff = nsd + ns*nq;
      for ii = 1:ns-1
        yidx = nsd+(ii-1)*nq+(1:nq);
        OPT.qp{jj}.A(xoffs+(ii-1)*nq+(1:nq),yidx) = eye(nq);
        OPT.qp{jj}.A(xoffs+(ii-1)*nq+(1:nq),yidx+nq) = -eye(nq);
        OPT.qp{jj}.A(xoffs+(ii-1)*nq+(1:nq),yoff+(ii-1)*nq+(1:nq)) = eye(nq);
        OPT.qp{jj}.A(xoffs2+(ii-1)*nq+(1:nq),yidx) = -eye(nq);
        OPT.qp{jj}.A(xoffs2+(ii-1)*nq+(1:nq),yidx+nq) = eye(nq);
        OPT.qp{jj}.A(xoffs2+(ii-1)*nq+(1:nq),yoff+(ii-1)*nq+(1:nq)) = eye(nq);
      end
      OPT.qp{jj}.A(xoffs+(ns-1)*nq+(1:nq),nsd+(1:nq)) = -eye(nq);
      OPT.qp{jj}.A(xoffs+(ns-1)*nq+(1:nq),yoff+(ns-1)*nq+(1:nq)) = eye(nq);
      OPT.qp{jj}.A(xoffs2+(ns-1)*nq+(1:nq),nsd+(1:nq)) = eye(nq);
      OPT.qp{jj}.A(xoffs2+(ns-1)*nq+(1:nq),yoff+(ns-1)*nq+(1:nq)) = eye(nq);
    else
      for ii = 1:ns-1
        yidx = nsd+(ii-1)*nq+(1:nq);
        OPT.qp{jj}.A(xoffs+(ii-1)*nq+(1:nq),yidx) = eye(nq);
        OPT.qp{jj}.A(xoffs+(ii-1)*nq+(1:nq),yidx+nq) = -eye(nq);
      end
      OPT.qp{jj}.A(xoffs+(ns-1)*nq+(1:nq),nsd+(1:nq)) = -eye(nq);
    end
  end
        
    % Gradient
    OPT.qp{jj}.g = OPT.cond{jj}.b;
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
      xoff = nsd+ns*nq;
      for ii=1:ns-1
        idx = xoff + (ii-1)*nq;
        OPT.qp{jj}.g(idx+(1:nq),1) =  OPT.eval{jj}.res.modgraddelq(:,ii);
      end
      idx = xoff + (ns-1)*nq;
      OPT.qp{jj}.g(idx+(1:nq),1) =  OPT.eval{jj}.res.modgraddelq(:,ns);
    end
  
    % RHS   
    if nrd > 0
        bigbnd = 1e12*ones(size(OPT.cond{jj}.rdres));
        OPT.qp{jj}.ineqlb = [OPT.cond{jj}.bndlo1;OPT.cond{jj}.rdres];
        OPT.qp{jj}.inequb = [OPT.cond{jj}.bndup1;bigbnd];
    else
        OPT.qp{jj}.ineqlb = OPT.cond{jj}.bndlo1;
        OPT.qp{jj}.inequb = OPT.cond{jj}.bndup1;
    end
    if CMR
      xoffs = ns * nsd + nrd;
      alpha = OPT.controlmove_alpha;
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        xoffs2 = xoffs + (ns-1)*nq+nq;
        bigbnd = 1e12;
        for ii = 1:ns-1
          OPT.qp{jj}.ineqlb(xoffs+(ii-1)*nq+(1:nq)) = pvars{jj}.q(:,ii+1) - pvars{jj}.q(:,ii);
          OPT.qp{jj}.inequb(xoffs+(ii-1)*nq+(1:nq)) = bigbnd*ones(nq,1);
          OPT.qp{jj}.ineqlb(xoffs2+(ii-1)*nq+(1:nq)) = pvars{jj}.q(:,ii) - pvars{jj}.q(:,ii+1);
          OPT.qp{jj}.inequb(xoffs2+(ii-1)*nq+(1:nq)) = bigbnd*ones(nq,1);
        end
        OPT.qp{jj}.ineqlb(xoffs+(ns-1)*nq+(1:nq)) = pvars{jj}.q(:,1) - OPT.controlmove_q0;
        OPT.qp{jj}.inequb(xoffs+(ns-1)*nq+(1:nq)) = bigbnd*ones(nq,1);
        OPT.qp{jj}.ineqlb(xoffs2+(ns-1)*nq+(1:nq)) = OPT.controlmove_q0 - pvars{jj}.q(:,1);
        OPT.qp{jj}.inequb(xoffs2+(ns-1)*nq+(1:nq)) = bigbnd*ones(nq,1);
        
      else
        for ii = 1:ns-1
          OPT.qp{jj}.ineqlb(xoffs+(ii-1)*nq+(1:nq)) = -alpha;
          OPT.qp{jj}.inequb(xoffs+(ii-1)*nq+(1:nq)) = alpha;
        end
        OPT.qp{jj}.ineqlb(xoffs+(ns-1)*nq+(1:nq)) = -OPT.controlmove_alpha0;
        OPT.qp{jj}.inequb(xoffs+(ns-1)*nq+(1:nq)) = OPT.controlmove_alpha0;
      end
    end
    
    % Bounds
    OPT.qp{jj}.lb = [OPT.deltas0{jj};OPT.cond{jj}.bndlo2];
    OPT.qp{jj}.ub = [OPT.deltas0{jj};OPT.cond{jj}.bndup2];
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
      xoff = nsd + ns*nq;
      for ii=1:ns-1
        idx = xoff + (ii-1)*nq;
        OPT.qp{jj}.lb(idx+(1:nq)) = -bigbnd*ones(nq,1);
        OPT.qp{jj}.ub(idx+(1:nq)) = OPT.controlmove_alpha;
      end
      idx = xoff + (ns-1)*nq;
      OPT.qp{jj}.lb(idx+(1:nq)) = -bigbnd*ones(nq,1);
      OPT.qp{jj}.ub(idx+(1:nq)) = OPT.controlmove_alpha0;
    end
end

% relax bounds for controls coupled by tree structure
    nd = ST.robust_horizon;
    epsbnd = 1e-6;
    for jj = 2:S
        for kk = 1:nd
            if  ST.Couple(jj-1,kk) == jj
                rows = (1*nsd + (kk-1)*nq)+1:(1*nsd + kk*nq);
                OPT.qp{jj}.lb(rows,1) = OPT.qp{jj}.lb(rows,1)-epsbnd;
                OPT.qp{jj}.ub(rows,1) = OPT.qp{jj}.ub(rows,1)+epsbnd;
            end
        end
    end

ST.Newton.G = ones(size(ST.lambda))';
it_N = 0;
sample = OPT.currentsample;
% switch sample
%     case 1
%         itmax_N = 10; %!
%     case 2
%         itmax_N = 400; %1000; %!
%     otherwise
%         itmax_N = ST.Newton.itmax;
% end

itmax_N = ST.Newton.itmax;
% M = eye(ST.na);
% alpha = 1;

OPT.qpoptions = qpOASES_options('reliable', 'maxIter', 10000, 'maxCpuTime', 60);
OPT.scenqpcumtime = cell(1,S);
for jj = 1:S
  OPT.scenqpcumtime{jj} = 0;
end

%%% STQP solution

% tic

while norm(ST.Newton.G) > ST.Newton.eps && it_N < itmax_N
    
%     G_old = ST.Newton.G;
    ST.Newton.G = zeros(size(ST.lambda));
    z = cell(S,1);
    
    for jj = 1:S  
        % solve scenario QP 
        mli_solvescenQP_cond(jj,ST.lambda,'m');
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
          z{jj} = [OPT.qp{jj}.solstep; OPT.qp{jj}.soldelq];
        else
          z{jj} = OPT.qp{jj}.solstep;
        end
        
        % update Newton gradient  
        ST.Newton.G = ST.Newton.G + (ST.Cc{jj}-ST.Ec{jj})*z{jj};
        
        % compute jjth component of Newton matrix
        DS = sparse(ST.Cc{jj}-ST.Ec{jj});
        DD = full(DS');
        nonzerocols = sum(DD.^2,1) > 0;
        nnz = sum(nonzerocols);
        Ob = ones(size(OPT.qp{jj}.lb(:))) * zeros(1,nnz);
        OA = ones(size(OPT.qp{jj}.ineqlb(:))) * zeros(1,nnz);
        
        [KtildeNNZ,~,~] = qpOASES_sequence( 'e', ...
            OPT.qp{jj}.qphandle,DD(:,nonzerocols),Ob,Ob,OA,OA);
        
        Ktilde = sparse(size(DD,1),size(DD,2));
        Ktilde(:,nonzerocols) = KtildeNNZ;
        ST.Newton.Mj{jj} = DS*Ktilde;
    end
    
    ST.obj = ST.objj*ST.weights';
    
    if norm(ST.Newton.G) > ST.Newton.eps
        
%         if it_N > 0
%             s = alpha*ST.deltalam;
%             yps = ST.Newton.G - G_old;
%             rho = 1/(yps'*s);
%             M = (eye(ST.na)-rho*s*yps')*M*(eye(ST.na)-rho*yps*s') + rho*(s*s'); %BFGS try
%         end
        
        % Newton matrix 
%         M = -eye(ST.na);
        M = zeros(ST.na,ST.na);
        qp_nreg = 0;
        qp_maxreg = 0;
        for jj = 1:ST.S
            M = M + ST.Newton.Mj{jj};
            qp_nreg = qp_nreg + OPT.qp{jj}.nreg;
            qp_maxreg = max(qp_maxreg,OPT.qp{jj}.maxreg);
        end

        % Compute Newton step direction
        [ST.deltalam, Newton_reg] = mli_solveSTNewton( M, ST.Newton.G, 1);
        reg = [Newton_reg,qp_nreg,qp_maxreg];

%         ST.deltalam = -M*ST.Newton.G;
%         nreg = [0,0];
        
        if ~all(isfinite(ST.deltalam))
            keyboard
        end
        
        % Newton line search
        alpha = mli_stepSTNewton;  %linesearch
%         alpha = 1;
        
        ST.lambda = ST.lambda + alpha*ST.deltalam;
        it_N = it_N+1;
        
        ST.history{sample}.alpha{it_N} = alpha;
        ST.history{sample}.lambda{it_N} = ST.lambda;
        ST.history{sample}.deltalam{it_N} = ST.deltalam;
        
    else
        alpha = 0;
        reg = [0,0,0];
        ST.deltalam = 0;
    end
    
    if 1 % print every nonsmooth Newton iteration
      fprintf('It%3d, Ph%1d, NIt%3d, |lam|: %9.2e, |dlam|: %9.2e, |G|: %9.2e, al: %9.2e, %d, %d\n',sample, phase, it_N, norm(ST.lambda), norm(ST.deltalam),norm(ST.Newton.G),alpha,reg(1),reg(2))
    end
    
    ST.it_N = it_N;
    ST.reg = reg;
end

% Feedback update
J = ceil(ST.S/2);
OPT.feedbackdelta = OPT.qp{J}.solstep(nsd+(1:nq));
% OPT.feedbackdelta = x(nsd+(1:nq));

% Synchronization step 
for jj = J+1:S
    P1 = eye(size(OPT.qp{jj}.solstep,1));
    P2 = P1;
    nonzero = sum(ST.Cc{jj-1}(:,1:nz).^2,1) > 0;
    P1(~nonzero,:) = 0;
    P2(nonzero,:) = 0;
    OPT.qp{jj}.solstep = P1*OPT.qp{jj-1}.solstep + P2*OPT.qp{jj}.solstep;
end
for jj = J-1:-1:1
    P1 = eye(size(OPT.qp{jj}.solstep,1));
    P2 = P1;
    nonzero = sum(ST.Cc{jj}(:,1:nz).^2,1) > 0;
    P1(~nonzero,:) = 0;
    P2(nonzero,:) = 0;
    OPT.qp{jj}.solstep = P1*OPT.qp{jj+1}.solstep + P2*OPT.qp{jj}.solstep;
end

OPT.qptime = toc(tqp);

