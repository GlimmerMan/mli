% -*-matlab-*-
function [outp] = eval_lsqfcn_C_est(inp, nodetype, varargin)
    
    global SLV
    
  
    if (nargin < 2)
        error('Too few arguments in eval_lsqfcn_C_est.');
    end
    if (nargin > 4)
        error('Too many arguments in eval_lsqfcn_C_est.');
    end
    
    if nargin == 3
        eta = varargin{1};
    end
    
    if nargin == 4
        eta = varargin{1};
        V = diag(varargin{2});
    end
    
    args.t = inp.t;    
    args.xd = inp.sd;
    args.qph = [inp.q;inp.p]; 

    evl = SLV.est.evl;

    
    switch nodetype
      case 's'
	args.rhs = solvind('evaluateFcn', evl, 'lsqfcn_s', args);
        lsqfcn = args.rhs;
        if nargin > 2
            args.fwdTCOrder = 0;
            args.nFwdTC = 0;
            args.fwdTC = [];
            args.nAdjTC = 1;
            if nargin == 3
                residual = args.rhs-eta;
            end
            if nargin == 4
                residual = V*(args.rhs-eta);
            end
            args.adjTC = V'*residual;
            result = solvind('evaluateDenseDer', evl, 'lsqfcn_s', ...
                             args);
        end
	
      case 'i'
	args.rhs = solvind('evaluateFcn', evl, 'lsqfcn_i', args);
        lsqfcn = args.rhs;
        if nargin > 2
            args.fwdTCOrder = 0;
            args.nFwdTC = 0;
            args.fwdTC = [];
            args.nAdjTC = 1;
            if nargin == 3
                residual = args.rhs-eta;
            end
            if nargin == 4
                residual = V*(args.rhs-eta);
            end
            args.adjTC = V'*residual;
            result = solvind('evaluateDenseDer', evl, 'lsqfcn_i', ...
                             args);
        end
	
      case 'e'
	args.rhs = solvind('evaluateFcn', evl, 'lsqfcn_e', args);
        lsqfcn = args.rhs;
        if nargin > 2
            args.fwdTCOrder = 0;
            args.nFwdTC = 0;
            args.fwdTC = [];
            args.nAdjTC = 1;
            if nargin == 3
                residual = args.rhs-eta;
            end
            if nargin == 4
                residual = V*(args.rhs-eta);
            end
            args.adjTC = V'*residual;
            result = solvind('evaluateDenseDer', evl, 'lsqfcn_e', args);
	end
        
      otherwise
	error('eval_lsqfcn_C: Invalid nodetype');
	
    end

    outp.lsqfcn = lsqfcn;
    
    if nargin > 2
        outp.residual = residual;
        outp.lsqgrad = result.propAdjTC(2:end);
        outp.lsqgrad = outp.lsqgrad(:);
    end


    
    
