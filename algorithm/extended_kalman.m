function extended_kalman
    
    global SLV EST SCEN 
        
    nx = EST.dims.nx;
    midx = EST.measindex;
    mcov = EST.meascov;
    
    int = SLV.est.int;
    evl = SVL.est.eval;
    
    int.setTapeNumber = 1;
    int.setTimeHorizon = [0 SCEN.sampling];
    int.setInitVals = [EST.s;EST.u];
    int.setForwardTaylorCoefficients = {nx, 1, EST.sensdirs}; 
    int.setAdjointTaylorCoefficients = [];
   
    status = int.evaluate;
    if status ~= 0
        error('integration failed');
    end
    Gx = int.getFwdSens;
   
    % Hier die Messfunktion
    
    s_minus = int.getSolution;
    P_minus = Gx * P_est * Gx';
    
    meas =  + sqrt(mcov) * randn(length(midx),1);
               
    y = meas - s_minus(midx);
    S = P_minus(midx, midx) + mcov;
    
    EST.s = s_minus + P_minus(:,midx) * (S\y); 
    EST.P = P_minus - P_minus(:,midx) * (S \ P_minus(midx,:));
    
 