function mli_timehorizon_shift_periodic
% shift the NLP variable when the timehorizon is advanced.
% updating OPT.phase{4} primal variables
global OPT SCEN

ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
np = OPT.dims.np;
nperiod=OPT.periodicity.periodic;

primal_old=OPT.phase{4}.var.primal;
%shift the primal variables and add a copy of the last state to the end

old_grid=OPT.discr.msgrid;
%update the time horizon
samples=size(SCEN.sampling);
if SCEN.currentsample<samples(2)
    new_grid=OPT.discr.initialgrid+SCEN.thistory(SCEN.currentsample+1)*ones(ns+1,1);
end

%initialization by shifting the primal variables and apply periodic
%continuation

primal_new.sd=[primal_old.sd(:,2:ns+1),primal_old.sd(:,ns-nperiod+2)];
%correct last state
primal_new.sd(end,ns+1)=primal_new.sd(end,ns)+primal_new.sd(end,OPT.periodicity.transient+1)-primal_new.sd(end,OPT.periodicity.transient);
primal_new.pconst=primal_old.pconst;
%primal_new.q=primal_old.q;
primal_new.q=[primal_old.q(:,2:ns),primal_old.q(:,ns+1-nperiod)];

% update data in OPT variable
OPT.phase{4}.var.primal=primal_new;

% calculate the new dual variables for the shifted time horizon
dual_old=OPT.phase{4}.var.dual;
dual_new.lambda=[dual_old.lambda(:,2:ns),dual_old.lambda(:,ns+1-nperiod)];
%adjust weight of the multiplier that moved over the transient/periodic boundary
dual_new.lambda(:,ns-nperiod)=dual_new.lambda(:,ns-nperiod);

dual_new.periodicity=dual_old.periodicity;

dual_new.mu_sd=[];
dual_new.mu_sd(:,1)=dual_old.mu_sd(:,2)-dual_old.lambda(:,1);
dual_new.mu_sd(OPT.dims.periodicity,1)=0;
if OPT.periodicity.transient-1>=1
    dual_new.mu_sd(:,2:OPT.periodicity.transient-1)=dual_old.mu_sd(:,3:OPT.periodicity.transient);
end
dual_new.mu_sd(:,OPT.periodicity.transient)=dual_old.mu_sd(:,OPT.periodicity.transient+1);
dual_new.mu_sd(:,OPT.periodicity.transient+1)=dual_old.mu_sd(:,OPT.periodicity.transient+2);
dual_new.mu_sd(:,OPT.periodicity.transient+(2:OPT.periodicity.periodic-1))=dual_old.mu_sd(:,OPT.periodicity.transient+1+(2:OPT.periodicity.periodic-1));
dual_new.mu_sd(:,OPT.periodicity.transient+OPT.periodicity.periodic)=dual_old.mu_sd(:,OPT.periodicity.transient+1);
%this multiplier either way should be zero, because the variable is
%without simple bounds
dual_new.mu_sd(:,OPT.periodicity.transient+OPT.periodicity.periodic+1)=dual_old.mu_sd(:,OPT.periodicity.transient+OPT.periodicity.periodic+1);


dual_new.mu_q=[dual_old.mu_q(:,2:ns),dual_old.mu_q(:,ns+2-nperiod)];

OPT.phase{4}.var.dual=dual_new;
%update the discretization grid
OPT.discr.msgrid=new_grid;



end
