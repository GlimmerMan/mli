function fcn = eval_nlp_hess(vars)
global SCEN OPT ST

if SCEN.use_st == 0
    fcn = @eval;
else
    fcn = @evalST;
end

ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
nmfcn = OPT.dims.mfcn;
nlfcn = OPT.dims.lfcn;

shift = nsd*nsd + 2*nsd*nq + nq*nq;
nnz = ns*shift;
if nmfcn > 0 && nlfcn <= 0

  nnz = nnz + nsd*nsd;
end

CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;

ridx = zeros(nnz,1);
cidx = zeros(nnz,1);
vals = zeros(nnz,1);

for kk=1:ns
  
  xidx = (kk-1)*nsd;
  qidx = nsd*(ns+1) + (kk-1)*nq;
  
  ridx((kk-1)*shift+(1:shift)) = ...
    [xidx + kron(ones(nsd,1),(1:nsd)'); ...
    xidx + kron(ones(nq,1),(1:nsd)'); ...
    qidx + kron(ones(nsd,1),(1:nq)'); ...
    qidx + kron(ones(nq,1),(1:nq)')];
  
  cidx((kk-1)*shift+(1:shift)) = ...
    [xidx + kron((1:nsd)',ones(nsd,1)); ...
    qidx + kron((1:nq)',ones(nsd,1)); ...
    xidx + kron((1:nsd)',ones(nq,1)); ...
    qidx + kron((1:nq)',ones(nq,1))];
  
end

if nmfcn > 0 && nlfcn <= 0
  
  xidx = ns*nsd;
  
  ridx(ns*shift+(1:nsd*nsd)) = xidx + kron(ones(nsd,1),(1:nsd)');
  cidx(ns*shift+(1:nsd*nsd)) = xidx + kron((1:nsd)',ones(nsd,1));
  
end

  function [hess] = eval(x, sigma, lambda)
    
    vars.primal.sd = reshape(x(1:nsd*(ns+1)), nsd, ns+1);
    vars.primal.q = reshape(x(nsd*(ns+1)+(1:nq*ns)), nq, ns);
    vars.objective.sigma = -sigma;
    vars.dual.lambda = reshape(lambda(1:nsd*ns), nsd, ns);
    if CMR
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
        vars.primal.delq = reshape(x((nsd+nq)*ns+nsd+(1:(nq*(ns-1+1)))), nq, ns-1+1);
      else
        for ii = 1:ns-1
          vars.primal.primal.delq(:,ii) = vars.primal.q(:,ii+1) - vars.primal.q(:,ii);
        end
        vars.primal.delq(:,ns) = vars.primal.q(:,1) - OPT.controlmove_q0;
      end
    end
    
    OPT.nlp.data = mli_evalNLP(vars);
    
    for msn=1:ns
      
      xidx = (msn-1)*nsd;
      qidx = nsd*(ns+1) + (msn-1)*nq;
      
      vals((msn-1)*shift+(1:shift)) = ...
        [reshape(OPT.nlp.data.mat.lambdaGxx{msn}, nsd*nsd, 1); ...
        reshape(OPT.nlp.data.mat.lambdaGxq{msn}, nsd*nq, 1); ...
        reshape(OPT.nlp.data.mat.lambdaGxq{msn}', nsd*nq, 1); ...
        reshape(OPT.nlp.data.mat.lambdaGqq{msn}, nq*nq, 1)];
      
    end
    
    if nmfcn > 0 && nlfcn <= 0
      if isfield(OPT,'periodicity')
        H_per=zeros((nsd+nq)*ns+nsd,(nsd+nq)*ns+nsd);
        for ii=1:OPT.periodicity.transient
          %parts of the hessian corresponding to (Li-Li+p)^2
          %Set up projection matrix correspponding to (Li-Li+p)^2
          P=zeros((nsd+nq)*ns+nsd,1);
          %the part of the transient interval
          P((ii-1)*nsd+(1:2*nsd),1)=[-OPT.nlp.data.res.mgrad{ii}.start',OPT.nlp.data.res.mgrad{ii}.end'];
          %the part of the periodic interval
          P((ii-1+OPT.periodicity.periodic)*nsd+(1:2*nsd),1)=-[-OPT.nlp.data.res.mgrad{ii+OPT.periodicity.periodic}.start',OPT.nlp.data.res.mgrad{ii+OPT.periodicity.periodic}.end'];
          %modify the hessian with the discount factor
          H_per=H_per+sigma*OPT.periodicity.alpha*(OPT.periodicity.discount^ii)*2*P*P';
        end
                
                
       if OPT.periodicity.transient>=1
         %add term for 2. derivatives wrt state and control deviation
         %state
         P_state=zeros((nsd+nq)*ns+nsd,1);
         %the state part of the transient interval
         P_state((OPT.periodicity.transient-1)*nsd+(1:nsd),1)=OPT.periodicity.beta.sd;
         %the state part of the periodic interval
         P_state((OPT.periodicity.transient-1+OPT.periodicity.periodic)*nsd+(1:nsd),1)=-OPT.periodicity.beta.sd;
         %modify the hessian
         H_per=H_per+sigma*2*P_state*P_state';
         %control
         P_control=zeros((nsd+nq)*ns+nsd,1);
         %the state part of the transient interval
         P_control((ns+1)*nsd+(OPT.periodicity.transient-1)*nq+(1:nq),1)=OPT.periodicity.beta.q;
         %the state part of the periodic interval
         P_control((ns+1)*nsd+(OPT.periodicity.transient-1+OPT.periodicity.periodic)*nq+(1:nq),1)=-OPT.periodicity.beta.q;
         %modify the hessian
         H_per=H_per+sigma*2*P_control*P_control';
       end



      else
      xidx = ns*nsd;      
      vals(ns*shift+(1:nsd*nsd)) = ...
        reshape(sigma * OPT.nlp.data.mat.mhess, nsd*nsd, 1);
      end  
    end
    
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
      if isfield(OPT, 'controlmove_R')
        [r,c,v] = find(sigma * OPT.controlmove_R);
        xoff = ns*(nsd+nq)+nsd;
        for ii=1:ns-1
          idx = xoff + (ii-1)*nq;
          ridx = [ridx; r + idx];
          cidx = [cidx; c + idx];
          vals = [vals; v];
        end
        [r,c,v] = find(sigma * OPT.controlmove_R0);
        idx = xoff + (ns-1)*nq;
        ridx = [ridx; r + idx];
        cidx = [cidx; c + idx];
        vals = [vals; v];
      end
      nz = ns*(nsd+nq)+nsd + (ns-1+1)*nq;
    else
      nz = ns*(nsd+nq)+nsd;
    end
    
    if isfield(OPT,'periodicity')
      hess = tril(sparse(ridx, cidx, vals, nz, nz)+sparse(H_per));
    else
      hess = tril(sparse(ridx, cidx, vals, nz, nz));
    end
    

  end

function [hess] = evalST(x, sigma, lambda)
    
    S = ST.S;
    rowindex = [];
    columnindex = [];
    values = [];
    
    for jj = 1:S
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
        nz = (nsd+nq)*ns+nsd+nq*(ns-1+1);
        offset = (jj-1)*nz;
      else
        nz = (nsd+nq)*ns+nsd;
        offset = (jj-1)*nz;
      end
      vars{jj}.primal.sd = reshape(x(offset+(1:nsd*(ns+1))), nsd, ns+1);
      vars{jj}.primal.q = reshape(x(offset+(nsd*(ns+1)+(1:nq*ns))), nq, ns);
      vars{jj}.objective.sigma = -1;
      vars{jj}.primal.z_ipopt = x(offset+(1:nz))';
      if CMR
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
          vars{jj}.primal.delq = reshape(x(offset+((nsd+nq)*ns+nsd+(1:(nq*(ns-1+1))))), nq, ns-1+1);
        else
          for ii = 1:ns-1
            vars{jj}.primal.delq(:,ii) = vars{jj}.primal.q(:,ii+1) - vars{jj}.primal.q(:,ii);
          end
          vars{jj}.primal.delq(:,ns) = vars{jj}.primal.q(:,1) - OPT.controlmove_q0;
        end
      end
    
      if CMR
        if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R'))
          cst_offset = (jj-1)*(nsd*ns+2*(ns-1+1)*nq);
        else
          cst_offset = (jj-1)*(nsd*ns+(ns-1+1)*nq);
        end
      else
        cst_offset = (jj-1)*(nsd*ns);
      end
      vars{jj}.dual.lambda = reshape(lambda(cst_offset+(1:nsd*ns)), nsd, ns); %! VZ
    end
    
    OPT.nlp.data = mli_evalNLP(vars);
    
    vals_scen = zeros(nnz,1);
    
    for jj = 1:S
      for msn=1:ns 
        xidx = (msn-1)*nsd;
        qidx = nsd*(ns+1) + (msn-1)*nq;
        
        vals_scen((msn-1)*shift+(1:shift)) = ...
          [reshape(OPT.nlp.data{jj}.mat.lambdaGxx{msn}, nsd*nsd, 1); ...
          reshape(OPT.nlp.data{jj}.mat.lambdaGxq{msn}, nsd*nq, 1); ...
          reshape(OPT.nlp.data{jj}.mat.lambdaGxq{msn}', nsd*nq, 1); ...
          reshape(OPT.nlp.data{jj}.mat.lambdaGqq{msn}, nq*nq, 1)];     
      end
      
      if nmfcn > 0 && nlfcn <= 0
        
        xidx = ns*nsd;
        
        vals_scen(ns*shift+(1:nsd*nsd)) = ...
          reshape(sigma * OPT.nlp.data{jj}.mat.mhess, nsd*nsd, 1);
        
      end

      r_CMR = [];
      c_CMR = [];
      v_CMR = [];
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
        if isfield(OPT, 'controlmove_R')
          [r,c,v] = find(sigma * OPT.controlmove_R);
          xoff = ns*(nsd+nq)+nsd;
          for ii=1:ns-1
            idx = xoff + (ii-1)*nq;
            r_CMR = [r_CMR; r + idx];
            c_CMR = [c_CMR; c + idx];
            v_CMR = [v_CMR; v];
          end
          [r,c,v] = find(sigma * OPT.controlmove_R0);
          idx = xoff + (ns-1)*nq;
          ridx = [ridx; r + idx];
          cidx = [cidx; c + idx];
          vals = [vals; v];
        end
        nz = ns*(nsd+nq)+nsd + (ns-1+1)*nq;
      else
        nz = ns*(nsd+nq)+nsd;
      end
      
      ridx_scen = [ridx; r_CMR] + (jj-1)*nz;
      cidx_scen = [cidx; c_CMR] + (jj-1)*nz;
      vidx_scen = [vals_scen; v_CMR];
      
      rowindex = [rowindex; ridx_scen];
      columnindex = [columnindex; cidx_scen];
      values = [values; vidx_scen];
    end
    
    hess = tril(sparse(rowindex, columnindex, values, S*nz, S*nz));
  end

end
