% -*-matlab-*-
function mli_initialize_scen
    
global SCEN EST

% setup SCEN
% this may change due to delays
    
SCEN.thistory = SCEN.sampling;
SCEN.steps = length(SCEN.thistory) - 1;

SCEN.xhistory = zeros(SCEN.dims.nx, SCEN.steps + 1);
SCEN.qhistory = zeros(SCEN.dims.nq, SCEN.steps);

SCEN.xhistory(:,1) = SCEN.initial_x;
SCEN.phistory = [];

if ~isfield(SCEN, 'print_interval')
    SCEN.print_interval = 5;
end

if SCEN.use_estimator
    SCEN.meashistory = zeros(size(SCEN.meascov,1), SCEN.steps + 1);
    inp = [];
    inp.t = SCEN.thistory(1);
    inp.sd = SCEN.initial_x;
    inp.p = SCEN.initial_parameters;
    inp.q = SCEN.default_control(SCEN.initial_x);
    outp = eval_lsqfcn_B_est(inp,'s');
    ytrue = outp.lsqfcn;
    epsilon = sqrtm(SCEN.meascov) * randn(size(ytrue));
    SCEN.meashistory(:,1) = ytrue + epsilon;
    
    nx = EST.dims.nsd;
    SCEN.xhathistory = zeros(nx, SCEN.steps);

    if EST.fixed_p
        SCEN.covhistory = mat2cell(zeros(nx,nx,SCEN.steps), nx, nx, ...
                                   ones(SCEN.steps,1)); 
    else
        np = EST.dims.np;
        SCEN.phathistory = zeros(np, SCEN.steps);
        SCEN.covhistory = mat2cell(zeros(nx+np,nx+np,SCEN.steps), nx+np, ...
                                   nx+np, ones(SCEN.steps,1));
    end
end


