% -*-matlab-*-
function [data] = mli_evalSTPhaseD(vars,jj)

global OPT ST

tic

ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd; 
nq = OPT.dims.nq; 
np = OPT.dims.np;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nlsqs = OPT.dims.lsqfcn_s;
nlsqi = OPT.dims.lsqfcn_i;
nlsqe = OPT.dims.lsqfcn_e;
nmfcn = OPT.dims.mfcn;
nlfcn = OPT.dims.lfcn;

pvars = vars.primal;
dvars = vars.dual;

sensdirs = [zeros(1,nsd+nq);eye(nsd+nq);zeros(np,nsd+nq)];

%%% Start Node
msn = 1;

% dynamical equations
inp = [];
inp.thoriz = [OPT.discr.msgrid(msn) OPT.discr.msgrid(msn+1)];
inp.sd = pvars.sd(:,msn);
inp.q = pvars.q(:,msn);
% inp.p = pvars.pconst;
inp.p = ST.branches{jj,msn}(:); % p of first tree node
inp.sensdirs = sensdirs;
inp.lambda = dvars.lambda(:,msn);

outp = eval_dyn_D(inp);

data.res.xd(:,msn) = outp.x;
data.res.c(:,msn) = outp.x - pvars.sd(:,msn+1);
data.res.lambdaGx(:,msn) = outp.lag(1:nsd);
data.res.lambdaGq(:,msn) = outp.lag(nsd+(1:nq));
data.res.modgradx(:,msn) = zeros(nsd,1);    
data.res.modgradq(:,msn) = zeros(nq,1);
data.res.refgradx(:,msn) = zeros(nsd,1);  
data.res.refgradq(:,msn) = zeros(nq,1);
if nlfcn > 0
    data.res.dlfcn(:,msn) = outp.dlfcn;
    data.res.lfcn(msn) = outp.lfcn;
end
data.mat.Gx{msn} = outp.G(1:nsd,1:nsd);
data.mat.Gq{msn} = outp.G(1:nsd,nsd+(1:nq));
data.mat.lambdaGxx{msn} = outp.H(1:nsd,1:nsd);
data.mat.lambdaGxq{msn} = outp.H(1:nsd,nsd+(1:nq));
data.mat.lambdaGqq{msn} = outp.H(nsd+(1:nq),nsd+(1:nq));

% decoupled node constraint
if nrds > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn);
  %   inp.p = pvars.pconst;
  inp.p = ST.branches{jj,msn}(:); % p of first tree node
  inp.sensdirs = sensdirs;
  inp.mu = dvars.mu_rds;

  outp = eval_rdfcn_D(inp,'s');
  
  data.res.rds = outp.rdfcn;
  data.mat.Jrdsx = outp.rdjac(1:nrds,1:nsd);
  data.mat.Jrdsq = outp.rdjac(1:nrds,nsd+(1:nq));
  data.res.muJrdsx = outp.mu_rdjac(1:nsd);    
  data.res.muJrdsq = outp.mu_rdjac(nsd+(1:nq));
  data.mat.muJrdsxx = outp.mu_rdhess(1:nsd,1:nsd);
  data.mat.muJrdsxq = outp.mu_rdhess(1:nsd,nsd+(1:nq));
  data.mat.muJrdsqq = outp.mu_rdhess(nsd+(1:nq),nsd+(1:nq));
end

% least squares objective
if nlsqs > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn);
%   inp.p = pvars.pconst;
  inp.p = ST.branches{jj,msn}(:); % p of first tree node
  inp.sensdirs = sensdirs;

  outp = eval_lsqfcn_D(inp,'s');
  
  data.res.lsqfcn_s = outp.lsqfcn;
  data.res.lsqsgradx = outp.lsqgrad(1:nsd);
  data.res.lsqsgradq = outp.lsqgrad(nsd+(1:nq));
  data.mat.Jlsqsx = outp.lsqjac(1:nlsqs,1:nsd); 
  data.mat.Jlsqsq = outp.lsqjac(1:nlsqs,nsd+(1:nq));
  data.mat.Hlsqsxx = outp.lsq2nd(1:nsd,1:nsd);
  data.mat.Hlsqsxq = outp.lsq2nd(1:nsd,nsd+(1:nq));
  data.mat.Hlsqsqq = outp.lsq2nd(nsd+(1:nq),nsd+(1:nq));
end

% Lagrange gradient
data.res.laggradx(:,msn) = - data.res.lambdaGx(:,msn) - dvars.mu_sd(:,msn);
data.res.laggradq(:,msn) = - data.res.lambdaGq(:,msn) - dvars.mu_q(:,msn);

if nrds > 0
  data.res.laggradx(:,msn) = data.res.laggradx(:,msn) ...
      - data.res.muJrdsx;
  data.res.laggradq(:,msn) = data.res.laggradq(:,msn) ...
      - data.res.muJrdsq;
end

if nlsqs > 0
  data.res.laggradx(:,msn) = data.res.laggradx(:,msn) ...
      + data.res.lsqsgradx;
  data.res.laggradq(:,msn) = data.res.laggradq(:,msn) ...
      + data.res.lsqsgradq;
  
  data.res.modgradx(:,msn) = data.res.modgradx(:,msn) ... 
      + data.res.lsqsgradx;
  data.res.modgradq(:,msn) = data.res.modgradq(:,msn) ... 
      + data.res.lsqsgradq;
  
  data.res.refgradx(:,msn) = data.res.refgradx(:,msn) ... 
      + data.res.lsqsgradx;
  data.res.refgradq(:,msn) = data.res.refgradq(:,msn) ... 
      + data.res.lsqsgradq;
end

if nlfcn > 0
    data.res.laggradx(:,msn) = data.res.laggradx(:,msn) ...
        + data.res.dlfcn(1:nsd,msn);
    data.res.laggradq(:,msn) = data.res.laggradq(:,msn) ...
        + data.res.dlfcn(nsd+(1:nq),msn);
    
    data.res.modgradx(:,msn) = data.res.modgradx(:,msn) ...
        + data.res.dlfcn(1:nsd,msn);
    data.res.modgradq(:,msn) = data.res.modgradq(:,msn) ...
        + data.res.dlfcn(nsd+(1:nq),msn);
    
    data.res.refgradx(:,msn) = data.res.refgradx(:,msn) ...
        + data.res.dlfcn(1:nsd,msn);
    data.res.refgradq(:,msn) = data.res.refgradq(:,msn) ...
        + data.res.dlfcn(nsd+(1:nq),msn);
end

%%% Interior Nodes
for msn=2:ns

% dynamical equations
  inp = [];
  inp.thoriz = [OPT.discr.msgrid(msn) OPT.discr.msgrid(msn+1)];
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn);
  if msn > ST.robust_horizon
      inp.p = pvars.pconst;
  else
      inp.p = ST.branches{jj,msn}(:); % uncertain p of node msn
  end
  inp.lambda = dvars.lambda(:,msn);
  inp.sensdirs = sensdirs;
  
  outp = eval_dyn_D(inp);
    
  data.res.xd(:,msn) = outp.x;
  data.res.c(:,msn) = outp.x - pvars.sd(:,msn+1);
  data.res.lambdaGx(:,msn) = outp.lag(1:nsd);
  data.res.lambdaGq(:,msn) = outp.lag(nsd+(1:nq));
  data.res.modgradx(:,msn) = zeros(nsd,1);    
  data.res.modgradq(:,msn) = zeros(nq,1);
  data.res.refgradx(:,msn) = zeros(nsd,1);  
  data.res.refgradq(:,msn) = zeros(nq,1);
  
  if nlfcn > 0
    data.res.dlfcn(:,msn) = outp.dlfcn;
    data.res.lfcn(msn) = outp.lfcn;
  end
  
  data.mat.Gx{msn} = outp.G(1:nsd,1:nsd);
  data.mat.Gq{msn} = outp.G(1:nsd,nsd+(1:nq));
  data.mat.lambdaGxx{msn} = outp.H(1:nsd,1:nsd);
  data.mat.lambdaGxq{msn} = outp.H(1:nsd,nsd+(1:nq));
  data.mat.lambdaGqq{msn} = outp.H(nsd+(1:nq),nsd+(1:nq));

% decoupled node constraint
  if nrdi > 0
    inp = [];
    inp.t = OPT.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.q = pvars.q(:,msn);
    if msn > ST.robust_horizon
        inp.p = pvars.pconst;
    else
        inp.p = ST.branches{jj,msn}(:); % uncertain p of node msn
    end
    inp.sensdirs = sensdirs;
    inp.mu = dvars.mu_rdi(:,msn-1);
    
    outp = eval_rdfcn_D(inp,'i');
    
    data.res.rdi(:,msn-1) = outp.rdfcn;
    data.mat.Jrdix{msn-1} = outp.rdjac(1:nrdi,1:nsd);
    data.mat.Jrdiq{msn-1} = outp.rdjac(1:nrdi,nsd+(1:nq));
    data.res.muJrdix(:,msn-1) = outp.mu_rdjac(1:nsd);    
    data.res.muJrdiq(:,msn-1) = outp.mu_rdjac(nsd+(1:nq));
    data.mat.muJrdixx{msn-1} = outp.mu_rdhess(1:nsd,1:nsd);
    data.mat.muJrdixq{msn-1} = outp.mu_rdhess(1:nsd,nsd+(1:nq));
    data.mat.muJrdiqq{msn-1} = outp.mu_rdhess(nsd+(1:nq),nsd+(1:nq));
  end

  % least squares objective
  if nlsqi > 0
      inp = [];
      inp.t = OPT.discr.msgrid(msn);
      inp.sd = pvars.sd(:,msn);
      inp.q = pvars.q(:,msn);
      if msn > ST.robust_horizon
          inp.p = pvars.pconst;
      else
          inp.p = ST.branches{jj,msn}(:); % uncertain p of node msn
      end
      inp.sensdirs = sensdirs;
      
      outp = eval_lsqfcn_D(inp,'i');
      
      data.res.lsqfcn_i(:,msn-1) = outp.lsqfcn;
      data.res.lsqigradx(:,msn-1) = outp.lsqgrad(1:nsd);
      data.res.lsqigradq(:,msn-1) = outp.lsqgrad(nsd+(1:nq));
      data.mat.Jlsqix{msn-1} = outp.lsqjac(1:nlsqi,1:nsd); 
      data.mat.Jlsqiq{msn-1} = outp.lsqjac(1:nlsqi,nsd+(1:nq));
      data.mat.Hlsqixx{msn-1} = outp.lsq2nd(1:nsd,1:nsd);
      data.mat.Hlsqixq{msn-1} = outp.lsq2nd(1:nsd,nsd+(1:nq));
      data.mat.Hlsqiqq{msn-1} = outp.lsq2nd(nsd+(1:nq),nsd+(1:nq));
  end

% Lagrange gradient
  data.res.laggradx(:,msn) = dvars.lambda(:,msn-1) - data.res.lambdaGx(:,msn) ...
      - dvars.mu_sd(:,msn);
  data.res.laggradq(:,msn) = - data.res.lambdaGq(:,msn) - dvars.mu_q(:,msn);

  if nrdi > 0
      data.res.laggradx(:,msn) = data.res.laggradx(:,msn) ...
          - data.res.muJrdix(:,msn-1);
      data.res.laggradq(:,msn) = data.res.laggradq(:,msn) ...
          - data.res.muJrdiq(:,msn-1);
  end
  
  if nlsqs > 0
      data.res.laggradx(:,msn) = data.res.laggradx(:,msn) ...
          + data.res.lsqigradx(:,msn-1);
      data.res.laggradq(:,msn) = data.res.laggradq(:,msn) ...
          + data.res.lsqigradq(:,msn-1);
  
      data.res.modgradx(:,msn) = data.res.modgradx(:,msn) ... 
          + data.res.lsqigradx(:,msn-1);
      data.res.modgradq(:,msn) = data.res.modgradq(:,msn) ... 
          + data.res.lsqigradq(:,msn-1);
  
      data.res.refgradx(:,msn) = data.res.refgradx(:,msn) ... 
          + data.res.lsqigradx(:,msn-1);
      data.res.refgradq(:,msn) = data.res.refgradq(:,msn) ... 
          + data.res.lsqigradq(:,msn-1);
  end

  if nlfcn > 0
    data.res.laggradx(:,msn) = data.res.laggradx(:,msn) ...
      + data.res.dlfcn(1:nsd,msn);
    data.res.laggradq(:,msn) = data.res.laggradq(:,msn) ...
      + data.res.dlfcn(nsd+(1:nq),msn);
    
    data.res.modgradx(:,msn) = data.res.modgradx(:,msn) ...
      + data.res.dlfcn(1:nsd,msn);
    data.res.modgradq(:,msn) = data.res.modgradq(:,msn) ...
      + data.res.dlfcn(nsd+(1:nq),msn);
    
    data.res.refgradx(:,msn) = data.res.refgradx(:,msn) ...
      + data.res.dlfcn(1:nsd,msn);
    data.res.refgradq(:,msn) = data.res.refgradq(:,msn) ...
      + data.res.dlfcn(nsd+(1:nq),msn);
  end
end

%%% End node (depends only on s, not on q)
msn = ns + 1;
data.res.modgradx(:,msn) = zeros(nsd,1);
data.res.refgradx(:,msn) = zeros(nsd,1);

% decoupled node constraint
if nrde > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn-1);
  if msn > ST.robust_horizon
      inp.p = pvars.pconst;
  else
      inp.p = ST.branches{jj,msn}(:); % uncertain p of node msn
  end
  inp.sensdirs = sensdirs;
  inp.mu = dvars.mu_rde;
  
  outp = eval_rdfcn_D(inp,'e');
  
  data.res.rde = outp.rdfcn;
  data.mat.Jrdex = outp.rdjac(1:nrde,1:nsd);
  data.res.muJrdex = outp.mu_rdjac(1:nsd);    
  data.mat.muJrdexx = outp.mu_rdhess(1:nsd,1:nsd);
end

  % least squares objective
if nlsqe > 0
    inp = [];
    inp.t = OPT.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.q = pvars.q(:,msn-1);
    if msn > ST.robust_horizon
        inp.p = pvars.pconst;
    else
        inp.p = ST.branches{jj,msn}(:); % uncertain p of node msn
    end
    inp.sensdirs = sensdirs;
    
    outp = eval_lsqfcn_D(inp,'e');
    
    data.res.lsqfcn_e = outp.lsqfcn;
    data.res.lsqegradx = outp.lsqgrad(1:nsd);
    data.mat.Jlsqex = outp.lsqjac(1:nlsqe,1:nsd); 
    data.mat.Hlsqexx = outp.lsq2nd(1:nsd,1:nsd);
end

% Mayer objective
if nmfcn > 0 && nlfcn <= 0
    inp = [];
    inp.t = OPT.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.q = pvars.q(:,msn-1);
    if msn > ST.robust_horizon
        inp.p = pvars.pconst;
    else
        inp.p = ST.branches{jj,msn}(:); % uncertain p of node msn
    end
    inp.sensdirs = sensdirs;
    
    outp = eval_mfcn_D(inp);
    
    data.res.mfun = outp.mfcn;
    data.res.mgrad = outp.grad(1:nsd);
    data.mat.mhess = outp.H(1:nsd,1:nsd);
end 
    
% Lagrange gradient
data.res.laggradx(:,msn) = dvars.lambda(:,msn-1) ...
    - dvars.mu_sd(:,msn);

if nrde > 0
    data.res.laggradx(:,msn) = data.res.laggradx(:,msn) ...
        - data.res.muJrdex;
end

if nlsqe > 0
    data.res.laggradx(:,msn) = data.res.laggradx(:,msn) ...
        + data.res.lsqegradx;
    
    data.res.modgradx(:,msn) = data.res.modgradx(:,msn) ... 
        + data.res.lsqegradx;
    
    data.res.refgradx(:,msn) = data.res.refgradx(:,msn) ... 
        + data.res.lsqegradx;
end

if nmfcn > 0 && nlfcn <= 0
    data.res.laggradx(:,msn) = data.res.laggradx(:,msn) ...
        + data.res.mgrad; 

    data.res.modgradx(:,msn) = data.res.modgradx(:,msn) ... 
        + data.res.mgrad;
    
    data.res.refgradx(:,msn) = data.res.refgradx(:,msn) ... 
        + data.res.mgrad;
end

OPT.evaltime{jj} = toc;    
