% -*-matlab-*-
function mli_solveuncondensedQP

global OPT SLV;

OPT.condtime = 0;

nsd = OPT.dims.nsd;
nq = OPT.dims.nq;
ns = OPT.dims.nshoot;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;
nrd = nrds + (ns-1) * nrdi + nrde;

CMR = isfield(OPT, 'controlmovereg') && OPT.controlmovereg == 1;

phase = OPT.currentphase;
switch phase

  case {0,4}
    pvars = OPT.phase{4}.var.primal;
    
  case 1
    pvars = OPT.phase{1}.ref.primal;
    
  case 2
    pvars = OPT.phase{2}.var.primal;
    
  case 3
    pvars = OPT.phase{3}.var.primal;
     
end

tic
if (phase==0) || (phase==4)
    % Hessematrix
    if isfield(OPT, 'lbfgshess') && OPT.lbfgshess
      for ii=1:ns
        stride = nsd+nq;
        idx = (ii-1)*stride;
        OPT.qp.H(idx+(1:stride),idx+(1:stride)) = lbfgshess_matrix(OPT.hess.lbfgshess{ii});
      end
      OPT.qp.H(ns*(nsd+nq)+(1:nsd),ns*(nsd+nq)+(1:nsd)) = lbfgshess_matrix(OPT.hess.lbfgshess{ns+1});
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
        xoff = ns*(nsd+nq)+nsd;
        for ii=1:ns
          idx = xoff + (ii-1)*nq;
          OPT.qp.H(idx+(1:nq),idx+(1:nq)) = lbfgshess_matrix(OPT.hess.lbfgsCMRhess{ii});
        end
      end
    else
      for ii=1:ns
        stride = nsd+nq;
        idx = (ii-1)*stride;
        OPT.qp.H(idx+(1:stride),idx+(1:stride)) = ...
          [OPT.hess.ssblock{ii} OPT.hess.sqblock{ii}
          OPT.hess.sqblock{ii}' OPT.hess.qqblock{ii}];
      end
      OPT.qp.H(ns*(nsd+nq)+(1:nsd),ns*(nsd+nq)+(1:nsd)) = OPT.hess.ssblock{ns+1};
      
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
        if isfield(OPT, 'controlmove_R')
          R = OPT.controlmove_R;
        else
          R = zeros(nq,nq);
        end
        xoff = ns*(nsd+nq)+nsd;
        for ii=1:ns-1
          idx = xoff + (ii-1)*nq;
          OPT.qp.H(idx+(1:nq),idx+(1:nq)) = 4 * R;
        end
        idx = xoff + (ns-1)*nq;
        OPT.qp.H(idx+(1:nq),idx+(1:nq)) = 4 * OPT.controlmove_R0;
      end
    end

    % Constraint matrix (matching and ipc)
    for ii=1:ns
        xidx = (ii-1)*nsd;
        yidx = (ii-1)*(nsd+nq);
        OPT.qp.A(xidx+(1:nsd),yidx+(1:nsd+nq+nsd)) = ...
            [OPT.eval.mat.Gx{ii}, OPT.eval.mat.Gq{ii}, -eye(nsd)];
    end
    if nrd > 0
        xidx = ns*nsd;
        yidx = 0;
        if nrds > 0
            OPT.qp.A(xidx+(1:nrds),yidx+(1:nsd+nq)) = ...
                [OPT.eval.mat.Jrdsx, OPT.eval.mat.Jrdsq];
            xidx = xidx+nrds;
        end
        if nrdi > 0
            for ii = 1:ns-1
                yidx = yidx + (nsd+nq);
                OPT.qp.A(xidx+(1:nrdi),yidx+(1:nsd+nq)) = ...
                    [OPT.eval.mat.Jrdix{ii}, OPT.eval.mat.Jrdiq{ii}];
                xidx = xidx + nrdi;
            end
        end
        if nrde > 0
            yidx = ns*(nsd+nq);
            OPT.qp.A(xidx+(1:nrde),yidx+(1:nsd)) = OPT.eval.mat.Jrdex;
        end
    end
    
    if CMR
      xoffs = ns*nsd+nrds+nrdi+nrde;
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) 
        xoffs2 = xoffs + (ns-1)*nq+nq;
        yoff = ns*(nsd+nq)+nsd;
        for ii = 1:ns-1
          yidx = (ii-1)*stride+((nsd+1):(nsd+nq));
          OPT.qp.A(xoffs+(ii-1)*nq+(1:nq),yidx) = eye(nq);
          OPT.qp.A(xoffs+(ii-1)*nq+(1:nq),yidx+stride) = -eye(nq);
          OPT.qp.A(xoffs+(ii-1)*nq+(1:nq),yoff+(ii-1)*nq+(1:nq)) = eye(nq);
          OPT.qp.A(xoffs2+(ii-1)*nq+(1:nq),yidx) = -eye(nq);
          OPT.qp.A(xoffs2+(ii-1)*nq+(1:nq),yidx+stride) = eye(nq);
          OPT.qp.A(xoffs2+(ii-1)*nq+(1:nq),yoff+(ii-1)*nq+(1:nq)) = eye(nq);
        end
        OPT.qp.A(xoffs+(ns-1)*nq+(1:nq),(nsd+1):(nsd+nq)) = -eye(nq);
        OPT.qp.A(xoffs+(ns-1)*nq+(1:nq),yoff+(ns-1)*nq+(1:nq)) = eye(nq);
        OPT.qp.A(xoffs2+(ns-1)*nq+(1:nq),(nsd+1):(nsd+nq)) = eye(nq);
        OPT.qp.A(xoffs2+(ns-1)*nq+(1:nq),yoff+(ns-1)*nq+(1:nq)) = eye(nq);
      else
        for ii = 1:ns-1
          yidx = (ii-1)*stride+((nsd+1):(nsd+nq));
          OPT.qp.A(xoffs+(ii-1)*nq+(1:nq),yidx) = eye(nq);
          OPT.qp.A(xoffs+(ii-1)*nq+(1:nq),yidx+stride) = -eye(nq);
        end
        OPT.qp.A(xoffs+(ns-1)*nq+(1:nq),(nsd+1):(nsd+nq)) = -eye(nq);
      end
    end
end

if (phase ~= 1)
    % Gradient
    stride = nsd+nq;
    
    for ii=1:ns
        idx = (ii-1)*stride;
        OPT.qp.g(idx+(1:stride),1) = ...
	[OPT.eval.res.modgradx(:,ii)
	 OPT.eval.res.modgradq(:,ii)];
    end
    OPT.qp.g(ns*(nsd+nq)+(1:nsd),1) = OPT.eval.res.modgradx(:,ns+1);
    
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
      xoff = ns*(nsd+nq)+nsd;
      for ii=1:ns-1
        idx = xoff + (ii-1)*nq;
        OPT.qp.g(idx+(1:nq),1) = OPT.eval.res.modgraddelq(:,ii);
      end
      idx = xoff + (ns-1)*nq;
      OPT.qp.g(idx+(1:nq),1) = OPT.eval.res.modgraddelq(:,ns);
    end
    
    % RHS (matching and ipc)
    for ii=1:ns
        OPT.qp.ineqlb((ii-1)*nsd+(1:nsd),1) = -OPT.eval.res.c(:,ii);
        OPT.qp.inequb((ii-1)*nsd+(1:nsd),1) = -OPT.eval.res.c(:,ii);
    end
    if nrd > 0
        bigbnd = 1e12;
        xoffs = ns*nsd;
        if nrds > 0
            OPT.qp.ineqlb(xoffs+(1:nrds)) = -OPT.eval.res.rds;
            OPT.qp.inequb(xoffs+(1:nrds)) = bigbnd*ones(size(OPT.eval.res.rds));
            xoffs = xoffs + nrds;
        end
        if nrdi > 0
            for jj=1:ns-1
                OPT.qp.ineqlb(xoffs+(1:nrdi)) = -OPT.eval.res.rdi(:,jj);
                OPT.qp.inequb(xoffs+(1:nrdi)) = bigbnd*ones(size(OPT.eval.res.rdi(:,jj)));
                xoffs = xoffs + nrdi;
            end
        end
        if nrde > 0
            OPT.qp.ineqlb(xoffs+(1:nrde)) = -OPT.eval.res.rde;
            OPT.qp.inequb(xoffs+(1:nrde)) = bigbnd*ones(size(OPT.eval.res.rde));
        end
    end
    if CMR
      xoffs = ns*nsd+nrds+nrdi+nrde;
      alpha = OPT.controlmove_alpha;
      if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) 
      xoffs2 = xoffs + (ns-1)*nq+nq;
      bigbnd = 1e12;
      for ii = 1:ns-1
        OPT.qp.ineqlb(xoffs+(ii-1)*nq+(1:nq)) = pvars.q(:,ii+1) - pvars.q(:,ii);
        OPT.qp.inequb(xoffs+(ii-1)*nq+(1:nq)) = bigbnd*ones(nq,1);
        OPT.qp.ineqlb(xoffs2+(ii-1)*nq+(1:nq)) = pvars.q(:,ii) - pvars.q(:,ii+1);
        OPT.qp.inequb(xoffs2+(ii-1)*nq+(1:nq)) = bigbnd*ones(nq,1);
      end
      OPT.qp.ineqlb(xoffs+(ns-1)*nq+(1:nq)) = pvars.q(:,1) - OPT.controlmove_q0;
      OPT.qp.inequb(xoffs+(ns-1)*nq+(1:nq)) = bigbnd*ones(nq,1);
      OPT.qp.ineqlb(xoffs2+(ns-1)*nq+(1:nq)) = OPT.controlmove_q0 - pvars.q(:,1);
      OPT.qp.inequb(xoffs2+(ns-1)*nq+(1:nq)) = bigbnd*ones(nq,1);
      
      else
        for ii = 1:ns-1
          OPT.qp.ineqlb(xoffs+(ii-1)*nq+(1:nq)) = -alpha;
          OPT.qp.inequb(xoffs+(ii-1)*nq+(1:nq)) = alpha;
        end
        OPT.qp.ineqlb(xoffs+(ns-1)*nq+(1:nq)) = -OPT.controlmove_alpha0;
        OPT.qp.inequb(xoffs+(ns-1)*nq+(1:nq)) = OPT.controlmove_alpha0;
      end
    end
    
    % If OPT.dims.mfcn > 0 we need this matrix to cut off the artificial
    % state
    % SLV.opt.lagObjModSd
    
    
    % Bounds
    for ii=1:ns
        idx = (ii-1)*stride+nsd;
        OPT.qp.lb(idx+(1:stride),1) = ...
	[OPT.bounds.qLoB(:,ii) - pvars.q(:,ii)
         SLV.opt.lagObjModSd*OPT.bounds.sdLoB(:,ii+1) - pvars.sd(:,ii+1)];
        
        OPT.qp.ub(idx+(1:stride),1) = ...
	[OPT.bounds.qUpB(:,ii) - pvars.q(:,ii)
         SLV.opt.lagObjModSd*OPT.bounds.sdUpB(:,ii+1) - pvars.sd(:,ii+1)];
    end
    
    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
      xoff = ns*(nsd+nq)+nsd;
      for ii=1:ns-1
        idx = xoff + (ii-1)*nq;
        OPT.qp.lb(idx+(1:nq)) = -bigbnd*ones(nq,1);
        OPT.qp.ub(idx+(1:nq)) = OPT.controlmove_alpha;
      end
      idx = xoff + (ns-1)*nq;
      OPT.qp.lb(idx+(1:nq)) = -bigbnd*ones(nq,1);
      OPT.qp.ub(idx+(1:nq)) = OPT.controlmove_alpha0;
    end
end

OPT.deltas0 = SLV.opt.lagObjModSd*OPT.x0(:) - pvars.sd(:,1);
OPT.qp.lb(1:nsd) = OPT.deltas0;
OPT.qp.ub(1:nsd) = OPT.deltas0;

% options = qpOASES_options('MPC');
options = qpOASES_options('reliable');
% options = qpOASES_options(options,'maxIter', 1e6);

%%% QP solution
switch phase
  case 0
	[x, obj, status, nWSRout, y] = qpOASES_sequence('m', ...
                                                        OPT.qp.qphandle, ...
                                                        OPT.qp.H, OPT.qp.g, ...
						  	OPT.qp.A, OPT.qp.lb, ...
						  	OPT.qp.ub, OPT.qp.ineqlb, ...
						  	OPT.qp.inequb, ...
                                                        options);

    case 4
        
%         [V,E] = eig(OPT.qp.H);
%         D = real(diag(E));
%         %DD = max(D,1e-4*max(abs(D)));
%         DD = D - min(D,-1e-4);
%         OPT.qp.HH = V*diag(DD)/V;
%     if isfield(OPT,'dualvarviaL') && OPT.dualvarviaL
%       OPT.qp.H = convexify_Hessian;
%     end
        
	[x, obj, status, nWSRout, y] = qpOASES_sequence('m', ...
                                                        OPT.qp.qphandle, ...
                                                        OPT.qp.H, OPT.qp.g, ...
						  	OPT.qp.A, OPT.qp.lb, ...
						  	OPT.qp.ub, OPT.qp.ineqlb, ...
						  	OPT.qp.inequb, ...
                                                        options);

    case {1,2,3}
	[x, obj, status, nWSRout, y] = qpOASES_sequence('h', OPT.qp.qphandle, OPT.qp.g, ...
						  	OPT.qp.lb, OPT.qp.ub,...
						  	OPT.qp.ineqlb, ...
                                                        OPT.qp.inequb, options);
end


if (status ~= 0)
  status
  error('QP solution failed!');
end

% gradstep = -OPT.qp.H*x;
% OPT.lbfgshess = lbfgshess_update(OPT.lbfgshess, -x, gradstep);

if (phase ~= 1)

    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
      xoff = ns*(nsd+nq) + nsd;
      OPT.qp.solstep = x(1:xoff);
      OPT.qp.soldelq = x((xoff+1):end);
    else
      OPT.qp.solstep = x;
    end
    
    if any(isnan(y))
        disp('NAN in QP Multipliers!');
        keyboard;
    end

    if (isfield(OPT, 'controlmove_c') || isfield(OPT, 'controlmove_R')) && CMR
      OPT.qp.solbndmult = y(nsd+(1:ns*(nsd+nq)));
      OPT.qp.solbndmult2 = y(nsd+ns*(nsd+nq)+(1:ns*nq));
      xoffs = ns*(nsd+nq) + nsd + ns*nq;
      OPT.qp.soleqmult = [y(1:nsd);y(xoffs+(1:ns*nsd))];
      OPT.qp.solCMRmult = y(xoffs+ns*nsd+(1:2*ns*nq));

    else
      %  multipliers of (q_0,sd_1,...,q_N-1,sd_N) bounds
      OPT.qp.solbndmult = y(nsd+(1:ns*(nsd+nq)));
      
      % multipliers of sd_0 bound and matching
      xoffs = ns*(nsd+nq) + nsd;
      
      OPT.qp.soleqmult = [y(1:nsd);y(xoffs+(1:ns*nsd))];
    end
    
%     [mu,lambda] = convexify_recovery(x,y);
%     mu_vec = cell2mat(mu);
%     OPT.qp.solbndmult = mu_vec((nsd+1):end);
%     OPT.qp.soleqmult = cell2mat(lambda);
    
    % multipliers of ipc
    xoffs = xoffs + ns*nsd;
    OPT.qp.solineqmult = y(xoffs+1:end);
end



% Feedback update
OPT.feedbackdelta = x(nsd+(1:nq));
OPT.qptime = toc;

