function [outp] = eval_mfcn_C(inp)
    
    global SLV
    
    args.t = inp.t;    
    args.xd = inp.sd;
    args.qph = [inp.q;inp.p];    
    args.fwdTCOrder = 0;
    args.nFwdTC = 0;
    args.fwdTC = [];
    args.nAdjTC = 1;
    args.adjTC = 1;

    evl = SLV.opt.evl;
    
    args.rhs = solvind('evaluateFcn', evl, 'mfcn', args);
    result = solvind('evaluateDenseDer', evl, 'mfcn', args);

    outp.mfcn = result.rhs;
    outp.grad = result.propAdjTC(2:end);
    outp.grad = outp.grad(:);

    
    
