% -*-matlab-*-
function est_evalPhaseD

global EST;

ns = EST.dims.nshoot;
nsd = EST.dims.nsd; 
nq = EST.dims.nq; 
np = EST.dims.np;
nlsqs = EST.dims.lsqfcn_s;
nlsqi = EST.dims.lsqfcn_i; 
nlsqe = EST.dims.lsqfcn_e;

pvars = EST.var.primal;

if EST.fixed_p
    sensdirs = [zeros(1,nsd);
                eye(nsd),
                zeros(nq+np,nsd)];
else
     sensdirs = [zeros(1,nsd+np);
                eye(nsd), zeros(nsd,np)  
                zeros(nq,nsd+np)
                zeros(np,nsd), eye(np)];
end

% dynamic equations and sensitivities
for msn = 1:ns
    inp = [];
    inp.thoriz = [EST.discr.msgrid(msn) EST.discr.msgrid(msn+1)];
    inp.sd = pvars.sd(:,msn);
    inp.p = pvars.p;
    inp.q = pvars.qconst(:,msn);
    inp.sensdirs = sensdirs;
    outp = eval_dyn_D_gn_est(inp);
    EST.eval.res.xd(:,msn) = outp.x;
    EST.eval.res.c(:,msn) = outp.x - pvars.sd(:,msn+1);
    EST.eval.mat.Gx{msn} = outp.G(1:nsd,1:nsd);
    if ~EST.fixed_p
        EST.eval.mat.Gp{msn} = outp.G(1:nsd,nsd+(1:np));
    end
end

% measurement function residuals and Jacobians
msn = 1;
inp = [];
inp.t = EST.discr.msgrid(msn);
inp.sd = pvars.sd(:,msn);
inp.p = pvars.p;
inp.q = pvars.qconst(:,msn);
inp.sensdirs = sensdirs;
outp = eval_lsqfcn_D_gn_est(inp,'s');

EST.eval.res.meas(:,msn) = outp.lsqfcn;
EST.eval.mat.Jmeasx{msn} = outp.lsqjac(1:nlsqs,1:nsd);
if ~EST.fixed_p
    EST.eval.mat.Jmeasp{msn} = outp.lsqjac(1:nlsqs,nsd+(1:np));
end
Vmsn =  diag(EST.V(:,msn));
EST.eval.res.lsq(:,msn) = Vmsn * ...
    (EST.eval.res.meas(:,msn) - EST.measurements(:,msn));
EST.eval.res.modgradx(:,msn) = EST.eval.mat.Jmeasx{msn}'*Vmsn'* ...
    EST.eval.res.lsq(:,msn);
if ~EST.fixed_p
    EST.eval.res.modgradp(:) = 0.0;
    EST.eval.res.modgradp = EST.eval.res.modgradp + ...
        EST.eval.mat.Jmeasp{msn}'*Vmsn'*EST.eval.res.lsq(:,msn);
end


for msn = 2:ns
    inp = [];
    inp.t = EST.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.p = pvars.p;
    inp.q = pvars.qconst(:,msn);
    inp.sensdirs = sensdirs;
    outp = eval_lsqfcn_D_gn_est(inp,'i');
    
    EST.eval.res.meas(:,msn) = outp.lsqfcn;
    EST.eval.mat.Jmeasx{msn} = outp.lsqjac(1:nlsqi,1:nsd);
    if ~EST.fixed_p
        EST.eval.mat.Jmeasp{msn} = outp.lsqjac(1:nlsqi,nsd+(1: ...
                                                          np));
    end
    Vmsn =  diag(EST.V(:,msn));
    EST.eval.res.lsq(:,msn) = Vmsn * ...
        (EST.eval.res.meas(:,msn) - EST.measurements(:,msn));
    EST.eval.res.modgradx(:,msn) = EST.eval.mat.Jmeasx{msn}'*Vmsn'* ...
        EST.eval.res.lsq(:,msn);
    if ~EST.fixed_p
        EST.eval.res.modgradp = EST.eval.res.modgradp + ...
            EST.eval.mat.Jmeasp{msn}'*Vmsn'*EST.eval.res.lsq(:,msn);
    end
end

msn = ns+1;
inp = [];
inp.t = EST.discr.msgrid(msn);
inp.sd = pvars.sd(:,msn);
inp.p = pvars.p;
inp.q = pvars.qconst(:,msn-1);
inp.sensdirs = sensdirs;
outp = eval_lsqfcn_D_gn_est(inp,'e');
   
EST.eval.res.meas(:,msn) = outp.lsqfcn;
EST.eval.mat.Jmeasx{msn} = outp.lsqjac(1:nlsqe,1:nsd);
if ~EST.fixed_p
    EST.eval.mat.Jmeasp{msn} = outp.lsqjac(1:nlsqe,nsd+(1:np));
end

%disp('End of evalPhaseD...');
%keyboard;
