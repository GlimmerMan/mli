% -*-matlab-*-
function [data] = mli_evalPhaseD_gn(vars)

global OPT;

tic

ns = OPT.dims.nshoot;
nsd = OPT.dims.nsd; 
nq = OPT.dims.nq; 
np = OPT.dims.np;
nlsqs = OPT.dims.lsqfcn_s;
nlsqi = OPT.dims.lsqfcn_i; 
nlsqe = OPT.dims.lsqfcn_e;
nrds = OPT.dims.rdfcn_s;
nrdi = OPT.dims.rdfcn_i;
nrde = OPT.dims.rdfcn_e;

pvars = vars.primal;
dvars = vars.dual;

sensdirs = [zeros(1,nsd+nq);eye(nsd+nq);zeros(np,nsd+nq)];

%%% Start Node
msn = 1;

% dynamical equations
inp = [];
inp.thoriz = [OPT.discr.msgrid(msn) OPT.discr.msgrid(msn+1)];
inp.sd = pvars.sd(:,msn);
inp.q = pvars.q(:,msn);
inp.p = pvars.pconst;
inp.sensdirs = sensdirs;
outp = eval_dyn_D_gn(inp);

lambda = dvars.lambda(:,msn);

data.res.xd(:,msn) = outp.x;
data.res.c(:,msn) = outp.x - pvars.sd(:,msn+1);
data.mat.Gx{msn} = outp.G(1:nsd,1:nsd);
data.mat.Gq{msn} = outp.G(1:nsd,nsd+(1:nq));
data.res.lambdaGx(:,msn) = data.mat.Gx{msn}'*lambda;
data.res.lambdaGq(:,msn) = data.mat.Gq{msn}'*lambda;

% least-squares objective
if nlsqs > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn);
  inp.p = pvars.pconst;
  inp.sensdirs = sensdirs;
  outp = eval_lsqfcn_D_gn(inp,'s');

  data.res.lsqfcn_s = outp.lsqfcn;
  data.res.lsqsgradx = outp.lsqgrad(1:nsd);    
  data.res.lsqsgradq = outp.lsqgrad(nsd+(1:nq));
  data.res.modgradx(:,msn) = outp.lsqgrad(1:nsd);    
  data.res.modgradq(:,msn) = outp.lsqgrad(nsd+(1:nq));
  data.res.refgradx(:,msn) = outp.lsqgrad(1:nsd);    
  data.res.refgradq(:,msn) = outp.lsqgrad(nsd+(1:nq));
  data.mat.Jlsqsx = outp.lsqjac(1:nlsqs,1:nsd);
  data.mat.Jlsqsq = outp.lsqjac(1:nlsqs,nsd+(1:nq));
end

% decoupled node constraint
if nrds > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn);
  inp.p = pvars.pconst;
  inp.sensdirs = sensdirs;
  outp = eval_rdfcn_D_gn(inp,'s');
  
  mu = dvars.mu_rds;

  data.res.rds = outp.rdfcn;
  data.mat.Jrdsx = outp.rdjac(1:nrds,1:nsd);
  data.mat.Jrdsq = outp.rdjac(1:nrds,nsd+(1:nq));
  data.res.muJrdsx = data.mat.Jrdsx' * mu;    
  data.res.muJrdsq = data.mat.Jrdsq' * mu;
end

% Lagrange gradient
data.res.laggradx(:,msn) = - data.res.lambdaGx(:,msn) - dvars.mu_sd(:,msn);
data.res.laggradq(:,msn) = - data.res.lambdaGq(:,msn) - dvars.mu_q(:,msn);

if nlsqs > 0
  data.res.laggradx(:,msn) = data.res.laggradx(:,msn) + data.res.lsqsgradx;
  data.res.laggradq(:,msn) = data.res.laggradq(:,msn) + data.res.lsqsgradq;
end

if nrds > 0
  data.res.laggradx(:,msn) = data.res.laggradx(:,msn) - data.res.muJrdsx;
  data.res.laggradq(:,msn) = data.res.laggradq(:,msn) - data.res.muJrdsq;
end

%%% Interior Nodes
for msn=2:ns

% dynamical equations
  inp = [];
  inp.thoriz = [OPT.discr.msgrid(msn) OPT.discr.msgrid(msn+1)];
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn);
  inp.p = pvars.pconst;
  inp.sensdirs = sensdirs;
  outp = eval_dyn_D_gn(inp);

  lambda = dvars.lambda(:,msn);
  
  data.res.xd(:,msn) = outp.x;
  data.res.c(:,msn) = outp.x - pvars.sd(:,msn+1);
  data.mat.Gx{msn} = outp.G(1:nsd,1:nsd);
  data.mat.Gq{msn} = outp.G(1:nsd,nsd+(1:nq));
  data.res.lambdaGx(:,msn) = data.mat.Gx{msn}'*lambda;
  data.res.lambdaGq(:,msn) = data.mat.Gq{msn}'*lambda;

% least-squares objective
  if nlsqi > 0
    inp = [];
    inp.t = OPT.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.q = pvars.q(:,msn);
    inp.p = pvars.pconst;
    inp.sensdirs = sensdirs;
    outp = eval_lsqfcn_D_gn(inp,'i');
    
    data.res.lsqfcn_i(:,msn-1) = outp.lsqfcn;
    data.res.lsqigradx(:,msn-1) = outp.lsqgrad(1:nsd);    
    data.res.lsqigradq(:,msn-1) = outp.lsqgrad(nsd+(1:nq));
    data.res.modgradx(:,msn) = outp.lsqgrad(1:nsd);    
    data.res.modgradq(:,msn) = outp.lsqgrad(nsd+(1:nq));
    data.res.refgradx(:,msn) = outp.lsqgrad(1:nsd);    
    data.res.refgradq(:,msn) = outp.lsqgrad(nsd+(1:nq));
    data.mat.Jlsqix{msn-1} = outp.lsqjac(1:nlsqi,1:nsd);
    data.mat.Jlsqiq{msn-1} = outp.lsqjac(1:nlsqi,nsd+(1:nq));
  end

% decoupled node constraint
  if nrdi > 0
    inp = [];
    inp.t = OPT.discr.msgrid(msn);
    inp.sd = pvars.sd(:,msn);
    inp.q = pvars.q(:,msn);
    inp.p = pvars.pconst;
    inp.sensdirs = sensdirs;
    outp = eval_rdfcn_D_gn(inp,'i');
    
    mu = dvars.mu_rdi(:,msn-1);
    
    data.res.rdi(:,msn-1) = outp.rdfcn;
    data.mat.Jrdix{msn-1} = outp.rdjac(1:nrdi,1:nsd);
    data.mat.Jrdiq{msn-1} = outp.rdjac(1:nrdi,nsd+(1:nq));
    data.res.muJrdix(:,msn-1) = data.mat.Jrdix{msn-1}' * mu;    
    data.res.muJrdiq(:,msn-1) = data.mat.Jrdiq{msn-1}' * mu;
  end

% Lagrange gradient
  data.res.laggradx(:,msn) = dvars.lambda(:,msn-1) - data.res.lambdaGx(:,msn) ...
      - dvars.mu_sd(:,msn);
  data.res.laggradq(:,msn) = - data.res.lambdaGq(:,msn) - dvars.mu_q(:,msn);

  if nlsqi > 0
    data.res.laggradx(:,msn) = data.res.laggradx(:,msn) + data.res.lsqigradx(:,msn-1);
    data.res.laggradq(:,msn) = data.res.laggradq(:,msn) + data.res.lsqigradq(:,msn-1);
  end
  
  if nrdi > 0
    data.res.laggradx(:,msn) = data.res.laggradx(:,msn) - data.res.muJrdix(:,msn-1);
    data.res.laggradq(:,msn) = data.res.laggradq(:,msn) - data.res.muJrdiq(:,msn-1);

  end
end

%%% End node (depends only on s, not on q)
msn = ns + 1;

% least-squares objective
if nlsqe > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn-1);
  inp.p = pvars.pconst;
  inp.sensdirs = sensdirs;
  outp = eval_lsqfcn_D_gn(inp,'e');

  data.res.lsqfcn_e = outp.lsqfcn;
  data.res.lsqegradx = outp.lsqgrad(1:nsd);    
  data.res.modgradx(:,msn) = outp.lsqgrad(1:nsd);    
  data.res.refgradx(:,msn) = outp.lsqgrad(1:nsd);    
  data.mat.Jlsqex = outp.lsqjac(1:nlsqe,1:nsd);
end

% decoupled node constraint
if nrde > 0
  inp = [];
  inp.t = OPT.discr.msgrid(msn);
  inp.sd = pvars.sd(:,msn);
  inp.q = pvars.q(:,msn-1);
  inp.p = pvars.pconst;
  inp.sensdirs = sensdirs;
  outp = eval_rdfcn_D_gn(inp,'e');
  
  mu = dvars.mu_rde;

  data.res.rde = outp.rdfcn;
  data.mat.Jrdex = outp.rdjac(1:nrde,1:nsd);
  data.res.muJrdex = data.mat.Jrdex' * mu;    
end

% Lagrange gradient
data.res.laggradx(:,msn) = dvars.lambda(:,msn-1) - dvars.mu_sd(:,msn);

if nlsqe > 0
  data.res.laggradx(:,msn) = data.res.laggradx(:,msn) + data.res.lsqegradx;
end

if nrde > 0
  data.res.laggradx(:,msn) = data.res.laggradx(:,msn) - data.res.muJrdex;
end

OPT.evaltime = toc;

    
