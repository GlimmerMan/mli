\documentclass{scrartcl}


% Define encoding
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
% Use a sans serif font
% Looks better imo, but produces warnings\dots.
% \renewcommand\familydefault{\sfdefault}


% Setup psstricks
\usepackage{pstricks}

% Setup mathtools
\usepackage{amsmath}
\usepackage{mathtools}

% Package to include source code
\usepackage{listings}
\usepackage{xcolor}
\lstset{language=C++,backgroundcolor=\color{black!5},basicstyle=\footnotesize}



\usepackage[framed]{matlab-prettifier}
\usepackage{dirtree}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% self defined commands
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\Dt}[1]{\frac{\mathrm{d}#1}{\mathrm{d}t}}
\newcommand{\mtrx}[1]{\begin{bmatrix}#1\end{bmatrix}}
\newcommand{\galert}[1]{\color{green}#1\color{black}}
\newcommand{\balert}[1]{\color{blue}#1\ \color{black}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\section{A MLI guiding example: Control of a simple pendulum}

This is an introductory tutorial for the usage the software package MLI. In this tutorial an example of a simple pendulum is reviewed. For the sake of simplicity, the most shortened configuration for a running example is used. The model equations as well as the pictures are taken from the lecture script ``Optimierung bei gewöhnlichen Differentialgleichungen'' by Christian Kirches. This example is deployed with the standard installation of MLI and can be executed by the Matlab command
\begin{verbatim}
	>>runMLI('pendulum')
\end{verbatim}
in the base installation directory of MLI. In the following, we will address the directory as \texttt{<mli\_dir>}.

The purpose of this tutorial is, to give a first overview of the MLI workflow. The structure of the main components of a MLI project is presented and the necessary files described. This tutorial covers only a short part of the MLI functionality.

\textbf{Disclaimer:} This is under construction. 


\subsection{Mathematical formulation}

\begin{figure}[h]
	\centering
	\input{pendulum}
	\caption{Physical interpretation of pendulum model}
\end{figure}
To start this tutorial, the mathematical model is reviewed. We consider a pendulum that can be controlled via a motor at the pivot point. The objective of the NMPC-controller is to steer the pendulum in a stable state. In this example angular coordinates are used. The current position of the pendulum is described by the angle $\phi$. The length of the rope is defined by the parameter $m$ and the mass of the pendulum by $m$. The control $u$ is the torque, applied by the motor at the pivot point. The dynamic behavior of this pendulum is described by the equation:

\begin{alignat*}{2}
	m l^2 \ddot{\phi} 			&= u(t)- m g \cdot \sin \phi \cdot l \\
	\Longrightarrow\quad \ddot{\phi} 	&= \frac{u(t)}{m l^2} - \frac{g}{l} \sin \phi.
	\intertext{To transform this equation to a ODE system first order, the differential states}
	x					&=\mtrx{x_1 \\ x_2} = \mtrx{\phi \\ \dot{\phi}}
	\intertext{are introduced. This leads to the following system:}
	\frac{\textup{d}}{\textup{d}t}\left[ {\begin{matrix} x_1 \\ x_2 \end{matrix}} \right] &= \left[ \begin{matrix} x_2 \\ \frac{u(t)}{m l^2} - \frac{g}{l} \sin x_1 \end{matrix} \right].
\end{alignat*}
In order to steer the pendulum to a certain position $\phi_{ref}$, a Lagrange objective function is used:
\begin{align*}
	\int_{t_0}^{t_f} \Vert \phi(t) -\phi_{ref} \Vert^2 \textup{d}t.
\end{align*}
The initial state of the system is described by the initial value $x_0$. The complete offline optimal control problem has the following form:
\begin{alignat*}{3}
			&\underset{\phi( \cdot), u(\cdot)}{\text{min}}	\quad\mathrlap{ \int_{t_0}^{t_f} \Vert x_1 -\phi_{ref} \Vert^2 \textup{d}t  }    \\
			&\text{s. t.}	\quad			&\dot{x_1}(t)			&= x_1(t) 	 						&\qquad	&t \in \left[t_0, t_f \right] \\
			&					&\dot{x_2}(t)			&= \frac{u(t)}{m l^2} - \frac{g}{l} \sin x_1(t)	 	&\qquad	&t \in \left[t_0, t_f \right] \\
			& 					&x(t_0)				&=x_0
\end{alignat*}

\subsection{General structure of a MLI project}

MLI projects are organized in two parts: the ordinary differential equation model is implemented as SolvIND model files, written in \texttt{C++}. The source files are saved in the directory
\begin{verbatim}
	<mli_dir>/examples/models/Src
\end{verbatim}
The main algorithm is configured in Matlab scripts, saved in the directory
\begin{verbatim}
	<mli_dir>/examples/problems/pendulum
\end{verbatim}
In figure \ref{dir:structure}, one can see the location of all user created files for a running example. In this tutorial, these files will be reviewed.

\begin{figure}[h]
\dirtree{%
.1 <mli\_dir>.
.2 examples.
.3 models.
.4 Src.
.5 pendulum\_sim.cpp.
.5 pendulum\_opt.cpp.
.5 CMakeLists.txt.
.4 Release.
.3 problems.
.4 pendulum.
.5 scenario\_initialize.m.
.5 controller\_initialize.m.
.5 user\_scen\_update.m.
.5 user\_opt\_p\_from\_scen.m.
.5 user\_opt\_x\_from\_scen.m.
.5 user\_plot\_init.m.
.5 user\_plot.m.
}
\caption{Folder structure of the pendulum example}
\label{dir:structure}
\end{figure}


\subsection{Setup of the ODE model}

MLI use SolvIND for integration. Because of that, the differential equations are implemented in SolvIND model files, written in \texttt{C++}. It is possible to use different model for optimization, estimation and simulation. Each model is implemented in a different file. Because the estimator is not used in this example, there are only model files for simulation and optimization. These model files need to be compiled after every change.


\subsubsection*{\texttt{CMakeLists.txt}}

To compile the SolvIND model files the \texttt{cmake} build system is used. In order to register the model files, the following lines were added to the \texttt{CMakeLists.txt} file:
\begin{verbatim}
	ADD_LIBRARY(model_pendulum_sim SHARED pendulum_sim.cpp)
	ADD_LIBRARY(model_pendulum_opt SHARED pendulum_opt.cpp)
	TARGET_LINK_LIBRARIES(model_pendulum_sim ${SOLVIND_LIBRARIES})
	TARGET_LINK_LIBRARIES(model_pendulum_opt ${SOLVIND_LIBRARIES})
\end{verbatim}

\subsubsection*{\texttt{pendulum\_sim.cpp}}

In the file \texttt{pendulum\_sim.cpp} is the simulation model for the scenario implemented. The model equations are defined in the function \texttt{pendulum\_rhs}. The in- and output arguments of the function are saved in the \texttt{args} data structure. To allow the automated derivative generation with ADOL-C, the function is templatized.

\begin{lstlisting}
template <typename T>
svLong pendulum_rhs(TArgs_ffcn<T> &args, TDependency *depends)
{
  T x1, x2, u;

  const double  g =   9.81;
  const double  l =   0.1;
  const double  m =   0.5;

  x1 = args.xd[0];
  x2 = args.xd[1];
  u = args.u[0];
  
  args.rhs[0] = x2;
  args.rhs[1] = u/(m*l*l)-(g/l)*sin(x1);
  
  return 0;
}
\end{lstlisting}
The dimensions are specified in the constructor of the \texttt{myDynModel} class. 
\begin{lstlisting}
myDynModel::myDynModel(  const std::string& options ):
IDynamicModelDescription()
{
  m_dims. dim [ Component_XD ] = 2;
  m_dims. dim [ Component_P  ] = 0;
  m_dims. dim [ Component_U  ] = 1;
  m_dims. nTrajectories        = 1;
  
  m_functions. setFunction<Function_ffcn>(&pendulum_rhs<double>);
  m_functions. setFunction<Function_ffcn>(&pendulum_rhs<adouble>);
  
}
\end{lstlisting}

\subsubsection*{\texttt{pendulum\_sim.cpp}}

The model for the controller is implemented in the file \texttt{pendulum\_sim.cpp}. Currently there is no Lagrange objective supported. To realize an objective function of Lagrange type, the objective is transformed to a Mayer functional. The function \texttt{pendulum\_mfcn} defines this function.

\begin{lstlisting}
template <typename T>
svLong pendulum_rhs(TArgs_ffcn<T> &args, TDependency *depends)
{
  T x1, x2, u;
  
  const double  g =   9.81;
  const double  l =   0.1;
  const double  m =   0.5;

  x1 = args.xd[0];
  x2 = args.xd[1];
  u = args.u[0];
  
  args.rhs[0] = x2;
  args.rhs[1] = u/(m*l*l)-(g/l)*sin(x1);
  args.rhs[2] = x1*x1;
  
  return 0;
}

template<typename T>
static svLong pendulum_mfcn( 
	SolvInd::TArgs_mfcn<T>& args,
	TDependency *depends ){
     
  args.mval[0] = args.xd[2];

  return 0;
}
\end{lstlisting}

Furthermore in the constructor of the class \texttt{myDynModel} the dimensions of the modified controller model are specified  and the Mayer objective function is registered.

\begin{lstlisting}
myDynModel::myDynModel(  const std::string& options )
:
IDynamicModelDescription()
{
  m_dims. dim [ Component_XD ] = 3;
  m_dims. dim [ Component_P  ] = 0;
  m_dims. dim [ Component_U  ] = 1;
  m_dims. nTrajectories        = 1;
  
  m_functions. setFunction<Function_ffcn>(&pendulum_rhs<double>);
  m_functions. setFunction<Function_ffcn>(&pendulum_rhs<adouble>);
  
  m_functions. setFunction< Function_mfcn >( &pendulum_mfcn<double> );
  m_functions. setFunction< Function_mfcn >( &pendulum_mfcn<adouble>  );
  
  m_fcnOutputDims. dim [ Function_mfcn    ] = 1;
}
\end{lstlisting}

\subsubsection*{Compilation}


After changes to the source files, the model needs to be recompiled. This can be done with the following commands, using the make build system:

\begin{verbatim}
	$cd <mli_dir>/examples/models/Release
	$make
\end{verbatim}

\subsection{Configuration of main algorithm}

\lstset{
  style              = Matlab-editor,
  escapechar         = ",
  mlshowsectionrules = true,
  basicstyle=\tiny,
}
To configure the main algorithm a set of Matlab scripts are used. They are saved in the directory

\begin{verbatim}
	<mli_dir>/examples/problems/pendulum
\end{verbatim}

\subsubsection*{\texttt{scenario\_initialize.m}} 

In the file \texttt{scenario\_initialize.m} the configuration for the scenario is done. This script is executed once at the beginning of the algorithm. The main options are the definition the sampling grid and the initial values for differential states, the control and the parameters. In this case, no parameters are needed and the default control is set to zero.

\lstinputlisting{../../examples/problems/pendulum/scenario_initialize.m}


\subsubsection*{\texttt{controller\_initialize.m}}

The script \texttt{controller\_initialize.m} defines the configuration for the controller. This script is executed once at the beginning of the algorithm. In this script the prediction horizon and the number of multiple shooting intervals are specified. Furthermore the sequence of phases for the multi level iteration is defined. For this introductory example only phase $4$ is used.
\lstinputlisting{../../examples/problems/pendulum/controller_initialize.m}

\subsubsection*{\texttt{user\_scen\_update.m}}

The script \texttt{user\_scen\_update.m} is responsible for updating the scenario after every sampling iteration. In this case, a simple integration of the sample interval with the applied control from the controller is performed. In a more complex project, one can use this file, to implement measurement errors and the communication with the estimator.
\lstinputlisting{../../examples/problems/pendulum/user_scen_update.m}

\subsubsection*{\texttt{user\_opt\_p\_from\_scen.m}}

The script \texttt{user\_opt\_p\_from\_scen.m} defines, how the parameters from the scenario are deployed to the controller. It is executed after every sampling phase. In this case, the script is empty, because the model has no parameters.

\lstinputlisting{../../examples/problems/pendulum/user_opt_p_from_scen.m}

\subsubsection*{\texttt{user\_opt\_x\_from\_scen.m}}

The script \texttt{user\_opt\_x\_from\_scen.m} defines, how the parameters from the scenario are deployed to the controller. It is executed after every sampling phase. In this case, the initial value for the next controller is set to the current state of the simulation. 

\lstinputlisting{../../examples/problems/pendulum/user_opt_x_from_scen.m}

\subsubsection*{\texttt{user\_plot\_init.m}}

The script \texttt{user\_plot\_init.m} initializes the the visualization. In this case one figure is used to plot the differential states in red and the control in green. Furthermore the predicted scenario is shown in blue. In addition to that, the current physical position of the pendulum is plotted in figure 2. 
\lstinputlisting{../../examples/problems/pendulum/user_plot_init.m}


\subsubsection*{\texttt{user\_plot.m}}
The script \texttt{user\_plot.m} is used to update the data for visualization after every iteration.
\lstinputlisting{../../examples/problems/pendulum/user_plot.m}

\end{document}
